![NOÉ](.gitlab/images/readme-banner.jpg)

> [❤️ Je souhaite **soutenir le projet** financièrement !](https://opencollective.com/noe-app)
>
> [🤓 Je souhaite **tester la plateforme** et **apprendre à l'utiliser**.](#par-où-commencer-)
>
> [🖥️ Je souhaite **contribuer au projet** et lire la **documentation technique**.](CONTRIBUTING.md)
>
> [📃 Je souhaite consulter le **Changelog**.](https://bit.ly/changelog-noeappli-gitlab)
>
> [🚀 Je souhaite **utiliser NOÉ pour mon événement**.](mailto:hello@noe-app.io?subject=J'aimerais&#32;utiliser&#32;NOÉ&#32;pour&#32;mon&#32;événement)

# NOÉ - Planification d'événements

## Présentation

NOÉ est le diminutif de "Nouvelle Organisation d'Événements". C'est un outil **open-source**, destiné aux organisateur⋅ices d'événements qui souhaitent organiser des événements plus horizontaux, auto-organisés et participatifs, grâce à un ensemble d'outils qui les aident à toutes les étapes de l'organisation :

- **Déclarez votre programme artistique, musical ou éducatif** directement dans NOÉ. L'application devient votre principale source d'information, où vous pouvez stocker le "quoi, où, quand et avec qui" de votre événement.

- **Invitez les participants à votre événement**. NOÉ permet à vos participant⋅es de s'inscrire aux activités, ateliers et sessions de volontariat qu'iels souhaitent, de manière totalement libre, même pour les très grands événements. Donnez les informations essentielles sur votre page d'accueil, entièrement modifiable avec un éditeur intégré dans NOÉ.

- **Intégrez le volontariat, de manière native**. Pour que votre événement se déroule bien, il se peut que vous ayez besoin que chaque participant⋅e passe un certain temps à aider en tant que bénévole. Définissez votre fourchette de temps de bénévolat acceptable par jour, afin que les participant⋅es soient encouragé⋅es à remplir leur jauge de bénévolat. Encouragez les personnes à se porter volontaires pour les bénévolats plus fatiguants, en fixant une majoration du temps de volontariat sur les postes qui ont besoin d'un coup de pouce.

- **Surveillez les inscriptions de vos participants et obtenez des informations clés pour votre événement**. Construisez votre formulaire d'inscription personnalisé directement dans NOÉ, demandez ce que vous avez besoin de savoir à vos participant⋅es. Compilez les résultats et prenez de meilleures décisions pour économiser du temps, de l'énergie et de l'argent. Exemples : "combien de personnes mangent sur place pour le déjeuner de demain, quelle quantité de nourriture dois-je préparer ?" / "Suis-je sûr d'avoir au moins 2 personnes formées aux premiers secours sur le site, à tout moment de l'événement ?" / "Combien de personnes doivent encore se présenter au point d'accueil ce soir ?" / "Combien de personnes participent à la formation XYZ à 18h ? Faut-il ajouter des bancs dans la salle ?"

- **Gardez le contact avec vos participant⋅es**. Vous êtes en mesure d'extraire les numéros de téléphone, les emails ou toute autre information utile très rapidement, ce qui vous permet de dialoguer facilement avec vos participants, d'envoyer des emails ciblés ou des SMS de groupe aux personnes qui en ont besoin. Exemple : envoyer des SMS à toutes les personnes qui se sont inscrites à un atelier particulier et dont l'horaire a changé.

Le projet NOÉ a été lancé par Alternatiba en 2019. Alternatiba est une association militant pour la justice sociale et climatique. À l'origine, NOÉ a été créé pour aider à organiser les Camps Climat (qui sont des rassemblements de militants dans toute la France pour s'informer sur les questions de climat et de justice sociale, se rencontrer, partager et fédérer le mouvement climat). 

Puis nous avons vu que NOÉ attirait également d'autres acteurs et associations, qui avaient également besoin d'un outil nativement horizontal pour l'organisation de leurs événements... C'est ainsi que NOÉ a commencé à être utilisé dans des festivals, et autres séminaires.

> _Englih version_
>
> NOÉ is the short name for "Nouvelle Organization d'Événements" (which could translate to "New Ways to Organize Events"). It is an **open-source** tool for event organizators that wish to organize more horizontal, auto-organized and participative events, through a set of tools that helps during all the steps of the organization:
>
> - **Declare your artistic, musical or educational schedule** directly in NOÉ. It becomes your main source of information, where you can store the "what, where, when and with who" information of your event.
>
> - **Invite participants to your event**. NOÉ provides a way for your participants to register to activities, workshops and volunteering sessions that they want, totally freely, even in very big events. Give essential information in your welcome page, fully editable with a built-in editor, inside NOÉ.
>
> - **Integrate volunteering, natively**. For your event to run perfectly, you may need each participant to spend some time helping as a volunteer. Set your desired range of acceptable volunteering time per day, so participants get encouraged to fill their volunteering gauge. Encourage people to volunteer in more exhausting volunteerings by setting a volunteering majoration on the shifts that need a boost. 
>
> - **Monitor your participants' registrations and get key insights from them**. Build your custom registration form directly inside NOÉ, ask what you need to know to your participants. Compile results and make better decisions overall to spare time, effort and money. Examples: "how many people are eating on site for tomorrow's lunch, how much food should I prepare ?" / "Am I sure that I have at least 2 people with first aid training on site, anytime in the event ?" / "How many people are still to come at the welcome point this evening ?" / “How many people are attending the formation XYZ at 6pm ? Should we add more benches ?”
>
> - **Keep in touch with your participants**. You’re able to extract phone numbers, emails or any other useful information really quickly, so you can talk easily with your participants, send targeted group emails or group text messages to people who need it. Ex: send text messages to all people who registered a particular workshop whose schedule has changed.
>
> The NOÉ project was launched by Alternatiba in 2019. Alternatiba is a French association campaigning for social and climate justice. Originally, NOÉ was created to help organize Climate Camps (which are gatherings of activists all over France to learn about climate and social justice issues, meet, share, and celebrate all together). 
>
> Then we saw that NOÉ was also attracting other actors and associations, who also needed a natively horizontal tool for the organization of their events. That’s how NOÉ began to be used in festivals, and other seminaries.

## Par où commencer ?

### Comprendre NOÉ en vidéo

[Je découvre NOÉ en 20 minutes !](https://peertube.virtual-assembly.org/videos/watch/37c55e47-428c-497f-b454-28a16de73041?start=4m28s)

### Tester NOÉ

Vous pouvez aller jouer sur le **serveur de démonstration**, notamment en allant jouer sur l'événement "Camp Climat IDF \[démo\]" (qui est une _copie_ du vrai Camp Climat IDF qui s'est déroulé à l'été 2021 sur NOÉ) :

**Urls :**
- Espace participant⋅e : https://demo.noe-app.io 
- Espace orga : https://orga.demo.noe-app.io

**Identifiants :**
- Participant⋅e simple : compte-participant.e@test.fr _(accès espace participant⋅e seulement)_
- Participant⋅e et encadrant.e : compte-participant.e-et-encadrant.e@test.fr _(accès espace participant⋅e seulement)_
- Organisateur⋅ice : compte-orga@test.fr _(accès espace participant⋅e + orga)_

Avec le même **mot de passe** pour tout le monde : test4NOE

### Installer un serveur NOÉ

Toutes les infos sont disponibles dans le [guide de contribution](CONTRIBUTING.md).

## Envie de sauter le pas avec votre événement ?

Envoyez un email à [hello@noe-app.io](mailto:hello@noe-app.io?subject=J'aimerais&#32;utiliser&#32;NOÉ&#32;pour&#32;mon&#32;événement), on se fera un plaisir de voir avec vous ce que l'on peut faire.

## Infos complémentaires

### Quelles fonctionnalités principales ? (liste non-exhaustive !)

> NOÉ est conçu dans une logique modulaire et s'adapte à de nombreuses typologies d'événements : festivals, résidences, universités d'été, etc. Pourquoi pas le vôtre ?

Les organisateur⋅ices peuvent :
- Saisir tous les éléments du programme de l'événement (lieux, activités, intervenant⋅es) ainsi que toutes les contraintes associées (disponibilité des personnes, des salles, etc.),
- Réagencer facilement les sessions grâce à une vue agenda qui permet de déplacer, redimensionner, ou cloner les sessions facilement
- Consulter et modifier en deux clics les données d'inscription des participant⋅es
- Créer des équipes de participant⋅es
- Inscrire, ou désinscrire des participant⋅es à des sessions, ou des équipes.
- Et plein d'autres choses...

Les participant⋅es peuvent :
- S'inscrire à l'événement,
- Choisir les activités auxquelles iels souhaitent prendre part, en recherchant, filtrant, les sessions disponibles,
- Consulter leur emploi du temps facilement, sur PC ou sur mobile.
- Exporter leur planning en PDF
- Et d'autres choses encore...


### Historique

Ce projet a été initié par le mouvement Alternatiba qui co-organise depuis 2016 [des Camps Climat](https://campclimat.eu). Au fil des années, l'événement a pris des proportions de plus en plus vastes, et le système d'inscription et d'élaboration du programme est devenu très complexe.

En 2019, [le Camp Climat de Kingersheim](https://alternatiba.eu/2019/08/camp-climat/) proposait un programme de formations étalé sur une dizaine de jours, pour plus de mille participant⋅es, 155 intervenant⋅es, dans une vingtaine de salles et d'espaces extérieurs : un véritable casse-tête à organiser ! Anticipant, une équipe s'est lancée dans le développement d'une application sur mesure.
Cette application a été opérationnelle et a permis de générer automatiquement le planning des formations, avec les contraintes concernant les disponibilités des intervenant⋅es, des salles, etc.

Toutefois, cette version était "artisanale" et il était nécessaire de reprendre le même principe sur des bases solides, tout en intégrant les retours d'expérience de la première utilisation en conditions réelles !
Cette nouvelle version de ce qui était "l'application du Camp Climat", c'est NOÉ. Fidèles à nos valeurs de partage et de solidarité, nous avons choisi de libérer le logiciel, afin qu'il puisse servir à d'autres organisations !
