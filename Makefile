.DEFAULT_GOAL := start
.PHONY: build start log stop prettier lint test

### DOCKER_COMPOSE VARIABLES ###

# The base config is in `docker-compose.yml`
DOCKER_COMPOSE=docker-compose -f docker-compose.yml
# In a local environment, add the `docker-compose.local.yml` configuration and env variables from `.env.local` file
DOCKER_COMPOSE_LOCAL=$(DOCKER_COMPOSE) -f docker-compose.local.yml --env-file .env.local
# For local development, add the `docker-compose.local.dev.yml` configuration to `docker-compose.local.yml`
DOCKER_COMPOSE_DEV=$(DOCKER_COMPOSE_LOCAL) -f docker-compose.local.dev.yml
# For tests, add the `docker-compose.local.test.yml` configuration to `docker-compose.local.yml`
DOCKER_COMPOSE_TEST=$(DOCKER_COMPOSE_LOCAL) -f docker-compose.local.test.yml
# To deploy in production, add config from `docker-compose.prod.yml` and env variables from `.env.prod` file
DOCKER_COMPOSE_PROD=$(DOCKER_COMPOSE) -f docker-compose.prod.yml --env-file .env.prod

# Listing of different services of the app
SERVICES=api inscription-front orga-front ia-back


############################
########### LOGS ###########
############################

log:
	docker-compose logs -f --tail=10000 api
log-back:
	docker-compose logs -f --tail=10000 api ia-back
log-front:
	docker-compose logs -f --tail=10000 orga-front inscription-front
log-all:
	docker-compose logs -f --tail=10000 api ia-back orga-front inscription-front

show-api-logs:
	ls -l api/logs


############################
###### INITIALIZATION ######
############################

### DEVELOPMENT ###

init: install-project-dev-deps build install-docker-deps

install-project-dev-deps:
	npm install

install-docker-deps: install-docker-deps-api install-docker-deps-inscription-front install-docker-deps-orga-front install-docker-deps-ia-back
install-docker-deps-api:
	$(DOCKER_COMPOSE) run --no-deps api npm install --omit=optional
install-docker-deps-ia-back:
	$(DOCKER_COMPOSE) run --no-deps ia-back npm install --omit=optional
install-docker-deps-orga-front:
	$(DOCKER_COMPOSE) run --no-deps orga-front npm install --legacy-peer-deps
install-docker-deps-inscription-front:
	$(DOCKER_COMPOSE) run --no-deps inscription-front npm install --legacy-peer-deps


############################
#### DEVELOPMENT START #####
############################

# Start containers
start:
	$(DOCKER_COMPOSE_DEV) up -d api inscription-front orga-front
full-start:
	$(DOCKER_COMPOSE_DEV) up -d
start-api:
	$(DOCKER_COMPOSE_DEV) up -d api
start-ia-back:
	$(DOCKER_COMPOSE_DEV) up -d ia-back
start-orga-front:
	$(DOCKER_COMPOSE_DEV) up -d orga-front
start-inscription-front:
	$(DOCKER_COMPOSE_DEV) up -d inscription-front

# Force recreating the container
force-start-api:
	$(DOCKER_COMPOSE_DEV) up -d --force-recreate api
force-start-ia-back:
	$(DOCKER_COMPOSE_DEV) up -d --force-recreate ia-back
force-start-orga-front:
	$(DOCKER_COMPOSE_DEV) up -d --force-recreate orga-front
force-start-inscription-front:
	$(DOCKER_COMPOSE_DEV) up -d --force-recreate inscription-front

# Local production build for frontends
start-build-orga-front:
	$(DOCKER_COMPOSE_LOCAL) up -d --force-recreate orga-front
start-build-inscription-front:
	$(DOCKER_COMPOSE_LOCAL) up -d --force-recreate inscription-front

# Stop containers
stop:
	$(DOCKER_COMPOSE_LOCAL) down
stop-api:
	$(DOCKER_COMPOSE_LOCAL) stop api
stop-ia-back:
	$(DOCKER_COMPOSE_LOCAL) stop ia-back
stop-orga-front:
	$(DOCKER_COMPOSE_LOCAL) stop orga-front
stop-inscription-front:
	$(DOCKER_COMPOSE_LOCAL) stop inscription-front


############################
#### PRODUCTION  START #####
############################

# Start containers
start-prod:
	$(DOCKER_COMPOSE_PROD) up -d api inscription-front orga-front traefik
full-start-prod:
	$(DOCKER_COMPOSE_PROD) up -d
start-prod-api:
	$(DOCKER_COMPOSE_PROD) up -d api
start-prod-ia-back:
	$(DOCKER_COMPOSE_PROD) up -d ia-back
start-prod-orga-front:
	$(DOCKER_COMPOSE_PROD) up -d orga-front
start-prod-inscription-front:
	$(DOCKER_COMPOSE_PROD) up -d inscription-front

# Force recreating the container
force-start-prod-api:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate api
force-start-prod-ia-back:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate ia-back
force-start-prod-orga-front:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate orga-front
force-start-prod-inscription-front:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate inscription-front

# Stop containers
stop-prod:
	$(DOCKER_COMPOSE_PROD) down
stop-prod-api:
	$(DOCKER_COMPOSE_PROD) stop api
stop-prod-ia-back:
	$(DOCKER_COMPOSE_PROD) stop ia-back
stop-prod-orga-front:
	$(DOCKER_COMPOSE_PROD) stop orga-front
stop-prod-inscription-front:
	$(DOCKER_COMPOSE_PROD) stop inscription-front


############################
### LOCAL NETWORK START ####
############################

start-local-network:
	$(DOCKER_COMPOSE_LOCAL) up -d


########################
######## OTHERS ########
########################

### DOCKER ###

# Launch the building process for all the containers
build:
	$(DOCKER_COMPOSE) build

# Clean all docker containers
remove:
	$(DOCKER_COMPOSE_LOCAL) kill
	$(DOCKER_COMPOSE_LOCAL) rm -v

# Make a `npm rebuild` in all Docker containers. You probably don't need this.
rebuild-docker-deps:
	for service in $(SERVICES) ; do \
		$(DOCKER_COMPOSE_DEV) run --no-deps $$service npm rebuild ; \
	done

### LINTING & FORMATTING ###

# Make a manual `npm run prettier` in all subdirectories
prettier:
	npm run prettier

# Make a manual `npm run prettier-check` in all subdirectories
prettier-check:
	npm run prettier-check

# Make a manual `npm run lint` in all subdirectories
lint:
	for service in $(SERVICES) ; do \
		npm run lint --prefix ./$$service ; \
	done

# Make a manual `npm run lint-show` in all subdirectories
lint-show:
	for service in $(SERVICES) ; do \
		npm run lint-show --prefix ./$$service || exit 1 ; \
	done

#### TESTS ####

# For tests we currently only need fuseki
test:
	$(DOCKER_COMPOSE_TEST) up api --exit-code-from=api
	$(DOCKER_COMPOSE_TEST) up ia-back --exit-code-from=ia-back

