![NOÉ](.gitlab/images/readme-banner.jpg)

# Contribuer au projet NOÉ

## Table des matières

[[_TOC_]]

## Structure de l'application

### Division en microservices

L'application est divisée en quatre micro services :

- deux frontends qui vivent dans `/orga-front` (frontend orga) et `/inscription-front` (frontend participant⋅e)
- un backend API qui vit dans `/api`
- un second backend dédié aux calculs d'IA qui vit dans `/ia-back`

![Structure de l'application](https://docs.google.com/drawings/d/e/2PACX-1vQExKtbzUq3-qQ0I9ttBDyS4UmYkFu55WhCMGmT2OQtvLZxE4SStQ9Cc8F0sHSfdIDdXQTAKfof9PkA/pub?w=1102&amp;h=744)

### Stack technique

#### Frontends orga et participant⋅e - `/orga-front` et `/inscription-front`

- **Base :** Javascript
- **Frameworks :** ReactJS / Redux
- **Librairie graphique :** [Ant.Design](https://ant.design/)

#### Serveurs backend API et IA - `/api` et `/ia-back`

- **Base :** NodeJS / Typescript (compilé en Javascript avec `gulp`)
- **Framework :** Express
- **Base de données :** Mongoose (base MongoDB)

#### Conteneurisation

L'application est entièrement conteneurisée dans des containers Docker. Nous utilisons Docker Compose pour gérer le
lancement des containers.

La configuration de l'application est décrite dans le fichier `docker-compose.yml` à la racine du projet.

> **Comment installer ça ?**
>
> Pour Linux, allez voir [ici pour Docker](https://linuxize.com/post/how-to-install-and-use-docker-on-debian-10/)
> et [là pour Docker Compose](https://linuxize.com/post/how-to-install-and-use-docker-compose-on-debian-10/)

## Commandes de base

Pour voir le détail des commandes, il suffit d'[aller consulter le fichier `Makefile`](Makefile) disponible à la racine
du projet, les commandes y sont très bien expliquées.

### Mettre en route l'application (en mode développement)

C'est super simple ! Suivez le guide :

1. **Lancez l'initialisation de l'application :**

   ```bash
   # À la racine du projet : /noe
   make init
   ```

   > Cette commande va :
   > - Installer certains outils de développement (pour pouvoir utiliser notamment les commandes liées à `eslint`
       et `prettier`, notamment)
   > - Préparer votre environnement Docker, en lançant la construction des containers
   > - Installer les dépendances NPM des containers

   > **La première mise en route de l'application peut prendre un peu de temps** (entre 5 et 10 minutes, en
   > fonction de votre connexion internet). Surveillez les logs pour vérifier qu'il n'y a pas d'erreurs.
   
2. **Lancez les migrations de la base de données :**

   ```bash
   # Allez dans le dossier /api et lancez la commande de migration :
   cd api
   npx migrate-mongo up
   ```

   > [Plus d'infos sur les migrations ici](#lancer-les-migrations)

3. **Lancez l'application !**

   ```bash
   # Retournez à la racine /noe :
   cd ..
   make start
   ```

   > Cette commande va lancer tous les services de l'application en une fois. **À l'avenir, vous aurez juste besoin de
   celle-ci pour démarrer NOÉ.**

Il ne vous reste plus qu'à attendre quelques minutes, puis vous pourrez vous rendre sur :

- [`localhost:5000`](http://localhost:5000/) pour accéder à l'interface orga
- [`localhost:5020`](http://localhost:5020/) pour accéder à l'interface participant⋅e

Pour information, l'API est exposée sur `localhot:4000` et le serveur IA sur `localhost:4010`.

> L'application supporte le **hot-restart**, donc une fois que les containers sont lancés, plus besoin de les relancer :
> la
> compilation se relance dès qu'un changement est détecté dans les fichiers.

### Stopper l'application

`make stop`

### Afficher les logs

- Logs des deux backends : `make log`
- Logs des deux frontends : `make log-front`
- Logs de tous les containers : `make log-all`

### Reformater le code avec [Prettier](https://prettier.io/)

- avec Make : `make prettier` (cela agit sur le projet entier)
- avec npm dans un des sous-dossiers : `npm run prettier`

**Attention :** si vous oubliez de faire `make prettier` avant de pousser des changements, Gitlab vous indiquera que les
tests sur le formatage de code ne sont pas passés et la pipeline de votre branche échouera (impossible alors d'accepter
un merge request dont la pipeline échoue).

> Pour faire une vérification simple _sans modifier les fichiers_, tapez `make prettier-check`.

> **Intégrez prettier à votre IDE**
>
> Prettier est compatible avec la plupart des
> IDEs : [le vôtre est forcément dans ce guide](https://prettier.io/docs/en/editors.html).
> Votre IDE missionnera alors Prettier pour reformater vos fichiers.

### Vérifier et résoudre les problèmes avec [ESLint](https://eslint.org/)

- avec Make : `make lint-show` (cela agit sur le projet entier)
- avec npm dans un des sous-dossiers : `npm run lint-show`

> Pour résoudre les problèmes qui peuvent être résolus automatiquement, tapez `make lint`.

> **Intégrez ESLint à votre IDE**
>
> ESLint est compatible avec la plupart des
> IDEs : [le vôtre est forcément dans ce guide](https://eslint.org/docs/user-guide/integrations).

### Gérer les migrations de la base de données

La base de données de NOÉ est versionnée, c'est-à-dire que nous utilisons un mécanisme de migrations successives pour
faire évoluer la structure de la base au fil du temps (pour que tout ne casse pas entre les versions de l'application
quand on fait les mises à jour !). Une [page Wikipédia](https://en.wikipedia.org/wiki/Schema_migration)
existe pour expliquer un peu les concepts.

> **Attention :** Toutes les opérations avec `migrate-mongo` se font depuis le dossier `/api`.
>
> Plus d'infos sur `migrate-mongo` : [Github](https://github.com/seppevs/migrate-mongo) /
> commande `npx migrate-mongo -h`

#### Voir l'état des migrations

```bash
npx migrate-mongo status
```

#### Créer une nouvelle migration

C'est une opération que l'on fait **exclusivement quand on développe !**

```bash
npx migrate-mongo create <nom_de_la_migration>
```

#### Lancer les migrations

Une fois que vous êtes sûr·es de votre coup, vous pouvez lancer la migration :

```bash
npx migrate-mongo up
```

#### Revenir en arrière

Si vous avez développé la fonction `down()` de la migration, vous pouvez aussi revenir en arrière. C'est utile si vous
voulez revenir à une version plus ancienne, et que vous voulez revenir dans le temps avec la base de données.

```bash
npx migrate-mongo down
```

## Déployer l'application

### Sur un serveur distant

> Si vous voulez automatiser votre déploiement et les mises à jour avec la CI Gitlab, contactez les membres du
> projet. On vous expliquera la marche à suivre.

1. **Clonez le projet :**
   ```bash
   # Dans votre dossier personnel sur le serveur : /~
   git clone git@gitlab.com:alternatiba/noe.git # ou https : https://gitlab.com/alternatiba/noe.git
   ```

2. **Créez votre fichier de configuration de production :**

   ```bash
   # À la racine du projet : /noe
   cp .env.prod.example .env.prod
   ```

   > Cette commande crée le fichier `.env.prod` dont l'application a besoin pour se déployer en production, à partir du
   fichier d'exemple `.env.prod.example` qui est déjà fourni.

   > **À vous de jouer !** Remplissez maintenant votre fichier `.env.prod` avec votre propre configuration.

   L'arborescence devrait être la suivante :

3. **Déployez !**

   Là, vous avez fait le plus gros du travail, maintenant c'est la machine qui va travailler pour vous !

   Vous n'avez qu'à suivre la démarche de lancement en développement, en lançant `make start-prod` à la place
   de `make start`. Ce qui nous donne donc :
   ```bash
   ### Etape 1 : Construction
   # À la racine du projet : /noe
   make init
   
   ### Etape 2 : Migration
   # Allez dans le dossier /api et lancez la commande de migration :
   cd api
   npx migrate-mongo up
   
   ### Etape 3 : Lancement
   # Retournez à la racine /noe :
   cd ..
   make start-prod
   ```

> Une version alternative consiste à stocker son propre fichier de configuration sur un dépot Git privé, par exemple
> nommé `prod-deployment`, et à le cloner juste à côté du dépôt Git de NOÉ :
> ```
>  /~
>  |
>  |- /prod-deployment # Votre dossier de déploiement
>  |   |- .env.prod # Votre fichier de configuration
>  |
>  |- /noe # Le dépôt NOÉ
>      |- /api
>      |- /ia-back
>      |- /incription-front
>      |- /orga-front
>      |- ...
> ```
>
> De cette manière, si vous faites des changements dans votre configuration, vous aurez simplement à faire un `git pull`
> dans votre dossier `prod-deployment`, et ensuite créer une copie de votre fichier `.env.prod` dans le dossier NOÉ.
> C'est aussi la structure de
> fichiers qui nous permet d'automatiser les déploiements en production depuis la CI Gitlab.

> Par défaut, le serveur IA n'est pas lancé automatiquement. Vous pouvez le lancer manuellement
> avec `make start-ia-back`.

### Sur un réseau local

Si vous le souhaitez, vous pouvez lancer l'application sur votre réseau local pour pouvoir accéder à NOÉ sur tous les
appareils connectés à votre réseau. Cela fonctionne sur une box internet classique par exemple.

L'application est alors compilée comme en mode "production", et sera rendu disponible sur les mêmes ports que
d'habitude.

> **Attention :** le déploiement sur un réseau local n'est pas encore supporté en HTTPS. Pour l'instant, c'est
> seulement du HTTP (non sécurisé).
> 
> La fonctionnalité n'a pas été testée pour de vrai et pourrait être buggée.

**Marche à suivre :**

1. Récupérez
   votre [adresse IP locale](https://blog.shevarezo.fr/post/2019/01/08/comment-obtenir-adresse-ip-linux-ligne-de-commande)
   (qui est différente de votre adresse IP publique). Elle commence par `192.xx.xx.xx.xx`.

2. Modifiez le fichier `.env.local` en dé-commantant les lignes dédiées au lancement sur un réseau local. Ajoutez votre
   adresse IP sur la variable `LOCAL_NETWORK_IP_ADDRESS`:

3. Lancez la commande `make start-local-network`.

## Guide de contribution

### Dossiers communs / synchronisés

> **Attention :** cette partie est obsolète et a besoin d'être rafraichie !

Les deux frontends de NOE partagent de nombreux comportements et composants. Il faut donc veiller à quelques petits
points lorsqu'on développe.

Certains dossiers sont partagés entre les deux frontends, c'est dire qu'on veille à ce qu'ils soient identiques à tout
instant sur les deux frontends. C'est le cas des dossiers :

- `/src/components/common`
- `/src/helpers`

Même si certains morceaux de code ne sont utilisés que dans un des deux frontends, nous choisissons de synchroniser les
comportements quoi qu'il en soit, pour minimiser la divergence des fichiers au cours du temps (en vrai y a parfois des
petites divergences, mais on essaie de minimiser).

### Les questions à garder en tête

De manière générale, quand on travaille dans les frontends, il y a deux questions à se poser en permanence :

- **"Est-ce que mon code est bien synchronisé autant que possible entre le front participant et le front orga ?"**

  Si non (par ex. du CSS qui n'a été écrit que sur un seul des deux frontends), alors il est encouragé de regarder la
  synchro de tes fichiers et d'homogénéiser le code le plus possible entre les deux frontends, histoire que l'on
  n'accumule pas de dette technique supplémentaire dûe à la divergence des fichiers entre les deux fronts.


- **"Ce code ne peut-il pas servir deux fois au lieu d'une ?"**

  Si oui, arrangez-vous pour qu'il figure soit dans le dossier `common`, pour que les deux frontends en profitent (par
  ex. le container d'une sidebar), et gardez le code spécifique (par ex. le contenu de la sidebar, qui diffère dans les
  deux fronts) dans un autre endroit. Ce qui peut être mis en commun, doit être mis en commun.

> **Comment veiller à la synchronisation des fichiers ?**
>
>- Comparer des fichiers sur [VSCode](https://stackoverflow.com/a/30142271/13976355)
>- Comparer des fichiers
   sur [Webstorm - Jetbrains](https://www.jetbrains.com/help/webstorm/comparing-files-and-folders.html)
>- Comparer des fichiers sur [Atom](https://stackoverflow.com/a/49169858/13976355)
>- Ajoute ton IDE s'il n'est pas listé !

### Conventions suivies dans le code

#### Mongoose: `id` ou `_id` ?

Avec Mongoose, l'ID des objets est toujours repéré par l'attribut `_id`. Mais pour nous rendre la vie plus simple,
Mongoose copie aussi cet ID dans un attribut `id`. Le problème, c'est qu'il n'est pas toujours présent partout, car on
manipule souvent les objets dans le frontend, et parfois `id` n'existe pas.

Du coup : **n'utilisez _jamais_ `id`** sur des objets Mongoose, **utilisez _toujours_ `_id`**. Dans le backend, parfois
les IDs sont des strings (quand ils viennent de la requête du front, par exemple), mais parfois ce seront des
ObjectIds (le
type de MongoDB pour ses ids). Pour faire des comparaisons, il faudra toujours les transformer en strings, en
utilisant `toString()` :

```js
myObjectId.toString() === myStringId
```

#### Variables d'environnement

Les deux frontends sont construits en React avec un boilerplate qui s'appelle CreateReactApp, qui propose plein d'outils
d'aide au développement. Notamment, il permet d'intégrer facilement des variables d'environnement dans les applis
React (ce qui n'est pas aisé à faire, vu que le code s'exécute chez le client). Pour cela, il est nécessaire que les
variables d'environnement commencent par `REACT_APP_`.

Par convention, on préfèrera donc mettre `REACT_APP_` devant toutes les variables d'environnement utilisées pour
homogénéiser les noms de variables.

```env
MA_VARIABLE=truc # ne sera pas prise en compte par les frontends
REACT_APP_MA_VARIABLE=truc # sera prise en compte par les frontends
```

#### Les différents scripts `docker-compose`

Vous avez dû voir qu'on avait beaucoup fichiers `docker-compose.bla-bla-bla.yml`. Ils ont tous des buts différents :

- `docker-compose.yml` lance toute l'appli en mode développement normal
- `docker-compose.debug-api.yml` et `docker-compose.debug-ia.yml` lancent tout pareil, mais permettent de lancer en
  debug l'`api` ou l'`ia` de manière indépendante.
- `docker-compose.front-build.yml` compile les frontends comme si on était en mode production. C'est à dire que les
  fronts ne seront plus servis par le serveur de développement, mais seront compilés et optimisés, et servis par le
  serveur dynamique (qui permet de créer des méta-tags dynamiques notamment)
- `docker-compose.local-network.yml` permet de lancer NOÉ sur son réseau local, pourvu qu'on ait spécifié son IP locale
  dans un fichier `.env`
- `docker-compose.test-api.yml` et `docker-compose.test-ia.yml` permettent de lancer les tests

Ces scripts peuvent être facilement activés à l'aide des commandes du [fichier `Makefile`](Makefile).

#### Routing

Pour le routing, nous utilisons le module [`@reach/router`](https://reach.tech/router/). Voici quelques conseils autour
de son utilisation :

##### La fonction `navigate`

Nous utilisons systématiquement la navigation du routeur grâce à la fonction `navigate`. Nous préférons utiliser des
liens relatifs plutôt que des liens absolus, partout où c'est possible. La navigation relative apporte plus de
résilience aux composants, car ils ne dépendent plus de l'URL à laquelle ils sont affichés.

Pour cela, la fonction `navigate` doit être importée via les `props` des composants, car c'est seulement de cette
manière qu'on peut gérer des liens relatifs.

> [**Documentation de Reach Router :**](https://reach.tech/router/api/navigate)
>
> _Or better, yet, use `props.navigate` passed to your route components, and then you can navigate to relative paths:_
>
>```react
>const Invoices = ({ navigate }) => (
>  <div>
>    <NewInvoiceForm
>      onSubmit={async event => {
>        const newInvoice = await createInvoice(
>          event.target
>        )
>        // can navigate to relative paths
>        navigate(newInvoice.id)
>      }}
>    />
>  </div>
>)
>```

Nous excluons, partout où c'est possible, les liens en durs (`href` et liens absolus), qui provoquent souvent un
rechargement total de la page et causent des désagréments pour l'utilisateur⋅ice (glitches et perte des données non
enregistrées)

##### L'élément `<Router/>`

L'élément Router est visible principalement dans les fichiers `App.js`, `ProjectLayout.js` et `MainLayout.js`.

Lorsqu'on doit faire des calculs préalables pour savoir si on peut afficher une page (vérification d'accès, connexion de
l'utilisateur, état de l'objet, ...), on préfère toujours utiliser le routeur plutôt qu'un `useEffect`.

**Bonne pratique :**

```react
export const ProjectLayout = (props) => {
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const userIsRegistered = currentProject?.currentRegistration?.booked;

  useEffect(() => {
    // Load something asynchronously that will change the data
    dispatch(currentProjectActions.load(envId));
  }, [envId]);

  return (
    <div>
      {userIsRegistered === undefined ? // If we don't know yet if the user has registered to the project, we wait
        <LayoutElement.Pending/> 
        :
        <Router style={{height: '100%'}}>
          <Welcome path="/welcome"/>
          <Registration path="/registration/:pane" envId={envId}/>
          {userIsRegistered === true && // If connected, allow those pages
          <>
            <SessionList path="/sessions"/>
            <SessionEdit path="/sessions/:id"/>
            <SessionAgenda path="/sessions/agenda"/>
          </>
          }
          <Redirect noThrow from="/*" to="./welcome"/>
        </Router> 
      }
    </div>
  );
```

**Mauvaise pratique :**

```react
export const ProjectLayout = (props) => {
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const userIsRegistered = currentProject?.currentRegistration?.booked;
  
  // BAD : This is bad cause the component will render, then directly redirect, resulting in a glitch for the final user
  if (userIsRegistered === false) {
    navigate('./projects'); 
  }

  useEffect(() => {
    // Load something asynchronously that will change the data
    dispatch(currentProjectActions.load(envId));
  }, [envId]);

  return (
    <div>
        <Router style={{height: '100%'}}>
          <Welcome path="/welcome"/>
          <Registration path="/registration/:pane" envId={envId}/>
          {userIsRegistered === true && // If connected, allow those pages
          <>
            <SessionList path="/sessions"/>
            <SessionEdit path="/sessions/:id"/>
            <SessionAgenda path="/sessions/agenda"/>
          </>
          }
          <Redirect noThrow from="/*" to="./welcome"/>
        </Router>
    </div>
  );
```

#### Les classes CSS

Nous avons créé des classes CSS customisées pour nous aider dans la construction des pages des frontends.

##### Classes `containerH` et `containerV`

Elles permettent d'afficher en mode `flex` les objets dans l'application, soit en horizontal, soit en vertical. C'est à
privilégier dès qu'on est face à ce besoin.

On peut les associer à `buttons-container` pour créer un bloc dont les éléments sont espacés comme il faut de manière
régulière et se réadaptent bien à la taille de l'écran.

_Exemple :_

```react
<div style={{border: "2px solid red"}}>
  <div className="containerH buttons-container">
    <Button>Bouton Bouton</Button><Button>Bouton Bouton</Button>
    <Button>Bouton Bouton</Button><Button>Bouton Bouton</Button>
    <Button>Bouton Bouton</Button><Button>Bouton Bouton</Button>
  </div>
</div>

<div style={{border: "2px solid blue"}}>
  <div className="containerV buttons-container">
    <Button>Bouton Bouton</Button>
    <Button>Bouton Bouton</Button>
    <Button>Bouton Bouton</Button>
  </div>
</div>
```

![](.gitlab/images/buttons-container.png)

##### Classe `container-grid`

Pour créer des vues avec des divisions en deux ou trois colonnes, nous utilisons `container-grid` associé aux classes :

- `two-per-row` et `three-per-row` pour créer des colonnes de même largeur,
- `two-thirds-one-third` pour avoir une division deux tiers / un tiers.

```react
<div className="container-grid two-thirds-one-third">
  <div>Contenu qui prend 2 tiers de l'écran</div>
  <div>Contenu qui prend 1 tiers de l'écran</div>
</div>
```

##### Classes `page-content`/`modal-page-content` et `page-container`

Pour pouvoir créer des bordures propres sur les pages, tout en se réservant la possibilité à tout moment de pouvoir
mettre des objets en pleine largeur (par exemple une barre de recherche, etc.), on utilise une imbrication de classes
CSS comme suit.

En imbriquant systématiquement un `page-container` dans un élément `page-content`, on crée une page avec des bordures
bien propres, génériques et responsives. Lorsque le contenu est affiché dans un modal (pas dans une page classique), on
préfère utiliser `modal-page-content`, car les modals de la librairie AntDesign ont des marges différentes.

Pour mettre un élément en pleine page, il suffit ensuite de rajouter la classe `full-width-content` à cet élément. Si on
veut mettre cet élément en pleine page, mais garder les marges à l'intérieur de cet élément, on ajoute `with-margins` à
côté de `full-width-content`.

_Exemple :_

```react
<div style={{border: "2px solid grey"}}>
  <div className="page-content">
    <div className="page-container">
      <div style={{border: "2px solid red"}}>
        Bloc simple avec marges classiques
      </div>

      <div className="full-width-content" style={{border: "2px solid orange"}}>
        Bloc en pleine largeur, contenu sans marges <code>className="full-width-content"</code>
      </div>

      <div className="full-width-content with-margins" style={{border: "2px solid green"}}>
        Bloc en pleine largeur, contenu avec marges <code>className="full-width-content with-margins"</code>
      </div>
    </div>
  </div>
</div>
```

![](.gitlab/images/page-container.png)

##### Classes `success-button` et `warning-button`

AntDesign ne permet que de faire des boutons bleus et des boutons rouges (en utilisant l'argument `danger`). Pour varier
un peu les couleurs, on peut ajouter des classes custom dessus. Il faut penser à bien mettre l'argument `type="primary"`
pour que ça fonctionne.

```react
<Button type="primary" className="success-button">
  Success Button
</Button>

<Button type="primary" className="warning-button">
  Warning Button
</Button>
```

> **Attention :**
>
> Cette technique ne fonctionne pas sur les boutons qui sont encapsulés dans un `PopConfirm` et
> `disabled` à la fois. Dans ce cas-là, il faut renlever la classe si le bouton est `disabled`.
> ```react
> <Button
>   className={isDisabled ? undefined : registrationButtonClassName}
>   disabled={isDisabled}>
>   Disabled button in Popconfirm
> </Button>
> ```
>

### Conventions suivies pour l'UI / UX (Interface utilisateur⋅ice / Expérience utilisateur⋅ice)

#### Le texte dans l'application

Lorsque nous développons, nous prenons garde à écrire de la manière la plus 'professionnelle' possible. Nous mettons des
majuscules en début de mots quand il faut, nous mettons des accents et vérifions systématiquement l'ortographe du texte
écrit (cela évite de repasser ensuite derrière pour corriger des fautes facilement évitables).

#### Convention d'écriture inclusive

Nous rédigeons nos textes de manière la plus inclusive possible. Dans certains cas, nous avons recours au point médian.
Voici la convention d'utilisation du point médian.

Ne pas faire :

- participant.e
- participant.es
- participant.e.s

Faire :

- participant⋅e
- participant⋅es

##### Messages à destination de l'utilisateur⋅ice

Nous appliquons les bonnes pratiques lorsqu'il s'agit d'informer l'utilisateur ou de l'encourager à exécuter une action.

Nous suivons [les principes suivants](https://uxplanet.org/how-to-write-good-error-messages-858e4551cd4), et en
particulier ceux énoncés ici :

- Be Clear And Not Ambiguous
- Be Short And Meaningful
- Don’t Use Technical Jargons
- Be Humble — Don’t Blame User
- Avoid Negative Words
- Give Direction to User
- Be Specific And Relevant
- Provide Appropriate Actions
- Use Proper Placement

### Utilisation de Git et de Gitlab

#### Les issues

Les issues sont créées par n'importe qui souhaitant soumettre une fonctionnalité ou référencer un bug. Il est fortement
encouragé de reprendre les modèles d'issue adaptés
en [sélectionnant le modèle adéquat dans le dropdown "Description"](https://docs.gitlab.com/ee/user/project/description_templates.html#use-the-templates)

Depuis les issues, il est possible
de [créer une nouvelle branche](https://docs.gitlab.com/ee/user/project/issues/issue_data_and_actions.html#create-merge-request)
qui sera directement nommée de la bonne manière et qui sera liée à l'issue.

#### Les merge requests (MRs)

![image initiale](https://wac-cdn.atlassian.com/dam/jcr:01b0b04e-64f3-4659-af21-c4d86bc7cb0b/01.svg?cdnVersion=1606)

Une fois notre branche de développement prête (disons qu'elle s'appelle `Feature` comme dans l'image), il y a deux
façons de fusionner les changements avec la branche `master` principale :

- Par `git merge`
- Par `git rebase`

> Les schémas de cette partie sont tirées
> de [cet excellent article](https://www.atlassian.com/fr/git/tutorials/merging-vs-rebasing) sur la différence
> entre `git merge` et `git rebase`.

##### Fusion par `git merge`

C'est la méthode la plus répandue. Il y a trois étapes :

- Merger `master` dans notre propre branche - c'est-à-dire, apporter les changements de `master` qui ont été faits
  depuis qu'on a commencé à développer, afin de se remettre à jour, puis s'il y a des conflits, résoudre ces conflits
- Retester l'application, car c'est souvent lors des `merge` que l'on crée de nouveaux bugs...
- Puis une fois que la MR a été relue par les pairs, la branche peut être mergée dans `master` (via l'interface Gitlab
  souvent).

![image merge](https://wac-cdn.atlassian.com/dam/jcr:e229fef6-2c2f-4a4f-b270-e1e1baa94055/02.svg?cdnVersion=1606)

**Avantage :** la simplicité.

**Inconvénient :** on obtient un arbre de développement assez chaotique et il est difficile de s'y retrouver lorsqu'on
cherche l'origine d'un bug, ou quand on doit revenir dans l'arbre des commits.

##### Fusion par `git rebase`

C'est une méthode plus avancée. Au lieu de fusionner des changements (faire que `master` et notre branche re-fusionnent
après s'être séparées), on va carrément remettre les changements apportés par notre branche au-dessus de tous les
nouveaux changements qu'on n'a pas pris en compte.

- Rebaser votre branche sur `master` - c'est-à-dire déplacer l'origine de la branche sur le dernier commit de master et
  réécrire les commits depuis cet endroit.
- S'il y a des conflits, les résoudre - c'est un peu plus tordu qu'un `merge` classique, car il peut y avoir des
  conflits sur plusieurs commits lorsque les commits sont mal délimités les uns des autres.
- Retester l'application, encore et toujours...
- Puis une fois que la MR a été relue par les pairs, la branche peut être mergée dans `master` (via l'interface Gitlab
  souvent).

![image rebase](https://wac-cdn.atlassian.com/dam/jcr:5b153a22-38be-40d0-aec8-5f2fffc771e5/03.svg?cdnVersion=1606)

**Avantage :** un arbre des commits super clean, un historique de développement clair comme de l'eau de roche.

**Inconvénient :** `rebase` est un peu plus technique et nécessite une bonne maîtrise de Git (mais une fois qu'on a
compris la chose, ça va aussi vite qu'un `merge`).

De nombreux IDEs intègrent le processus de `rebase` nativement dans leur outil de versionning. Regardez peut-être si le
vôtre le fait !

Pour aller plus loin sur `rebase`, c'est [ici](https://www.atlassian.com/fr/git/tutorials/merging-vs-rebasing).

### Ressources utiles

#### Extensions dans le navigateur (super utiles, vraiment!)

- React Dev Tools :
    - [Firefox](https://addons.mozilla.org/fr/firefox/addon/react-devtools/)
    - [Chrome](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
- Redux Dev Tools :
    - [Firefox](https://addons.mozilla.org/en-US/firefox/addon/reduxdevtools/)
    - [Chrome](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=fr)

#### Tutoriels

- [Initiation à React](https://fr.reactjs.org/tutorial/tutorial.html)
- [Initiation à Redux](https://redux.js.org/tutorials/index)

#### À rajouter dans ce Guide

- le fichier configuration.js pour paramétrer l'apparence graphique de NOE
- comment débugger directement dans son IDE
- copier-coller la config d'une instance heroku vers une autre : le one-liner appréciable:
  `heroku config -a <nom_de_lappli_donneuse> | sed '1d' | sed -e 's/: /=/g' | sed -e 's/ //g' | tr '\n' ' ' | xargs heroku config:set -a <nom_de_lappli_receveuse>`