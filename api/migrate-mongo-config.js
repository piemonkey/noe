const path = require("path");
require("dotenv").config({path: path.join(__dirname, "../.env.prod")});

const dbUriGivenInEnvFile = process.env.MONGODB_URI && process.env.MONGODB_URI.length > 0;

const dbUrl = dbUriGivenInEnvFile
  ? process.env.MONGODB_URI
  : "mongodb://noe:noe@localhost:27017/main";

console.log(
  dbUriGivenInEnvFile
    ? "=== Connecting to remote Mongo DB - URI from file `.env.prod` : ==="
    : "=== Connecting to local Mongo DB ===",
  `\n > ${dbUrl.replace(/\/\/.*:.*@/, "// ... : ... @")}\n`
);

module.exports = {
  mongodb: {
    // The url to your MongoDB database
    url: dbUrl,
    options: {useNewUrlParser: true, useUnifiedTopology: true},
  },

  // The migrations dir, can be an relative or absolute path. Only edit this when really necessary.
  migrationsDir: "migrations",

  // The mongodb collection where the applied changes are stored. Only edit this when really necessary.
  changelogCollectionName: "migrations",

  // Don't change this, unless you know what you're doing
  moduleSystem: "commonjs",
};
