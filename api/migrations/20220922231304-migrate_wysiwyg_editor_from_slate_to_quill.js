const {ObjectId} = require("mongodb");

const fieldsToMigrate = [
  {collection: "activities", fields: ["description", "notes"]},
  {collection: "teams", fields: ["description", "notes"]},

  {collection: "activities", fields: ["notes"]},
  {collection: "teams", fields: ["notes"]},
  {collection: "sessions", fields: ["notes"]},
  {collection: "stewards", fields: ["notes"]},
  {collection: "places", fields: ["notes"]},
];

const serializeElement = (level, element, verbose) => {
  if (Array.isArray(element)) return serialize(element);

  const {type, children, chidren, ...rest} = element;
  verbose && console.log("  ".repeat(level) + `serializeElement (${type})`);
  if (rest && Object.keys(rest).length > 0) {
    console.log("Forgotten (Element) !", rest);
    throw new Error("Forgot Element");
  }

  let html = children
    .map((child) =>
      child.type ? serializeElement(level + 1, child) : serializeLeaf(level + 1, child)
    )
    .join("");

  switch (type) {
    case "block-quote":
      html = `<blockquote>${html}</blockquote>`;
      break;
    case "hyperlink":
      html = `<a href="${html}">${html}</a>`;
      break;
    case "bulleted-list":
      html = `<ul>${html}</ul>`;
      break;
    case "heading-one":
      html = `<h1>${html}</h1>`;
      break;
    case "heading-two":
      html = `<h2>${html}</h2>`;
      break;
    case "list-item":
      html = `<li>${html}</li>`;
      break;
    case "numbered-list":
      html = `<ol>${html}</ol>`;
      break;
    default:
      html = `<p>${html}</p>`;
  }
  verbose && console.log("  ".repeat(level) + ` -> ${html}`);
  return html;
};

const serializeLeaf = (level, {text, bold, italic, underline, code, ...rest}, verbose) => {
  verbose &&
    console.log(
      "  ".repeat(level) + `serializeLeaf (${JSON.stringify({bold, italic, underline, code})})`
    );

  if (rest && Object.keys(rest).length > 0) {
    console.log("Forgotten (Leaf) !", rest);
    throw new Error("Forgot Leaf");
  }

  if (bold) {
    text = `<strong>${text}</strong>`;
  }
  if (code) {
    text = `<code>${text}</code>`;
  }
  if (italic) {
    text = `<em>${text}</em>`;
  }
  if (underline) {
    text = `<u>${text}</u>`;
  }
  text = `<span>${text}</span>`;

  verbose && console.log("  ".repeat(level) + ` -> ${text}`);
  return text;
};

const serialize = (arrayOfElements, verbose = false) => {
  const result = arrayOfElements.map((el) => serializeElement(1, el, verbose)).join("");
  if (result === "<p><span></span></p>") return undefined; // It's the default value once computed. Don't keep it.
  return result;
};

module.exports = {
  async up(db, client) {
    // *********** Remove all useless '[]' values
    for (const {collection, fields} of fieldsToMigrate) {
      for (const field of fields) {
        console.log("Removing '[]' for field", field, "in collection", collection);
        const res = await db
          .collection(collection)
          .updateMany({[field]: []}, {$unset: {[field]: true}});
        console.log("  -> Result:", res);
      }
    }

    // *********** Transform Slate's JSON structure into HTML
    for (const {collection, fields} of fieldsToMigrate) {
      // Get all the elements of the collection
      const projection = Object.fromEntries(fields.map((field) => [field, 1]));
      const allElements = await db.collection(collection).find().project(projection).toArray();

      for (const field of fields) {
        console.log("For field", field, "in collection", collection, projection);

        // For each field, and each element
        for (const el of allElements) {
          try {
            // If the field does not exist, or if it's a string, don't do anything
            if (!el[field] || typeof el[field] === "string") continue;

            // Serialize the field
            const result = serialize(el[field]);

            // If the result returns a HTML string, set it in the DB. If it returns undefined,
            // then unset the field in the DB.
            await db
              .collection(collection)
              .updateOne(
                {_id: el._id},
                result ? {$set: {[field]: result}} : {$unset: {[field]: 1}}
              );
          } catch (e) {
            console.log(
              "\n================= ERROR: =================\n",
              collection,
              field,
              el._id,
              JSON.stringify(el[field], null, 2)
            );
            console.error(e);
            throw e;
          }
        }
      }
    }
  },

  async down(db, client) {
    // *********** Remove all stuff that is an empty string ""
    for (const {collection, fields} of fieldsToMigrate) {
      for (const field of fields) {
        console.log("Removing empty strings for field", field, "in collection", collection);
        const res = await db
          .collection(collection)
          .updateMany({[field]: ""}, {$unset: {[field]: true}});
        console.log("  -> Result:", res);
      }
    }
  },
};
