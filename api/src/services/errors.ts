import * as Sentry from "@sentry/node";
import {NextFunction, Request, Response, RequestHandler} from "express";
import config from "../config/config";
import {logger} from "./logger";

export function initSentry(app: any) {
  Sentry.init({
    debug: true,
    environment: config.appMode,
    dsn: config.sentryDsn,
  });

  app.use(Sentry.Handlers.requestHandler() as RequestHandler);

  // TracingHandler creates a trace for every incoming request
  // app.use(Sentry.Handlers.tracingHandler());
}

// Router function wrapper. We encapsulate them into that so the errors created can be redirected in the
// middleware pipeline, and be captured by Sentry
export function handleErrors(func: (req: Request, res: Response, next?: NextFunction) => void) {
  return async function (req: Request, res: Response, next: NextFunction) {
    try {
      await func(req, res);
    } catch (e) {
      next(e);
    }
  };
}

export function globalErrorHandler(
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
): void {
  // Check if it is a known error, and return if that's the case
  let status, message;

  // Dirty stuff to extract keys that are in conflict
  const getConflictingResourcesFromError = (err: Error): string =>
    /dup key:\s\{(.*)\}/
      .exec(err.message)[1] // Get the JSON-like string
      .match(/(,?\s.*?: )/g) // Match all the fields
      .filter((el) => !/(project|deletedAt)/.exec(el)) // Remove project and deletedAt attributes
      .map((el) => el.replace(/(:|,| )/g, "")) // Clean
      .join(", "); // Join

  // Mongoose version conflict
  if (err.name === "VersionError") {
    status = 409;
  }
  // Resource already exists
  if (/duplicate key error/.exec(err.message)) {
    status = 403;
    message = `Un autre objet a déjà les mêmes champs suivants : ${getConflictingResourcesFromError(
      err
    )}. Deux objets ne peuvent pas avoir ces champs identiques.`;
  }

  status = status || 500; // If status hasn't been set, make it 500

  logger.error(err); // Display the trace in the logs

  try {
    // Send the error to Sentry with all the necessary information
    Sentry.captureException(err, {
      extra: {
        "a - URL": `${req.method} ${req.url}`,
        "b - Origin": req.headers?.origin,
        "c - User": req.user,
        "d - authenticatedUser":
          req.user?._id !== req.authenticatedUser?._id ? req.authenticatedUser : "same as user",
        "e - Request params": JSON.stringify(req.params, null, 2),
        "f - Request query": JSON.stringify(req.query, null, 2),
        "g - Request body": JSON.stringify(req.body, null, 2),
        "h - Full request": req,
      },
    });
  } catch (e) {
    logger.warn("Problem with Sentry. Error not reported.");
  }
  return res.status(status).end(message); // Return status with the Sentry eventId
}
