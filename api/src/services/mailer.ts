import {Request, Response} from "express";
import {createTransport} from "nodemailer";
import {logger} from "./logger";
import config from "../config/config";
import {compileHtmlTemplate} from "./html-and-pdf";

const md = require("markdown").markdown;

const SMTP_CONFIG =
  config.smtp.server && config.smtp.address && config.smtp.pass
    ? `smtps://${config.smtp.address}:${config.smtp.pass}@${config.smtp.server}/?pool=true`
    : undefined;

const transporter = SMTP_CONFIG && createTransport(SMTP_CONFIG);

const headerImageUrl = `${config.urls.api}/assets/logos/logo-colors-with-blue-text.png`;

class Mailer {
  async sendMail(req: Request, res: Response, emailKey: string, emailInfos: any): Promise<void> {
    const emailParams = {
      from: `${config.instanceName} <${config.smtp.address || "fake@smtp.test"}>`, // Fallback on fake@smtp.test if there is no proper config
      subject: res.locals.t(`emails.${emailKey}.subject`, emailInfos),
      ...emailInfos,
    };

    const emailTexts = {
      html: await compileHtmlTemplate("emails", {
        headerImageUrl,
        emailBody: md.toHTML(res.locals.t(`emails.${emailKey}.body`, emailInfos)),
      }),
      text: res.locals.t(`emails.${emailKey}.body`, emailInfos),
    };

    if (SMTP_CONFIG && config.env === "production") {
      // If we are not in test mode, and that we have a correct SMTP config, use the real SMTP provider
      await transporter.sendMail({...emailParams, ...emailTexts});
      logger.info(`Email '${emailKey}' sent:`, emailParams);
    } else {
      // If we are in test mode, or if we don't have a correct SMTP config, use the fake one
      logger.info(`[FAKE SMTP] Fake email '${emailKey}' sent:`, {
        ...emailParams,
        text: emailTexts.text,
        html: emailTexts.html,
      });
      logger.warn("[FAKE SMTP] No valid SMTP config is given, so emails are not sent for real");
    }
  }
}

export default new Mailer();
