import {transports, format, createLogger} from "winston";
import * as DailyRotateFile from "winston-daily-rotate-file";
import expressWinston = require("express-winston");
import {join} from "path";
import {UserD} from "../models/users";
import config from "../config/config";
const {combine, timestamp, metadata, printf, colorize, uncolorize, errors, splat} = format;

// The logs formatting function
const logsFormatter = printf(
  ({
    level,
    message,
    metadata: {timestamp, requestMeta, stack},
    [Symbol.for("splat") as any]: splat,
  }) => {
    const origin = requestMeta?.originType ? requestMeta.originType : "";
    const userIdentity = requestMeta?.userIdentity ? ` - ${requestMeta.userIdentity}` : "";
    const splatString = splat ? JSON.stringify(splat, null, 2) : "";
    return (
      `${timestamp} [${level}]\t` +
      (stack
        ? `/// ERROR /// ${message} \n${stack}`
        : `${message}${origin}${userIdentity} ${splatString}`)
    );
  }
);

// The Winston logger. Logs to both console and file in the /logs folder
const logger = createLogger({
  // Format timestamp, add metadata, and align all logs
  format: combine(
    timestamp({format: "DD/MM/YY HH:mm:ss"}), // Pretty format the timestamp
    errors({stack: true}), // Get errors stacktrace in they occur
    metadata() // Get any metadata available
    // splat()
  ),

  transports: [
    // Colorize console output and only log above "info" flag
    new transports.Console({
      level: "info",
      format: combine(colorize(), logsFormatter),
    }),
    // Don't colorize log files and log from the "debug" flag
    new DailyRotateFile({
      level: "debug",
      filename: join(__dirname, "../../logs/%DATE%_debug.log"), // Store in folder /api
      datePattern: "YYYY-MM-DD",
      format: combine(uncolorize(), logsFormatter),
    }),
  ],
});

const userLog = (user: UserD) => {
  let userMsg = "";
  if (user) {
    userMsg += `${user.email} (${user._id})`;
    const registration = user.registration;
    if (registration) {
      userMsg += " ~ ";
      registration.role && (userMsg += `${registration.role} `);
      userMsg += registration._id;
    }
  }

  return userMsg;
};

// Create the logger middleware to log the Express requests
const loggerMiddleware = expressWinston.logger({
  // Use the current winston logger
  winstonInstance: logger,

  // optional: control whether you want to log the meta data about the request (default to true)
  meta: true,

  // Dynamic log level depending on the request status code
  level: (req, {statusCode}) => {
    if (statusCode >= 500) return "error";
    if (statusCode >= 400) return "warn";
    if (statusCode >= 100) return "info";
  },

  dynamicMeta: (req) => {
    return {
      userIdentity:
        req.authenticatedUser?._id !== req.user?._id
          ? `${userLog(req.authenticatedUser)} >> connected as >> ${userLog(req.user)}`
          : userLog(req.user),
      originType:
        req.headers.origin && ` --> ${config.urlsNames[req.headers.origin] || req.headers.origin}`,
    };
  },

  // Custom message formatting
  msg: "{{res.statusCode}} {{req.method}} {{req.url}} {{res.responseTime}}ms",

  // Colorize parts of this message
  colorize: true,

  // Store the express-winston data in a custom field
  metaField: "requestMeta",

  // Ignore routes, like all the requests for assets
  ignoreRoute: function (req, res): boolean {
    return !!/assets/.exec(req.originalUrl || req.url);
  },
});

logger.on("error", function (err: any) {
  console.error("Logging error", err);
});

logger.info("Starting server...");

export {logger, loggerMiddleware};
