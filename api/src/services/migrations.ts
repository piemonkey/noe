// eslint-disable-next-line @typescript-eslint/no-var-requires
import {logger} from "./logger";

import {status, database, config as migrateMongoConfig} from "migrate-mongo";
import config from "../config/config";

// Get the config from the local config file, but change the database url with the appropriate one
const migrateMongoConfigOptions = require("./../../migrate-mongo-config");
migrateMongoConfigOptions.mongodb.url = config.mongoose.uri;

migrateMongoConfig.set(migrateMongoConfigOptions);

export const checkDatabaseMigrationStatus = async (): Promise<void> => {
  const {db, client} = await database.connect();
  try {
    const migrationStatus = await status(db);
    const pendingMigrations = migrationStatus.filter(
      (migration: any) => migration.appliedAt === "PENDING"
    );
    if (pendingMigrations.length > 0) {
      if (config.env === "production") {
        throw new Error(
          "Some database migrations are still pending. Please run them by running `npx mongo-migrate up`."
        );
      } else {
        logger.error(
          "WARNING: Some database migrations are still pending. Please run them by running `npx mongo-migrate up`."
        );
      }
    }
  } finally {
    client.close();
  }
};
