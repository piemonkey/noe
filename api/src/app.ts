import * as express from "express";
import * as passport from "passport";
import * as cors from "cors";
import mongoose = require("mongoose");
import helmet = require("helmet");
import expressValidator = require("express-validator");
import bodyParser = require("body-parser");
import cookieParser = require("cookie-parser");
import {join} from "path";
import {UserRouter} from "./controllers/user";
import {AuthRouter} from "./controllers/auth";
import {SessionRouter} from "./controllers/session";
import {TeamRouter} from "./controllers/team";
import {ProjectRouter} from "./controllers/project";
import {RegistrationRouter} from "./controllers/registration";
import {ActivityRouter} from "./controllers/activity";
import {StewardRouter} from "./controllers/steward";
import {PlaceRouter} from "./controllers/place";
import {CategoryRouter} from "./controllers/category";
import {PdfRouter} from "./controllers/pdf";
import {TicketingRouter} from "./controllers/ticketing";
import {ComputingRouter} from "./controllers/computing";
import {logger, loggerMiddleware} from "./services/logger";
import {globalErrorHandler, initSentry} from "./services/errors";
import {i18nMiddleware} from "./config/i18n";
import config from "./config/config";
import {checkDatabaseMigrationStatus} from "./services/migrations";

export const app = express();
app.disable("x-powered-by");

// Check database migration status
checkDatabaseMigrationStatus();

// Setup database
mongoose
  .connect(config.mongoose.uri, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .catch((error) => {
    logger.error(
      `MongoDB connection error to ${config.mongoose.uri}. Please make sure MongoDB is running.`,
      error
    );
    process.exit();
  });

// Initialize Sentry
if (config.env === "production") initSentry(app);

// Helmet: protection against well known vulnerabilities
app.use(helmet());

// Cross origin ressources sharing
app.use(cors({credentials: true, origin: true}));

// Parsing options
app.use(bodyParser.json({limit: "15mb"}));
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

// Request validation
app.use(expressValidator());

// Authentication
app.use(passport.initialize());

// Request logger
app.use(loggerMiddleware);

// // Internationalization
app.use(i18nMiddleware);

const router = express.Router();

// Then redirect to the appropriate sub-routers
app.use(router);
router.use("/users", UserRouter);
router.use("/projects", ProjectRouter);
router.use("/auth", AuthRouter);
router.use("/computing", ComputingRouter);
ProjectRouter.use("/:projectId/registrations", RegistrationRouter);
ProjectRouter.use("/:projectId/categories", CategoryRouter);
ProjectRouter.use("/:projectId/places", PlaceRouter);
ProjectRouter.use("/:projectId/stewards", StewardRouter);
ProjectRouter.use("/:projectId/activities", ActivityRouter);
ProjectRouter.use("/:projectId/sessions", SessionRouter);
ProjectRouter.use("/:projectId/teams", TeamRouter);
ProjectRouter.use("/:projectId/pdf", PdfRouter);
ProjectRouter.use("/:projectId/ticketing", TicketingRouter);

// Static assets
app.use("/assets", express.static(join("src", "assets")));

// All the rest: 404
app.get("*", (req: any, res: any) => {
  res.status(404).end();
});

// Global error Handler
app.use(globalErrorHandler);
