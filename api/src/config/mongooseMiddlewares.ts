import {Schema} from "mongoose";
const diffHistory = require("mongoose-diff-history/diffHistory");

export function excludeInFindQueriesDeletedAt(): void {
  this._conditions?.searchAlsoInDeletedElements
    ? delete this._conditions?.searchAlsoInDeletedElements // Don't do anything, just remove the fake condition
    : this.where({deletedAt: {$exists: false}}); // Filter the deleted elements
}

const typesFindQueryMiddleware = [
  "count",
  "countDocuments",
  "find",
  "findOne",
  "findOneAndDelete",
  "findOneAndRemove",
  "findOneAndUpdate",
  "update",
  "updateOne",
  "updateMany",
  "distinct",
];

export function softDelete(schema: Schema<any>): void {
  typesFindQueryMiddleware.forEach((type) => {
    schema.pre(type, excludeInFindQueriesDeletedAt);
  });
}

export function preventDuplicatesInProject(
  schema: Schema<any>,
  additionalUniqueKeys?: any[] // Give any primary key combination we want to be set
): void {
  [{_id: 1}, ...(additionalUniqueKeys || [])].forEach((fields) =>
    schema.index({...fields, project: 1, deletedAt: 1}, {unique: true})
  );
}

export function addDiffHistory(schema: Schema<any>, omit: any[] = []) {
  schema.plugin(diffHistory.plugin, {omit: ["createdAt", "updatedAt", ...omit]});
}

export function indexByIdAndProject(schema: Schema<any>) {
  schema.index({_id: 1, project: 1}, {unique: true});
}
