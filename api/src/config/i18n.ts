import {logger} from "../services/logger";
import config from "./config";

const i18n = require("i18n-node-yaml")({
  debug: config.env !== "production",
  translationFolder: __dirname + "/../../locales",
  locales: ["fr_FR"],
  defaultLocale: "fr_FR",
  queryParameters: ["locale"],
});

i18n.ready.catch((error: any) => {
  logger.error("Failed loading translations", error);
});

const i18nMiddleware = i18n.middleware;

export {i18nMiddleware};
