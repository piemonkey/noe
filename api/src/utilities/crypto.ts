import config from "../config/config";

const crypto = require("crypto");

const algorithm = "aes-256-ctr";
const secretKey = config.encryptionKey;

export const encrypt = (text: string) => {
  const iv = crypto.randomBytes(16);

  const cipher = crypto.createCipheriv(algorithm, secretKey, iv);

  const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);

  return {
    iv: iv.toString("hex"),
    content: encrypted.toString("hex"),
  };
};

export const decrypt = (hash: {content: string; iv: string}) => {
  const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(hash.iv, "hex"));

  const decrpyted = Buffer.concat([
    decipher.update(Buffer.from(hash.content, "hex")),
    decipher.final(),
  ]);

  return decrpyted.toString();
};

export const MongoEncrypted = {
  type: Object,
  set: (v: string) => (v?.length > 0 ? encrypt(v) : undefined),
  get: (v: any) => (v?.content && v?.iv ? decrypt(v) : undefined),
};
