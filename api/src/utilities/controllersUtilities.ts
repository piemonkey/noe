import {Model, QueryWithHelpers, Types} from "mongoose";
import {RequestHandler, Response} from "express";
import {UserD} from "../models/users";
import {Registration} from "../models/registrations";
const diffHistory = require("mongoose-diff-history/diffHistory");

export const getDependenciesList = async (
  entityClass: Model<any>,
  conditions: any,
  getEntityName: any = (e: any) => e?.name || e._id
) => {
  const dependentEntities = await entityClass.find(conditions);

  if (dependentEntities.length > 0) {
    // Need to wait for promises to be resolved if getEntityName is async
    const entityNames = await Promise.all(dependentEntities.map(getEntityName));
    return entityNames.join(", ");
  } else {
    return;
  }
};

export const isValidSlug = (id: string) =>
  !/(new|clone|groupedit|projects|auth|users)/.exec(id) && id?.length > 1;

export const isValidObjectId = (id: string | number | Types.ObjectId): boolean =>
  Types.ObjectId.isValid(id);

/**
 * Update a one to many relationship whose link is stored in the "to many" elements.
 * For example: the "many sessions / one team" link is stored in the sessions with field team.
 * To update the links, you can call updateOneToManyRelationship(team, sessionsIds, Session, "team").
 * @param elementToRelateTo The base element that is linked/unlinked with all the others
 * @param manyOtherElementsIds The other elements ids
 * @param Entity The type of the other elements in `manyOtherElementsIds`
 * @param fieldName The field name of the base element, in the schema definition of `Entity`
 */
export const updateOneToManyRelationship = async (
  elementToRelateTo: any,
  manyOtherElementsIds: string[],
  Entity: Model<any>,
  fieldName: string,
  additionalFieldsToAdd?: any
) => {
  // -- Create selections --
  // Select and update elements to link with the element
  const entitiesToLinkConditions = {
    [fieldName]: {$not: {$in: elementToRelateTo}},
    _id: manyOtherElementsIds,
  };
  // Select and update entities to unlink from the element
  const entitiesToUnlinkConditions = {
    [fieldName]: elementToRelateTo,
    _id: {$not: {$in: manyOtherElementsIds}},
  };

  // -- Get all the ids of the entities to update (before they get updated, after the update we will not be able to find them anymore) --
  const entitiesToUpdate = await Entity.find(
    {$or: [entitiesToLinkConditions, entitiesToUnlinkConditions]},
    "_id"
  ).lean();
  const entitiesToUpdateIds = entitiesToUpdate.map((s) => s._id);

  // -- Make the updates --
  await Entity.updateMany(entitiesToLinkConditions, {
    [fieldName]: elementToRelateTo,
    ...additionalFieldsToAdd,
  });
  await Entity.updateMany(entitiesToUnlinkConditions, {
    $unset: {[fieldName]: true},
  });

  return entitiesToUpdateIds;
};

export const updateManyToManyRelationship = async (
  elementToRelateTo: any,
  manyOtherElementsIds: string[],
  Entity: Model<any>,
  fieldName: string,
  nestedArrayName?: string,
  additionalFieldsToAdd?: any
) => {
  const queryField = nestedArrayName ? `${nestedArrayName}.${fieldName}` : fieldName;

  // Select and update elements to link with the element
  const entitiesToLinkConditions = {
    [queryField]: {$not: {$in: elementToRelateTo}},
    _id: manyOtherElementsIds,
  };
  // Select and update entities to unlink from the element
  const entitiesToUnlinkConditions = {
    [queryField]: elementToRelateTo,
    _id: {$not: {$in: manyOtherElementsIds}},
  };

  // -- Get all the ids of the entities to update (before they get updated, after the update we will not be able to find them anymore) --
  const entitiesToUpdate = await Entity.find(
    {$or: [entitiesToLinkConditions, entitiesToUnlinkConditions]},
    "_id"
  );
  const entitiesToUpdateIds = entitiesToUpdate.map((s) => s._id);

  // -- Make the updates --
  await Entity.updateMany(entitiesToLinkConditions, {
    $push: {[nestedArrayName]: [{[fieldName]: elementToRelateTo, ...additionalFieldsToAdd}]},
  });
  await Entity.updateMany(entitiesToUnlinkConditions, {
    $pull: {[nestedArrayName]: {[fieldName]: elementToRelateTo}},
  });

  return entitiesToUpdateIds;
};

export const listAllEntities = async (query: any, sortOptions: any, res: Response) => {
  const elements = await query.collation({locale: "fr", strength: 1}).sort(sortOptions).lean();
  return res.json(elements);
};

export const readEntity = async <EntityD>(
  id: string,
  projectId: string,
  res: Response,
  queryPromise: (id: string, projectId?: string) => any,
  additionalActions?: (entity: EntityD) => void
) => {
  let entity;

  // If there are both a projectId and an id: check that projectId is a slug, at least + check if the id is an objectid
  // If there is only an id, check if it is a slug only (cause it can be an id for a project.
  if (projectId ? isValidObjectId(id) && isValidSlug(projectId) : isValidSlug(id)) {
    entity = await queryPromise(id, projectId);
    if (!entity) return res.status(404).end();

    if (additionalActions) {
      const finalReturnedEntity = await additionalActions(entity);
      return res.status(200).json(finalReturnedEntity);
    } else {
      return res.status(200).json(entity);
    }
  } else {
    return res.status(404).end();
  }
};

export const createEntity = async <EntityD>(
  Entity: Model<EntityD>,
  args: any,
  res: Response,
  additionalActions?: (entity: EntityD) => any
) => {
  let entity = await Entity.create(args);
  if (additionalActions) entity = await additionalActions(entity);
  return res.status(201).json(entity);
};

export const updateEntity = async <EntityD>(
  id: string,
  projectId: string,
  Entity: Model<EntityD>,
  args: any,
  updatedByUser: UserD,
  res: Response,
  customQuery?: (query: QueryWithHelpers<any, any>) => Promise<EntityD>,
  additionalActions?: (entity: any) => Promise<void>
) => {
  const query = Entity.findOneAndUpdate({_id: id, project: projectId} as any, args, {
    new: true,
    __user: updatedByUser._id,
  } as any);
  const entity = customQuery ? await customQuery(query) : await query.lean();
  additionalActions && (await additionalActions(entity));
  return res.status(201).json(entity);
};

export const deleteEntity = async (
  id: string,
  Entity: Model<any>,
  updatedByUser: UserD,
  res: Response,
  additionalActions?: (entity?: any) => void,
  returnValue?: any,
  hardDelete = false // If true, don't put deletedAt, instead erase the record *for real* in the database
) => {
  const entity = hardDelete
    ? await Entity.findById(id)
    : await Entity.findByIdAndUpdate(
        id,
        {deletedAt: new Date()} as any,
        {new: true, __user: updatedByUser._id} as any
      );
  if (!entity) return res.status(404).end(); // Not found
  hardDelete && (await entity.remove());
  additionalActions && (await additionalActions(entity));
  return res.status(200).send(returnValue);
};

export const readEntityHistory = async (
  id: string,
  projectId: string,
  entityClassName: string,
  res: Response
) => {
  const history = await diffHistory.getDiffs(entityClassName, id, {sort: "-createdAt"});
  const registrations = await Registration.find(
    {user: history.map((h: any) => h.user), project: projectId} as any,
    "user _id"
  )
    .populate("user", "firstName lastName")
    .lean();
  return res.send(
    history.map((h: any) => ({
      date: h.createdAt,
      diff: h.diff,
      registration: registrations.find((r) => r.user._id.toString() === h.user?.toString()),
    }))
  );
};

export const lightSelection = {
  activity: {
    name: 1,
    summary: 1,
    volunteeringCoefficient: 1,
    maxNumberOfParticipants: 1,
    description: 1,
    tags: 1,
    secondaryCategories: 1,
  },
  team: {name: 1},
  place: {name: 1, maxNumberOfParticipants: 1, availabilitySlots: 1}, // Avail slots for agenda
  steward: {firstName: 1, lastName: 1, availabilitySlots: 1}, // Avail slots for agenda
  category: {color: 1, name: 1},
  user: {email: 1, firstName: 1, lastName: 1},
  project: {name: 1, start: 1, end: 1, slug: 1},
};

// Replace nested objects content with only their ID, so we don't save nested objects information
// that is not correct anymore (if somebody modified that in between)
export const removeNestedObjectsContent =
  (...args: string[]): RequestHandler =>
  (req, res, next) => {
    for (const arg of args) {
      if (req.body[arg]?._id) {
        // If this is a simple nested object, replace it with the id
        req.body[arg] = req.body[arg]._id;
      } else if (Array.isArray(req.body[arg])) {
        // If this is an array of nested objects, replace each object in the array with its ID, if it has one
        req.body[arg] = req.body[arg].map((el: any) => (el._id ? el._id : el));
      }
    }
    next();
  };

export async function computeFunctionForAllElements<T>(
  elements: Array<T>,
  functionToCompute: (el: T) => Promise<void>
): Promise<void> {
  // Gain of the async version is around 20% to 40% compared to synchronous version

  // Create all the promises
  const arrayOfPromises = elements.map(functionToCompute);

  // // Resolve them all in the same time
  await Promise.all(arrayOfPromises);
}
