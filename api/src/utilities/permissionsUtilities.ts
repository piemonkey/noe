import {NextFunction, RequestHandler} from "express";
import {UserD} from "../models/users";
import {validationResult} from "express-validator/check";
import {logger} from "../services/logger";

/********************
 * PERMISSIONS
 *******************/

export type UserPermission = {
  challenge: (user: UserD, request?: any) => boolean;
  errorReason: string;
};

export const ROLES = {
  ADMIN: "admin",
  CONTRIBUTOR: "contrib",
  GUEST: "guest",
};

export const ROLES_LABELS = {
  admin: "Administrateur⋅ice",
  contrib: "Contributeur⋅ice",
  guest: "Invité⋅e",
};

/**
 * Permit function wrapper
 * @param permission the permission object
 * @param excludeScope an optional URL string or regexp that excludes a scope from the check
 */
export const permit = (
  permission: UserPermission,
  excludeScope?: string | RegExp
): RequestHandler => {
  return async (req, res, next) => {
    // If the user is a super admin, it has all rights. Let it pass.
    if (req.authenticatedUser.superAdmin) return next();

    // If no excludeScope URL is given, don't check. Else, if the origin is different from the scope, let it pass.
    if (excludeScope && req.headers.origin?.match(excludeScope)) return next();

    // Send 404 if no project registration given at all
    if (!req.authenticatedUser.registration) return res.status(404).end();

    // If the challenge fails, the user lacks sufficient rights: send a 403 error with the error reason
    if (!permission.challenge(req.authenticatedUser, req)) {
      return res.status(403).send(permission.errorReason);
    }

    // If everything passes, we're all good
    next();
  };
};

/**
 * User permissions
 */

// Allow only super admins to do this
export const nobodyExceptSuperAdmins: UserPermission = {
  challenge: (user) => false,
  errorReason: "Cette action n'est pas disponible.",
};

// Allow only admins on the current project
export const projectAdmins: UserPermission = {
  challenge: (user) => user.registration?.role === ROLES.ADMIN,
  errorReason: "Seul⋅e un⋅e administrateur⋅ice peut effectuer cette action.",
};

// Allow only people having at least a contribution role on the current project
export const projectContributors: UserPermission = {
  challenge: (user) => !![ROLES.ADMIN, ROLES.CONTRIBUTOR].includes(user.registration?.role),
  errorReason:
    "Seul⋅e un⋅e administrateur⋅ice ou un⋅e contributeur⋅ice peut effectuer cette action",
};

// Allow only people having at least a guest role on the current project
export const projectGuests: UserPermission = {
  challenge: (user) => !!Object.values(ROLES).includes(user.registration?.role),
  errorReason:
    "Seul⋅e un⋅e administrateur⋅ice, un⋅e contributeur⋅ice ou un⋅e invité⋅e peut effectuer cette action",
};

/********************
 * DATA VALIDATION
 *******************/

export const emailNormalizationParams = {
  gmail_remove_dots: false,
  gmail_remove_subaddress: false,
  outlookdotcom_remove_subaddress: false,
  yahoo_remove_subaddress: false,
  icloud_remove_subaddress: false,
};

function cancelRequestIfValidationErrors(req: any, res: any, next: NextFunction) {
  const errors = validationResult(req);
  if (errors.isEmpty()) next();
  else return res.status(400).json({errors: errors.array()});
}

// Filter fields if they are not wanted
export function filterBody(args: string[]): RequestHandler {
  const allowedArgs = [...args, "__v"];
  return (req, res, next) => {
    const notWanted = Object.keys(req.body).filter((key) => !allowedArgs.includes(key));
    notWanted.forEach((key) => delete req.body[key]);
    notWanted.length && logger.debug(`Fields not wanted : ${notWanted.join(", ")}`);
    next();
  };
}

// Validate the body with some express validator rules
export function validateAndSanitizeBody(...args: any[]): RequestHandler[] {
  return [
    ...args,
    // Cancel the request if some errors are found
    cancelRequestIfValidationErrors,
  ];
}
