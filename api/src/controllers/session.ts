import {Request, Response, Router} from "express";
import {userLoggedIn} from "../config/passport";
import moment = require("moment");
import {
  readEntity,
  lightSelection,
  removeNestedObjectsContent,
  deleteEntity,
  updateManyToManyRelationship,
  computeFunctionForAllElements,
  readEntityHistory,
} from "../utilities/controllersUtilities";
import {handleErrors} from "../services/errors";
import {
  projectAdmins,
  permit,
  projectContributors,
  filterBody,
  projectGuests,
} from "../utilities/permissionsUtilities";

import {StewardD} from "../models/stewards";
import {Project, ProjectD} from "../models/projects";
import {
  allowedBodyArgs,
  getAllSessionsSlotsInProject,
  schemaDescription,
  Session,
  SessionD,
} from "../models/sessions";
import {Registration, RegistrationD} from "../models/registrations";
import {Slot, SlotD} from "../models/slots";
import config from "../config/config";

export const SessionRouter = Router({mergeParams: true});

const readSessionQuery = (id: string, projectId: string) =>
  Session.findOne({_id: id, project: projectId} as any).populate([
    {path: "stewards"},
    {path: "places"},
    {path: "activity", populate: ["stewards", "places", "category"]},
    {path: "slots", populate: ["stewards", "places"]},
    {path: "team", select: lightSelection.team},
  ]);

const readSessionAdditionalActions =
  (registration: RegistrationD, isRequestedFromInscriptionFrontend = true) =>
  async (session: any) => {
    // User is allowed to see participants if...
    const canSendAllParticipantsInfo =
      registration && // a registration exists...
      (registration.role || // ... and it is either an orga with a role
        (registration.steward && // ... or a steward on the current session
          session.stewards.find(
            (steward: StewardD) => steward._id.toString() === registration.steward.toString()
          )));

    // Populate the participants, and send the whole data only if we're allowed to
    await session.populateParticipants(canSendAllParticipantsInfo);

    const sessionData = session.toObject();
    if (isRequestedFromInscriptionFrontend) {
      await session.computeSameTimeSessions(registration);
      Object.assign(sessionData, {
        subscribed: registration
          ? registration.sessionsSubscriptions
              .map((ss) => ss.session)
              .includes(session._id.toString())
          : false,
        isSteward: isStewardForSession(session, registration),
        sameTimeSessions: session.sameTimeSessions,
      });
    }

    if (canSendAllParticipantsInfo) sessionData.participants = session.participants;
    sessionData.numberParticipants = session.numberParticipants;
    return sessionData;
  };

const isStewardForSession = (session: SessionD, currentRegistration: RegistrationD) => {
  if (!currentRegistration?.steward) return false;

  const registrationStewardId = currentRegistration.steward._id.toString();
  return (
    session.stewards.find((steward) => steward._id.toString() === registrationStewardId) ||
    session.slots.find(
      (slot) =>
        !slot.stewardsSessionSynchro &&
        slot.stewards.find((steward) => steward._id.toString() === registrationStewardId)
    )
  );
};

const inconsistenciesForEntities: any = (
  sessionsOfProject: SessionD[],
  entityName: string,
  arrayOfEntitiesName: string,
  slotTested: SlotD
) => {
  const inconsistenciesFound = [];

  const entitiesTested = slotTested?.[arrayOfEntitiesName];
  if (entitiesTested?.length > 0) {
    const inconsistentEntities: any[] = [];
    entitiesTested.forEach((entity: any) => {
      // If we find one, it means the entity has an availability during this time, so that's great
      const anAvailabilityExists = entity.availabilitySlots?.find(
        (a: any) =>
          moment(a.start) <= moment(slotTested.start) && moment(a.end) >= moment(slotTested.end)
      );
      if (!anAvailabilityExists) {
        // If the entity is unavailable, that is an inconsistency
        inconsistentEntities.push(entity);
      }
    });
    if (inconsistentEntities.length > 0) {
      inconsistenciesFound.push({
        type: "availabilitiesOverlap",
        entity: entityName,
        entitiesInvolved: inconsistentEntities,
      });
    }

    // Check if entity is still available
    const alreadyUsedEntities: any[] = [];
    sessionsOfProject.forEach((session: SessionD) => {
      session.slots.forEach((existingSlot: SlotD) => {
        if (existingSlot._id != slotTested._id) {
          const existingEntities = existingSlot[arrayOfEntitiesName].map(
            (entity: any) => entity._id
          );
          if (
            moment(slotTested.start) < moment(existingSlot.end) &&
            moment(slotTested.end) > moment(existingSlot.start)
          ) {
            const overlappingEntities = entitiesTested.filter((entityTested: any) =>
              existingEntities.includes(entityTested._id)
            );
            alreadyUsedEntities.push(...overlappingEntities);
          }
        }
      });
    });
    if (alreadyUsedEntities.length > 0) {
      inconsistenciesFound.push({
        type: "alreadyUsedEntity",
        entity: entityName,
        entitiesInvolved: alreadyUsedEntities,
      });
    }
  }
  return inconsistenciesFound;
};

/**
 * Get the session schema definition
 * GET /schema
 */
SessionRouter.get(
  "/schema",
  handleErrors(async function getSchema(req: Request, res: Response) {
    return res.send(schemaDescription);
  })
);

/**
 * List all the sessions in the project
 * GET /
 * POST /selectiveLoad
 *
 *   Query params:
 *     - type: "subscribed" | "all"
 *     - fromDate: number (date in Unix format)
 *   Body (for POST method only):
 *     - alreadyLoaded: list of sessions ids separated with a comma
 *
 *   Returns:
 *     - list: list of sessions
 *     - allLoaded: boolean - tells if some sessions are still not loaded for this user

 */
SessionRouter.get("/", userLoggedIn, handleErrors(listAll));
SessionRouter.post("/selectiveLoad", userLoggedIn, handleErrors(listAll));
/** ************************************************************* **/
async function listAll(req: Request, res: Response) {
  const projectId = req.params.projectId;
  const registration = req.user.registration;
  const loadParticipantsData = req.query.loadParticipantsData === "true";
  const isRequestedFromInscriptionFrontend = req.headers.origin === config.urls.inscriptionFront;

  // Filtering options
  /** **** Support for deprecated requests using the body params **** **/
  const type: string = req.query?.type?.toString() || req.body?.type;
  const fromDateUnix: number = parseInt(req.query?.fromDate?.toString()) || req.body?.fromDate;
  const alreadyLoaded: Array<string> =
    req.query?.alreadyLoaded?.toString().split(",") || req.body?.alreadyLoaded;

  // Build the base filter
  let filterConditions: any = {project: projectId, _id: {$not: {$in: alreadyLoaded}}};

  // Only fetch from today's date, we'll see later if we need to load the rest
  if (fromDateUnix) {
    filterConditions = {$and: [filterConditions, {end: {$gte: new Date(fromDateUnix)}}]};
  }

  // Only fetch the type of stuff we were asked
  if (type === "subscribed") {
    filterConditions = {
      $and: [
        filterConditions,
        {
          $or: [
            {_id: registration?.sessionsSubscriptions.map((ss) => ss.session)},
            {stewards: registration?.steward},
            // TODO We can't do that right now because it's way too expensive, but we need to improve this.
            //  see issue https://gitlab.com/alternatiba/noe/-/issues/398
            //  {slots: {$elemMatch: {stewardsSessionSynchro: false, stewards: registration?.steward}}},
          ],
        },
      ],
    };
  }

  // Get the sessions from the database
  const sessions: SessionD[] = await Session.find(
    filterConditions,
    {notes: 0, isScheduleFrozen: 0, __v: 0, createdAt: 0, updatedAt: 0, id: 0, project: 0} // No need for those things in the sessions list
  )
    .sort({start: 1})
    .populate([
      {path: "stewards", select: "firstName lastName", options: {lean: true}},
      {path: "places", select: "name maxNumberOfParticipants", options: {lean: true}},
      {
        path: "slots",
        options: {lean: true},
        populate: [
          {path: "stewards", select: "firstName lastName"},
          {path: "places", select: "name maxNumberOfParticipants"},
        ],
      },
      {
        path: "activity",
        options: {lean: true},
        populate: [{path: "category", select: lightSelection.category}],
        select: lightSelection.activity,
      },
      {path: "team", select: lightSelection.team, options: {lean: true}},
    ]);

  // Compute all the expensive values needed asynchronously so we gain (a lot of) time
  await computeFunctionForAllElements(sessions, (session: any) =>
    loadParticipantsData // If we ask to
      ? session.populateParticipants(
          isStewardForSession(session, registration) || registration?.role
        )
      : session.populateParticipants(false)
  );

  // Only calculate same time sessions for inscription front, and not for participant front
  if (isRequestedFromInscriptionFrontend) {
    const allSessionsSlotsInProject = await getAllSessionsSlotsInProject(projectId.toString());
    await computeFunctionForAllElements(sessions, (session: any) =>
      session.computeSameTimeSessions(registration, allSessionsSlotsInProject)
    );
  }

  const sessionsList = sessions.map((session) => {
    const sessionData = session.toObject();
    if (isRequestedFromInscriptionFrontend) {
      Object.assign(sessionData, {
        subscribed:
          type === "subscribed"
            ? true
            : !!registration?.sessionsSubscriptions.map((ss) => ss.session).includes(session._id),
        isSteward: isStewardForSession(session, registration),
        sameTimeSessions: session.sameTimeSessions,
      });
    }
    sessionData.numberParticipants = session.numberParticipants;
    sessionData.participants = session.participants;
    return sessionData;
  });

  const allLoaded =
    alreadyLoaded !== undefined &&
    (await Session.countDocuments({project: projectId} as any)) ===
      (alreadyLoaded?.length || 0) + sessionsList.length;

  return res.status(200).json({list: sessionsList, allLoaded});
}

/**
 * Get a session
 * GET /:id
 */
SessionRouter.get(
  "/:id",
  userLoggedIn,
  handleErrors(async function read(req: Request, res: Response) {
    const registration = req.user.registration;
    const isRequestedFromInscriptionFrontend = req.headers.origin === config.urls.inscriptionFront;

    return await readEntity(
      req.params.id,
      req.params.projectId,
      res,
      readSessionQuery,
      readSessionAdditionalActions(registration, isRequestedFromInscriptionFrontend)
    );
  })
);

/**
 * Create a session
 * POST /
 */
SessionRouter.post(
  "/",
  userLoggedIn,
  permit(projectContributors),
  filterBody(allowedBodyArgs),
  removeNestedObjectsContent("stewards", "places", "activity", "team", "project"),
  handleErrors(async function create(req: Request, res: Response) {
    const {slots: slotsData, registrations: newSessionsSuscriptions, ...sessionBody} = req.body;
    let session = new Session(sessionBody);

    if (slotsData?.length > 0) {
      for (const querySlot of slotsData) {
        // If we are cloning, we don't want to save with the ids, but we want to create new slots
        if (querySlot._id) querySlot._id = undefined;
        const newSlot = await Slot.create(querySlot);
        session.slots.push(newSlot);
      }
    }

    session = await session.save();

    const registrationsToUpdateIds = newSessionsSuscriptions
      ? await updateManyToManyRelationship(
          session,
          newSessionsSuscriptions,
          Registration,
          "session",
          "sessionsSubscriptions",
          {subscribedBy: req.authenticatedUser, updatedAt: Date.now()}
        )
      : [];

    await session
      .populate([
        {path: "stewards"},
        {path: "places"},
        {path: "slots", populate: [{path: "stewards"}, {path: "places"}]},
        {path: "activity", populate: [{path: "category"}]},
        {path: "team", select: lightSelection.team},
      ])
      .execPopulate();

    const updatedSession = await readSessionAdditionalActions(
      req.authenticatedUser.registration,
      false
    )(session);
    return res.status(201).send({...updatedSession, registrationsToUpdateIds});
  })
);

/**
 * Modify a session
 * PATCH /:id
 */
SessionRouter.patch(
  "/:id",
  userLoggedIn,
  permit(projectContributors),
  filterBody(["_id", ...allowedBodyArgs]),
  removeNestedObjectsContent("stewards", "places", "activity", "team", "project"),
  handleErrors(async function update(req: Request, res: Response) {
    const {registrations: newSessionsSubscriptions, ...sessionBody} = req.body;

    // Modify or create the slots based on the new data. Only if they are given !
    if (sessionBody.slots !== undefined) {
      const session = await Session.findOne({
        _id: req.params.id,
        project: req.params.projectId,
      } as any);
      if (!session) return res.status(404).end();

      const newSlots = [];
      for (const querySlot of sessionBody.slots) {
        if (querySlot._id) {
          // If the slot already exists, update it
          newSlots.push(await Slot.findByIdAndUpdate(querySlot._id, querySlot, {new: true}));
        } else {
          // Else, create it
          newSlots.push(await Slot.create(querySlot));
        }
      }

      // Delete unused slots
      const slotsToDelete = session.slots.filter(
        (slotId) => !sessionBody.slots.find((newSlot: SlotD) => newSlot._id === slotId.toString())
      );
      await Slot.deleteMany({_id: slotsToDelete.map((s) => s._id)});

      sessionBody.slots = newSlots.map((slot) => slot._id);
    }

    // Save the session with the new data
    const newSession = await Session.findOneAndUpdate(
      {project: req.params.projectId, _id: req.params.id} as any,
      sessionBody,
      {new: true, __user: req.authenticatedUser._id, __reason: "update()"} as any
    ).populate([
      {path: "stewards"},
      {path: "places"},
      {path: "slots", populate: [{path: "stewards"}, {path: "places"}]},
      {path: "activity", populate: [{path: "category"}]},
      {path: "team", select: lightSelection.team},
    ]);

    const registrationsToUpdateIds = newSessionsSubscriptions
      ? await updateManyToManyRelationship(
          newSession,
          newSessionsSubscriptions,
          Registration,
          "session",
          "sessionsSubscriptions",
          {subscribedBy: req.authenticatedUser, updatedAt: Date.now()}
        )
      : [];

    const updatedSession = await readSessionAdditionalActions(
      req.authenticatedUser.registration,
      false
    )(newSession);
    return res.status(200).send({...updatedSession, registrationsToUpdateIds});
  })
);

/**
 * Delete a session
 * DELETE /:id
 */
SessionRouter.delete(
  "/:id",
  userLoggedIn,
  permit(projectAdmins),
  handleErrors(async function del(req: Request, res: Response) {
    const sessionId = req.params.id;

    const registrationsToUpdateIds = await updateManyToManyRelationship(
      sessionId,
      [],
      Registration,
      "session",
      "sessionsSubscriptions"
    );

    return await deleteEntity(
      sessionId,
      Session,
      req.authenticatedUser,
      res,
      async (session: SessionD) => await Slot.deleteMany({_id: {$in: session.slots}}),
      {registrationsToUpdateIds}
    );
  })
);

/**
 * Subscribe the user emitting the request to the given session
 * GET /:id/subscribe
 */
SessionRouter.get(
  "/:id/subscribe",
  userLoggedIn,
  handleErrors(async function subscribe(req: Request, res: Response) {
    const sessionId = req.params.id;
    const projectId = req.params.projectId;
    const registration = req.user.registration;
    const session = await readSessionQuery(sessionId, projectId);
    if (!session) return res.status(404).end();

    // Check if there is still some pace available in the session
    await session.populateParticipants(false);
    if (session.numberParticipants >= session.computedMaxNumberOfParticipants) {
      return res.status(405); // Someone already subscribed !
    }

    // Add the session to subscribed sessions
    registration.sessionsSubscriptions.push({
      session: sessionId,
      subscribedBy: req.authenticatedUser._id,
    } as any);
    await registration.save({__reason: "subscribe()", __user: req.authenticatedUser._id} as any);

    await registration.computeVoluntaryCounter();
    await registration.populate("user").execPopulate();

    const updatedSession = await readSessionAdditionalActions(registration)(session);
    return res.status(201).json({registration, updatedSession});
  })
);

/**
 * Unsubscribe the user emitting the request to the given session
 * GET /:id/unsubscribe
 */
SessionRouter.get(
  "/:id/unsubscribe",
  userLoggedIn,
  handleErrors(async function unsubscribe(req: Request, res: Response) {
    const sessionId = req.params.id;
    const projectId = req.params.projectId;
    const project = await Project.findByIdOrSlug(projectId);
    const registration = req.user.registration;
    const authenticatedRegistration = req.authenticatedUser.registration;

    if (project.blockSubscriptions && !authenticatedRegistration.role) return res.status(401).end();

    const session = await readSessionQuery(sessionId, projectId);
    if (!session) return res.status(404).end();

    // Remove the session from subscribed sessions
    registration.sessionsSubscriptions = registration.sessionsSubscriptions.filter(
      (ss) => ss.session.toString() !== sessionId
    );
    await registration.save({__reason: "unsubscribe()", __user: req.authenticatedUser._id} as any);

    await registration.computeVoluntaryCounter();
    await registration.populate("user").execPopulate();

    const updatedSession = await readSessionAdditionalActions(registration)(session);
    return res.status(201).json({registration, updatedSession});
  })
);

// TODO test check inconsistencies
/**
 * Check inconsistencies for the given session
 * POST /checkInconsistencies
 */
SessionRouter.post(
  "/checkInconsistencies",
  userLoggedIn,
  permit(projectContributors),
  handleErrors(async function checkInconsistencies(req: Request, res: Response) {
    const sessionTested = req.body;
    const projectId = req.params.projectId;

    const project: ProjectD = await Project.findByIdOrSlug(projectId);
    const sessionsOfProject: SessionD[] = await Session.find({project: projectId} as any, {
      slots: 1,
    })
      .populate({
        path: "slots",
        populate: [
          {path: "stewards", select: {firstName: 1, lastName: 1, availabilitySlots: 1}},
          {path: "places", select: {name: 1, availabilitySlots: 1}},
        ],
      })
      .lean();

    const inconsistencies: any[] = [];
    let slotIndex = 0;

    for (const slotTested of sessionTested.slots) {
      // const slotIndex=session.slots.findIndex(slot)
      const inconsistenciesSlot: any[] = [];

      const aProjectAvailabilityExists = project.availabilitySlots.find(
        (a) =>
          moment(a.start) <= moment(slotTested.start) && moment(a.end) >= moment(slotTested.end)
      );
      if (!aProjectAvailabilityExists) {
        inconsistenciesSlot.push({
          type: "availabilitiesOverlap",
          entity: "project",
          entitiesInvolved: [project],
        });
      }

      inconsistenciesSlot.push(
        ...inconsistenciesForEntities(sessionsOfProject, "place", "places", slotTested)
      );
      inconsistenciesSlot.push(
        ...inconsistenciesForEntities(sessionsOfProject, "steward", "stewards", slotTested)
      );

      if (inconsistenciesSlot.length > 0) {
        inconsistencies.push({
          session: sessionTested,
          slot: slotIndex + 1,
          inconsistencies: inconsistenciesSlot,
        });
      }
      slotIndex++;
    }

    return res.status(200).json(inconsistencies);
  })
);

/**
 * Get a session history
 * GET /:id/history
 */
SessionRouter.get(
  "/:id/history",
  userLoggedIn,
  permit(projectGuests),
  handleErrors(async function readHistory(req: Request, res: Response) {
    return await readEntityHistory(req.params.id, req.params.projectId, "Session", res);
  })
);
