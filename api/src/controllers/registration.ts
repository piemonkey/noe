import {Request, Response, Router} from "express";
import {allowedBodyArgs, Registration, RegistrationD} from "../models/registrations";
import {userLoggedIn} from "../config/passport";
import {handleErrors} from "../services/errors";
import {
  createEntity,
  deleteEntity,
  readEntity,
  updateEntity,
  readEntityHistory,
  removeNestedObjectsContent,
  lightSelection,
  getDependenciesList,
  isValidObjectId,
} from "../utilities/controllersUtilities";
import {
  emailNormalizationParams,
  filterBody,
  permit,
  projectAdmins,
  projectGuests,
  ROLES_LABELS,
  UserPermission,
  validateAndSanitizeBody,
} from "../utilities/permissionsUtilities";
import {User, UserD} from "../models/users";
import {body} from "express-validator/check";
import {randomBytes} from "crypto";
import Mailer from "../services/mailer";
import {Project} from "../models/projects";
import {personName} from "../utilities/utilities";
import config from "../config/config";

// If the user has no sufficient rights, clean the stuff that is not supposed to be visible to participants
const cleanOrgaStuffIfNeeded = (registration: any, authenticatedUser: UserD) => {
  if (!authenticatedUser?.registration?.role) {
    registration.notes = registration.tags = undefined;
  }
};

const userIsTheOnlyAdminOnCurrentProject = async (req: Request) =>
  (await Registration.countDocuments({
    project: req.params.projectId,
    role: "admin",
    user: {$ne: req.user._id},
  } as any)) === 0;

// Custom permission to let proprietaries of the registrtion have full rights, but also permit admins or contributors if necessary
const registrationProprietaryOr = (permission: UserPermission): UserPermission => ({
  challenge: (user, request) =>
    // Either satisfy the permission, or being the proprietary of the project registration
    permission.challenge(user) || user.registration._id === request.params.id,
  errorReason: permission.errorReason,
});

export const RegistrationRouter = Router({mergeParams: true});

/**
 * List all the registrations in the project
 * GET /                (when you want to load everything no matter what)
 * POST /selectiveLoad  (when you want to load only a part of the elements)
 */
RegistrationRouter.get("/", userLoggedIn, handleErrors(listAll));
RegistrationRouter.post("/selectiveLoad", userLoggedIn, handleErrors(listAll));
async function listAll(req: Request, res: Response) {
  const alreadyLoaded: Array<string> = req.body?.alreadyLoaded;
  const registrations = await Registration.find({
    project: req.params.projectId,
    _id: {$not: {$in: alreadyLoaded}},
  } as any)
    .populate([
      {path: "user", select: lightSelection.user, options: {lean: true}},
      {path: "steward", select: lightSelection.steward, options: {lean: true}},
      {path: "teamsSubscriptions.team", model: "Team", select: "name"},
    ])
    .sort({updatedAt: "desc"});

  await Promise.all(registrations.map((r) => r.computeVoluntaryCounter()));
  return res.status(200).json(registrations);
}

/**
 * Create a registration
 * POST /
 */
RegistrationRouter.post(
  "/",
  userLoggedIn,
  // TODO check if that works
  // permit(projectContributors),
  filterBody(allowedBodyArgs),
  removeNestedObjectsContent("project", "steward", "user"),
  handleErrors(async function create(req: Request, res: Response) {
    return await createEntity(Registration, req.body, res, async (registration) => {
      await registration
        .populate([
          {path: "user", select: lightSelection.user, options: {lean: true}},
          {path: "steward", select: lightSelection.steward, options: {lean: true}},
          {path: "teamsSubscriptions.team", model: "Team", select: "name"},
        ])
        .execPopulate();
      await registration.computeVoluntaryCounter();

      // If the role of a person is updated by another person, then the person whose role is updated receives an email.
      if (registration.role && req.authenticatedUser._id.toString() !== req.user._id.toString()) {
        const {name: projectName} = await Project.findByIdOrSlug(
          req.params.projectId,
          "name"
        ).lean();
        await Mailer.sendMail(req, res, "invitation.send_updated_role_notification", {
          fromPersonName: personName(req.authenticatedUser),
          to: registration.user.email,
          projectName,
          role: ROLES_LABELS[registration.role].toLowerCase(),
          link: `${process.env.REACT_APP_ORGA_FRONT_URL}/projects/${req.params.projectId}`,
        });
      }

      return registration;
    });
  })
);

/**
 * Get a registration
 * GET /:id
 */
RegistrationRouter.get(
  "/:id",
  userLoggedIn,
  // TODO check if that works
  // permit(registrationProprietaryOr(projectContributors)),
  handleErrors(async function read(req: Request, res: Response) {
    // Replace particular cases with real ID
    if (req.params.id === "current") req.params.id = req.user.registration?._id.toString();

    return await readEntity(req.params.id, req.params.projectId, res, async (id, projectId) => {
      const registration = await Registration.findById(id).populate([
        {path: "user", select: lightSelection.user, options: {lean: true}},
        {path: "steward", select: lightSelection.steward, options: {lean: true}},
        {path: "teamsSubscriptions.team", model: "Team", select: "name"},
      ]);
      await registration.computeVoluntaryCounter();
      cleanOrgaStuffIfNeeded(registration, req.authenticatedUser);
      return registration;
    });
  })
);

/**
 * Get a registration by invitationToken
 * GET /:id
 */
RegistrationRouter.get(
  "/byInvitationToken/:invitationToken",
  handleErrors(async function read(req: Request, res: Response) {
    // Replace particular cases with real ID
    const registration = await Registration.findOne(
      {invitationToken: req.params.invitationToken, project: req.params.projectId} as any,
      "role"
    )
      .populate([
        {path: "user", select: {_id: 1, ...lightSelection.user}},
        {path: "steward", select: "firstName"},
      ])
      .lean();
    return res.status(200).json(registration);
  })
);

/**
 * Modify a registration
 * PATCH /:id
 */
RegistrationRouter.patch(
  "/:id",
  userLoggedIn,
  // TODO make this work !
  // permit(registrationProprietaryOr(projectContributors)),
  filterBody(["_id", ...allowedBodyArgs]),
  removeNestedObjectsContent("project", "steward", "user"),
  validateAndSanitizeBody(body(["project", "user"]).isMongoId().optional()),
  handleErrors(async function update(req: Request, res: Response) {
    const {role, ...registrationBody} = req.body;

    // If the steward is given, and is not "null" or a valid mongoId, then return
    if (
      registrationBody.steward !== undefined &&
      registrationBody.steward !== null &&
      !isValidObjectId(registrationBody.steward)
    )
      return res.status(400).send("Invalid steward ID");

    cleanOrgaStuffIfNeeded(registrationBody, req.authenticatedUser);

    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Registration,
      registrationBody,
      req.authenticatedUser,
      res,
      async (query) => {
        const registration = await query.populate([
          {path: "user", select: lightSelection.user, options: {lean: true}},
          {path: "steward", select: lightSelection.steward, options: {lean: true}},
          {path: "teamsSubscriptions.team", model: "Team", select: "name"},
        ]);

        let needSave = false;

        // Clean DB if steward = null or role = null
        if (registration.steward === null) {
          registration.steward = undefined;
          needSave = true;
        }

        // if availability slots have been submitted, save the document to trigger the updates.
        if (registrationBody.availabilitySlots) needSave = true;

        // If the person is modifying the roles, make some checks
        if (role !== undefined && role !== registration.role) {
          // Prevent non admin user to change rights
          if (req.authenticatedUser.registration.role !== "admin") {
            return res
              .status(403)
              .send("Vous ne pouvez pas modifier de droits si vous n'êtes pas administrateur⋅ice.");
          }

          // If the person is admin and wants to not be admin anymore, check that
          // there are other admins on the project so there is always at least one admin on the project
          if (
            req.user.registration._id.toString() === req.params.id && // ...Admin user is modifying its own registration...
            req.body.role !== "admin" && // ...and wants to change its role to something else than "admin"...
            (await userIsTheOnlyAdminOnCurrentProject(req)) // ...but is the only admin on the project
          ) {
            return res
              .status(403)
              .send(
                "Vous êtes le⋅la seul⋅e administrateur⋅ice du projet. Désignez un⋅e autre" +
                  " administrateur⋅ice pour pouvoir modifier votre inscription."
              );
          }

          const projectName = (await Project.findByIdOrSlug(req.params.projectId)).name;
          const userIsModifyingItsOwnRegistration =
            req.authenticatedUser.registration._id.toString() === registration._id.toString();

          // Finally, update the role. When new roles are given, send an email (except if the user is modifying its own registration role)
          if (role === null) {
            // If it's null, it means the role has been removed.
            const oldRole = registration.role;
            registration.role = undefined;

            !userIsModifyingItsOwnRegistration &&
              (await Mailer.sendMail(req, res, "invitation.send_deleted_role_notification", {
                fromPersonName: personName(req.authenticatedUser),
                to: registration.user.email,
                projectName,
                oldRole: ROLES_LABELS[oldRole].toLowerCase(),
                link: `${process.env.REACT_APP_INSCRIPTION_FRONT_URL}/projects/${req.params.projectId}`,
              }));
          } else {
            registration.role = role;

            !userIsModifyingItsOwnRegistration &&
              (await Mailer.sendMail(req, res, "invitation.send_updated_role_notification", {
                fromPersonName: personName(req.authenticatedUser),
                to: registration.user.email,
                projectName,
                role: ROLES_LABELS[registration.role].toLowerCase(),
                link: `${process.env.REACT_APP_ORGA_FRONT_URL}/projects/${req.params.projectId}`,
              }));
          }
          needSave = true;
        }

        if (needSave) registration.save();

        await registration.computeVoluntaryCounter();
        return registration;
      }
    );
  })
);

/**
 * Check in a registration to a session
 * post /:id/checkIn/:sessionId
 */
RegistrationRouter.post(
  "/:id/checkIn/:sessionId",
  userLoggedIn,
  validateAndSanitizeBody(body("hasCheckedIn").isBoolean()),
  handleErrors(async function checkInSession(req: Request, res: Response) {
    const sessionId = req.params.sessionId;
    const hasRole = req.authenticatedUser.registration.role;

    if (!hasRole) return res.status(401).end();

    const registration = await Registration.findOne({
      project: req.params.projectId,
      _id: req.params.id,
    } as any).populate([
      {path: "user", select: lightSelection.user, options: {lean: true}},
      {path: "steward", select: lightSelection.steward, options: {lean: true}},
      {path: "teamsSubscriptions.team", model: "Team", select: "name"},
    ]);

    const sessionSubscription = registration.sessionsSubscriptions.find(
      (ss) => ss.session.toString() === sessionId
    );
    sessionSubscription.hasCheckedIn = req.body.hasCheckedIn
      ? req.authenticatedUser._id
      : undefined;
    registration.save({__reason: "update()", __user: req.authenticatedUser._id} as any);

    return res.status(200).send(registration);
  })
);

/**
 * Delete a registration
 * DELETE /:id
 */
RegistrationRouter.delete(
  "/:id",
  userLoggedIn,
  permit(registrationProprietaryOr(projectAdmins)), // All users are admins from there
  handleErrors(async function del(req: Request, res: Response) {
    // If the person is admin and wants to not be admin anymore, check that
    // there are other admins on the project so there is always at least one admin on the project
    if (
      req.user.registration._id.toString() === req.params.id && // Admin user wants to delete its own registration...
      (await userIsTheOnlyAdminOnCurrentProject(req)) // ...but is the only admin on the project
    ) {
      return res.status(403); // Can't allow this
    }

    // If the registration is only an invitation for a person that is not yet on the platform, also delete the user
    // WARNING : REGISTRATIONS ARE DELETED FOR REAL
    return await deleteEntity(
      req.params.id,
      Registration,
      req.authenticatedUser,
      res,
      async (deletedRegistration: RegistrationD) => {
        // Also delete the user if this was only an invitation, not a real registration
        if (deletedRegistration.invitationToken) {
          // Check nobody also invited this user as well (in that case, don't delete the user)
          const linkedRegistrations = await getDependenciesList(Registration, {
            user: deletedRegistration.user,
          });
          // Delete *for real* the user related to this registration if there is no other linked registration
          if (!linkedRegistrations) await User.deleteOne({_id: deletedRegistration.user});
        }
      },
      undefined,
      true
    );
  })
);

/**
 * Get a registration history
 * GET /:id/history
 */
RegistrationRouter.get(
  "/:id/history",
  userLoggedIn,
  permit(projectGuests),
  handleErrors(async function read(req: Request, res: Response) {
    return await readEntityHistory(req.params.id, req.params.projectId, "Registration", res);
  })
);

/**
 * Send an invitation email to somebody to allow them to collaborate on the project
 * POST /sendInvitationEmail
 */
RegistrationRouter.post(
  "/sendInvitationEmail",
  userLoggedIn,
  permit(projectAdmins),
  validateAndSanitizeBody(
    body("user.email").isEmail().normalizeEmail(emailNormalizationParams),
    body(["user.firstName", "user.lastName", "role"]).isString().optional(),
    body("steward").isString().isMongoId().optional()
  ),
  handleErrors(async function sendInvitationEmail(req: Request, res: Response) {
    const invitedUser = req.body.user;

    // If we find an existing *real* user (that has a password), we cancel the call
    const existingUser = await User.findOne({
      email: invitedUser.email,
      password: {$exists: true}, // If a password exists, it means it's a real user account. Else, it's just an invitation.
    });
    if (existingUser) return res.status(404).end();

    // Generate a random token and save it in the user with an expiration date
    const invitationToken = (await randomBytes(32)).toString("hex");
    const newInvitedRegistration = await Registration.create({
      role: req.body.role,
      steward: req.body.steward,
      invitationToken,
      user:
        (await User.findOne({email: invitedUser.email})) || // Check if a simili user already exists, even if that's a fake user
        (await User.create(invitedUser)),
      project: req.params.projectId,
    });

    const projectName = (await Project.findByIdOrSlug(req.params.projectId, "name").lean()).name;

    const roleLabel =
      newInvitedRegistration.role && ROLES_LABELS[newInvitedRegistration.role].toLowerCase();
    const rootLinkUrl = roleLabel ? config.urls.orgaFront : config.urls.inscriptionFront;

    // Send the reset password email
    await Mailer.sendMail(req, res, "invitation.send_new_orga_invitation", {
      fromPersonName: `${req.user.firstName} ${req.user.lastName}`,
      to: newInvitedRegistration.user.email,
      as: roleLabel
        ? newInvitedRegistration.steward
          ? ` en tant qu'encadrant⋅e et ${roleLabel}`
          : ` en tant que ${roleLabel}`
        : newInvitedRegistration.steward && " en tant qu'encadrant⋅e",
      projectName,
      link:
        `${rootLinkUrl}/projects/${req.params.projectId}/signup?` +
        `invitationToken=${newInvitedRegistration.invitationToken}`,
    });

    return res.status(200).send(newInvitedRegistration);
  })
);
