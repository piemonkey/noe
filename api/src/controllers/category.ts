import {Request, Response, Router} from "express";
import {userLoggedIn} from "../config/passport";
import {handleErrors} from "../services/errors";
import {
  createEntity,
  deleteEntity,
  getDependenciesList,
  readEntityHistory,
  lightSelection,
  readEntity,
  removeNestedObjectsContent,
  updateEntity,
  listAllEntities,
} from "../utilities/controllersUtilities";
import {
  permit,
  projectContributors,
  projectAdmins,
  filterBody,
  projectGuests,
} from "../utilities/permissionsUtilities";

import {Activity} from "../models/activities";
import {Category, allowedBodyArgs, schemaDescription} from "../models/categories";

export const CategoryRouter = Router({mergeParams: true});

/**
 * Get the category schema definition
 * GET /schema
 */
CategoryRouter.get(
  "/schema",
  handleErrors(async function getSchema(req: Request, res: Response) {
    return res.send(schemaDescription);
  })
);

/**
 * List all the categories in the project
 * GET /
 */
CategoryRouter.get(
  "/",
  userLoggedIn,
  handleErrors(async function listAll(req: Request, res: Response) {
    return listAllEntities(
      Category.find({project: req.params.projectId} as any, lightSelection.category),
      {name: "asc"},
      res
    );
  })
);

/**
 * Get a category
 * GET /:id
 */
CategoryRouter.get(
  "/:id",
  userLoggedIn,
  handleErrors(async function read(req: Request, res: Response) {
    return await readEntity(req.params.id, req.params.projectId, res, (id, projectId) =>
      Category.findOne({_id: id, project: projectId} as any).lean()
    );
  })
);

/**
 * Create a category
 * POST /
 */
CategoryRouter.post(
  "/",
  userLoggedIn,
  permit(projectContributors),
  filterBody(allowedBodyArgs),
  removeNestedObjectsContent("project"),
  handleErrors(async function create(req: Request, res: Response) {
    return await createEntity(Category, req.body, res);
  })
);

/**
 * Modify a category
 * PATCH /:id
 */
CategoryRouter.patch(
  "/:id",
  userLoggedIn,
  permit(projectContributors),
  filterBody(["_id", ...allowedBodyArgs]),
  removeNestedObjectsContent("project"),
  handleErrors(async function update(req: Request, res: Response) {
    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Category,
      req.body,
      req.authenticatedUser,
      res
    );
  })
);

/**
 * Delete a category
 * DELETE /:id
 */
CategoryRouter.delete(
  "/:id",
  userLoggedIn,
  permit(projectAdmins),
  handleErrors(async function del(req: Request, res: Response) {
    const categoryId = req.params.id;

    const dependentActivitiesList = await getDependenciesList(Activity, {category: categoryId});
    if (dependentActivitiesList) {
      return res
        .status(405)
        .send(
          `Cette catégorie est encore référencée par des activités (${dependentActivitiesList}). ` +
            "Assurez-vous qu'elle ne soit plus référencée avant de la supprimer."
        );
    }

    return await deleteEntity(categoryId, Category, req.authenticatedUser, res);
  })
);

/**
 * Get a category history
 * GET /:id/history
 */
CategoryRouter.get(
  "/:id/history",
  userLoggedIn,
  permit(projectGuests),
  handleErrors(async function readHistory(req: Request, res: Response) {
    return await readEntityHistory(req.params.id, req.params.projectId, "Category", res);
  })
);
