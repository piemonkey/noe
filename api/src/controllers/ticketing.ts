import {Request, Response, Router} from "express";
import {userLoggedIn} from "../config/passport";
import {handleErrors} from "../services/errors";
import {Project, ProjectD} from "../models/projects";
import fetch from "node-fetch";
import Mailer from "../services/mailer";
import {User} from "../models/users";
import {Ticket} from "../models/registrations";
import config from "../config/config";
import {logger} from "../services/logger";

const ticketingUtilities = {
  /**
   * Hello Asso ticketing utility functions
   */
  helloAsso: {
    fetch: async (project: ProjectD, ticketId: string) => {
      await project.refreshHelloAssoAuthToken();
      return await fetch(`https://api.helloasso.com/v5/orders/${ticketId}`, {
        headers: {Authorization: `Bearer ${project.helloAsso?.token}`},
      });
    },
    createTicketFromResponse: (response: any, ticketId: string) => {
      const {id, name, amount, user, payments} = response.items.find((i: any) => i.id == ticketId);
      return {
        id,
        name,
        amount: amount / 100, // Divide by 100 to get amount in euro and not in cents
        user,
        payments,
      };
    },
    parseOrderInfo: (orderInfo: any) => ({
      firstName: orderInfo.data.payer.firstName,
      lastName: orderInfo.data.payer.lastName,
      email: orderInfo.data.payer.email,
      ticketId: orderInfo.data.items?.[0].id,
      eventSlug: orderInfo.data.formSlug,
    }),
  },

  /**
   * Custom ticketing utility functions
   */
  customTicketing: {
    fetch: async (project: ProjectD, ticketId: string) =>
      await fetch(project.customTicketing, {
        headers: {Accept: "application/json", "Content-Type": "application/json"},
        method: "POST",
        body: JSON.stringify({ticketId}),
      }),
    createTicketFromResponse: (response: any, ticketId: string) => ({
      id: ticketId,
      name: response?.name,
      amount: response?.amount,
    }),
  },
  parseOrderInfo: (order: any) => order, // No need to parse
};

export const TicketingRouter = Router({mergeParams: true});

/**
 * Check if  ticket is valid
 * POST /check/:ticketId
 */
TicketingRouter.post(
  "/check/:ticketId",
  userLoggedIn,
  handleErrors(async function checkTicket(req: Request, res: Response) {
    const projectId: string = req.params.projectId;
    const ticketId: string = req.params.ticketId;
    const project = await Project.findByIdOrSlug(projectId).populate("registrations");
    if (!project?.ticketingMode) return res.status(404).end();

    const ticketingMode = project.ticketingMode.valueOf() as string;

    // Check i the ticket has already been used
    const registrationWithSameTicket = project.registrations.find((registration) =>
      registration[`${ticketingMode}Tickets`]
        ?.map((ticketInfo: Ticket) => {
          if (!ticketInfo || !ticketInfo?.id)
            logger.warn("NO ID FOR TICKET IN REGISTRATION:", registration);
          return ticketInfo?.id;
        })
        .includes(ticketId)
    );
    if (registrationWithSameTicket) return res.status(405).send("Ticket already used"); // Ticket already used

    // Then if not already used, check that the ticket is valid by asking the ticket checker service
    const ticketCheckResponse = await ticketingUtilities[ticketingMode].fetch(project, ticketId);
    // Analyze response
    if (ticketCheckResponse.status != 200) {
      console.error(`${ticketingMode} returned an error during ticket check`, ticketCheckResponse);
      return res.status(404).end(); // Hello Asso error, often because ticket number not found
    } else {
      const ticket = ticketingUtilities[ticketingMode].createTicketFromResponse(
        await ticketCheckResponse.json(),
        ticketId
      );
      logger.debug(`Ticket validated: ${ticket?.id}`);
      return res.json(ticket);
    }
  })
);

/**
 * Provides a test endpoint to make tests on the registration page without needing a complete ticketing setup
 * This returns always success except if the ticket Id is equal to "fail"
 * POST /test
 */
TicketingRouter.post(
  "/test",
  handleErrors(function test(req: Request, res: Response) {
    if (req.body.ticketId !== "fail") {
      return res.status(200).send({name: "Test", amount: 100});
    } else {
      return res.sendStatus(404);
    }
  })
);

/**
 * Sends and email based on the person first name, last name, email, and ticketId with a custom URL so that the person can register easily after she has
 * ordered a ticket on Hello Asso. The Hello Asso ticketing should be activated
 * POST /sendInvitationEmailAfterHelloAssoOrder
 */
TicketingRouter.post(
  "/sendInvitationEmailAfterHelloAssoOrder",
  handleErrors(async function sendInvitationEmailAfterOrder(req: Request, res: Response) {
    logger.debug("Started sendInvitationEmailAfterHelloAssoOrder()");

    const projectId: string = req.params.projectId;
    const project = await Project.findByIdOrSlug(projectId, "ticketingMode helloAsso");
    const ticketingMode = project?.ticketingMode?.valueOf() as string;

    // CHECKS

    // Hello Asso ticketing is activated
    if (!ticketingMode || ticketingMode !== "helloAsso") {
      logger.debug("Hello Asso ticketing not activated");
      return res.sendStatus(404);
    }

    // The api hook is sending an "Order"
    if (req.body.eventType !== "Order") return res.status(200).send("Not an order");

    const {email, firstName, lastName, ticketId, eventSlug} = ticketingUtilities[
      ticketingMode
    ].parseOrderInfo(req.body);
    logger.debug("Parsed ticket info:", {email, firstName, lastName, ticketId, eventSlug});

    // The ticketId is valid according to Hello Asso
    const ticketCheckResponse = await ticketingUtilities.helloAsso.fetch(project, ticketId);
    if (ticketCheckResponse.status !== 200) {
      logger.debug("Invalid ticketId or event");
      return res.sendStatus(200);
    }
    // The ticket formSlug matches a correct event according to Hello Asso
    if ((await ticketCheckResponse.json()).formSlug !== eventSlug) {
      logger.debug("Bad formSlug between api order and ticketId");
      return res.sendStatus(404);
    }

    const foundMatchingHelloAssoEvent = project.helloAsso.selectedEventCandidates.find(
      (event) => event.formSlug === eventSlug
    );

    // The formSlug is matching one of some correct events that we have in our DB
    if (!foundMatchingHelloAssoEvent) {
      logger.warn("No event matching one of the known events");
      return res.sendStatus(404);
    }

    // SEND THE EMAIL

    // Compile information
    const emailInfos = {
      to: email,
      link: `${config.urls.inscriptionFront}/projects/${projectId}/signup?firstName=${firstName}&lastName=${lastName}&email=${email}&ticketId=${ticketId}`,
      firstName,
      ticketId,
      eventName: project.helloAsso.selectedEventCandidates.find(
        (event) => event.formSlug === eventSlug
      ).title,
    };

    // Check if the user email is already in NOÉ's database
    const userHasAlreadyAnAccountInNOE = await User.exists({email});

    // Send the invitation email
    await Mailer.sendMail(
      req,
      res,
      userHasAlreadyAnAccountInNOE
        ? "invitation.send_known_participant_invitation"
        : "invitation.send_new_participant_invitation",
      emailInfos
    );
    return res.sendStatus(200);
  })
);
