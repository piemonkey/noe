import {Request, Response, Router} from "express";
import * as jwt from "jsonwebtoken";
import {randomBytes} from "crypto";
import {default as Mailer} from "../services/mailer";
import {body} from "express-validator/check";
import {compare as bcryptCompare} from "bcryptjs";
import {handleErrors} from "../services/errors";
import {userLoggedIn} from "../config/passport";
import {emailNormalizationParams, validateAndSanitizeBody} from "../utilities/permissionsUtilities";
import {User} from "../models/users";
import config from "../config/config";

export const AuthRouter = Router();

/**
 * Tell if the user can authenticate or not (used for log in)
 * POST /authenticate
 */
AuthRouter.post(
  "/authenticate",
  validateAndSanitizeBody(
    body("email").isEmail().normalizeEmail(emailNormalizationParams),
    body("password").exists()
  ),
  handleErrors(async function authenticate(req: Request, res: Response) {
    // Get the user and select the password with it, because we will need it
    const user = await User.findOne({email: req.body.email}).select("+password");

    if (!user) return res.status(401).end(); // User not found (but no 404 cause we don't want the user to know it

    // Check password
    const passwordsAreEqual = await bcryptCompare(req.body.password, user.password);
    if (!passwordsAreEqual) return res.status(401).json({error: "Incorrect email or password"});

    // Create JWT token
    const payload = {id: user._id};
    const token = jwt.sign(payload, config.jwt.secret, {expiresIn: `${config.jwt.expiresIn} days`});
    const jsonResponse = {jwt_token: token};
    return res.status(200).json(jsonResponse);
  })
);

/**
 * Refresh the user's XSRF and JWT authentication tokens
 * GET /refreshAuthTokens
 */
AuthRouter.get(
  "/refreshAuthTokens",
  userLoggedIn,
  handleErrors(async function refresh(req: Request, res: Response) {
    const payload = {id: req.authenticatedUser._id};
    const token = jwt.sign(payload, config.jwt.secret, {expiresIn: `${config.jwt.expiresIn} days`});

    return res.status(200).json({
      jwt_token: token,
      authenticatedUser: await req.authenticatedUser
        .populate({path: "registrations", select: "project role", options: {lean: true}})
        .execPopulate(),
      user: req.user,
    });
  })
);

/**
 * Send a password forgotten email link to the user
 * POST /password/sendResetEmail
 */
AuthRouter.post(
  "/password/sendResetEmail",
  validateAndSanitizeBody(
    body("email").isEmail().normalizeEmail(emailNormalizationParams),
    body("url").exists()
  ),
  handleErrors(async function passwordForgot(req: Request, res: Response) {
    const user = await User.findOne({email: req.body.email});
    if (!user) return res.status(401).end(); // User not found (but no 404 cause we don't want the user to know it

    // Generate a random token and save it in the user with an expiration date
    const buf = await randomBytes(32);
    user.passwordResetToken = buf.toString("hex");
    user.passwordResetExpires = new Date(Date.now() + 3600000); // 1 hour
    await user.save();

    // Send the reset password email
    await Mailer.sendMail(req, res, "auth.send_forgot_password", {
      to: user.email,
      link: req.body.url + "/resetpassword?token=" + user.passwordResetToken,
    });

    return res.status(200).end();
  })
);

/**
 * Check the password reset token from the client, to tell if it is allowed to change password
 * GET /password/checkResetToken/:token
 */
AuthRouter.get(
  "/password/checkResetToken/:token",
  handleErrors(async function checkPasswordResetToken(req: Request, res: Response) {
    // Get the user with the given token
    const user = await User.findOne({passwordResetToken: req.params.token})
      .where("passwordResetExpires")
      .gt(Date.now());

    // If no user with this token is found, return error
    if (!user) return res.status(401).end();

    // Else, token is valid
    return res.status(200).end();
  })
);

/**
 * Validate the password reset
 * POST /password/reset
 */
AuthRouter.post(
  "/password/reset",
  validateAndSanitizeBody(body("token").exists(), body("password").isLength({min: 8})),
  handleErrors(async function passwordReset(req: Request, res: Response) {
    // Get the user with the given token
    const user = await User.findOne({passwordResetToken: req.body.token})
      .where("passwordResetExpires")
      .gt(Date.now());

    // If no user with this token is found, return error
    if (!user) return res.status(401).end(); // User not found (but no 404 cause we don't want the user to know it

    // If valid, reset the token, and save the new password
    user.passwordResetToken = null;
    user.password = req.body.password;
    await user.save();

    // Send a password changed confirmation email
    await Mailer.sendMail(req, res, "auth.send_reset_password", {
      to: user.email,
      email: user.email,
    });

    return res.status(200).end();
  })
);
