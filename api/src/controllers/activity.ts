import {Request, Response, Router} from "express";
import {userLoggedIn} from "../config/passport";
import {handleErrors} from "../services/errors";
import {
  createEntity,
  deleteEntity,
  getDependenciesList,
  readEntityHistory,
  lightSelection,
  readEntity,
  removeNestedObjectsContent,
  updateEntity,
  listAllEntities,
} from "../utilities/controllersUtilities";
import {
  permit,
  projectContributors,
  projectAdmins,
  filterBody,
  projectGuests,
} from "../utilities/permissionsUtilities";

import {Activity, allowedBodyArgs, schemaDescription} from "../models/activities";
import {Session} from "../models/sessions";

export const ActivityRouter = Router({mergeParams: true});

/**
 * Get the activity schema definition
 * GET /schema
 */
ActivityRouter.get(
  "/schema",
  handleErrors(async function getSchema(req: Request, res: Response) {
    return res.send(schemaDescription);
  })
);

/**
 * List all the activities in the project
 * GET /
 */
ActivityRouter.get(
  "/",
  userLoggedIn,
  handleErrors(async function listAll(req: Request, res: Response) {
    return listAllEntities(
      Activity.find({project: req.params.projectId} as any, lightSelection.activity).populate([
        {path: "category", select: lightSelection.category},
        {path: "places", select: lightSelection.place},
        {path: "stewards", select: lightSelection.steward},
        {path: "slots"},
      ]),
      {name: "asc"},
      res
    );
  })
);

/**
 * Get an activity
 * GET /:id
 */
ActivityRouter.get(
  "/:id",
  userLoggedIn,
  handleErrors(async function read(req: Request, res: Response) {
    return await readEntity(req.params.id, req.params.projectId, res, (id, projectId) =>
      Activity.findOne({_id: id, project: projectId} as any)
        .populate([
          {path: "category", select: lightSelection.category},
          {path: "places", select: lightSelection.place},
          {path: "stewards", select: lightSelection.steward},
          {path: "slots"},
        ])
        .lean()
    );
  })
);

/**
 * Create an activity
 * POST /
 */
ActivityRouter.post(
  "/",
  userLoggedIn,
  permit(projectContributors),
  filterBody(allowedBodyArgs),
  removeNestedObjectsContent("category", "places", "stewards", "project"),
  handleErrors(async function create(req: Request, res: Response) {
    return await createEntity(Activity, req.body, res, (activity) =>
      activity
        .populate([
          {path: "category", select: lightSelection.category, options: {lean: true}},
          {path: "places", select: lightSelection.place, options: {lean: true}},
          {path: "stewards", select: lightSelection.steward, options: {lean: true}},
          {path: "slots", options: {lean: true}},
        ] as any)
        .execPopulate()
    );
  })
);

/**
 * Modify an activity
 * PATCH /:id
 */
ActivityRouter.patch(
  "/:id",
  userLoggedIn,
  permit(projectContributors),
  filterBody(["_id", ...allowedBodyArgs]),
  removeNestedObjectsContent("category", "places", "stewards", "project"),
  handleErrors(async function update(req: Request, res: Response) {
    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Activity,
      req.body,
      req.authenticatedUser,
      res,
      async (query) =>
        await query
          .populate([
            {path: "category", select: lightSelection.category},
            {path: "places", select: lightSelection.place},
            {path: "stewards", select: lightSelection.steward},
            {path: "slots"},
          ])
          .lean()
    );
  })
);

/**
 * Delete an activity
 * DELETE /:id
 */
ActivityRouter.delete(
  "/:id",
  userLoggedIn,
  permit(projectAdmins),
  handleErrors(async function del(req: Request, res: Response) {
    const activityId = req.params.id;

    const dependentSessionsList = await getDependenciesList(Session, {activity: activityId});
    if (dependentSessionsList) {
      return res
        .status(405)
        .send(
          `Cette activité est encore référencée par des sessions (${dependentSessionsList}). ` +
            "Assurez-vous qu'elle ne soit plus référencée avant de la supprimer."
        );
    }

    return await deleteEntity(activityId, Activity, req.authenticatedUser, res);
  })
);

/**
 * Get an activity history
 * GET /:id/history
 */
ActivityRouter.get(
  "/:id/history",
  userLoggedIn,
  permit(projectGuests),
  handleErrors(async function readHistory(req: Request, res: Response) {
    return await readEntityHistory(req.params.id, req.params.projectId, "Activity", res);
  })
);
