import {Request, Response, Router} from "express";
import {userLoggedIn} from "../config/passport";
import {handleErrors} from "../services/errors";
import {
  createEntity,
  deleteEntity,
  readEntityHistory,
  lightSelection,
  readEntity,
  removeNestedObjectsContent,
  updateEntity,
  updateManyToManyRelationship,
  updateOneToManyRelationship,
  listAllEntities,
} from "../utilities/controllersUtilities";
import {
  permit,
  projectContributors,
  projectAdmins,
  filterBody,
  projectGuests,
} from "../utilities/permissionsUtilities";

import {Team, allowedBodyArgs, schemaDescription} from "../models/teams";
import {Session} from "../models/sessions";
import {Registration} from "../models/registrations";

const updateSessionsSubscriptionsInRegistrations = async (
  teamId: any,
  registrationsToUpdateIds: Array<string> = [],
  newTeamRegistrations: Array<any> = [],
  newTeamSessions: Array<any>,
  userId: any
) => {
  // Three cases for sessionsSubscription updates:
  // 1. The user is being added to in the team and should have all the team's sessions subscribed
  // 2. The user is already in the team and the sessions have changed, so we have to add or remove sessions
  // 3. The user is not anymore in the team and should have all subscriptions deleted
  // As a shortcut, we will gather case 2 and 3, and for them, delete all the sessions not
  // containing the team and put back all the subscriptions.

  // Gather registrations to update + all registrations belonging to the team
  const registrationsToUpdate = await Registration.find({
    _id: [...registrationsToUpdateIds, ...newTeamRegistrations],
  });

  for (const registrationToUpdate of registrationsToUpdate) {
    const existingSubscriptionsWithoutCurrentTeam =
      registrationToUpdate.sessionsSubscriptions?.filter(
        (ss: any) => ss.team?.toString() !== teamId.toString()
      ) || [];

    const registrationBelongsToTeam = newTeamRegistrations.find(
      (registrationId: string) => registrationToUpdate._id.toString() === registrationId
    );
    if (registrationBelongsToTeam) {
      // case 1. + 2.
      const currentTeamSessionsSubscriptions =
        newTeamSessions?.map((session: any) => ({
          session,
          team: teamId,
          subscribedBy: userId,
        })) || [];
      registrationToUpdate.sessionsSubscriptions = [
        ...existingSubscriptionsWithoutCurrentTeam,
        ...currentTeamSessionsSubscriptions,
      ] as any;
    } else {
      // case 3.
      registrationToUpdate.sessionsSubscriptions = existingSubscriptionsWithoutCurrentTeam;
    }
    await registrationToUpdate.save();
  }
};

export const TeamRouter = Router({mergeParams: true});

/**
 * Get the  schema definition
 * GET /schema
 */
TeamRouter.get(
  "/schema",
  handleErrors(async function getSchema(req: Request, res: Response) {
    return res.send(schemaDescription);
  })
);

/**
 * List all the teams in the project
 * GET /
 */
TeamRouter.get(
  "/",
  userLoggedIn,
  handleErrors(async function listAll(req: Request, res: Response) {
    const projectId = req.params.projectId;
    return listAllEntities(
      Team.find({project: projectId} as any, lightSelection.team).populate({
        path: "activity",
        select: "_id name",
        populate: {path: "category", select: lightSelection.category},
      }),
      {name: "asc"},
      res
    );
  })
);

/**
 * Get a team
 * GET /:id
 */
TeamRouter.get(
  "/:id",
  userLoggedIn,
  handleErrors(async function read(req: Request, res: Response) {
    return await readEntity(req.params.id, req.params.projectId, res, (id, projectId) =>
      Team.findOne({_id: id, project: projectId} as any)
        .populate([{path: "activity", populate: [{path: "category"}]}])
        .lean()
    );
  })
);

/**
 * Create a team
 * POST /
 */
TeamRouter.post(
  "/",
  userLoggedIn,
  permit(projectContributors),
  filterBody(allowedBodyArgs),
  removeNestedObjectsContent("activity", "project", "sessions", "registrations"),
  handleErrors(async function create(req: Request, res: Response) {
    // Save sessions/team links independently
    const {sessions: newTeamSessions, registrations: newTeamRegistrations, ...teamBody} = req.body;

    return await createEntity(Team, teamBody, res, async (team) => {
      // Update sessions that belong to the team
      const sessionsToUpdateIds = await updateOneToManyRelationship(
        team,
        newTeamSessions,
        Session,
        "team"
      );
      // Update participants that are part of the team
      const registrationsToUpdateIds = await updateManyToManyRelationship(
        team,
        newTeamRegistrations,
        Registration,
        "team",
        "teamsSubscriptions",
        {subscribedBy: req.authenticatedUser, updatedAt: Date.now()}
      );
      await updateSessionsSubscriptionsInRegistrations(
        team._id,
        registrationsToUpdateIds,
        newTeamRegistrations,
        newTeamSessions,
        req.user._id
      );
      await team
        .populate([{path: "activity", options: {lean: true}, populate: [{path: "category"}]}])
        .execPopulate();
      return {...team.toObject(), sessionsToUpdateIds, registrationsToUpdateIds};
    });
  })
);

/**
 * Modify a team
 * PATCH /:id
 */
TeamRouter.patch(
  "/:id",
  userLoggedIn,
  permit(projectContributors),
  filterBody(["_id", ...allowedBodyArgs]),
  removeNestedObjectsContent("activity", "project", "sessions", "registrations"),
  handleErrors(async function update(req: Request, res: Response) {
    // Save sessions/team links independently
    const {sessions: newTeamSessions, registrations: newTeamRegistrations, ...teamBody} = req.body;

    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Team,
      teamBody,
      req.authenticatedUser,
      res,
      async (query) =>
        await query.populate([{path: "activity", populate: [{path: "category"}]}]).lean(),
      async (team) => {
        // Update sessions that belong to the team
        team.sessionsToUpdateIds = await updateOneToManyRelationship(
          team,
          newTeamSessions,
          Session,
          "team"
        );
        // Update participants that are part of the team
        team.registrationsToUpdateIds = await updateManyToManyRelationship(
          team,
          newTeamRegistrations,
          Registration,
          "team",
          "teamsSubscriptions",
          {subscribedBy: req.authenticatedUser, updatedAt: Date.now()}
        );

        // If some registrations have changed, we need to update the filling counters of the existing team sessions
        if (team.registrationsToUpdateIds?.length > 0)
          team.sessionsToUpdateIds = [...team.sessionsToUpdateIds, ...newTeamSessions];

        await updateSessionsSubscriptionsInRegistrations(
          team._id,
          team.registrationsToUpdateIds,
          newTeamRegistrations,
          newTeamSessions,
          req.user._id
        );
      }
    );
  })
);

/**
 * Delete a team
 * DELETE /:id
 */
TeamRouter.delete(
  "/:id",
  userLoggedIn,
  permit(projectAdmins),
  handleErrors(async function del(req: Request, res: Response) {
    const teamId = req.params.id;

    const sessionsToUpdateIds = await updateOneToManyRelationship(teamId, [], Session, "team");
    const registrationsToUpdateIds = await updateManyToManyRelationship(
      teamId,
      [],
      Registration,
      "team",
      "teamsSubscriptions"
    );
    await updateSessionsSubscriptionsInRegistrations(
      teamId,
      registrationsToUpdateIds,
      [],
      [],
      req.user._id
    );

    return await deleteEntity(teamId, Team, req.authenticatedUser, res, undefined, {
      sessionsToUpdateIds,
      registrationsToUpdateIds,
    });
  })
);

/**
 * Get a team history
 * GET /:id/history
 */
TeamRouter.get(
  "/:id/history",
  userLoggedIn,
  permit(projectGuests),
  handleErrors(async function readHistory(req: Request, res: Response) {
    return await readEntityHistory(req.params.id, req.params.projectId, "Team", res);
  })
);
