import {Request, Response, Router} from "express";
import {userLoggedIn} from "../config/passport";
import {handleErrors} from "../services/errors";
import * as handlebars from "handlebars";
import {getSessionVolunteeringCoefficient} from "../models/sessions";
import {Session, SessionD} from "../models/sessions";
import {Slot, SlotD} from "../models/slots";
import {UserD} from "../models/users";
import {FormMapping, Registration, RegistrationD} from "../models/registrations";
import {permit, projectGuests} from "../utilities/permissionsUtilities";
import {Steward, StewardD} from "../models/stewards";
import {Project, ProjectD} from "../models/projects";
import {Team, TeamD} from "../models/teams";
import momentTz = require("moment-timezone");
import {personName} from "../utilities/utilities";
import {compileHtmlTemplate, printPdf} from "../services/html-and-pdf";

export const PdfRouter = Router({mergeParams: true});

/**
 * Date formatting helpers
 */
const momentWithTz = momentTz.tz;
const longDateFormat = (date: any) =>
  momentWithTz(date, "Europe/Paris").locale("fr").format("dddd D MMMM");
const timeFormat = (date: any) => momentWithTz(date, "Europe/Paris").locale("fr").format("HH:mm");

/**
 * Handlebars helpers
 */
handlebars.registerHelper("slotBorder", function () {
  if (this.isStewardOnThisSession) {
    return "border: 5px solid rgba(0, 0, 255, 0.5)";
  } else if (this.isVolunteering) {
    return `border: 4px solid ${this.session.activity.category.color}`;
  }
});
handlebars.registerHelper("categoryColorDot", function () {
  return `<div class="containerH" style="height: 20pt; width: 20pt; background-color: ${this.session.activity.category.color}; border-radius: 50%;"></div>`;
});

const cleanAnswer = (answer: any) => {
  // If the answer is a checkbox answer, it is like an object. So we keep only the true values and display them
  if (typeof answer === "object") {
    if (answer.maskName && answer.value) {
      // If the answer looks like a formatted phone number...
      return answer.value;
    } else {
      // Else if it looks like a multiple choice answser...
      return Object.entries(answer)
        .filter((entry) => entry[1])
        .map((entry) => entry[0])
        .join(",\n");
    }
  } else {
    return answer;
  }
};

// A copy of this function is also available in both frontends (with small modifs)
const slotNumberString = (slot: SlotD) => {
  const numberOfSlotsInSession = slot.session.slots.length;
  return numberOfSlotsInSession > 1
    ? "(" +
        (slot.session.slots.findIndex((s) => s.toString() === slot._id.toString()) + 1) +
        "/" +
        numberOfSlotsInSession +
        ") "
    : "";
};

// A copy of this function is also available in both frontends (with small modifs)
export const getSessionName = (session: SessionD, teams: Array<TeamD>) => {
  const sessionName = session.name?.length > 0 ? session.name + " - " : "";
  const teamName = teams?.find((t) => t._id.toString() === session.team?.toString())?.name;
  const sessionNameSuffix = teamName ? ` (${teamName})` : "";
  return sessionName + session.activity?.name + sessionNameSuffix;
};

/**
 * Generate and download the PDF planning for a particular registration
 * GET /registrationPlanning/:id
 */
PdfRouter.get(
  "/registrationPlanning/:id",
  userLoggedIn,
  handleErrors(async function registrationPdfPlanning(req: Request, res: Response) {
    const projectId = req.params.projectId;
    const project = await Project.findById(projectId);

    const formFieldsToDisplay = project.formMapping?.filter((mapping) => mapping.inPdfExport);

    if (!req.user.registration.role && req.user.registration._id.toString() !== req.params.id) {
      return res.sendStatus(403); // If a non-orga user wants something else than its own registration, forbid it
    }

    const allRegistrations = await Registration.find(
      {project},
      "user project _id sessionsSubscriptions specific"
    )
      .populate("user")
      .lean();

    const registration = await Registration.findById(req.params.id)
      .populate(["user", "steward"])
      .lean();

    const compiledHtml = await createHtmlUserPlanning(
      registration as RegistrationD,
      registration.steward as StewardD,
      project as ProjectD,
      registration.user as UserD,
      allRegistrations as Array<RegistrationD>,
      formFieldsToDisplay
    );
    const pdf = await printPdf(compiledHtml);
    return res.contentType("application/pdf").send(pdf);
  })
);

/**
 * Generate and download the PDF planning for a particular steward
 * GET /stewardPlanning/:id
 *
 * GET /stewardsPlanning/all gets all plannings from project
 * GET /stewardsPlanning/xxxxxxx gets planning for id xxxxxxx
 * GET /stewardsPlanning/xxxxxxx,yyyyyyy,zzzzzzz gets planning for id xxxxxxx, yyyyyyy and zzzzzzz
 */
PdfRouter.get(
  "/stewardPlanning/:id",
  userLoggedIn,
  permit(projectGuests),
  handleErrors(async function registrationPdfPlanning(req: Request, res: Response) {
    const stewardId = req.params.id;
    const projectId = req.params.projectId;
    const project = await Project.findByIdOrSlug(projectId);

    // If "all" the stewards are wanted, select all fro project. Else, take the id and split it if there are some multiple ids there
    const researchConditions =
      stewardId === "all" ? {project: projectId} : {project: projectId, _id: stewardId.split(",")};

    const formFieldsToDisplay = project.formMapping?.filter((mapping) => mapping.inPdfExport);

    const allRegistrations = await Registration.find(
      {project},
      "user project _id sessionsSubscriptions specific"
    )
      .populate("user")
      .lean();

    const stewards = await Steward.find(researchConditions as any, "_id firstName lastName")
      .sort({lastName: -1})
      .lean();

    const allHtmlTemplates = await Promise.all(
      stewards.map(async (currentSteward) => {
        // Maybe this registration doesn't exist
        const registrationLinkedToSteward = await Registration.findOne({steward: currentSteward})
          .populate("user")
          .lean();

        return await createHtmlUserPlanning(
          registrationLinkedToSteward as RegistrationD,
          currentSteward as StewardD,
          project as ProjectD,
          registrationLinkedToSteward?.user as UserD,
          allRegistrations as Array<RegistrationD>,
          formFieldsToDisplay
        );
      })
    );

    // Join all the generated HTML templates together with page breaks
    const compiledHtml = allHtmlTemplates.join(
      "<div style='page-break-before: always; height: 30px'></div>"
    );
    const pdf = await printPdf(compiledHtml);
    return res.contentType("application/pdf").send(pdf);
  })
);

const createHtmlUserPlanning = async (
  registration: RegistrationD,
  currentSteward: StewardD,
  project: ProjectD,
  user: UserD,
  allRegistrations: Array<RegistrationD>,
  formFieldsToDisplay: Array<FormMapping>
) => {
  // This doesn't select session where only a slot has the user as a steward
  const sessionsSubscribedOrAsSteward = await Session.find({
    $or: [
      {_id: registration?.sessionsSubscriptions?.map((ss: any) => ss.session)}, // Find the user's subscribed sessions
      {stewards: currentSteward}, // Or that the user animates
    ],
  }).lean();

  const sessionsSubscribedOrAsStewardSlotIds = sessionsSubscribedOrAsSteward
    .map((session: any) => session.slots)
    .reduce((acc: any, val: any) => acc.concat(val), []);

  const slots = await Slot.find({
    $or: [
      {_id: sessionsSubscribedOrAsStewardSlotIds}, // Select the slots corresponding to the sessions
      {stewards: currentSteward}, // And also the slots where the steward is assigned on the slot itself
    ],
  })
    .sort({start: 1})
    .populate([
      "stewards",
      "places",
      {
        path: "session",
        populate: ["stewards", "places", {path: "activity", populate: ["category"]}],
      },
    ])
    .lean();

  const teams = await Team.find({project}, "_id name").lean();

  const slotsGroupedByDate = slots
    .filter((slot) => slot.session) // Be sure that the slot is assigned to a session (this can happen that we loose slots in the nature...
    .map((slot: any) => {
      const slotPlaces = slot.placesSessionSynchro ? slot.session.places : slot.places;
      const placesString = slotPlaces.map((el: any) => el.name).join(", ");

      const slotStewards = slot.stewardsSessionSynchro ? slot.session.stewards : slot.stewards;

      const isStewardOnThisSession =
        slotStewards?.findIndex(
          (steward: any) => steward._id.toString() === currentSteward?._id.toString()
        ) !== -1;

      const stewardsString = slotStewards
        .filter((el: any) => el._id.toString() !== currentSteward?._id.toString())
        .map(personName)
        .join(", ");

      const hasMultipleStewards = slotStewards.length > 1;

      const isVolunteering = getSessionVolunteeringCoefficient(slot.session) > 0;

      const participants =
        (isStewardOnThisSession || registration?.role) &&
        allRegistrations
          .filter((r) =>
            r.sessionsSubscriptions?.find(
              (sessionSubscription) =>
                sessionSubscription.session.toString() === slot.session._id.toString()
            )
          )
          .map((r) => {
            const additionalFields =
              formFieldsToDisplay?.length > 0
                ? [
                    r.user.email,
                    ...formFieldsToDisplay.map(
                      ({columnName, formField}) =>
                        `${columnName}: ${cleanAnswer(r.specific?.[formField]) || "/"}`
                    ),
                  ].join(" - ")
                : r.user.email;
            return `${personName(r.user)} (${additionalFields})`;
          });

      return {
        ...slot,
        title: slotNumberString(slot) + getSessionName(slot.session, teams as Array<TeamD>),
        startFormatted: timeFormat(slot.start),
        endFormatted: timeFormat(slot.end),
        places: slotPlaces,
        stewards: slotStewards,
        placesString,
        stewardsString,
        hasMultipleStewards,
        participants,
        isStewardOnThisSession,
        isVolunteering,
      };
    })
    .reduce((groupingAcc: any, slot: any) => {
      // Group slots by day
      const dateString = longDateFormat(slot.start);
      return {
        ...groupingAcc,
        [dateString]: [...(groupingAcc[dateString] || []), slot],
      };
    }, {});

  // Format in a way that Handlebar can manage
  const slotsForHandlebars = Object.entries(slotsGroupedByDate).map(([day, slots]) => ({
    dayName: day,
    slot: slots,
  }));

  const pdfPlanningData = {
    currentExportDate: `${longDateFormat(momentWithTz())} ${timeFormat(momentWithTz())}`,
    day: slotsForHandlebars,
    user: user,
    hasRoleOrSteward: registration?.role || currentSteward,
    steward: currentSteward,
    projectName: project.name,
  };

  return await compileHtmlTemplate("pdf-user-planning", pdfPlanningData);
};
