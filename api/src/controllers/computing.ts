import {Request, Response, Router} from "express";
import {userLoggedIn} from "../config/passport";
import {handleErrors} from "../services/errors";
import fetch from "node-fetch";

import {Computation, ComputationState} from "../models/computations";
import {permit, projectContributors, projectGuests} from "../utilities/permissionsUtilities";
import config from "../config/config";
import {readEntityHistory} from "../utilities/controllersUtilities";

export const ComputingRouter = Router();

async function getNextRun() {
  const next = await Computation.find({
    state: ComputationState.WAITING,
  })
    .sort([["createdAt", 1]])
    .exec();
  if (next) return next[0];
  else return undefined;
}

async function launchAI(authToken: string) {
  const options = {headers: {authorization: authToken}};
  const nextOne = await getNextRun();
  if (nextOne) {
    nextOne.state = ComputationState.WORKING;
    await nextOne.save();
    await fetch(`${config.urls.iaBack}/v1.0/computing/testCall/${nextOne.project}`, options);
  }
}

async function isAIAvailable() {
  const res = await Computation.find({state: ComputationState.WORKING});
  return res.length === 0;
}

async function resetComputations() {
  // In case some computations timed out, we reset them at the
  const res = await Computation.find({state: ComputationState.WORKING});
  if (res.length) {
    res.map(async (doc: any) => {
      await Computation.updateOne({_id: doc._id}, {$set: {state: ComputationState.FAILED}});
    });
  }
}

async function resetOtherComputations(projectId: any) {
  // Max one computation per project in the queue
  const op = await Computation.updateMany(
    {project: projectId, state: ComputationState.WAITING},
    {$set: {state: ComputationState.CANCELLED}}
  );
  return op;
}

/**
 * Launch a new computation on project <projectId>
 * GET /:projectId
 */
ComputingRouter.get(
  "/:projectId",
  userLoggedIn,
  permit(projectContributors),
  handleErrors(async function create(req: Request, res: Response) {
    const projectId = req.params.projectId;
    const comp = new Computation({
      state: ComputationState.WAITING,
      project: projectId,
    });
    await resetOtherComputations(projectId);
    comp.save().then(async (ret: any) => {
      const available = await isAIAvailable();
      if (available) launchAI(req.headers.authorization);

      res.status(201).json(ret);
    });
  })
);

ComputingRouter.post(
  "/reset",
  userLoggedIn,
  permit(projectContributors),
  handleErrors(async function reset(req: Request, res: Response) {
    await resetComputations();
    res.status(201).json({status: "ok"});
  })
);

ComputingRouter.post(
  "/next",
  userLoggedIn,
  permit(projectContributors),
  handleErrors(async function getNextJob(req: Request, res: Response) {
    const current = await Computation.findOne({
      state: ComputationState.WORKING,
    });
    current.state = req.body.state;
    current.result = req.body.result;
    current.save();
    launchAI(req.headers.authorization);

    res.status(201).json({status: "ok"});
  })
);

/**
 * Get the status for the Computation n°<id> of project <projectId>
 * GET /:projectId/status/:id
 */
ComputingRouter.get(
  "/:projectId/status/:id",
  userLoggedIn,
  permit(projectContributors),
  handleErrors(async function getStatus(req: Request, res: Response) {
    const computationId = req.params.id;
    const next = await Computation.find({
      state: ComputationState.WAITING,
    })
      .sort([["createdAt", 1]])
      .exec();
    const indexes = next.map((elem) => elem._id.toString()).reverse();
    let position = Math.round((indexes.indexOf(computationId) / indexes.length) * 100);
    const computation = await Computation.findById(computationId).exec();
    // If the position computing failed, it means the computation is either succeeded or something failed in the process
    if (!isFinite(position)) {
      position = 100;
    }
    // Add a fake advancement when working so the user doesn't get the feeling the computation didn't start
    if (computation.state === ComputationState.WORKING) {
      position = 50;
    }
    res.json({
      state: computation.state,
      length: indexes.length,
      position: position,
      result: computation.result,
    });
  })
);

/**
 * Get a computing history
 * GET /:id/history
 */
ComputingRouter.get(
  "/:id/history",
  userLoggedIn,
  permit(projectGuests),

  handleErrors(async function readHistory(req: Request, res: Response) {
    return await readEntityHistory(req.params.id, req.params.projectId, "Computing", res);
  })
);
