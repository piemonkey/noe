import {Request, Response, Router} from "express";
import {userLoggedIn} from "../config/passport";
import {handleErrors} from "../services/errors";
import {
  createEntity,
  deleteEntity,
  getDependenciesList,
  readEntityHistory,
  lightSelection,
  readEntity,
  updateEntity,
  listAllEntities,
} from "../utilities/controllersUtilities";
import {
  permit,
  projectContributors,
  projectAdmins,
  filterBody,
  projectGuests,
} from "../utilities/permissionsUtilities";

import {Activity} from "../models/activities";
import {Slot} from "../models/slots";
import {Steward, allowedBodyArgs, schemaDescription} from "../models/stewards";
import {Session} from "../models/sessions";

export const StewardRouter = Router({mergeParams: true});

/**
 * Get the steward schema definition
 * GET /schema
 */
StewardRouter.get(
  "/schema",
  handleErrors(async function getSchema(req: Request, res: Response) {
    return res.send(schemaDescription);
  })
);

/**
 * List all the stewards in the project
 * GET /
 */
StewardRouter.get(
  "/",
  userLoggedIn,
  handleErrors(async function listAll(req: Request, res: Response) {
    return listAllEntities(
      Steward.find({project: req.params.projectId} as any, lightSelection.steward),
      {firstName: "asc"},
      res
    );
  })
);

/**
 * Get a steward
 * GET /:id
 */
StewardRouter.get(
  "/:id",
  userLoggedIn,
  handleErrors(async function read(req: Request, res: Response) {
    return await readEntity(req.params.id, req.params.projectId, res, (id, projectId) =>
      Steward.findOne({_id: id, project: projectId} as any).lean()
    );
  })
);

/**
 * Create a steward
 * POST /
 */
StewardRouter.post(
  "/",
  userLoggedIn,
  permit(projectContributors),
  filterBody(allowedBodyArgs),
  handleErrors(async function create(req: Request, res: Response) {
    return await createEntity(Steward, req.body, res);
  })
);

/**
 * Modify a steward
 * PATCH /:id
 */
StewardRouter.patch(
  "/:id",
  userLoggedIn,
  permit(projectContributors),
  filterBody(["_id", ...allowedBodyArgs]),
  handleErrors(async function update(req: Request, res: Response) {
    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Steward,
      req.body,
      req.authenticatedUser,
      res
    );
  })
);

/**
 * Delete a steward
 * DELETE /:id
 */
StewardRouter.delete(
  "/:id",
  userLoggedIn,
  permit(projectAdmins),
  handleErrors(async function del(req: Request, res: Response) {
    const stewardId = req.params.id;

    const dependentActivitiesList = await getDependenciesList(Activity, {stewards: stewardId});
    if (dependentActivitiesList) {
      return res
        .status(405)
        .send(
          `Cet encadrant est encore référencé par des activités (${dependentActivitiesList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }

    // TODO make it work with session slots too!
    const dependentSessionsList = await getDependenciesList(Session, {stewards: stewardId});
    if (dependentSessionsList) {
      return res
        .status(405)
        .send(
          `Cet encadrant est encore référencé par des sessions (${dependentSessionsList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }
    const dependentSlotsList = await getDependenciesList(
      Slot,
      {stewards: stewardId},
      async (e: any) => {
        await e.populate("session").execPopulate();
        return e.session._id;
      }
    );
    if (dependentSlotsList) {
      return res
        .status(405)
        .send(
          `Cet encadrant est encore référencé par des plages de sessions (${dependentSlotsList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }

    return await deleteEntity(stewardId, Steward, req.authenticatedUser, res);
  })
);

/**
 * Get a steward history
 * GET /:id/history
 */
StewardRouter.get(
  "/:id/history",
  userLoggedIn,
  permit(projectGuests),
  handleErrors(async function readHistory(req: Request, res: Response) {
    return await readEntityHistory(req.params.id, req.params.projectId, "Steward", res);
  })
);
