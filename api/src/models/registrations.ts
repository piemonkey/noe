import {Document, model, Schema, SchemaTypes} from "mongoose";
import {UserD} from "./users";
import {Project, ProjectD} from "./projects";
import {StewardD} from "./stewards";
import {AvailabilitySlotD, AvailabilitySlotSchema} from "./availabilitySlots";
import {Session, SessionD} from "./sessions";
import * as moment from "moment";
import {
  addDiffHistory,
  preventDuplicatesInProject,
  softDelete,
} from "../config/mongooseMiddlewares";
import {getSessionVolunteeringCoefficient} from "../models/sessions";
import {TeamD} from "./teams";

export interface Ticket {
  id: string;
  name: string;
  amount: number;
}

interface HelloAssoTicket extends Ticket {
  priceCategory: string;
  ticketUrl: string;
}

// An interface for put together a start time and an end time
interface StartEnd {
  start: moment.Moment;
  end: moment.Moment;
}

export interface FormMapping {
  formField: string;
  columnName: string;
  inPdfExport?: boolean;
}

// An interface to store if for a given range (startEnd) a meal or a dinner is consumed
interface StartEndWithConfirmLunch {
  plage: StartEnd;
  breakfast: boolean;
  lunch: boolean;
  dinner: boolean;
  projectDay: boolean;
}

type SessionSubscriptionD = Document & {
  subscribedBy: UserD;
  session: SessionD;
  hasCheckedIn?: boolean | UserD;
  team?: TeamD; // If filled, it means that the session subscription has been created because of the team registration
};
const SessionSubscriptionSchema = new Schema<SessionSubscriptionD>(
  {
    subscribedBy: {type: SchemaTypes.ObjectId, ref: "User"},
    session: {type: SchemaTypes.ObjectId, ref: "Session"},
    hasCheckedIn: {type: SchemaTypes.ObjectId, ref: "User"}, // Save the ref of the user who checked the person as checkedIn
    // Redundant info to tell that the session subscription was subscribed
    // either on its own (no team), or subscribed via a team subscription.
    team: {type: SchemaTypes.ObjectId, ref: "Team"},
  },
  {timestamps: true}
);

type TeamSubscriptionD = Document & {
  subscribedBy: UserD;
  team: TeamD;
};
const TeamSubscriptionSchema = new Schema<TeamSubscriptionD>(
  {
    subscribedBy: {type: SchemaTypes.ObjectId, ref: "User"},
    team: {type: SchemaTypes.ObjectId, ref: "Team"},
  },
  {timestamps: true}
);

// See https://mongoosejs.com/docs/typescript.html#creating-your-first-document
// and https://stackoverflow.com/questions/34482136/mongoose-the-typescript-way/36661990
// (D like Document)

export type RegistrationD = Document & {
  // Entities
  user: UserD;
  project: ProjectD;
  steward: StewardD;

  // Project role
  role: string;

  // Invitation data
  invitationToken: string;

  // Registration
  booked: boolean;
  specific: Object;
  sessionsSubscriptions: Array<SessionSubscriptionD>;
  teamsSubscriptions: Array<TeamSubscriptionD>;
  availabilitySlots: Array<AvailabilitySlotD>;
  hasCheckedIn: {type: boolean; default: false};

  // Fields for the orga team
  notes: string;
  tags: Array<string>;

  // Ticketing
  helloAssoTickets: Array<HelloAssoTicket>;
  customTicketingTickets: Array<Ticket>;

  // Tells if the registration is hidden from public, and is only scoed to the project
  hidden: boolean;

  // Computed stuff
  computeVoluntaryCounter: () => void;
  voluntaryCounter: number;
  computeDaysOfPresence: () => void;
  numberOfDaysOfPresence: number;
  daysOfPresence: Array<Object>;

  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
};

const RegistrationSchema = new Schema<RegistrationD>(
  {
    user: {type: SchemaTypes.ObjectId, ref: "User"},
    project: {type: SchemaTypes.ObjectId, ref: "Project"},
    steward: {type: SchemaTypes.ObjectId, ref: "Steward"},

    role: String,

    invitationToken: String,

    booked: Boolean,
    specific: Object,
    sessionsSubscriptions: [SessionSubscriptionSchema],
    teamsSubscriptions: [TeamSubscriptionSchema],
    availabilitySlots: [AvailabilitySlotSchema],
    hasCheckedIn: {type: Boolean, default: false},

    notes: String,
    tags: [String],

    helloAssoTickets: [Object],
    customTicketingTickets: [Object],

    hidden: Boolean,

    voluntaryCounter: {
      type: Number,
      get: getVoluntaryCounter,
    },
    numberOfDaysOfPresence: Number,
    daysOfPresence: [Object],

    createdAt: Date,
    updatedAt: Date,
    deletedAt: Date,
  },
  {timestamps: true}
);

// Create unique constraint with project and user
RegistrationSchema.index({project: 1, user: 1, deletedAt: 1}, {unique: true});

// Compute voluntaryCounter
async function getVoluntaryCounter(value?: any) {
  const registration = this as RegistrationD;

  // The steward can be undefined
  const {steward} = registration;

  // TODO use the slots instead cause you can be steward only on one slot of a session
  const sessionsSubscriptions = await Session.find({
    $or: [
      {_id: {$in: registration.sessionsSubscriptions.map((ss) => ss.session)}},
      {stewards: steward?._id},
    ],
  }).populate([{path: "activity"}, {path: "slots"}]);

  // TODO this doesn't take in account the case of unsynchronized slot stewards
  // Compute the weighted volunteering time in all session for the current user
  const weightedTime = sessionsSubscriptions
    .map((session) => {
      const isStewardForThisSession = session.stewards.toString().includes(steward?._id);
      const volunteeringCoefficient = isStewardForThisSession
        ? session.activity.stewardVolunteeringCoefficient || 0
        : getSessionVolunteeringCoefficient(session) || 0;
      const duration =
        session.slots.reduce((durationSum, slot) => durationSum + slot.duration, 0) || 0;
      return volunteeringCoefficient * duration;
    })
    .reduce((totalWeightedTime, sessionWeightedTime) => totalWeightedTime + sessionWeightedTime, 0);

  // Compute numberOfDaysOfPresence if not computed already
  if (
    (!registration.daysOfPresence || registration.daysOfPresence.length === 0) &&
    registration.availabilitySlots?.length > 0
  ) {
    await registration.computeDaysOfPresence();
  }

  // To avoid division by 0
  if (registration.numberOfDaysOfPresence === 0) {
    return 0;
  } else {
    return weightedTime / registration.numberOfDaysOfPresence;
  }
}

RegistrationSchema.pre(["save", "updateOne"], async function (next) {
  const registration = this as RegistrationD;
  await registration.computeDaysOfPresence();
  next();
});

RegistrationSchema.methods = {
  async computeVoluntaryCounter() {
    const registration = this as RegistrationD;

    registration.voluntaryCounter = await getVoluntaryCounter.bind(this)();
  },

  async computeDaysOfPresence() {
    const registration = this as RegistrationD;

    // If there are no days of presence, everything is equal to zero
    if (!registration.availabilitySlots || registration.availabilitySlots.length === 0) {
      registration.numberOfDaysOfPresence = 0;
      registration.daysOfPresence = [];
      return;
    }

    // Get the project
    const project = await Project.findByIdOrSlug(registration.project?._id || registration.project);

    // Get meals times from project
    const {breakfastTime, lunchTime, dinnerTime} = project;

    // Convert project dates to Moment dates
    const projectStart = moment(project.start);
    const projectEnd = moment(project.end);
    const projectSlots: Array<StartEnd> = project.availabilitySlots.map((as) => ({
      start: moment(as.start),
      end: moment(as.end),
    }));

    // Convert the registration slots to moment dates
    const registrationSlots: Array<StartEnd> = registration.availabilitySlots.map((as) => ({
      start: moment(as.start),
      end: moment(as.end),
    }));

    // Get the earliest and latest presence time
    const registrationStart: moment.Moment = registrationSlots.reduce(
      (acc: any, slot) => (acc !== undefined ? moment.min(acc, slot.start) : slot.start),
      undefined
    );
    const registrationEnd: moment.Moment = registrationSlots.reduce(
      (accumulator: any, slot) =>
        accumulator !== undefined ? moment.max(accumulator, slot.end) : slot.end,
      undefined
    );

    // Total days of presence of the user, no matter the project dates
    const totalDaysOfPresenceForRegistration =
      registrationEnd.clone().startOf("day").diff(registrationStart.clone().startOf("day"), "day") +
      1;

    // Compute the real days of presence on site: iterate through each day and get data
    const daysOfPresenceOnSite: Array<StartEndWithConfirmLunch> = [];
    for (let dayIndex = 0; dayIndex < totalDaysOfPresenceForRegistration; dayIndex++) {
      // Create the current day object
      const currentDayStart = registrationStart.clone().startOf("day");
      currentDayStart.add(dayIndex, "days");
      const currentDayEnd = currentDayStart.clone().endOf("day");

      // Get user availabilitySlots for this day (even if they are on multiple days, if the current days is inside, get it)
      let registrationSlotsThisDay = registrationSlots.filter(
        (slot) =>
          currentDayStart.diff(slot.start.clone().startOf("day"), "days") >= 0 &&
          currentDayStart.diff(slot.end.clone().startOf("day"), "days") <= 0
      );

      // If no slots on this day, don't add this day to the days of presence, and skip it
      // If there are slots on this day, compute the breakfastTime, lunchTime and dinnerTime and all the rest
      if (registrationSlotsThisDay.length > 0) {
        // Truncate multi-days slots with the begging and the end of the current day
        registrationSlotsThisDay = registrationSlotsThisDay.map((slot) => ({
          start: moment.max(currentDayStart.clone(), slot.start),
          end: moment.min(currentDayEnd.clone(), slot.end),
        }));

        const [userEatsForBreakfastThisDay, userEatsForLunchThisDay, userEatsForDinnerThisDay] = [
          breakfastTime,
          lunchTime,
          dinnerTime,
        ].map((mealTime) => {
          // Build the real lunchTime and dinnerTime dates for this current day
          const mealTimeThisDay = moment(mealTime as any);
          mealTimeThisDay.year(currentDayStart.year());
          mealTimeThisDay.month(currentDayStart.month());
          mealTimeThisDay.date(currentDayStart.date());

          // If the project ends before or start after the meal times, then it means the user will not eat this meal
          const projectEndsBeforeOrStartsAfterMeal =
            projectEnd <= mealTimeThisDay || projectStart >= mealTimeThisDay;

          // Know if the user eats for lunch or dinner this current day
          const userEatsForMealThisDay =
            !projectEndsBeforeOrStartsAfterMeal &&
            !!registrationSlotsThisDay.find(
              (slot) => slot.start <= mealTimeThisDay && slot.end > mealTimeThisDay
            );

          return userEatsForMealThisDay;
        });

        const thereIsAProjectSlotOnThisDay = !!projectSlots.find(
          (slot) =>
            currentDayStart.diff(slot.start.startOf("day"), "days") >= 0 &&
            currentDayStart.diff(slot.end.startOf("day"), "days") <= 0
        );

        daysOfPresenceOnSite.push({
          plage: {
            start: registrationSlotsThisDay.reduce(
              (acc: any, slot) => (acc ? moment.min(acc, slot.start) : slot.start),
              undefined
            ),
            end: registrationSlotsThisDay.reduce(
              (acc: any, slot) => (acc ? moment.max(acc, slot.end) : slot.end),
              undefined
            ),
          },
          breakfast: userEatsForBreakfastThisDay,
          lunch: userEatsForLunchThisDay,
          dinner: userEatsForDinnerThisDay,
          projectDay: thereIsAProjectSlotOnThisDay,
        });
      }
    }

    // A day of presence is a day when the participant eats at least for lunch or for dinner during a project day.
    // A day of presence out of the project days is not counted
    registration.numberOfDaysOfPresence = daysOfPresenceOnSite.filter(
      (p) => (p.lunch || p.dinner) && p.projectDay
    ).length;
    registration.daysOfPresence = daysOfPresenceOnSite.map((d) => ({
      start: d.plage?.start?.toISOString(),
      end: d.plage?.end?.toISOString(),
      breakfast: d.breakfast,
      lunch: d.lunch,
      dinner: d.dinner,
      projectDay: d.projectDay,
    }));
  },
};

RegistrationSchema.plugin(softDelete);
RegistrationSchema.plugin(addDiffHistory, ["user"]);
RegistrationSchema.plugin(preventDuplicatesInProject, [{user: 1}]);

export const Registration = model<RegistrationD>("Registration", RegistrationSchema);

export const allowedBodyArgs = [
  "user",
  "project",
  "steward",
  "role",
  "booked",
  "specific",
  "sessionsSubscriptions",
  "teamsSubscriptions",
  "availabilitySlots",
  "hasCheckedIn",
  "notes",
  "tags",
  "hidden",
  "helloAssoTickets",
  "customTicketingTickets",
];
