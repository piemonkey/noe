import {Schema, Document, model, SchemaTypes} from "mongoose";
import {StewardD} from "./stewards";
import {PlaceD} from "./places";
import {SessionD} from "./sessions";
import {
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "../config/mongooseMiddlewares";

// See https://mongoosejs.com/docs/typescript.html#creating-your-first-document
// and https://stackoverflow.com/questions/34482136/mongoose-the-typescript-way/36661990
// (D like Document)

export type SlotD = Document & {
  duration: number;
  start: Date;
  end: Date;
  stewardsSessionSynchro: boolean;
  placesSessionSynchro: boolean;
  session: SessionD;
  stewards: Array<StewardD>;
  places: Array<PlaceD>;

  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
};

const SlotSchema = new Schema<SlotD>(
  {
    duration: Number,
    stewardsSessionSynchro: Boolean,
    placesSessionSynchro: Boolean,
    stewards: [{type: SchemaTypes.ObjectId, ref: "Steward"}],
    places: [{type: SchemaTypes.ObjectId, ref: "Place"}],

    start: Date,
    end: Date,

    createdAt: Date,
    updatedAt: Date,
    deletedAt: Date,
  },
  {timestamps: true, toJSON: {virtuals: true}, toObject: {virtuals: true}}
);

SlotSchema.virtual("session", {
  ref: "Session",
  localField: "_id",
  foreignField: "slots",
  justOne: true,
});

SlotSchema.plugin(softDelete);
SlotSchema.plugin(indexByIdAndProject);
SlotSchema.plugin(preventDuplicatesInProject);

export const Slot = model<SlotD>("Slot", SlotSchema);
