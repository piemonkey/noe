import {Document, EnforceDocument, Model, model, QueryWithHelpers, Schema} from "mongoose";
import {FormMapping, RegistrationD} from "./registrations";
import {AvailabilitySlotD, AvailabilitySlotSchema} from "./availabilitySlots";
import fetch from "node-fetch";
import AbortController from "abort-controller";
import {addDiffHistory, softDelete} from "../config/mongooseMiddlewares";
import defaultWelcomePageContent from "../config/defaultWelcomePageContent";
import momentTz = require("moment-timezone");
import {logger} from "../services/logger";
import {MongoEncrypted} from "../utilities/crypto";
import {isValidObjectId} from "../utilities/controllersUtilities";
const mongooseSlugPlugin = require("mongoose-slug-plugin");

const momentWithTz = momentTz.tz;

// See https://mongoosejs.com/docs/typescript.html#creating-your-first-document
// and https://stackoverflow.com/questions/34482136/mongoose-the-typescript-way/36661990
// (D like Document)

export interface HelloAssoConfig {
  clientId: string;
  clientSecret: string;
  selectedEvent?: string;
  token?: string;
  tokenExpirationDate?: Date;
  refreshToken?: string;
  organizationSlug?: string;
  selectedEventCandidates?: Array<any>;
}
const HelloAssoConfigSchema = new Schema<HelloAssoConfig>({
  clientId: String,
  clientSecret: String,
  selectedEvent: String,
  token: String,
  tokenExpirationDate: Date,
  refreshToken: String,
  organizationSlug: String,
  selectedEventCandidates: [Object],
});

export interface TiBilletConfig {
  serverUrl: string;
  eventSlug: string;
  apiKey: string;
}
const TiBilletConfigSchema = new Schema<TiBilletConfig>(
  {
    serverUrl: String,
    eventSlug: String,
    apiKey: MongoEncrypted,
  },
  {toObject: {getters: true}, toJSON: {getters: true}}
);

export interface ProjectTheme {
  bg?: string;
  accent1?: string;
  accent2?: string;
  imageUrl?: string;
}
const ProjectThemeSchema = new Schema<ProjectTheme>({
  bg: String,
  accent1: String,
  accent2: String,
  imageUrl: String,
});

export type ProjectD = Document & {
  name: string;
  slug: string;

  // Form data
  formComponents: Object;
  // Columns we want to map so they are displayed in the participant list
  formMapping: Array<FormMapping>;
  // Welcome page content
  content: Object;

  // Project opening times
  start: string;
  end: string;
  availabilitySlots: Array<AvailabilitySlotD>;

  // Project opening state management
  openingState: string;
  isPublic: boolean;
  blockSubscriptions: boolean;
  full: boolean;
  secretSchedule: boolean;

  // Enable features in project
  usePlaces: boolean;
  useTeams: boolean;
  useAI: boolean;

  // Ticketing
  // Mode chosen for the ticketing. If no mode is chosen, it means that
  // the ticketing is not mandatory to register, and is deactivated
  ticketingMode?: string;
  customTicketing?: string; // Validation URL to validate ticket numbers
  helloAsso?: HelloAssoConfig; // Hello Asso crendentials
  tiBillet?: TiBilletConfig;
  getHelloAssoAuthTokens: () => void;
  refreshHelloAssoAuthToken: () => void;
  populateHelloAssoEventCandidates: () => void;

  // Volunteering
  minMaxVolunteering: Array<number>;
  blockVolunteeringUnsubscribeIfBeginsSoon: boolean;

  // Orga notes
  notes: string;

  // Allow time overlap when subscribing to sessions
  notAllowOverlap: boolean;

  // Meal times
  breakfastTime: Date;
  lunchTime: Date;
  dinnerTime: Date;

  // Customization
  theme: ProjectTheme;

  deletedAt: Date;
  registrations: Array<RegistrationD>;
  hideSensitiveFields: (userRegistration: RegistrationD) => void;
};

interface ProjectModel extends Model<ProjectD> {
  findByIdOrSlug(
    id: any,
    ...args: any[]
  ): QueryWithHelpers<
    EnforceDocument<ProjectD, {}> | null,
    EnforceDocument<ProjectD, {}>,
    {},
    ProjectD
  >;
}

const ProjectSchema = new Schema<ProjectD, ProjectModel>(
  {
    name: {type: String, required: true},
    slug: {type: String, required: true},

    formComponents: Object,
    formMapping: [Object],
    content: {type: Object, default: defaultWelcomePageContent},

    start: String,
    end: String,
    availabilitySlots: [AvailabilitySlotSchema],

    openingState: {
      type: String,
      required: true,
      enum: ["notOpened", "preRegisterOnly", "registerForStewardsOnly", "registerForAll"],
      default: "notOpened",
    },
    isPublic: {type: Boolean, required: true, default: false},
    blockSubscriptions: {type: Boolean, required: true, default: false},
    full: {type: Boolean, required: true, default: false},
    secretSchedule: {type: Boolean, required: true, default: false},

    usePlaces: {type: Boolean, default: true},
    useTeams: {type: Boolean, default: false},
    useAI: {type: Boolean, default: false},

    ticketingMode: {
      type: String,
      enum: [null, "helloAsso", "customTicketing", "tiBillet"],
      default: null,
    },
    helloAsso: HelloAssoConfigSchema,
    tiBillet: TiBilletConfigSchema,
    customTicketing: String,

    minMaxVolunteering: {type: Array, default: [60, 120]},
    blockVolunteeringUnsubscribeIfBeginsSoon: {type: Boolean, default: false},

    notes: String,

    notAllowOverlap: {type: Boolean, required: true, default: false},

    breakfastTime: {
      type: Date,
      required: true,
      default: momentWithTz("2000-01-01T08:30:00", "Europe/Paris"),
    },
    lunchTime: {
      type: Date,
      required: true,
      default: momentWithTz("2000-01-01T12:00:00", "Europe/Paris"),
    },
    dinnerTime: {
      type: Date,
      required: true,
      default: momentWithTz("2000-01-01T19:30:00", "Europe/Paris"),
    },

    // Customization
    theme: ProjectThemeSchema,

    deletedAt: Date,
  },
  {
    timestamps: true,
    toJSON: {virtuals: true, getters: true},
    toObject: {virtuals: true, getters: true},
  }
);

ProjectSchema.virtual("registrations", {
  ref: "Registration",
  localField: "_id",
  foreignField: "project",
});

ProjectSchema.static("findByIdOrSlug", function findByIdOrSlug(id, ...args) {
  const conditions = isValidObjectId(id)
    ? {$or: [{slug: id}, {_id: id}, {slug_history: id}]}
    : {$or: [{slug: id}, {slug_history: id}]};
  return Project.findOne(conditions, ...args);
});

ProjectSchema.methods = {
  async getHelloAssoAuthTokens() {
    const project = this as ProjectD;

    if (project.helloAsso?.clientId && project.helloAsso?.clientSecret) {
      try {
        const params = new URLSearchParams();
        params.append("grant_type", "client_credentials");
        params.append("client_id", project.helloAsso?.clientId);
        params.append("client_secret", project.helloAsso?.clientSecret);

        const response = await fetch("https://api.helloasso.com/oauth2/token", {
          method: "POST",
          body: params,
        } as any);
        const authResult = await response.json();

        project.helloAsso.token = authResult.access_token;
        project.helloAsso.refreshToken = authResult.refresh_token;
        project.helloAsso.tokenExpirationDate = authResult.expires_in
          ? new Date(new Date().getTime() + authResult.expires_in * 1000)
          : undefined; // Set date at expiration time

        const organisationResponse = await fetch(
          "https://api.helloasso.com/v5/users/me/organizations",
          {
            headers: {Authorization: `Bearer ${authResult.access_token}`},
          }
        );
        if (organisationResponse.status === 200) {
          const organisationObject = await organisationResponse.json();
          project.helloAsso.organizationSlug = organisationObject[0].organizationSlug;
        } else {
          // If error, reset the Hello Asso stuff
          project.helloAsso.organizationSlug = undefined;
          project.helloAsso.selectedEvent = undefined;
          project.helloAsso.selectedEventCandidates = undefined;
        }
      } catch (error) {
        logger.error(error);
      }
    }
  },

  async refreshHelloAssoAuthToken() {
    const project = this as ProjectD;
    if (
      project.helloAsso?.clientId &&
      project.helloAsso?.clientSecret &&
      project.helloAsso?.refreshToken &&
      (!project.helloAsso?.tokenExpirationDate ||
        project.helloAsso?.tokenExpirationDate?.getTime() < new Date().getTime()) // if token has expired only, refresh the token, or if the expiration date doesn't exist
    ) {
      try {
        const params = new URLSearchParams();
        params.append("grant_type", "refresh_token");
        params.append("client_id", project.helloAsso.clientId);
        params.append("refresh_token", project.helloAsso.refreshToken);

        const response = await fetch("https://api.helloasso.com/oauth2/token", {
          method: "POST",
          body: params,
        } as any);
        const authResult = await response.json();

        project.helloAsso.token = authResult.access_token;
        project.helloAsso.refreshToken = authResult.refresh_token;
        project.helloAsso.tokenExpirationDate = authResult.expires_in
          ? new Date(new Date().getTime() + authResult.expires_in * 1000)
          : undefined; // Set date at expiration time

        await project.save({timestamps: false});
      } catch (error) {
        logger.error(error);
      }
    }
  },

  async populateHelloAssoEventCandidates() {
    const project = this as ProjectD;
    if (project.helloAsso?.token && project.helloAsso?.organizationSlug) {
      try {
        const controller = new AbortController();
        const timeout = setTimeout(() => {
          controller.abort();
        }, 10000);

        const response = await fetch(
          `https://api.helloasso.com/v5/organizations/${project.helloAsso.organizationSlug}/forms?formTypes=Event`,
          {
            headers: {Authorization: `Bearer ${project.helloAsso.token}`},
            signal: controller.signal,
          }
        );
        const events = await response.json();

        clearTimeout(timeout);
        project.helloAsso.selectedEventCandidates = events.data;
      } catch (error) {
        logger.error(error);
      }
    }
  },

  async hideSensitiveFields(userRegistration: RegistrationD) {
    const project = this as any;

    if (!userRegistration?.role) project.customTicketing = undefined;

    if (project.helloAsso) {
      if (!userRegistration?.role) {
        project.helloAsso.clientSecret = undefined;
      }
      project.helloAsso.token = undefined;
      project.helloAsso.tokenExpirationDate = undefined;
      project.helloAsso.refreshToken = undefined;
    }
  },
};

ProjectSchema.plugin(softDelete);
ProjectSchema.plugin(addDiffHistory, ["helloAsso", "registrations"]);
ProjectSchema.plugin(mongooseSlugPlugin, {tmpl: "<%=name%>"});
ProjectSchema.index({name: 1, deletedAt: 1}, {unique: true}); // Prevent projects with the same name

export const Project = model<ProjectD, ProjectModel>("Project", ProjectSchema);
