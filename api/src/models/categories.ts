import {Schema, Document, model, SchemaTypes} from "mongoose";
import {ProjectD} from "./projects";
import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "../config/mongooseMiddlewares";

// See https://mongoosejs.com/docs/typescript.html#creating-your-first-document
// and https://stackoverflow.com/questions/34482136/mongoose-the-typescript-way/36661990
// (D like Document)

export type CategoryD = Document & {
  name: string;
  project: ProjectD;
  color: string;
  summary: string;

  deletedAt: Date;
  importedId: string;
};

const CategorySchema = new Schema<CategoryD>(
  {
    name: {type: String, required: true},
    color: String,
    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    summary: String,

    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true}
);

CategorySchema.plugin(softDelete);
CategorySchema.plugin(addDiffHistory);
CategorySchema.plugin(indexByIdAndProject);
CategorySchema.plugin(preventDuplicatesInProject, [{name: 1}]);

export const Category = model<CategoryD>("Category", CategorySchema);

export const schemaDescription = [
  {key: "name", label: "Nom de la catégorie", noGroupEditing: true},
  {key: "color", label: "Couleur"},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "summary", label: "Résumé"},
];
export const allowedBodyArgs = Object.values(schemaDescription.map((field) => field.key));
