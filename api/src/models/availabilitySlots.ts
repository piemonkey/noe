import {Schema, Document} from "mongoose";

// See https://mongoosejs.com/docs/typescript.html#creating-your-first-document
// and https://stackoverflow.com/questions/34482136/mongoose-the-typescript-way/36661990
// (D like Document)

export type AvailabilitySlotD = Document & {
  start: Date;
  end: Date;
};

export const AvailabilitySlotSchema = new Schema<AvailabilitySlotD>({
  start: {type: Date, required: true},
  end: {type: Date, required: true},
});
