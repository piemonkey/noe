import {Schema, Document, model, SchemaTypes} from "mongoose";
import {ProjectD} from "./projects";
import {addDiffHistory} from "../config/mongooseMiddlewares";

export enum ComputationState {
  WAITING = "WAITING",
  WORKING = "WORKING",
  SUCCESS = "SUCCESS",
  FAILED = "FAILED",
  CANCELLED = "CANCELLED",
}

// See https://mongoosejs.com/docs/typescript.html#creating-your-first-document
// and https://stackoverflow.com/questions/34482136/mongoose-the-typescript-way/36661990
// (D like Document)

export type ComputationD = Document & {
  project: ProjectD;
  state: ComputationState;
  startedAt: Date;
  result: any;
};

const ComputationSchema = new Schema<ComputationD>(
  {
    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    state: {type: String, required: true},
    deletedAt: Date,
    result: SchemaTypes.Mixed,
  },
  {timestamps: true}
);

ComputationSchema.plugin(addDiffHistory);

export const Computation = model<ComputationD>("Computation", ComputationSchema);
