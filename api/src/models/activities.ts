import {Schema, Document, model, SchemaTypes} from "mongoose";
import {StewardD} from "./stewards";
import {PlaceD} from "./places";
import {ProjectD} from "./projects";
import {CategoryD} from "./categories";
import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "../config/mongooseMiddlewares";

// See https://mongoosejs.com/docs/typescript.html#creating-your-first-document
// and https://stackoverflow.com/questions/34482136/mongoose-the-typescript-way/36661990
// (D like Document)

export type ActivityD = Document & {
  name: string;
  stewards: Array<StewardD>;
  places: Array<PlaceD>;
  category: CategoryD;
  secondaryCategories: Array<string>;
  summary: string;
  description: string;
  notes: string;
  tags: Array<string>;
  project: ProjectD;
  slots: Array<{duration: number}>;
  numberOfSessions: number;
  nonBlockingActivity: boolean;
  allowSameTime: boolean;
  maxNumberOfParticipants: number;
  volunteeringCoefficient: number;
  stewardVolunteeringCoefficient: number;

  minNumberOfStewards: number;
  deletedAt: Date;
  importedId: string;
};

const ActivitySchema = new Schema<ActivityD>(
  {
    name: String,
    stewards: [{type: SchemaTypes.ObjectId, ref: "Steward"}],
    places: [{type: SchemaTypes.ObjectId, ref: "Place"}],
    category: {type: SchemaTypes.ObjectId, ref: "Category"},
    secondaryCategories: [String],
    summary: String,
    description: String,
    notes: String,
    tags: [String],
    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    slots: [{duration: Number}],
    numberOfSessions: Number,
    nonBlockingActivity: {type: Boolean, default: false},
    allowSameTime: {type: Boolean, default: false},
    maxNumberOfParticipants: Number,
    minNumberOfStewards: Number,
    volunteeringCoefficient: Number,
    stewardVolunteeringCoefficient: {type: Number, required: false, default: 1.5},

    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true}
);

ActivitySchema.plugin(softDelete);
ActivitySchema.plugin(addDiffHistory);
ActivitySchema.plugin(indexByIdAndProject);
ActivitySchema.plugin(preventDuplicatesInProject, [{name: 1}]);

export const Activity = model<ActivityD>("Activity", ActivitySchema);

export const schemaDescription = [
  {key: "name", label: "Nom de l'activité", noGroupEditing: true},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "stewards", label: "Encadrant⋅es éligibles"},
  {key: "places", label: "Espaces éligibles", usePlaces: true},
  {key: "slots", label: "Plages"},
  {key: "category", label: "Catégorie"},
  {key: "secondaryCategories", label: "Catégories secondaires"},
  {key: "summary", label: "Résumé"},
  {key: "description", label: "Description détaillée"},
  {key: "notes", label: "Notes privées pour les orgas"},
  {key: "tags", label: "Tags"},
  {key: "maxNumberOfParticipants", label: "Nombre maximum de participant⋅es"},
  {key: "volunteeringCoefficient", label: "Coefficient de bénévolat"},
  {key: "stewardVolunteeringCoefficient", label: "Coefficient de bénévolat encadrant⋅es"},
  {key: "allowSameTime", label: "Autoriser les inscriptions en même temps"},
  {key: "numberOfSessions", label: "Nombre de sessions voulues", useAI: true},
  {key: "minNumberOfStewards", label: "Nombre minimum d'encadrant⋅es", useAI: true},
  {key: "nonBlockingActivity", label: "Ne pas bloquer les espaces", useAI: true},
];
export const allowedBodyArgs = Object.values(schemaDescription.map((field) => field.key));
