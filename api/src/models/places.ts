import {Schema, Document, model, SchemaTypes} from "mongoose";
import {ProjectD} from "./projects";
import {AvailabilitySlotSchema, AvailabilitySlotD} from "./availabilitySlots";
import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "../config/mongooseMiddlewares";

// See https://mongoosejs.com/docs/typescript.html#creating-your-first-document
// and https://stackoverflow.com/questions/34482136/mongoose-the-typescript-way/36661990
// (D like Document)

export type PlaceD = Document & {
  name: string;
  project: ProjectD;
  summary: string;
  notes: string;
  availabilitySlots: Array<AvailabilitySlotD>;
  maxNumberOfParticipants: number;

  deletedAt: Date;
  importedId: string;
};

const PlaceSchema = new Schema<PlaceD>(
  {
    name: {type: String, required: true},
    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    summary: String,
    notes: String,
    availabilitySlots: [AvailabilitySlotSchema],
    maxNumberOfParticipants: Number,

    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true}
);

PlaceSchema.plugin(softDelete);
PlaceSchema.plugin(addDiffHistory);
PlaceSchema.plugin(indexByIdAndProject);
PlaceSchema.plugin(preventDuplicatesInProject, [{name: 1}]);

export const Place = model<PlaceD>("Place", PlaceSchema);

export const schemaDescription = [
  {key: "name", label: "Nom de l'espace", noGroupEditing: true},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "summary", label: "Informations"},
  {key: "maxNumberOfParticipants", label: "Nombre maximum de personnes"},
  {key: "notes", label: "Notes privées pour les orgas"},
  {key: "availabilitySlots", label: "Disponibilités"},
];
export const allowedBodyArgs = Object.values(schemaDescription.map((field) => field.key));
