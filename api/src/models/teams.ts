import {Schema, Document, model, SchemaTypes} from "mongoose";
import {ProjectD} from "./projects";
import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "../config/mongooseMiddlewares";
import {ActivityD} from "./activities";

// See https://mongoosejs.com/docs/typescript.html#creating-your-first-document
// and https://stackoverflow.com/questions/34482136/mongoose-the-typescript-way/36661990
// (D like Document)

export type TeamD = Document & {
  name: string;
  summary: string;
  description: string;
  notes: string;
  activity: ActivityD;
  project: ProjectD;

  deletedAt: Date;
  importedId: string;
};

const TeamSchema = new Schema<TeamD>(
  {
    name: String,
    summary: String,
    description: String,
    notes: String,
    activity: {type: SchemaTypes.ObjectId, ref: "Activity"},
    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},

    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true}
);

TeamSchema.plugin(softDelete);
TeamSchema.plugin(addDiffHistory);
TeamSchema.plugin(indexByIdAndProject);
TeamSchema.plugin(preventDuplicatesInProject, [{name: 1, activity: 1}]); // We can have same names but not if the linked activity is the same

export const Team = model<TeamD>("Team", TeamSchema);

export const schemaDescription = [
  {key: "name", label: "Nom de l'équipe"},
  {key: "activity", label: "Activité liée"},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "summary", label: "Informations"},
  {key: "description", label: "Description détaillée"},
  {key: "notes", label: "Notes privées pour les orgas"},

  // Those are not proper arguments, but can be given and will modify the appropriate objects accordingly
  {key: "sessions", label: "Sessions"},
  {key: "registrations", label: "Participant⋅es"},
];
export const allowedBodyArgs = Object.values(schemaDescription.map((field) => field.key));
