import {Schema, Document, model, SchemaTypes} from "mongoose";
import {ProjectD} from "./projects";
import {UserD} from "./users";
import {AvailabilitySlotSchema, AvailabilitySlotD} from "./availabilitySlots";
import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "../config/mongooseMiddlewares";

// See https://mongoosejs.com/docs/typescript.html#creating-your-first-document
// and https://stackoverflow.com/questions/34482136/mongoose-the-typescript-way/36661990
// (D like Document)

export type StewardD = Document & {
  firstName: string;
  lastName: string;
  phoneNumber: string;
  summary: string;
  notes: string;
  project: ProjectD;
  availabilitySlots: Array<AvailabilitySlotD>;

  deletedAt: Date;
  importedId: string;
};

const StewardSchema = new Schema<StewardD>(
  {
    firstName: {type: String, required: true},
    lastName: String,
    phoneNumber: String,
    summary: String,
    notes: String,

    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    availabilitySlots: [AvailabilitySlotSchema],

    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true}
);

StewardSchema.plugin(softDelete);
StewardSchema.plugin(addDiffHistory);
StewardSchema.plugin(indexByIdAndProject);
StewardSchema.plugin(preventDuplicatesInProject, [{firstName: 1, lastName: 1}]); // Can't have two stewars with same name and last name in the same project

export const Steward = model<StewardD>("Steward", StewardSchema);

export const schemaDescription = [
  {key: "firstName", label: "Prénom", noGroupEditing: true},
  {key: "lastName", label: "Nom", noGroupEditing: true},
  {key: "phoneNumber", label: "Téléphone"},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "summary", label: "Résumé"},
  {key: "notes", label: "Notes privées pour les orgas"},
  {key: "availabilitySlots", label: "Disponibilités"},
];
export const allowedBodyArgs = Object.values(schemaDescription.map((field) => field.key));
