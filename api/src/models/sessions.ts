import {Schema, Document, model, SchemaTypes} from "mongoose";
import {UserD} from "./users";
import {Registration, RegistrationD} from "./registrations";
import {StewardD} from "./stewards";
import {ActivityD} from "./activities";
import {PlaceD} from "./places";
import {ProjectD} from "./projects";
import {Slot, SlotD} from "./slots";
import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "../config/mongooseMiddlewares";
import {TeamD} from "./teams";

// See https://mongoosejs.com/docs/typescript.html#creating-your-first-document
// and https://stackoverflow.com/questions/34482136/mongoose-the-typescript-way/36661990
// (D like Document)

export type SessionD = Document & {
  name: string;
  start: Date;
  end: Date;

  notes: string;
  tags: Array<string>;
  maxNumberOfParticipants: number;
  volunteeringCoefficient: number;
  stewards: Array<StewardD>;
  places: Array<PlaceD>;
  activity: ActivityD;
  team: TeamD;
  project: ProjectD;
  slots: Array<SlotD>;

  isScheduleFrozen: boolean;

  busyVolume: number;

  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  importedId: string;

  //*** Fields calculated on demand: they do not always exist and are not stored in database ***//

  participants: Array<RegistrationD>;
  numberParticipants: number;
  populateParticipants: (includeParticipants: boolean) => Promise<void>;

  computedMaxNumberOfParticipants: number;

  sameTimeSessions: Array<SessionD>;
  computeSameTimeSessions: (
    registrationModel?: RegistrationD,
    allProjectSlots?: Array<Object>
  ) => Promise<void>;

  // Utility functions
  conflictsWith: (otherSession: SessionD) => boolean;
};

const SessionSchema = new Schema<SessionD>(
  {
    name: String,
    start: {type: Date, required: false},
    end: {type: Date, required: false},
    notes: String,
    tags: [String],
    maxNumberOfParticipants: Number,
    volunteeringCoefficient: {type: Number, required: false},
    stewards: [{type: SchemaTypes.ObjectId, ref: "Steward"}],
    places: [{type: SchemaTypes.ObjectId, ref: "Place"}],
    project: {type: SchemaTypes.ObjectId, ref: "Project"},
    activity: {type: SchemaTypes.ObjectId, ref: "Activity"},
    team: {type: SchemaTypes.ObjectId, ref: "Team"},
    slots: [{type: SchemaTypes.ObjectId, ref: "Slot"}],
    isScheduleFrozen: {type: Boolean, default: false},

    createdAt: Date,
    updatedAt: Date,
    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true, toObject: {virtuals: true}, toJSON: {virtuals: true}}
);

// This function is duplicated in orga-front/src/helpers/sessionUtilities.js
export const getMaxParticipantsBasedOnPlaces = (session: SessionD) =>
  session.places?.length > 0
    ? session?.places?.reduce((acc, place) => acc + (place.maxNumberOfParticipants || 0), 0)
    : Infinity; // Warning: Infinity is transmitted as "null" in JSON, so if it's "null" in the frontends, it's like infinity

export const getMaxParticipants = (session: SessionD): number | undefined => {
  // Compute the jauge based on th session places
  const sessionMax = session.maxNumberOfParticipants;
  const activityMax = session.activity?.maxNumberOfParticipants;

  const maxParticipantsBasedOnPlaces = getMaxParticipantsBasedOnPlaces(session);
  if (sessionMax !== undefined && sessionMax !== null) {
    return maxParticipantsBasedOnPlaces !== undefined
      ? Math.min(sessionMax, maxParticipantsBasedOnPlaces)
      : sessionMax;
  } else if (activityMax !== undefined && activityMax !== null) {
    return maxParticipantsBasedOnPlaces !== undefined
      ? Math.min(activityMax, maxParticipantsBasedOnPlaces)
      : activityMax;
  } else {
    return maxParticipantsBasedOnPlaces;
  }
};

// The max number of participants computed based on the places, the activity and the session data
SessionSchema.virtual("computedMaxNumberOfParticipants").get(function (): number {
  return getMaxParticipants(this);
});

export const getSessionVolunteeringCoefficient = (session: SessionD): number =>
  session.volunteeringCoefficient || session.volunteeringCoefficient === 0
    ? session.volunteeringCoefficient
    : session.activity.volunteeringCoefficient || 0;

export const getAllSessionsSlotsInProject = async (projectId: string): Promise<Array<Object>> => {
  // @ts-ignore
  const allSessions = await Session.find({project: {_id: projectId}})
    .select({_id: 1})
    .populate([
      {path: "slots", select: {start: 1, end: 1, stewards: 1}},
      {path: "activity", select: {name: 1, allowSameTime: 1}},
    ]);
  const allSessionsSlotsInfo = allSessions
    .map((session) =>
      session.slots.map((slot) => ({
        session: {
          _id: session._id,
          activity: session.activity,
        },
        start: slot.start,
        end: slot.end,
        stewards: slot.stewards,
        _id: slot._id,
      }))
    )
    .flat();
  return allSessionsSlotsInfo;
};

SessionSchema.methods = {
  /**
   * Populates the session with its subscribed users, and adds also the number of participants
   * @param includeParticipants if true, include the participants, if false don't, and just count the
   * number of participants without fetching the data.
   */
  async populateParticipants(includeParticipants = true): Promise<void> {
    const session = this as SessionD;

    if (includeParticipants) {
      // If we include participants, fetch them entirely
      session.participants = await Registration.find(
        {sessionsSubscriptions: {$elemMatch: {session: session._id}}} as any,
        "tags teamsSubscriptions sessionsSubscriptions specific"
      ).populate([
        {path: "user", select: "firstName lastName email"},
        {path: "teamsSubscriptions.team", select: "name"},
      ]);
      session.numberParticipants = session.participants.length;
    } else {
      // Else, just count the project registrations, that's enough
      session.numberParticipants = await Registration.countDocuments({
        sessionsSubscriptions: {$elemMatch: {session: session._id}},
      } as any);
    }
  },
  /**
   * Computes the sessions that occur in the same time asa the current session. It will be useful so we can then
   * calculate in the frontends which session is potentially in conflict with another
   * @param registration the current registration, containing a list of subscribed sessions by a user
   * @param allSessionsSlots the project slots. They are given for performance improvement reasons, but can
   * also be calculated if not given.
   */
  async computeSameTimeSessions(
    registration?: RegistrationD,
    allSessionsSlots?: Array<Object>
  ): Promise<void> {
    const session = this as SessionD;

    // If not given, calculate the project slots
    if (!allSessionsSlots) {
      const projectId = session.project?._id || session.project;
      allSessionsSlots = await getAllSessionsSlotsInProject(projectId.toString());
    }

    //enlever les slots de la session
    const sessionsSlots = allSessionsSlots.filter(
      (s: any) => s.session._id.toString() !== session._id.toString()
    );

    const sameTimeSlots = session.slots
      .map((currentSlot) => {
        const conflictSlots = sessionsSlots.filter(
          (slot: any) =>
            slot.session.activity.allowSameTime !== true &&
            slot.start < currentSlot.end &&
            slot.end > currentSlot.start &&
            slot._id != currentSlot._id
        );
        return conflictSlots.map((conflictSlot: any) => ({
          sameTimeSlot: conflictSlot,
          slot: currentSlot,
        }));
      })
      .flat();

    const sameTimeSessions = sameTimeSlots.reduce((acc: any, currentValue) => {
      const injection = {
        sameTimeSession: currentValue.sameTimeSlot.session,
        sameTimeSlot: {
          _id: currentValue.sameTimeSlot._id,
          start: currentValue.sameTimeSlot.start,
          end: currentValue.sameTimeSlot.end,
          stewards: currentValue.sameTimeSlot.stewards,
        },
        sessionSlot: {
          _id: currentValue.slot._id,
          start: currentValue.slot.start,
          end: currentValue.slot.end,
        },
      };

      const existingSession = acc.find(
        (session: any) => session.sameTimeSession._id == injection.sameTimeSession._id
      );
      if (existingSession) {
        existingSession.sameTimeSlots.push({
          sameTimeSlot: injection.sameTimeSlot,
          sessionSlot: injection.sessionSlot,
        });
      } else {
        acc.push({
          sameTimeSession: injection.sameTimeSession,
          sameTimeSessionRegistered: registration
            ? registration.sessionsSubscriptions
                .map((ss) => ss.session)
                .includes(injection.sameTimeSession._id.toString())
            : undefined,
          sameTimeSessionSteward: registration?.steward
            ? injection.sameTimeSlot.stewards.includes(registration.steward._id.toString())
            : undefined,
          sameTimeSlots: [
            {sameTimeSlot: injection.sameTimeSlot, sessionSlot: injection.sessionSlot},
          ],
        });
      }
      return acc;
    }, []);

    session.sameTimeSessions = sameTimeSessions;
  },
};

SessionSchema.plugin(softDelete);
SessionSchema.plugin(addDiffHistory);
SessionSchema.plugin(indexByIdAndProject);
SessionSchema.plugin(preventDuplicatesInProject);

export const Session = model<SessionD>("Session", SessionSchema);

export const schemaDescription = [
  {key: "name", label: "Nom de la session"},
  {key: "start", label: "Début", noGroupEditing: true},
  {key: "end", label: "Fin", noGroupEditing: true},
  {key: "notes", label: "Notes privées pour les orgas"},
  {key: "tags", label: "Tags"},
  {key: "maxNumberOfParticipants", label: "Nombre maximum de participant⋅es"},
  {key: "volunteeringCoefficient", label: "Coefficient de bénévolat"},
  {key: "stewards", label: "Encadrant⋅es"},
  {key: "places", label: "Espaces", usePlaces: true},
  {key: "activity", label: "Activité"},
  {key: "team", label: "Équipe", useTeam: true},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "slots", label: "Plages"},
  {key: "registrations", label: "Participant⋅es"},
  {key: "isScheduleFrozen", label: "Session figée", useAI: true},
];

export const allowedBodyArgs = Object.values(schemaDescription.map((field) => field.key));
