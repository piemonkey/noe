import * as bcryptjs from "bcryptjs";
import * as uuid from "uuid";
import {Schema, Document, model} from "mongoose";
import {RegistrationD} from "./registrations";
import {softDelete} from "../config/mongooseMiddlewares";

// See https://mongoosejs.com/docs/typescript.html#creating-your-first-document
// and https://stackoverflow.com/questions/34482136/mongoose-the-typescript-way/36661990
// (D like Document)

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    interface User extends UserD {}

    export interface Request {
      authenticatedUser: User;
    }
  }
}

export type UserD = Document & {
  firstName: string;
  lastName: string;
  email: string;

  password?: string;
  passwordResetToken: string;
  passwordResetExpires: Date;

  xsrfToken: string;

  superAdmin: boolean;

  locale: string;

  deletedAt: Date;

  // Virtuals
  registration: RegistrationD;
  registrations: Array<RegistrationD>;

  createdAt: Date;
  updatedAt: Date;
};

const UserSchema = new Schema<UserD>(
  {
    firstName: String,
    lastName: String,
    email: {type: String, required: true},

    password: {type: String, required: false, select: false}, // Don't select the password when getting a user
    passwordResetToken: String,
    passwordResetExpires: Date,

    xsrfToken: {
      type: String,
      required: true,
      default: function () {
        return uuid.v4();
      },
    },

    superAdmin: Boolean,

    locale: String,

    deletedAt: Date,
  },
  {
    timestamps: true,
    toObject: {virtuals: true},
    toJSON: {virtuals: true},
  }
);

// Returns the project registration when there a unique project registration present and populated
UserSchema.virtual("registration").get(function () {
  return this.registrations?.length === 1 ? this.registrations[0] : undefined;
});

UserSchema.virtual("registrations", {ref: "Registration", localField: "_id", foreignField: "user"});

// On save, hash the password if it has changed
UserSchema.pre(["save", "updateOne"], async function (next) {
  if (!this.isModified("password")) return next();

  try {
    const salt = await bcryptjs.genSalt(10);
    this.password = await bcryptjs.hash(this.password, salt);
    return next();
  } catch (e: any) {
    return next(e);
  }
});

UserSchema.plugin(softDelete);
UserSchema.index({deletedAt: 1, email: 1}, {unique: true, collation: {locale: "en", strength: 2}});

export const User = model<UserD>("User", UserSchema);

export const allowedBodyArgs = [
  "firstName",
  "lastName",
  "email",
  "password",
  "oldPassword",
  "locale",
];
