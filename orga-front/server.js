const express = require("express");
const i18n = require("i18next");
const i18nextMiddleware = require("i18next-http-middleware");
const app = express();
const fetch = require("node-fetch");
const path = require("path");
const PORT = process.env.PORT;
const fs = require("fs");

/*
  I18N SETUP
*/

const i18nParams = {
  resources: {
    fr: {
      common: {
        default: {
          title: "NOÉ - ORGA",
          description: "Organisez votre événement participatif en un clic avec NOÉ.",
        },
        project: {
          title: "{{projectName}} - NOÉ - ORGA",
          description: "Organisez votre événement {{projectName}} en un clic avec NOÉ.",
        },
        element: {
          title: "{{elementName}} - {{projectName}} - NOÉ - ORGA",
          description: "Organisez votre événement {{projectName}} en un clic avec NOÉ.",
        },
      },
    },
    en: {
      common: {
        default: {
          title: "NOE - ORGA",
          description: "Organize your next participative event in a glimpse, with NOE.",
        },
        project: {
          title: "{{projectName}} - NOE - ORGA",
          description: "Organize your event {{projectName}} in a glimpse, with NOE.",
        },
        element: {
          title: "{{elementName}} - {{projectName}} - NOE - ORGA",
          description: "Organize your event {{projectName}} in a glimpse, with NOE.",
        },
      },
    },
  },
  fallbackLng: "fr",
  defaultNS: "common",
  preload: ["en", "fr"],
};
const defaultTags = i18nParams.resources[i18nParams.fallbackLng][i18nParams.defaultNS].default;

i18n.use(i18nextMiddleware.LanguageDetector).init(i18nParams);
app.use(i18nextMiddleware.handle(i18n));

/*
  EXPRESS SETUP
*/

// Static resources should just be served as they are
app.use(express.static(path.resolve(__dirname, "build"), {maxAge: "30d"}));

// Don't know why but the app is still always querying this image no matter what. So just dont give it.
app.get("/images/react.png", (req, res) => res.sendStatus(200));

// Set up the server to listen on the right port
app.listen(PORT, (error) => {
  if (error) return console.log("Error during app startup", error);
  console.log("Dynamic server listening on " + PORT + "...");
});

/*
  COMMON INDEX.HTML BUILD FUNCTION
*/
// Generic function to build the index files
const indexPath = path.resolve(__dirname, "build", "index.html");
const buildIndexFile = (req, res, type, values) => {
  fs.readFile(indexPath, "utf8", (err, htmlData) => {
    if (err) {
      console.error("Error during file reading", err);
      return res.status(404).end();
    }
    const tags = {
      title: req.t(`${type}.title`, values),
      description: req.t(`${type}.description`, values),
    };
    htmlData = htmlData
      .replace(`<title>${defaultTags.title}</title>`, `<title>${tags.title}</title>`)
      .replace(`"${defaultTags.title}"`, `"${tags.title}"`)
      .replace(new RegExp(defaultTags.description, "g"), tags.description);
    return res.send(htmlData);
  });
};

const getProject = async (id) => {
  try {
    return await fetch(`${process.env.REACT_APP_API_INTERNAL_URL}/projects/${id}/public`).then(
      (r) => r.json()
    );
  } catch {
    return null;
  }
};

/*
  DEFAULT INDEX SERVE
*/

const returnDefaultIndex = (req, res) => buildIndexFile(req, res, "default");

/*
  PROJECT INDEX SERVE
*/

const returnProjectIndex = async (req, res) => {
  const project = await getProject(req.params.projectId);
  return project
    ? buildIndexFile(req, res, "project", {projectName: project.name})
    : buildIndexFile(req, res, "default");
};

// /*
//   ELEMENT INDEX SERVE
// */
//
// // Copied from api service
// const personName = (person) =>
//   person
//     ? person.firstName && person.firstName.length > 0
//       ? person.lastName && person.lastName.length > 0
//         ? `${person.firstName} ${person.lastName}`
//         : person.firstName
//       : ""
//     : "";
//
// const customNaming = {
//   participants: (r) => personName(r.user),
//   stewards: personName,
//   sessions: (s) => s.activity.name,
// };
//
// const returnElementIndex = async (req, res) => {
//   const project = await getProject(req.params.projectId);
//
//   if (project.openingState === "registerForAll") {
//     const element = await fetch(
//       `${process.env.REACT_APP_API_INTERNAL_URL}/projects/${req.params.projectId}/${req.params.elementEndpoint}/${req.params.elementId}`
//     ).then((r) => r.json());
//     console.log(element);
//     const elementName =
//       (customNaming[req.params.elementEndpoint] &&
//         customNaming[req.params.elementEndpoint](element)) ||
//       element.name;
//     console.log(elementName);
//     return buildIndexFile(req, res, "element", {projectName: project.name, elementName});
//   } else {
//     return buildIndexFile(req, res, "project", {projectName: project.name});
//   }
// };

/*
  ROUTING
 */

// First, serve common routes we often land to with default index
app.get(["/", "/projects", "/public-projects", "/new", "/login", "/signup"], returnDefaultIndex);

// Then serve LEGACY routes that begin with /projects, so they are not messing up in the later routing.
// Elements are served before projects because the project URL has a wildcard
// app.get("/projects/:projectId/:elementEndpoint/:elementId", returnElementIndex);
app.get(["/projects/:projectId/*", "/projects/:projectId"], returnProjectIndex);

// Then only, serve routes that begin with project ID directly.
// Elements are served before projects because the project URL has a wildcard
// app.get("/:projectId/:elementEndpoint/:elementId", returnElementIndex);
app.get(["/:projectId/*", "/:projectId"], returnProjectIndex);
