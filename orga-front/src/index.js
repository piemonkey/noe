import React, {Suspense} from "react";
import ReactDOM from "react-dom";

import {initI18n} from "./app/i18n";
import {t} from "i18next";

import {Provider} from "react-redux";
import {PersistGate} from "@rifkyrangkuti/redux-persist/integration/react";
import {persistor, store} from "./app/store";

import {ConfigProvider} from "antd";
import frFR from "antd/es/locale/fr_FR";
import {Button, message, notification} from "antd";
import "moment/locale/fr";
import browserUpdate from "browser-update";
import * as serviceWorkerRegistration from "./serviceWorkerRegistration";

import App from "./App";
import {OFFLINE_MODE} from "./helpers/offlineModeUtilities";
import {OnlineStatusProvider} from "./helpers/onlineStatusCheckerUtilities";
import {WindowDimensionsProvider} from "./helpers/viewUtilities";
import {displayNotification} from "./helpers/notificationUtilities";
import {initSentry} from "./app/sentry";
import {URLS} from "./app/configuration";

// Make sure we are executing the app on the right domain name, and if not, redirect
if (window.location.origin !== URLS.CURRENT) {
  serviceWorkerRegistration.unregister();
  window.location.replace(URLS.CURRENT + window.location.pathname);
}

initI18n();
initSentry();

browserUpdate({
  required: {e: -4, f: -3, o: -3, s: -1, c: -3},
  notify_esr: true,
  // For testing purpose right now
  reminderClosed: 24 * 30,
  nomessage: true,
  onshow: (infos) => {
    console.log(infos);
    displayNotification("warn", "browserOutdatedNotification", {
      message: t("common:browserUpdate.message"),
      description: t("common:browserUpdate.description", {t: infos.browser.t}),
      btn: (
        <div className="containerH buttons-container">
          <Button
            type="primary"
            onClick={() => {
              notification.close("browserOutdatedNotification");
              window.open("https://browser-update.org/update-browser.html", "_blank").focus();
            }}>
            {t("common:browserUpdate.button")}
          </Button>
          <Button
            onClick={() => {
              infos.onclose(infos);
              infos.setCookie(infos.reminderClosed);
              notification.close("browserOutdatedNotification");
              setTimeout(
                () =>
                  message.info(
                    t("common:browserUpdate.notifyAgainInXDays", {
                      days: Math.round(infos.reminderClosed / 24),
                    })
                  ),
                1000
              );
            }}>
            {t("common:ignore")}
          </Button>
        </div>
      ),
    });
  },
});

const app = (
  <Suspense fallback={() => null}>
    <WindowDimensionsProvider>
      <OnlineStatusProvider>
        <ConfigProvider locale={frFR}>
          <App />
        </ConfigProvider>
      </OnlineStatusProvider>
    </WindowDimensionsProvider>
  </Suspense>
);

ReactDOM.render(
  <Provider store={store}>
    {OFFLINE_MODE ? (
      <PersistGate loading={null} persistor={persistor}>
        {app}
      </PersistGate>
    ) : (
      app
    )}
  </Provider>,
  document.getElementById("root")
);

// Service worker registration - if an update is available, reload the page without asking the user at all.
// Not the best behavior, but this is the less complicated.
serviceWorkerRegistration.register({
  onUpdate: (registration) => {
    message.loading(t("common:pwa.newVersionAvailable")).then(() => {
      try {
        console.log('registration.waiting.postMessage({type: "SKIP_WAITING"})');
        registration.waiting.postMessage({type: "SKIP_WAITING"});
        console.log("Attempting reload...");
        window.location.reload();
      } catch (e) {
        console.error("Huhooo error : ", e);
        message.warn(t("common:pwa.reloadFailedPleaseCloseTabs"));
      }
    });
  },
});
