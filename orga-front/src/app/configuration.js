import logoColors from "./logos/logo-colors.svg";
import logoColorsWithWhiteText from "./logos/logo-colors-with-white-text.svg";

// Base default spinner logo to take
export const defaultSpinnerLogo = logoColors;

// Spinner logo for the fullscreen arrival loading page
export const arrivalSpinnerLogo = logoColors;

// Logo to display in the UserConnectionPage
export const connectionPageLogo = logoColorsWithWhiteText;

// Default background theme for the app
export const defaultBackgroundClassName = "bg-noe";

// Instance name
export const instanceName = process.env.REACT_APP_INSTANCE_NAME || "NOÉ";

// URLs
export const URLS = {
  ORGA_FRONT: process.env.REACT_APP_ORGA_FRONT_URL,
  INSCRIPTION_FRONT: process.env.REACT_APP_INSCRIPTION_FRONT_URL,
  API: process.env.REACT_APP_API_URL,
};
URLS.CURRENT = URLS.ORGA_FRONT;
