import {Button, Modal, notification} from "antd";
import {WifiOutlined} from "@ant-design/icons";
import React from "react";
import {personName} from "./utilities";

let serverNotAvailableModalVisible = false;

export const displaySessionsInconsistenciesNotifications = (inconsistenciesList) => {
  if (inconsistenciesList.length > 0) {
    const listOfMessages = [];
    for (let inconsistency of inconsistenciesList) {
      for (let inconsistencyDetail of inconsistency.inconsistencies) {
        let message;
        if (inconsistencyDetail.type === "availabilitiesOverlap") {
          switch (inconsistencyDetail.entity) {
            case "project":
              message = `est hors des plages d'ouverture du projet`;
              break;
            case "place":
              message = `est hors des plages d'ouverture de l'espace (${inconsistencyDetail.entitiesInvolved
                .map((e) => e.name)
                .join(", ")})`;
              break;
            case "steward":
              message = `est hors des disponibilités de l'encadrant⋅e (${inconsistencyDetail.entitiesInvolved
                .map(personName)
                .join(", ")})`;
              break;
            default:
          }
        } else if (inconsistencyDetail.type === "alreadyUsedEntity") {
          switch (inconsistencyDetail.entity) {
            case "place":
              message = `rentre en conflit avec une autre plage qui utilise le même espace au même moment (${inconsistencyDetail.entitiesInvolved
                .map((e) => e.name)
                .join(", ")})`;
              break;
            case "steward":
              message = `rentre en conflit avec une autre plage qui mobilise le même encadrant au même moment (${inconsistencyDetail.entitiesInvolved
                .map(personName)
                .join(", ")})`;
              break;
            default:
          }
        }
        listOfMessages.push(`La plage n°${inconsistency.slot} ${message}.`);
      }
    }
    console.log(inconsistenciesList[0]);
    displayNotification("open", `inconsistency-${inconsistenciesList[0].session}`, {
      message: "Des incohérences ont été trouvées sur la session:",
      description: (
        <ul>
          {listOfMessages.map((message) => (
            <li>{message}</li>
          ))}
        </ul>
      ),
      duration: 5 + inconsistenciesList.length * 2,
    });
  }
};

export const displayNotification = (type, key, {buttonText, onClickButton, ...props}) =>
  notification[type]({
    key: key,

    btn: buttonText && onClickButton && (
      <Button
        type="primary"
        onClick={() => {
          onClickButton();
          notification.close(key);
        }}>
        {buttonText}
      </Button>
    ),
    duration: 0, // by default, notif will stay forever
    placement: "bottomRight",
    ...props, // Override with props if given
  });

export const displayServerNotAvailableErrorModal = () => {
  if (!serverNotAvailableModalVisible) {
    serverNotAvailableModalVisible = true;
    Modal.error({
      icon: <WifiOutlined />,
      title: "Allô Houston ? On a un problème.",
      content: (
        <>
          <p>
            <strong>Il semblerait qu'il n'y ait "plus personne au bout du fil".</strong>
          </p>

          <p>
            La raison la plus probable ? Vous n'avez plus d'internet. Autre raison possible : nous
            faisons une mise à jour du serveur (ça ne prend pas longtemps !).
          </p>

          <p>
            Si vous aviez des données à enregistrer, ne touchez à rien,{" "}
            <strong>attendez quelques minutes ici</strong>, puis fermez cette fenêtre et retentez
            d'enregistrer vos modifications.
          </p>
        </>
      ),
      okText: "Réessayer",
      onOk: () => (serverNotAvailableModalVisible = false),
      onCancel: () => (serverNotAvailableModalVisible = false),
      closable: true,
      maskClosable: true,
    });
  }
};
