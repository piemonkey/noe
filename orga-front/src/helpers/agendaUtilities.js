import {Button, Drawer, Dropdown, Input, Menu, message, Select, Tag, Tooltip} from "antd";
import moment from "moment";
import React, {useEffect, useState} from "react";
import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  DownOutlined,
  PictureOutlined,
  SearchOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import {DayView} from "@devexpress/dx-react-scheduler-material-ui";
import {FormElement} from "../components/common/FormElement";
import {InputElement} from "../components/common/InputElement";
import {TableElement} from "../components/common/TableElement";
import {paginationPageSizes} from "../features/view";
import {listSorter, searchInObjectsList} from "./listUtilities";
import {personName} from "./utilities";
import {categoriesSelectors} from "../features/categories";
import {useSelector} from "react-redux";
import {getVolunteeringCoefficient} from "./sessionsUtilities";
import {projectsSelectors} from "../features/projects";

//*******************************//
//******* SLOT UTILITIES ********//
//*******************************//

// DISPLAY THE SLOT NUMBER
export const slotNumberString = (slot) => {
  const numberOfSlotsInSession = slot.session.slots.length;
  return numberOfSlotsInSession > 1
    ? "(" +
        (slot.session.slots.findIndex((s) => s._id === slot._id) + 1) +
        "/" +
        numberOfSlotsInSession +
        ") "
    : "";
};

// DISPLAY THE SLOT NAME
export const getSessionName = (session, teams?) => {
  const sessionName = session.name?.length > 0 ? session.name + " - " : "";
  const teamName =
    teams?.find((t) => t._id === (session.team?._id || session.team))?.name || session.team?.name;
  const sessionNameSuffix = teamName ? ` (${teamName})` : "";
  return sessionName + session.activity?.name + sessionNameSuffix;
};

export const getElementsFromListInSlot = (elementName, slot, getKey: (s: any) => string) =>
  slot[elementName]?.length > 0 && slot[`${elementName}SessionSynchro`] === false
    ? slot[elementName].map(getKey)
    : slot.session[elementName].map(getKey);

const specialCategoriesFilterOptions = ["volunteering", "allTheRest"];

export const filterAppointments = (slots, searchBarValue, categoriesFilter, registrations) => {
  // This code is shared across frontends, cf. inscription-front/src/features/sessions.js updateFilteredList()
  if (categoriesFilter?.length > 0) {
    // Then filter only by category so we need to remove the special categories entries from the rest
    const filterVolunteering = categoriesFilter.includes("volunteering");
    const filterAllTheRest = categoriesFilter.includes("allTheRest");
    const pureCategoriesFilter = categoriesFilter.filter(
      (category) => !specialCategoriesFilterOptions.includes(category)
    );

    slots = slots?.filter((slot) => {
      // Filter by "special categories"
      let specialCategoriesFilter;
      if (filterAllTheRest && !filterVolunteering) {
        // "- All the rest -" is toggled but not "- All the volunteering -"
        specialCategoriesFilter = getVolunteeringCoefficient(slot.session) === 0;
      } else if (filterVolunteering && !filterAllTheRest) {
        // "- All the volunteering -" is toggled but not "- All the rest -"
        specialCategoriesFilter = getVolunteeringCoefficient(slot.session) > 0;
      } else if (filterVolunteering && filterAllTheRest) {
        // Both are toggled
        specialCategoriesFilter = true; // if  we have both volunteering and allTheRest, it's like we don't filter at all
      } else {
        // None are toggled
        specialCategoriesFilter = false; // if we have none, then we don't wanna filter with it at all
      }

      return (
        specialCategoriesFilter ||
        (pureCategoriesFilter.length > 0
          ? pureCategoriesFilter.includes(slot.session.activity.category._id)
          : false) // Don't filter if there are no pure categories selected
      );
    });
  }

  return searchInObjectsList(searchBarValue, slots, (slot) => [
    slot.session.name,
    slot.session.activity.name,
    slot.session.activity.category.name,
    ...(slot.session.activity.secondaryCategories || []),
    ...(slot.stewards?.map(personName) || []),
    ...(slot.session.stewards?.map(personName) || []),
    ...(slot.places?.map((p) => p.name) || []),
    ...(slot.session.places?.map((p) => p.name) || []),
    ...(registrations
      ?.filter((r) => r.sessionsSubscriptions.find((ss) => ss.session === slot.session._id))
      .map((r) => personName(r.user)) || []),
    slot.session.team?.name,
  ]);
};

//*******************************//
//****** DISPLAY UTILITIES ******//
//*******************************//

// Handles dates when end date is at midnight o'clock. Without it, any session ending at midnight will
// overflow on the next day, which is really annoying. This manager aims to remove this behavior.
export const midnightDateManager = {
  dataToDisplay: (slot) => {
    // If end date is midnight, remove one second
    const endDate = moment(slot.endDate);
    if (endDate.hour() === 0 && endDate.minutes() === 0) endDate.subtract(1, "minute");
    return {...slot, endDate};
  },
  displayToData: (existingEndDate, newEndDate) => {
    // If end date is midnight, remove one minute
    const endDate = moment(existingEndDate);
    newEndDate = moment(newEndDate);
    if (endDate.hour() === 0 && endDate.minutes() === 0) newEndDate.add(1, "minute");
    return newEndDate;
  },
};

// LOADING MESSAGES
const LOADING_MESSAGE_KEY = "loading-message";
export const displayLoadingMessage = () =>
  message.open({
    key: LOADING_MESSAGE_KEY,
    type: "loading",
    content: "Chargement...",
  });

// Destroy the loading message when done loading
export const useKillLoadingMessage = () => useEffect(() => message.destroy(LOADING_MESSAGE_KEY));

// USEEFFECT HELPERS
export const trySeveralTimes = (
  actionToTry,
  whatToDoAfter,
  ...{limitOfTrials = 5, frequency = 100, errorHandling}
) => {
  let trials = 0;
  const tryInterval = setInterval(() => {
    try {
      if (actionToTry()) {
        clearInterval(tryInterval);
        whatToDoAfter && whatToDoAfter();
      } else {
        trials += 1;
        if (trials >= limitOfTrials) {
          clearInterval(tryInterval);
        }
      }
    } catch (e) {
      errorHandling && errorHandling(e);
    }
  }, frequency);
};

export const useInitialScrollToProjectAvailabilities = (agendaDisplayParams, cellDisplayHeight) => {
  // Scroll to the optimal place should be done once. If done, will be set to true in useEffect
  const [initialScrollIsDone, setInitialScrollIsDone] = useState(false);

  // Scroll on page load to reach the best optimal place to be on the view
  useEffect(() => {
    // The main table element has this class (it's not the only one, but it will always come first in the querySelector)
    if (!initialScrollIsDone && agendaDisplayParams?.shouldScroll && cellDisplayHeight) {
      trySeveralTimes(
        () => {
          // TODO difference between frontends is normal
          const mainTimeTableElement = document.querySelector(".MuiGrid-container");
          // Scroll to reach the diff between the agenda display and the project availabilities
          mainTimeTableElement.scrollTop =
            agendaDisplayParams.hoursDiff * cellDisplayHeight * (60 / CELL_DURATION_MINUTES);
          return true;
        },
        () => setInitialScrollIsDone(true),
        {frequency: 30}
      );
    }
  }, [agendaDisplayParams, cellDisplayHeight]);
};
// Synchronize the height of the little ticks on the side of the calendar

export const synchronizeTicksOnSideOfAgenda = (className, cellDisplayHeight) => {
  // The trick is to spot the timescale labels but there is no eaasy hook (class names change all the time)
  // We created a custom class on which we can get a hook, named "timescale-cell".
  trySeveralTimes(() => {
    const elements = document.querySelector(`.${className}`)?.parentNode?.parentNode?.parentNode
      ?.parentNode?.parentNode?.lastChild?.firstChild.childNodes; // Go to the TicksLayout-table-XXXX and iterate
    if (elements?.length > 0) {
      elements.forEach((el) => (el.firstChild.style.height = `${cellDisplayHeight}px`));
      return true;
    }
  });
};

//*****************************//
//******* AGENDA PARAMS *******//
//*****************************//

// Cell sizes and duration
export const CELL_DURATION_MINUTES = 30; // The smallest unit of time for drag and drop
export const CELLS_DISPLAY_HEIGHTS = [25, 35, 45];

// Max recommended number of simultaneous columns
export const RECOMMENDED_LIMIT_OF_RESOURCE_COLUMNS = 15;

// DEFAULT AGENDA PARAMS
export const DEFAULT_AGENDA_PARAMS = {
  slotsOnEachOther: false,
  cellDisplayHeight: CELLS_DISPLAY_HEIGHTS[1],
  groupByDaysFirst: true,
  defaultNewSlotDuration: 90,
  displayAvailabilities: true,
  selectedResources: [],
  showResourcesListingOnAgendaCards: [
    "places",
    "stewards",
    "registrations",
    "maxNumberOfParticipants",
  ],
  resourcesFilterSelections: {},
  showResourcesAvailabilities: {},
  categoriesFilter: [],
};

// Number of days to display by default
const MAX_DAYS_TO_DISPLAY_BY_DEFAULT = 4;
export const getNumberOfDaysToDisplay = (windowWidth, project) =>
  Math.min(
    Math.max(1, Math.ceil((windowWidth - 280) / 450)), // Screen adaption
    moment(project.end).diff(project.start, "day") + 1, // Not more than the project opening dates
    MAX_DAYS_TO_DISPLAY_BY_DEFAULT // Default limit
  );

// Returns the hour of the day based on hours and minutes
const hoursOfDay = (momentDate) => momentDate.minutes() / 60.0 + momentDate.hours();

// Tells if a slot is located on two different days
const slotEndsAfterMidnight = (slot) => {
  const start = moment(slot.start);
  const end = moment(slot.end);
  const length = end.diff(start, "minutes", true);
  return start.hour() * 60 + start.minute() + length > 1439;
};

// Get the min and max hour of all slots
const getSlotsMinMaxHours = (slots) => {
  // If no slots, return
  if (!slots || !(slots?.length > 0)) return;

  let minStart, maxEnd;
  for (const slot of slots) {
    if (slotEndsAfterMidnight(slot)) {
      // If any of the slots ends after midnight, display all the hours
      return {start: 0, end: 24};
    } else {
      // Else, compute dates depending on the
      const slotStart = hoursOfDay(moment(slot.start));
      const slotEnd = hoursOfDay(moment(slot.end));
      minStart = minStart && minStart < slotStart ? minStart : slotStart;
      maxEnd = maxEnd && maxEnd > slotEnd ? maxEnd : slotEnd;
    }
  }

  return {start: minStart, end: maxEnd};
};

export const getAgendaDisplayStartDate = (project) => {
  const now = moment();
  const start = moment(project.start);
  const end = moment(project.end);

  // If we are during the event, start at the current date. Else, start at project start date
  return (now.isBefore(start) || now.isAfter(end) ? start : now).unix() * 1000;
};

// Get the min and max hour of the project availability slots
const getProjectMinMaxHours = (project) => {
  // If there is no availability slots, don't return anything, we can't calculate.
  if (!(project?.availabilitySlots?.length > 0)) return;

  const projectSlotThatEndsAfterMidnight = project.availabilitySlots.find((slot) =>
    slotEndsAfterMidnight(slot)
  );
  // If some slots pass through midnight, we have to display everything.
  if (projectSlotThatEndsAfterMidnight) return;

  // If not, then just take the event's slots as reference
  return {
    start: hoursOfDay(moment(project.start)),
    end: hoursOfDay(moment(project.end)),
  };
};

// Get the display parameters to give to the Agenda view
export const getAgendaDisplayParams = (slots, project) => {
  let params;
  const projectMinMaxHours = getProjectMinMaxHours(project);
  const slotsMinMaxHours = getSlotsMinMaxHours(slots);

  if (projectMinMaxHours && slotsMinMaxHours) {
    // If there are both slots and project hours, calculate based on both

    const slotsExistBeforeProjectHours = slotsMinMaxHours.start < projectMinMaxHours.start;
    const slotsExistAfterProjectHours = slotsMinMaxHours.end > projectMinMaxHours.end;

    params = {
      start: Math.floor(
        slotsExistBeforeProjectHours ? slotsMinMaxHours.start : projectMinMaxHours.start
      ),
      end: Math.ceil(slotsExistAfterProjectHours ? slotsMinMaxHours.end : projectMinMaxHours.end),
      shouldScroll: true,
    };

    // Set up the scroll diff
    params.hoursDiff =
      slotsMinMaxHours && slotsExistBeforeProjectHours
        ? Math.floor(projectMinMaxHours.start) - params.start
        : 0;
  } else if (slotsMinMaxHours || projectMinMaxHours) {
    // If there are only one of them, just use the ones we have
    params = {
      start: Math.floor(
        slotsMinMaxHours?.start !== undefined ? slotsMinMaxHours?.start : projectMinMaxHours?.start
      ),
      end: Math.ceil(
        slotsMinMaxHours?.end !== undefined ? slotsMinMaxHours?.end : projectMinMaxHours?.end
      ),
    };
  } else {
    // if there are none, fail safe
    params = {start: 0, end: 24};
  }

  params.start = Math.max(params.start - 1, 0); // Display one hour before display start, but do not go below 0 hours
  params.end = Math.min(params.end + 1, 24); // Display one hour after display end, but do not go after 24 hours
  return params;
};

//************************************//
//****** AGENDA BASE COMPONENTS ******//
//************************************//

// Left and right arrows to navigate in the agenda days
export const NavButton = ({type, onClick}) => (
  <Button
    type="link"
    size="large"
    icon={type === "forward" ? <ArrowRightOutlined /> : <ArrowLeftOutlined />}
    onClick={onClick}
  />
);

// Renders the date labels on the side of the agenda
export const getTimeScaleLabelComponent =
  (cellDisplayHeight) =>
  ({time, ...otherProps}) => {
    // If there is no 'time', it means it's the very first, which has no time in it.
    const style = time
      ? {height: cellDisplayHeight, lineHeight: `${cellDisplayHeight}px`}
      : {height: cellDisplayHeight / 2}; // It should be twice as small.

    // Only display time when it's o'clock
    const isOClock = time?.getMinutes() === 0;

    return (
      <DayView.TimeScaleLabel
        className="timescale-cell" // Needed to get a hook
        time={isOClock ? time : undefined}
        style={style}
        {...otherProps}
      />
    );
  };

//*********************************//
//****** AGENDA CONTROLS BAR ******//
//*********************************//

const SearchBox = ({searchBarValue, setSearchBarValue}) => (
  <Tooltip
    mouseEnterDelay={0.3}
    title={
      <>
        Recherchez par :
        <br />- activité,
        <br />- nom de session,
        <br />- encadrant⋅e,
        <br />- catégorie,
        <br />- participant⋅e,
        <br />- équipe,
        <br />- espace.
      </>
    }>
    <Input.Search
      style={{minWidth: 150, flexBasis: "50%"}}
      placeholder="Rechercher..."
      defaultValue={searchBarValue}
      onSearch={(value) => displayLoadingMessage().then(() => setSearchBarValue(value))}
    />
  </Tooltip>
);

const NumberOfDaysSelector = ({
  numberOfDaysDisplayed,
  setNumberOfDaysDisplayed,
  isMobileView = false,
}) => {
  const currentProject = useSelector(projectsSelectors.selectEditing);

  return (
    <Dropdown
      trigger={["click"]}
      overlay={
        <Menu
          onClick={(item) => displayLoadingMessage().then(() => setNumberOfDaysDisplayed(item.key))}
          items={[
            {
              label: "Tout l'événement",
              key:
                moment(currentProject.end)
                  .startOf("day")
                  .diff(moment(currentProject.start).startOf("day"), "days") + 1,
            },
            {type: "divider"},
            {label: "1 Jour", key: 1},
            {label: "3 Jours", key: 3},
            {label: "5 Jours", key: 5},
            {type: "divider"},
            {
              label: "Autre",
              children: [...Array(14).keys()].map((i) => ({
                label: `${i + 2} Jours`,
                key: i + 2,
              })),
            },
          ]}
        />
      }>
      <Button>
        {numberOfDaysDisplayed}{" "}
        {isMobileView ? "J." : `Jour${numberOfDaysDisplayed > 1 ? "s" : ""}`}
        {!isMobileView && <DownOutlined style={{marginLeft: 6}} />}
      </Button>
    </Dropdown>
  );
};

const CategoriesFilterSelector = ({categoriesFilter, setCategoriesFilter}) => {
  const categories = useSelector(categoriesSelectors.selectList);
  const categoriesReady = categories.length > 0;
  const categoriesOptions = [
    {key: "volunteering", value: "volunteering", label: "- Tout le bénévolat -"},
    {key: "allTheRest", value: "allTheRest", label: "- Tout le reste -"},
    ...categories.map((category) => ({
      key: category._id,
      value: category._id,
      label: category.name,
    })),
  ];
  return (
    <Select
      style={{minWidth: 200, flexBasis: "50%"}}
      defaultValue={categoriesReady ? categoriesFilter : undefined}
      allowClear
      mode="multiple"
      placeholder="Filtrez par catégorie..."
      onChange={(filter) => displayLoadingMessage().then(() => setCategoriesFilter(filter))}
      options={categoriesOptions}
    />
  );
};

export const AgendaControls = ({
  searchBarValue,
  setSearchBarValue,
  numberOfDaysDisplayed,
  setNumberOfDaysDisplayed,
  categoriesFilter,
  setCategoriesFilter,
  isMobileView,
  FullSettingsDrawer,
  GroupByControls,
}) => {
  const [sessionFilterDrawerVisible, setSessionFilterDrawerVisible] = useState(false);

  return isMobileView ? (
    <>
      {/*On mobile, display a button to open the drawer to filter the sessions*/}
      <Button
        type="primary"
        onClick={() => setSessionFilterDrawerVisible(true)}
        style={{marginRight: 10, marginLeft: 10, flexGrow: 1}}
        icon={<SearchOutlined />}>
        Filtrer
      </Button>

      <NumberOfDaysSelector
        isMobileView={true}
        numberOfDaysDisplayed={numberOfDaysDisplayed}
        setNumberOfDaysDisplayed={setNumberOfDaysDisplayed}
      />

      <Drawer
        placement="top"
        height="auto" // adjust to content
        closable={false}
        onClose={() => setSessionFilterDrawerVisible(false)}
        visible={sessionFilterDrawerVisible}>
        <div className="containerH buttons-container" style={{alignItems: "center"}}>
          <SearchBox searchBarValue={searchBarValue} setSearchBarValue={setSearchBarValue} />
          <CategoriesFilterSelector
            categoriesFilter={categoriesFilter}
            setCategoriesFilter={setCategoriesFilter}
          />

          <FullSettingsDrawer />
          <GroupByControls />
        </div>
      </Drawer>
    </>
  ) : (
    <div
      style={{width: "100%", alignItems: "center", flexWrap: "nowrap", overflowX: "auto"}}
      className="containerH buttons-container">
      {/*Separator*/} <div style={{width: 20}} />
      <SearchBox searchBarValue={searchBarValue} setSearchBarValue={setSearchBarValue} />
      <CategoriesFilterSelector
        categoriesFilter={categoriesFilter}
        setCategoriesFilter={setCategoriesFilter}
      />
      <GroupByControls />
      {/*Separator*/} <div style={{width: 12}} />
      <NumberOfDaysSelector
        numberOfDaysDisplayed={numberOfDaysDisplayed}
        setNumberOfDaysDisplayed={setNumberOfDaysDisplayed}
      />
      {/*Separator*/} <div style={{width: 10}} />
      <FullSettingsDrawer />
    </div>
  );
};

export const AgendaSettingsDrawer = ({
  children,
  cellDisplayHeight,
  slotsOnEachOther,
  setAgendaParams,
  buttonStyle,
  showResourcesListingOnAgendaCards,
}) => {
  const [drawerVisible, setDrawerVisible] = useState(false);

  const printAgendaView = () => {
    const toHide = [
      document.getElementsByTagName("aside")[0],
      document.getElementsByTagName("header")[0],
      document.getElementsByClassName("navbar-menu-button")[0],
      document.getElementsByClassName("MuiToolbar-root")[0],
      document.getElementsByClassName("ant-drawer")[0],
    ].filter((el) => !!el);
    const agendaSpace =
      document.getElementsByClassName("scheduler-wrapper")[0].firstChild.lastChild;

    const toHideWithOldValues = toHide.map((el) => ({el, value: el.style.display}));

    toHide.forEach((el) => (el.style.display = "none"));
    agendaSpace.style.overflowY = "visible";

    window.print();

    toHideWithOldValues.forEach(({el, value}) => (el.style.display = value));
    agendaSpace.style.overflowY = "auto";
  };

  return (
    <>
      <Button
        style={buttonStyle}
        icon={<SettingOutlined />}
        type="link"
        shape="circle"
        onClick={() => setDrawerVisible(true)}
      />
      <Drawer placement="right" onClose={() => setDrawerVisible(false)} visible={drawerVisible}>
        <h3>Paramètres généraux de l'agenda</h3>
        <FormElement style={{marginTop: 26}} className="container-grid">
          {children}

          {/*Select if we want to see events one on the other or one next to each other. */}
          <InputElement.Select
            label="Affichage des sessions"
            onChange={(value) =>
              displayLoadingMessage().then(() => setAgendaParams({slotsOnEachOther: value}))
            }
            defaultValue={slotsOnEachOther}
            options={[
              {value: false, label: "Les unes à côté des autres"},
              {value: true, label: "Les unes au dessus des autres"},
            ]}
            tooltip="⚠️ Fonctionnalité expérimentale : si ça bug, recharger la page :)"
          />

          {/*Increase or decrease the cells heights*/}
          <InputElement.Select
            label="Densité d'affichage"
            onChange={(value) =>
              displayLoadingMessage().then(() => setAgendaParams({cellDisplayHeight: value}))
            }
            defaultValue={cellDisplayHeight}
            options={[
              {value: CELLS_DISPLAY_HEIGHTS[0], label: "Compact"},
              {value: CELLS_DISPLAY_HEIGHTS[1], label: "Confort"},
              {value: CELLS_DISPLAY_HEIGHTS[2], label: "Large"},
            ]}
          />

          {/*Add info to the cards on the agenda*/}
          <InputElement.Select
            label="Informations affichées sur les cartes de l'agenda"
            defaultValue={showResourcesListingOnAgendaCards}
            mode="multiple"
            placeholder="sélectionnez des informations à afficher..."
            onChange={(value) => setAgendaParams({showResourcesListingOnAgendaCards: value})}
            options={[
              {value: "places", label: "Espaces"},
              {value: "stewards", label: "Encadrant⋅es"},
              {value: "registrations", label: "Participant⋅es"},
              {value: "maxNumberOfParticipants", label: "Jauge de participant⋅es"},
            ]}
          />

          <Button icon={<PictureOutlined />} onClick={printAgendaView}>
            Exporter la vue agenda
          </Button>
        </FormElement>
      </Drawer>
    </>
  );
};

export const ResourceFilterSelector = ({
  resource,
  changeGrouping,
  selectedResources,
  resourcesFilterSelections,
  showResourcesAvailabilities,
  agendaParams,
  setAgendaParams,
}) => {
  const [tmpResourceFilterSelection, setTmpResourceFilterSelection] = useState(
    resourcesFilterSelections[resource.resourceName]?.map((el) => ({id: el})) || []
  );
  const [drawerVisible, setDrawerVisible] = useState(false);
  const checked = selectedResources.includes(resource.fieldName);

  const rowSelectionResourceFilterSelection = {
    onChange: (selectedRowKeys, selectedRowObject) =>
      setTmpResourceFilterSelection(selectedRowObject),
  };

  const validateFilter = () => {
    changeGrouping(resource, true, {
      ...resourcesFilterSelections,
      [resource.resourceName]: tmpResourceFilterSelection.map((r) => r.id),
    });
    setDrawerVisible(false);
  };

  const resetFilter = () => {
    changeGrouping(resource, false);
    setDrawerVisible(false);
  };

  return (
    <>
      <Tag.CheckableTag
        key={resource.fieldName}
        className={`${resource.resourceName}-grouping grouping-button`}
        checked={checked}
        onChange={() =>
          resource.instances.length === 0
            ? message.warn(
                `Vous devez déjà avoir créé des ${resource.title.toLowerCase()} pour effectuer cette action.`
              )
            : !checked && selectedResources?.length >= 2
            ? message.warn("Pas plus de deux filtres à la fois, autrement votre PC va exploser 😉")
            : setDrawerVisible(true)
        }>
        {resource.title}
      </Tag.CheckableTag>
      <Drawer
        placement="right"
        onClose={() => setDrawerVisible(false)}
        visible={drawerVisible}
        zIndex={10000}>
        {/*Display availabilities or not*/}
        {resource.hasAvailabilities && (
          <InputElement.Switch
            label="Affichage des disponibilités"
            tooltip={
              <>
                Afficher les disponibilités des {resource.title} sur le planning lorsque l'on active
                le groupage.
                <br />- Le rose rayé représente les moment d'indisponibilité des salles.
                <br />- Le bleu rayé représente les moments d'indisponibilité des encadrant⋅es.
              </>
            }
            checked={showResourcesAvailabilities[resource.fieldName]}
            onChange={(value) =>
              displayLoadingMessage().then(() =>
                setAgendaParams({
                  showResourcesAvailabilities: {
                    ...agendaParams.showResourcesAvailabilities,
                    [resource.fieldName]: value,
                  },
                })
              )
            }
          />
        )}
        <TableElement.Simple
          showHeader
          scroll={{y: "calc( 100vh - 340px)"}}
          pagination={{
            position: ["bottomCenter"],
            pageSize: 40,
            size: "small",
            pageSizeOptions: paginationPageSizes,
            showSizeChanger: true,
          }}
          rowSelection={rowSelectionResourceFilterSelection}
          selectedRowKeys={tmpResourceFilterSelection}
          setSelectedRowKeys={setTmpResourceFilterSelection}
          columns={[
            {
              title: `Filtrage des ${resource.title}`,
              dataIndex: "text",
              searchable: true,
              searchText: (record) => record.text,
              sorter: (a, b) => listSorter.text(a.text, b.text),
            },
          ]}
          rowKey="id"
          dataSource={resource.instances}
        />
        <Button
          type="primary"
          style={{width: "100%", marginTop: 8}}
          onClick={validateFilter}
          disabled={tmpResourceFilterSelection.length === 0}>
          Valider le groupage
        </Button>
        {checked && (
          <Button type="link" danger style={{width: "100%", marginTop: 8}} onClick={resetFilter}>
            Désactiver le groupage
          </Button>
        )}
      </Drawer>
    </>
  );
};
