import React, {useRef} from "react";
import {Parser} from "json2csv";
import {Button, Tooltip} from "antd";
import {ExportOutlined} from "@ant-design/icons";

export const CsvExportButton = ({
  dataExportFunction,
  withFields,
  children,
  tooltip,
  getExportName,
  ...props
}) => {
  const downloadButton = useRef();

  const exportData = () => {
    const exportName = getExportName();

    try {
      const exportedData = dataExportFunction();
      const fields = withFields ? exportedData[0] : undefined;
      const parser = new Parser({fields});
      const csv = parser.parse(withFields ? exportedData.slice(1) : exportedData);

      let file = new File([csv], exportName, {
        type: "text/csv",
      });
      let exportUrl = URL.createObjectURL(file);

      downloadButton.current.setAttribute("href", exportUrl);
      downloadButton.current.setAttribute("download", exportName);
      downloadButton.current.click();
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <>
      <Tooltip title={tooltip}>
        <Button
          onClick={(e) => {
            exportData();
          }}
          icon={<ExportOutlined />}
          type="link"
          {...props}>
          {children}
        </Button>
      </Tooltip>
      <a ref={downloadButton} style={{display: "none"}} />
    </>
  );
};
