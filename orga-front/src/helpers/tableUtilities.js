import React, {useRef, useState} from "react";
import {Button, Input, message, Tag} from "antd";
import {SearchOutlined} from "@ant-design/icons";
import {listRenderer, listSorter, normalize} from "./listUtilities";
import {Link} from "@reach/router";
import {SessionFilling} from "../components/sessions/SessionEdit";
import {registrationsActions, registrationsSelectors} from "../features/registrations";
import {InputElement} from "../components/common/InputElement";
import {useDispatch} from "react-redux";
import moment from "moment";
import {sessionsActions, sessionsSelectors} from "../features/sessions";
import {personName} from "./utilities";
import {FormElement} from "../components/common/FormElement";
import {WaitingInvitationTag} from "./registrationsUtilities";

const {Search} = Input;

export const addKeyToItemsOfList = (data, keyName = undefined) => {
  return data ? data?.map((t, i) => ({...t, key: keyName ? t[keyName] : i})) : [];
};

export const fieldToData = (fields, acc = {}) => {
  return fields.reduce((accumulator, currentValue) => {
    accumulator[currentValue.name] = currentValue.value;
    return accumulator;
  }, acc);
};

export const dataToFields = (data, formatFieldsFn = undefined) => {
  let clone = {...data};

  formatFieldsFn && formatFieldsFn(clone);

  const fields = Object.keys(clone).reduce((accumulator, currentValue) => {
    accumulator.push({name: currentValue, value: clone[currentValue]});
    return accumulator;
  }, []);
  return fields;
};

export const useCopyColumns = (dataSource, columns, handleDisplayConfigChangeFn?) => {
  const [currentDataSource, setCurrentDataSource] = useState();

  return {
    columns: columns.map((column) => ({
      ...column,
      onHeaderCell: (column) => ({
        ...column.onHeaderCell,
        onContextMenu: (e) => {
          e.preventDefault();
          const searchTextFn = column.searchText || ((record) => record[column.dataIndex]);
          const copyableText = (currentDataSource || dataSource)?.map(searchTextFn).join(", ");
          navigator.clipboard
            .writeText(copyableText)
            .then(() => message.success("La colonne a été copiée dans le presse-papier."));
        },
      }),
    })),
    handleDisplayConfigChange: (pagination, filters, sorter, extra) => {
      setCurrentDataSource(extra.currentDataSource);
      handleDisplayConfigChangeFn &&
        handleDisplayConfigChangeFn(pagination, filters, sorter, extra);
    },
    currentDataSource,
  };
};

export const useSearchInColumns = (columns) => {
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  let searchInput = useRef(null);

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters, confirm) => {
    clearFilters();
    setSearchText("");
    confirm();
  };

  const enableColumnSearchProps = (dataIndex, columnTitle, searchText) => ({
    filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
      <div className="containerH" style={{padding: 8}}>
        <Search
          autoComplete="new-password" // Prevent any autocomplete service to complete this, cause autoComplete="off" doesn't seem to work
          enterButton
          style={{maxWidth: 250}}
          value={selectedKeys[0]}
          // On press search button
          onSearch={() => handleSearch(selectedKeys, confirm, dataIndex)}
          // On keyboard input
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          // on press enter
          onPressEnter={(e) => {
            e.stopPropagation();
            handleSearch(selectedKeys, confirm, dataIndex);
          }}
          placeholder={`filtrer dans ${columnTitle}...`}
          ref={searchInput}
        />
        <Button type="link" onClick={() => clearFilters && handleReset(clearFilters, confirm)}>
          Effacer
        </Button>
      </div>
    ),
    filterIcon: (filtered) => <SearchOutlined style={{color: filtered ? "#1890ff" : "#999999"}} />,
    onFilter: (text, record) => {
      if (searchText) return normalize(searchText(record)).includes(normalize(text));
      else if (record[dataIndex]) {
        return normalize(record[dataIndex]).includes(normalize(text));
      } else {
        return "";
      }
    },
    onFilterDropdownVisibleChange: (visible) =>
      visible && setTimeout(() => searchInput.current?.select(), 100),
  });

  return columns.map((column) => ({
    // The column data
    ...column,
    // Enable search options if needed
    ...(column.searchable
      ? enableColumnSearchProps(column.dataIndex, column.title, column.searchText)
      : undefined),
  }));
};

export const columnsStewards = [
  {
    title: "Prénom",
    dataIndex: "firstName",
    sorter: (a, b) => listSorter.text(a.firstName, b.firstName),
    defaultSortOrder: "ascend",
    searchable: true,
  },
  {
    title: "Nom",
    dataIndex: "lastName",
    sorter: (a, b) => listSorter.text(a.lastName, b.lastName),
    searchable: true,
  },
];

export const columnsPlaces = [
  {
    title: "Nom",
    dataIndex: "name",
    sorter: (a, b) => listSorter.text(a.name, b.name),
    defaultSortOrder: "ascend",
    searchable: true,
  },
  {
    title: "Jauge max",
    dataIndex: "maxNumberOfParticipants",
    render: (text) => `jauge de ${text}`,
    sorter: (a, b) => listSorter.number(a.maxNumberOfParticipants, b.maxNumberOfParticipants),
  },
];

// Same function as in api/src/controllers/pdf.ts
export const cleanAnswer = (answer, separator = ",\n") => {
  // If the answer is a checkbox answer, it is like an object. So we keep only the true values and display them
  if (typeof answer === "object") {
    if (answer.maskName && answer.value) {
      // If the answer looks like a formatted phone number...
      return answer.value;
    } else {
      // Else if it looks like a multiple choice answser...
      return Object.entries(answer)
        .filter((entry) => entry[1])
        .map((entry) => entry[0])
        .join(separator);
    }
  } else {
    return answer;
  }
};

const EditableInlineTags = ({name, elementsActions, elementsSelectors, record}) => {
  const dispatch = useDispatch();
  const [editMode, setEditMode] = useState(false);

  const modifyTagsFn = (recordId) => (tags) => {
    const recordToUpdate = {_id: recordId, [name]: tags};
    dispatch(elementsActions.persist(recordToUpdate));
  };

  return editMode ? (
    <FormElement initialValues={{[name]: record[name]}}>
      <InputElement.TagsSelect
        autoFocus={true}
        name={name}
        formItemProps={{style: {marginBottom: 0}}}
        elementsSelectors={elementsSelectors}
        onChange={modifyTagsFn(record._id)}
      />
    </FormElement>
  ) : (
    <div
      onClick={() => setEditMode(true)}
      style={{
        cursor: "pointer",
        minHeight: 32,
        display: "flex",
        flexWrap: "wrap",
        alignItems: "center",
        rowGap: 2,
      }}>
      {record[name]?.map((tagName, index) => (
        <Tag key={index}>{tagName}</Tag>
      ))}
    </div>
  );
};

export const editableTagsColumn = (name, title, elementsActions, elementsSelectors) => ({
  title,
  dataIndex: name,
  sorter: (a, b) => listSorter.text(a[name]?.join(" "), b[name]?.join(" ")),
  render: (text, record) => (
    <EditableInlineTags
      record={record}
      name={name}
      elementsActions={elementsActions}
      elementsSelectors={elementsSelectors}
    />
  ),
  searchable: true,
  searchText: (record) => record[name]?.join(" "),
});

export const registrationDispoColumn = {
  title: "Disponibilité",
  dataIndex: "dispo",
  render: (text, record) => {
    const nextOrCurrentSession = record.nextOrCurrentSession;
    if (nextOrCurrentSession) {
      const nextSessionIn = -moment().diff(nextOrCurrentSession.startDate, "minutes");
      if (nextSessionIn <= 0) {
        return <Link to={`../sessions/${nextOrCurrentSession.sessionId}`}>Occupé.e</Link>;
      } else if (nextSessionIn < 120) {
        return <Link to={`../sessions/${nextOrCurrentSession.sessionId}`}>Bientôt occupé.e</Link>;
      }
    }

    return "Libre";
  },
  sorter: (a, b) =>
    listSorter.date(a.nextOrCurrentSession?.startDate, b.nextOrCurrentSession?.startDate),
};

export const ROLES_MAPPING = [
  {
    label: "Invité⋅e",
    value: "guest",
    color: "#6cdac5",
    explanation: "Droits de consultation seulement.",
  },
  {
    label: "Contributeur⋅ice",
    value: "contrib",
    color: "#5f98ed",
    explanation: "Droits de consultation, de création et d'édition.",
  },
  {
    label: "Administrateur⋅ice",
    value: "admin",
    color: "#1330f5",
    explanation:
      "Droits de consultation, de création, d'édition et de suppression. Accès à la configuration avancée et à la gestion des droits.",
  },
];

export const roleTag = (text, small = false) => {
  const role = ROLES_MAPPING.find((r) => r.value === text);
  return <Tag color={role?.color}>{small ? role?.label[0] : role?.label}</Tag>;
};

// Generate participants columns
export const generateRegistrationsColumns = (project, {start, middle, end} = {}) =>
  [
    ...(start || []),
    {
      title: "Prénom",
      dataIndex: "firstName",
      render: (text, record) => (
        <>
          {record.role && roleTag(record.role, true)}
          {record.user?.firstName}
        </>
      ),
      sorter: (a, b) => listSorter.text(a.user?.firstName, b.user?.firstName),
      searchable: true,
      searchText: (record) => record.user?.firstName,
    },
    {
      title: "Nom",
      dataIndex: "lastName",
      render: (text, record) => record.user?.lastName,
      sorter: (a, b) => listSorter.text(a.user?.lastName, b.user?.lastName),
      defaultSortOrder: "ascend",
      searchable: true,
      searchText: (record) => record.user?.lastName,
    },
    {
      title: "Email",
      dataIndex: "email",
      render: (text, record) => (
        <>
          {record.invitationToken && <WaitingInvitationTag />}
          {record.user.email}
        </>
      ),
      sorter: (a, b) => listSorter.text(a.user.email, b.user.email),
      searchable: true,
      searchText: (record) => record.user.email,
    },
    ...(middle || []),
    ...(project.formMapping?.map(({formField, columnName}) => ({
      title: columnName,
      dataIndex: columnName,
      render: (text, record) => cleanAnswer(record.specific?.[formField]),
      sorter: (a, b) =>
        listSorter.text(cleanAnswer(a.specific?.[formField]), cleanAnswer(b.specific?.[formField])),
      searchable: true,
      searchText: (record) => cleanAnswer(record.specific?.[formField]),
    })) || []),
    editableTagsColumn("tags", "Tags", registrationsActions, registrationsSelectors),
    project.useTeams && {
      title: "Équipe",
      dataIndex: "team",
      render: (text, record) =>
        listRenderer.listOfClickableElements(record.teamsSubscriptions, (el, index) => (
          <Link to={`../teams/${el.team._id}`} key={index}>
            {el.team.name}
          </Link>
        )),
      sorter: (a, b) => listSorter.text(a.teamsSubscriptionsNames, b.teamsSubscriptionsNames),
      searchable: true,
      searchText: (record) => record.teamsSubscriptionsNames,
    },
    {
      title: "Jours",
      dataIndex: "numberOfDaysOfPresence",
      sorter: (a, b) => listSorter.number(a.numberOfDaysOfPresence, b.numberOfDaysOfPresence),
      searchable: true,
      searchText: (record) => record.numberOfDaysOfPresence,
      width: 95,
    },
    ...(end || []),
  ].filter((el) => !!el);

const getCategoryTitle = (el) => el.activity?.category?.name;
const getSessionName = (session, simpleMode = true, path?) => {
  const sessionNamePrefix = session.name?.length > 0 ? session.name + " - " : "";
  return simpleMode ? (
    sessionNamePrefix + session.activity?.name
  ) : (
    <>
      {sessionNamePrefix}
      <Link to={`${path}/activities/${session.activity?._id}`}>{session.activity?.name}</Link>
    </>
  );
};
export const generateSessionsColumns = (
  path,
  showPlaces,
  showTeams,
  byDate = false,
  simpleMode = false,
  registrations
) =>
  [
    {
      title: "Catégorie",
      dataIndex: "category",
      sorter: (a, b) => listSorter.text(getCategoryTitle(a), getCategoryTitle(b)),
      render: (text, record) => (
        <Link to={`${path}/categories/${record.activity?.category?._id}`}>
          <Tag
            style={{
              cursor: "pointer",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              overflow: "hidden",
              maxWidth: 125,
            }}
            color={record.activity?.category?.color}>
            {getCategoryTitle(record)}
          </Tag>
        </Link>
      ),
      searchable: true,
      width: 140,
      ellipsis: true,
      searchText: getCategoryTitle,
    },
    {
      title: "Activité",
      dataIndex: "activity",
      render: (text, record) => getSessionName(record, simpleMode, path),
      // Filter by activity name, then by session name
      sorter: (a, b) =>
        listSorter.text(a.activity?.name, b.activity?.name) || listSorter.text(a.name, b.name),
      searchable: true,
      searchText: (record) => getSessionName(record),
    },
    {
      title: "Début – Fin",
      dataIndex: "start",
      sorter: (a, b) => listSorter.date(a.start, b.start),
      render: (text, record) =>
        listRenderer.longDateTimeRangeFormat(record.start, record.end, true),
      defaultSortOrder: byDate && "ascend",
      width: 200,
      searchable: true,
      searchText: (record) => listRenderer.longDateTimeRangeFormat(record.start, record.end, true),
    },
    {
      title: "Encadrant⋅es",
      dataIndex: "stewards",
      render: (text, record) =>
        listRenderer.listOfClickableElements(record.stewards, (el, index) =>
          simpleMode ? (
            personName(el)
          ) : (
            <Link to={`${path}/stewards/${el._id}`} key={index}>
              {personName(el)}
            </Link>
          )
        ),
      sorter: (a, b) =>
        listSorter.text(
          a.stewards.map(personName).join(", "),
          b.stewards.map(personName).join(", ")
        ),
      searchable: true,
      ellipsis: true,
      searchText: (record) => record.stewards.map(personName).join(", "),
    },
    showPlaces && {
      title: "Espaces",
      dataIndex: "places",
      render: (text, record) =>
        listRenderer.listOfClickableElements(record.places, (el, index) =>
          simpleMode ? (
            el.name
          ) : (
            <Link to={`${path}/places/${el._id}`} key={index}>
              {el.name}
            </Link>
          )
        ),
      sorter: (a, b) =>
        listSorter.text(
          a.places.map((el) => el.name).join(", "),
          b.places.map((el) => el.name).join(", ")
        ),
      searchable: true,
      ellipsis: true,
      searchText: (record) => record.places.map((el) => el.name).join(", "),
    },
    showTeams && {
      title: "Équipe",
      dataIndex: "team",
      render: (text, record) => (
        <Link to={`../teams/${record.team?._id}`}>{record.team?.name}</Link>
      ),
      sorter: (a, b) => listSorter.text(a.team?.name, b.team?.name),
      searchable: true,
      ellipsis: true,
      searchText: (record) => record.team?.name,
    },
    editableTagsColumn("tags", "Tags", sessionsActions, sessionsSelectors),
    {
      title: "Remplissage",
      dataIndex: "numberParticipants",
      render: (text, record) => {
        return (
          <SessionFilling
            computedMaxNumberOfParticipants={record.computedMaxNumberOfParticipants}
            numberOfParticipants={record.numberParticipants}
            showLabel={false}
          />
        );
      },
      sorter: (a, b) => {
        const maxA = a.computedMaxNumberOfParticipants;
        const maxB = b.computedMaxNumberOfParticipants;
        return listSorter.number(
          maxA !== undefined && maxA !== 0 ? a.numberParticipants / maxA : -1,
          maxB !== undefined && maxB !== 0 ? b.numberParticipants / maxB : -1
        );
      },
      width: 150,
    },
    registrations && {
      title: "Arrivées",
      dataIndex: "allArrived",
      render: (text, record) => {
        const numberOfArrivedParticipants = registrations.filter(
          (r) => r.hasCheckedIn && r.sessionsSubscriptions.find((ss) => ss.session === record._id)
        )?.length;
        const color = numberOfArrivedParticipants >= record.numberParticipants ? "green" : "red";
        const string = record.numberParticipants
          ? `${numberOfArrivedParticipants}/${record.numberParticipants}`
          : numberOfArrivedParticipants;
        return record.numberParticipants > 0 ? (
          <span
            title={`Sur les ${record.numberParticipants} participant⋅es inscrites, ${numberOfArrivedParticipants} sont arrivé⋅es à l'événement\n(case "Arrivé.e" dans la liste des participant⋅es).`}
            style={{color}}>
            {string}
          </span>
        ) : (
          ""
        );
      },
      width: 95,
    },
  ].filter((el) => !!el);

export const generateColumnsTeams = (path) => [
  {
    title: "Catégorie",
    dataIndex: "category",
    sorter: (a, b) => listSorter.text(getCategoryTitle(a), getCategoryTitle(b)),
    render: (text, record) => (
      <Link to={`${path}/categories/${record.activity?.category?._id}`}>
        <Tag
          style={{
            cursor: "pointer",
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
            overflow: "hidden",
            maxWidth: 125,
          }}
          color={record.activity?.category?.color}>
          {getCategoryTitle(record)}
        </Tag>
      </Link>
    ),
    searchable: true,
    width: 140,
    ellipsis: true,
    searchText: getCategoryTitle,
  },
  {
    title: "Activité liée",
    dataIndex: "activity",
    render: (text, record) => (
      <Link to={`${path}/activities/${record.activity?._id}`}>{record.activity?.name}</Link>
    ),
    sorter: (a, b) => listSorter.text(a.activity?.name, b.activity?.name),
    searchable: true,
    searchText: (record) => record.activity?.name,
  },
  {
    title: "Nom",
    dataIndex: "name",
    sorter: (a, b) => listSorter.text(a.name, b.name),
    searchable: true,
  },
];

export const generateSubscriptionInfo = (record, sessionSubscription, registrations, showTeam) => {
  if (!sessionSubscription) return "Inscription non enregistrée";
  let res;

  // added by who ?
  if (registrations) {
    const subscribedByRegistration = registrations.find(
      (r) => r.user._id === sessionSubscription.subscribedBy
    );

    if (subscribedByRegistration?._id === record._id) res = "Manuelle";
    else if (subscribedByRegistration?.role) {
      const subscribedByName = ` ${personName(subscribedByRegistration.user)}`;

      res = (
        <>
          Par{" "}
          <Link to={`../../participants/${subscribedByRegistration._id}`}>{subscribedByName}</Link>
        </>
      );
    }
  }

  // Added as a session or team subscription ?
  if (showTeam) {
    const sessionSubscriptionTeam = record.teamsSubscriptions
      .map((ts) => ts.team)
      ?.find((t) => t._id === sessionSubscription.team);
    if (sessionSubscriptionTeam) {
      res = (
        <>
          {res}
          <br />
          Via équipe{" "}
          <Link to={`../../teams/${sessionSubscriptionTeam._id}`}>
            {sessionSubscriptionTeam.name}
          </Link>
        </>
      );
    }
  }
  return res;
};
