import {
  createLocalStorageGetSetter,
  getLocalStorage,
  setLocalStorage,
} from "./localStorageUtilities";
import {Alert, Button, message, Popconfirm, Switch, Tooltip} from "antd";
import localforage from "localforage";
import {persistReducer, persistStore} from "@rifkyrangkuti/redux-persist";
import hardSet from "@rifkyrangkuti/redux-persist/lib/stateReconciler/hardSet";
import React, {useEffect, useState} from "react";
import {sessionsActions} from "../features/sessions";
import {activitiesActions} from "../features/activities";
import {placesActions} from "../features/places";
import {stewardsActions} from "../features/stewards";
import {teamsActions} from "../features/teams";
import {registrationsActions} from "../features/registrations";
import {DownloadOutlined, SyncOutlined, WifiOutlined} from "@ant-design/icons";
import {InputElement} from "../components/common/InputElement";
import {configureStore} from "@reduxjs/toolkit";
import {useDispatch, useSelector} from "react-redux";
import {navigate} from "@reach/router";
import {currentProjectSelectors} from "../features/currentProject";
import {useOnlineStatus} from "./onlineStatusCheckerUtilities";

/**
 * Variables setters and getters
 */

export const displayOfflineModeFeatureGetSetter = createLocalStorageGetSetter(
  "display-offline-mode-feature",
  false
);
export const DISPLAY_OFFLINE_MODE_FEATURE = displayOfflineModeFeatureGetSetter();

export let OFFLINE_MODE = DISPLAY_OFFLINE_MODE_FEATURE && getLocalStorage("offline-mode", false);

export const activatingOfflineModeGetSetter = createLocalStorageGetSetter(
  "activating-offline-mode",
  false
);
export let ACTIVATING_OFFLINE_MODE =
  DISPLAY_OFFLINE_MODE_FEATURE && activatingOfflineModeGetSetter();

/**
 * Activates or not the variables for offline mode, on start of the app.
 */
export const initializeOfflineModeOnStart = () => {
  // Get current state of offline mode in local storage
  const shouldSave = activatingOfflineModeGetSetter();

  // If Offline mode is active, just notify it
  !shouldSave && OFFLINE_MODE && message.success("Mode offline actif.", 5);

  // If we should activate offline mode right now, here is what we do
  if (!OFFLINE_MODE && shouldSave) {
    // Activate offline mode
    OFFLINE_MODE = true;
    setLocalStorage("offline-mode", true);
  }
};
initializeOfflineModeOnStart(); // Initialize it right away

/**
 * Returns the reducer depending of the activation of the offline mode or not
 * @param mainReducer the main reducer
 * @param enhancers optional enhancers
 * @return {*}
 */
export const getConfiguredStoreAndPersistor = (mainReducer, enhancers: Array<any> = undefined) => {
  const store = configureStore({
    reducer: OFFLINE_MODE
      ? persistReducer({key: "root", storage: localforage, stateReconciler: hardSet}, mainReducer)
      : mainReducer,
    enhancers,
  });
  return [store, OFFLINE_MODE && persistStore(store)];
};

const notifyReloadMessage = () =>
  message.open({
    key: "reload",
    content: (
      <div className="containerH buttons-container">
        <span>Rechargez la page pour appliquer les modifications.</span>
        <Button type="primary" onClick={() => window.location.reload()}>
          Recharger
        </Button>
      </div>
    ),
    duration: 10,
  });

/**
 * Sets the offline mode to be activated or deactivated on next start of the app
 * @param value activation of the offline mode
 * @param notifyReload if we should notify the usere about reloading th page
 */
export const setOfflineMode = async (value, notifyReload = true) => {
  activatingOfflineModeGetSetter.set(value);
  if (!value) {
    setLocalStorage("offline-mode", false);
    await localforage.clear();
  }
  notifyReload && notifyReloadMessage();
};

const loadAllEntities = async (dispatch) =>
  await Promise.all([
    dispatch(sessionsActions.loadList()),
    dispatch(sessionsActions.loadCategoriesList()),
    dispatch(activitiesActions.loadList()),
    dispatch(placesActions.loadList()),
    dispatch(stewardsActions.loadList()),
    dispatch(teamsActions.loadList()),
    dispatch(registrationsActions.loadList()),
  ]);

const prepareOfflineMode = async (dispatch, registrationId) => {
  if (ACTIVATING_OFFLINE_MODE && registrationId) {
    message.loading({
      content: "Préparation du mode offline.",
      key: "offline-preparation",
      duration: 0,
    });

    await loadAllEntities(dispatch);

    activatingOfflineModeGetSetter.set(false);
    ACTIVATING_OFFLINE_MODE = false;

    message.success({
      content: (
        <>
          <strong>Mode offline actif.</strong>
          <p>Toutes les données sont chargées pour le mode offline.</p>
          ⚠️ Le mode offline est à but <i>purement consultatif</i>. Vous ne pouvez pas apporter de
          modifications aux données. Désactivez le mode offline pour apporter des modifications.
        </>
      ),
      key: "offline-preparation",
      duration: 8,
      onClick: () => message.destroy("offline-preparation"),
    });
  }
};

/**
 * Used to load all entities of the project automatically on start of the app
 * @param dispatch
 * @param registrationId registration Id
 */
export const useOfflineMode = (registrationId) => {
  const dispatch = useDispatch();
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  useEffect(() => {
    currentProject._id &&
      prepareOfflineMode(dispatch, currentProject._id, registrationId).catch(() => {
        message.error("Oups, le mode offline n'a pas marché !");
        setOfflineMode(false);
      });
  }, [registrationId, currentProject._id]);
};

export const useRedirectToProjectIfOffline = () => {
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  // Redirect automatically to the saved project when offline
  useEffect(() => {
    if (OFFLINE_MODE && currentProject._id)
      message
        .loading("Redirection vers le projet sauvegardé.", 2)
        .then(() => navigate(`/${currentProject.slug || currentProject._id}`));
  }, [currentProject._id]);
};

/**
 * Switch component to activate of deactivate the offline mode
 * @return {JSX.Element}
 */
export const OnlineOfflineSwitch = () => {
  const [offlineState, setOfflineState] = useState(OFFLINE_MODE);
  const online = useOnlineStatus();

  const shouldWarnOnDeactivation = !online && OFFLINE_MODE;

  const toggleOfflineMode = () => {
    setOfflineMode(!offlineState);
    setOfflineState(!offlineState);
  };

  const OfflineSwitch = ({onClick}) => (
    <Switch
      checked={offlineState}
      onClick={onClick}
      checkedChildren={
        <>
          <DownloadOutlined /> Offline
        </>
      }
      unCheckedChildren={
        <>
          <WifiOutlined /> Online
        </>
      }
    />
  );

  return (
    <>
      {shouldWarnOnDeactivation ? (
        <Popconfirm
          title={
            <>
              Vous n'êtes actuellement pas connecté⋅e <br />
              à internet, vous risquez de ne plus pouvoir <br />
              recharger de nouvelles données. <br />
              Voulez-vous continuer ?
            </>
          }
          placement="topLeft"
          okText="Désactiver quand même"
          okButtonProps={{danger: true}}
          cancelText="Annuler"
          onConfirm={toggleOfflineMode}>
          <OfflineSwitch />
        </Popconfirm>
      ) : (
        <OfflineSwitch onClick={toggleOfflineMode} />
      )}
      {offlineState && online && (
        <Tooltip title="Actualiser les données">
          <Button
            ghost
            style={{marginLeft: 8}}
            icon={<SyncOutlined />}
            onClick={async () => {
              await setOfflineMode(false, false);
              await setOfflineMode(true, false);
              window.location.reload();
            }}
          />
        </Tooltip>
      )}
    </>
  );
};

/**
 * The switch component to activte or deactivate completely the display of the offline mode feature
 * @return {JSX.Element}
 */
export const DisplayOfflineModeFeatureSwitch = () => (
  <>
    <Alert
      style={{marginBottom: 26}}
      message="Gardez vos données avec vous, même quand vous êtes au fond de la pampa !"
      description={
        <>
          <p>
            NOÉ vous propose de pouvoir consulter vos données même quand vous n'avez pas de réseau.
          </p>
          <ol>
            <li>Activez l'interrupteur ci-dessous et rechargez la page.</li>
            <li>
              Un contrôle de mode online/offline apparaît en bas de votre barre latérale. Activez le
              mode offline et rechargez la page à nouveau.
            </li>
            <li>
              NOÉ va alors charger toutes les données possibles, et vous afficher un message dès que
              tout est prêt.
            </li>
            <li>
              Vos données sont alors gardées en lieu sûr sur votre appareil, quoi que vous fassiez.
              Vous pouvez fermer la page web, ou même éteindre votre téléphone ou votre PC: les
              données sont conservées !
            </li>
            <li>
              Vous avez de nouveau internet et souhaitez réactualiser vos données ? Cliquez sur le
              bouton <SyncOutlined /> pour recharger des données fraîches.
            </li>
          </ol>
          <p> Bien sûr, cela vient avec quelques compromis :</p>
          <ul>
            <li>
              Quand vous êtes en mode offline, <strong>NOÉ EST "coupé du monde extérieur"</strong>.
              Si d'autres personnes font des modifications sur la plateforme, ces modifications
              n'apparaîtront pas sur votre appareil tant que vous êtes en mode offline.
            </li>
            <li>
              C'est donc à vous de choisir quand vous avez envie de mettre à jour vos données, en
              cliquant sur le bouton <SyncOutlined /> ou en quittant le monde offline.
            </li>
          </ul>
        </>
      }
    />
    <InputElement.Switch
      label="Afficher la fonctionnalité de mode offline"
      defaultChecked={displayOfflineModeFeatureGetSetter()}
      onChange={(val) => {
        displayOfflineModeFeatureGetSetter.set(val);
        if (!val) localforage.clear();

        notifyReloadMessage();
      }}
    />
  </>
);
