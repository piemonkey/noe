import moment from "moment";
import {Drawer} from "antd";
import {useMemo, useState} from "react";

const compare = (a, b) => {
  if (a === b) {
    return 0;
  } else {
    const aDefined = a !== undefined && a !== null;
    const bDefined = b !== undefined && b !== null;
    if (aDefined && bDefined) return a > b ? 1 : -1;
    else if (aDefined) return 1;
    else if (bDefined) return -1;
    else return 0;
  }
};

export const normalize = (string) =>
  string
    ?.toString() // Must be done if the string is actually a number
    .toLowerCase()
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "") || "";

export const listSorter = {
  date: (a, b) => (a ? moment(a).valueOf() : -Infinity) - (b ? moment(b).valueOf() : -Infinity),

  text: (a, b) => compare(normalize(a).replace(/ /g, ""), normalize(b).replace(/ /g, "")),

  number: compare,
};

export const listRenderer = {
  timeFormat: (date) => (date ? moment(date).format("LT") : ""),

  timeRangeFormat: (startDate, endDate) =>
    startDate && endDate
      ? listRenderer.timeFormat(startDate) + " – " + listRenderer.timeFormat(endDate)
      : "",

  shortDateFormat: (date) => {
    if (!date) return "";
    const parsedDate = Date.parse(date);
    return Intl.DateTimeFormat("fr-FR", {
      day: "numeric",
      month: "numeric",
    }).format(parsedDate);
  },

  longDateFormat: (date, short = false) => {
    if (!date) return "";
    const parsedDate = Date.parse(date);
    let day = Intl.DateTimeFormat("fr-FR", {
      day: "numeric",
      weekday: short ? "short" : "long",
    }).format(parsedDate);
    day = day.charAt(0).toUpperCase() + day.slice(1); // capitalize day
    let month = Intl.DateTimeFormat("fr-FR", {
      month: short ? "short" : "long",
    }).format(parsedDate);
    month = month.charAt(0).toUpperCase() + month.slice(1); // capitalize month
    return day + " " + month;
  },

  longDateTimeFormat: (date, short) =>
    date ? listRenderer.longDateFormat(date, short) + " " + listRenderer.timeFormat(date) : "",

  longDateRangeFormat: (startDate, endDate, short) => {
    if (!startDate || !endDate) return "";
    const start = moment(startDate);
    const end = moment(endDate);
    return `${listRenderer.longDateFormat(start, short)} – ${listRenderer.longDateFormat(
      end,
      short
    )}`;
  },

  longDateTimeRangeFormat: (startDate, endDate, short) => {
    if (!startDate || !endDate) return "";
    const start = moment(startDate);
    const end = moment(endDate);
    if (start.isSame(end, "day")) {
      return `${listRenderer.longDateTimeFormat(start, short)} – ${listRenderer.timeFormat(end)}`;
    } else {
      return `${listRenderer.longDateTimeFormat(start, short)} – ${listRenderer.longDateTimeFormat(
        end,
        short
      )}`;
    }
  },

  durationFormat: (minutes, short = false) => {
    if (minutes < 60) return `${Math.floor(minutes)}min`;
    const hours = moment.utc().startOf("day").add(minutes, "minutes").format("H[h]mm");
    const numberOfDays = Math.floor(minutes / 1440);
    return (
      `${numberOfDays > 0 ? `${numberOfDays} jours et ` : ""}${hours}` +
      (short ? "" : ` (${Math.floor(minutes)}min)`)
    );
  },

  listOfClickableElements: (elements, clickableElement) =>
    elements
      ?.map((el, index) => el && clickableElement(el, index))
      .reduce((acc, el) => (acc ? [acc, ", ", el] : el), null) || "",
};

// const getNestedField = (obj, fieldPathArray) =>
//   fieldPathArray.reduce((o, key) => (o && o[key] !== "undefined" ? o[key] : undefined), obj);

export const matchesSearchValueWords = (
  item: any,
  normalizedSearchValueWords: [string],
  searchInFields: [string] | ((item: any) => [string])
): boolean => {
  // For every word in the user search...

  // If the item is an object, and some searchInFields fields are given, then pick them.
  // Otherwise, we assume that item is already an array of strings to search into.
  const potentialMatches =
    typeof searchInFields === "function"
      ? searchInFields(item)
      : searchInFields.map((field) => item[field]);

  // Make sure that for every search word given, there is a match in our potential matches
  return normalizedSearchValueWords.every((word) =>
    potentialMatches.find((field) => field?.length && normalize(field).includes(word))
  );
};

export const searchInObjectsList = (
  searchValue: string,
  list: [any],
  searchInFields: [string] | ((item: any) => [string])
) => {
  if (!list) return;
  if (!searchValue || searchValue.length === 0) return list;

  const normalizedSearchValueWords = normalize(searchValue).split(" ");

  // Filter elements that match search value
  return list.filter((item) =>
    matchesSearchValueWords(item, normalizedSearchValueWords, searchInFields)
  );
};

export const removeDuplicates = (array) => array?.filter((v, i, a) => a.indexOf(v) === i); // Remove duplicates

export const useDrawer = (props, memo = []) => {
  const [visible, setVisible] = useState(false);

  const MyDrawer = useMemo(
    () =>
      ({children}) =>
        (
          <Drawer placement="right" onClose={() => setVisible(false)} visible={visible} {...props}>
            {children}
          </Drawer>
        ),
    [visible, ...memo]
  );

  return [setVisible, MyDrawer];
};
