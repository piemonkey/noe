import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {projectsActions, projectsSelectors} from "../../features/projects.js";
import {ListPage} from "../common/ListPage";
import {listRenderer, listSorter} from "../../helpers/listUtilities";
import {currentProjectActions, currentProjectSelectors} from "../../features/currentProject";
import {PlaySquareOutlined, SaveOutlined, SettingOutlined} from "@ant-design/icons";
import {Button, Tooltip} from "antd";
import {usersSelectors} from "../../features/users.js";
import {roleTag} from "../../helpers/tableUtilities";
import {registrationsActions} from "../../features/registrations";
import {currentUserActions} from "../../features/currentUser";
import {URLS} from "../../app/configuration";

export function ProjectList({navigate, userCanCreateProject}) {
  const dispatch = useDispatch();
  const projects = useSelector(projectsSelectors.selectList);
  const alreadyLoadedProject = useSelector(currentProjectSelectors.selectProject);
  const currentUser = useSelector(usersSelectors.selectEditing);

  useEffect(() => {
    dispatch(projectsActions.loadList());
  }, []);

  const projectsWithRegistrationRoles = projects.map((project) => ({
    ...project,
    registrationRole: currentUser.registrations.find((r) => r.project === project._id)?.role,
  }));

  const columns = [
    {
      title: "Nom",
      dataIndex: "name",
      sorter: (a, b) => listSorter.text(a.name, b.name),
      render: (text, record) => (
        <>
          {text}
          {alreadyLoadedProject._id === record._id && (
            <Tooltip
              title={
                "Cette icône indique que certaines données de cet événement sont déjà chargées dans votre navigateur. " +
                "Si vous revenez sur cet événement, vous les retrouverez telles quelles sans avoir à attendre à nouveau qu'elles chargent. " +
                "Si vous décidez d'accéder à un autre événement, les données seront remplacées par celles du nouvel événement."
              }>
              <SaveOutlined style={{marginLeft: 8}} />
            </Tooltip>
          )}
        </>
      ),
      searchable: true,
    },
    {
      title: "Accès interfaces",
      key: "access",
      render: (text, record) => (
        <>
          <Button
            icon={<SettingOutlined />}
            type="link"
            style={{marginRight: 8}}
            onClick={() => cleanStateAndNavigate(record.slug || record._id)}>
            Orga
          </Button>
          <Button
            type="link"
            href={`${URLS.INSCRIPTION_FRONT}/${record._id}`}
            icon={<PlaySquareOutlined />}>
            Participant⋅e
          </Button>
        </>
      ),
    },
    {
      title: "Début – Fin",
      dataIndex: "start",
      sorter: (a, b) => listSorter.date(a.start, b.start),
      render: (text, record) => listRenderer.longDateRangeFormat(record.start, record.end, true),
      searchable: true,
      searchText: (record) => listRenderer.longDateRangeFormat(record.start, record.end, true),
    },
    {
      title: "Rôle",
      dataIndex: "role",
      render: (text, record) =>
        record.registrationRole ? (
          roleTag(record.registrationRole)
        ) : (
          <Button onClick={() => onRequireAdmin(record)}>Devenir administrateur⋅ice</Button>
        ),
      sorter: (a, b) => listSorter.text(a.registrationRole, b.registrationRole),
    },
  ];

  const onRequireAdmin = async (project) => {
    await dispatch(
      registrationsActions.persist({
        _id: "new",
        user: currentUser._id,
        project: project._id,
        role: "admin",
      })
    );
    dispatch(currentUserActions.refreshAuthTokens()); // Reload the user and all its registrations
  };

  const cleanStateAndNavigate = async (projectId) => {
    const shouldReload =
      alreadyLoadedProject._id &&
      alreadyLoadedProject._id !== projectId &&
      alreadyLoadedProject.slug !== projectId;
    // If there is already a loaded project, and that it's not the same as the requested project, clean everything. Otherwise, keep the data
    if (shouldReload) await dispatch(currentProjectActions.cleanProject());

    // Then navigate only after cleaning
    navigate("../" + projectId);
  };

  return (
    <ListPage
      title="Événements"
      buttonTitle={userCanCreateProject ? "Créer un événement" : undefined}
      elementsActions={projectsActions}
      navigateFn={cleanStateAndNavigate}
      navigable
      deletable
      columns={columns}
      dataSource={projectsWithRegistrationRoles}
    />
  );
}
