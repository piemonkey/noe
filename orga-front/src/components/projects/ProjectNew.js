import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {projectsActions, projectsSelectors} from "../../features/projects.js";
import {InputElement} from "../common/InputElement";
import {EditPage} from "../common/EditPage";
import {Availability} from "../utils/Availability";
import {useLoadEditing} from "../../helpers/editingUtilities";
import {Alert} from "antd";
import {CardElement} from "../common/LayoutElement";
import {AdditionalFeatures} from "../config/FeaturesFormContent";

export function ProjectNew() {
  const dispatch = useDispatch();
  const project = useSelector(projectsSelectors.selectEditing);
  const [isModified, setIsModified] = useState(false);

  useLoadEditing(projectsActions, "new");

  const toggle = (key) => {
    setIsModified(true);
    dispatch(projectsActions.changeEditing({[key]: !project[key]}));
  };

  return (
    <EditPage
      createTitle="Créer un événement"
      record={project}
      initialValues={project}
      elementsActions={projectsActions}
      forceModifButtonActivation={isModified}
      navigateAfterValidation={false}
      createAndStayButton={false}>
      <Alert
        type="success"
        style={{marginBottom: 26}}
        message="Bienvenue sur NOÉ !"
        description="Cette page vous permet de créer votre événement en renseignant les informations de base."
      />
      <CardElement>
        <div className="container-grid two-per-row">
          <InputElement.Text
            label="Nom de l'événement"
            name="name"
            placeholder="nom"
            rules={[{required: true}]}
          />
          <InputElement.Switch
            label="Rendre l'événement public"
            tooltip="L'événement sera visible dans la liste de tous les événements publics de la plateforme, côté participant⋅e."
            name="isPublic"
          />
        </div>
      </CardElement>
      <div className="container-grid">
        <Availability
          title="Plages d'ouverture de l'événement"
          setIsModified={setIsModified}
          subtitle={
            "Ce sont les heures d'ouverture de votre événement. Vous pourrez revenir les modifier dans l'onglet \"Plages d'ouverture\" de la page Configuration."
          }
          entity={project}
          actions={projectsActions}
        />
      </div>

      <AdditionalFeatures
        subtitle={
          <div style={{marginTop: -14, marginBottom: 26}}>
            Vous pouvez configurer NOÉ selon vos besoins, en activant ou en désactivant certaines
            fonctionnalités de la plateforme. Retrouvez ces contrôles dans les onglets "Avancé" >
            "Fonctionnalités" de la page Configuration.
          </div>
        }
        project={project}
        toggle={toggle}
        noWarning
      />
    </EditPage>
  );
}
