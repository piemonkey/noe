import React, {lazy, useCallback, useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {projectsSelectors} from "../../features/projects.js";
import {stewardsActions, stewardsSelectors} from "../../features/stewards.js";
import {Button, Checkbox, Space, Tag, Tooltip} from "antd";
import {listRenderer, listSorter} from "../../helpers/listUtilities";
import {InputElement} from "../common/InputElement";
import {ListPage} from "../common/ListPage";
import {
  columnsStewards,
  generateRegistrationsColumns,
  registrationDispoColumn,
} from "../../helpers/tableUtilities";
import {CardElement, LayoutElement} from "../common/LayoutElement";
import {VolunteeringGauge} from "../common/VolunteeringGauge";
import {
  EditOutlined,
  UsergroupAddOutlined,
  UsergroupDeleteOutlined,
  UserSwitchOutlined,
} from "@ant-design/icons";
import {registrationsActions} from "../../features/registrations";
import {Link} from "@reach/router";
import {GetPdfPlanningButton, useNewElementModal} from "../common/EditPage";
import {
  getFullHeightWithMargins,
  navbarHeight,
  useColumnsBlacklistingSelector,
  useWindowDimensions,
} from "../../helpers/viewUtilities";
import {FormElement} from "../common/FormElement";
import {currentUserSelectors} from "../../features/currentUser";
import {personName} from "../../helpers/utilities";
import {TableElement} from "../common/TableElement";
import {StewardEdit} from "../stewards/StewardEdit";
import moment from "moment";
import {URLS} from "../../app/configuration";
const RegistrationForm = lazy(() =>
  import(/* webpackPrefetch: true */ "../common/RegistrationForm").then((module) => ({
    default: module["RegistrationForm"],
  }))
);

export const ChangeIdentityButton = ({registration, envId}) => (
  <Tooltip title="Changer d'identité" placement="left">
    <Button
      type="link"
      icon={<UserSwitchOutlined />}
      href={`${URLS.INSCRIPTION_FRONT}/${envId}/participants?connectedAs=${registration._id}`}
    />
  </Tooltip>
);

export function ParticipantList({
  navigate,
  registrations,
  displayUnbookedUsers,
  setDisplayUnbookedUsers,
  advancedMode,
  setAdvancedMode,
  envId,
}) {
  const {windowWidth} = useWindowDimensions();
  const contentWidth =
    windowWidth - document.getElementsByClassName("ant-layout-sider")?.[0]?.offsetWidth - 30;
  const project = useSelector(projectsSelectors.selectEditing);
  const stewards = useSelector(stewardsSelectors.selectList);
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelector();
  const dispatch = useDispatch();
  const [showModalStewards, setShowModalStewards] = useState(false);
  const [selectedSteward, setSelectedSteward] = useState([]);
  const [selectedRegistration, setSelectedRegistration] = useState();
  const [setShowNewStewardModal, NewStewardModal] = useNewElementModal(StewardEdit);

  const registrationsFinal = displayUnbookedUsers
    ? registrations
    : registrations.filter((r) => r.availabilitySlots?.length > 0);

  const openStewardSelectionModal = (record) => {
    setSelectedRegistration(record);
    setShowModalStewards(true);
  };

  const closeStewardSelectionModal = () => setShowModalStewards(false);

  const removeSteward = (record) =>
    dispatch(
      registrationsActions.persist({
        steward: null,
        _id: record._id,
      })
    );

  const modifySteward = () => {
    const {key, ...selectedStewardWithoutKey} = selectedSteward[0];
    const registrationToUpdate = {
      _id: selectedRegistration._id,
      steward: selectedStewardWithoutKey,
    };
    dispatch(registrationsActions.persist(registrationToUpdate));
    setShowModalStewards(false);
  };

  const modifyBooleanFieldFn = (registrationId, field) => (e) => {
    const registrationToUpdate = {
      _id: registrationId,
      [field]: e.target.checked,
    };
    dispatch(registrationsActions.persist(registrationToUpdate));
  };

  const columns = generateRegistrationsColumns(project, {
    start: [
      {
        width: 48,
        render: (text, record) => (
          <GetPdfPlanningButton elementsActions={registrationsActions} id={record._id} noText />
        ),
      },
      {
        width: 65,
        title: "Date",
        dataIndex: "createdAt",
        sorter: (a, b) => listSorter.date(a.createdAt, b.createdAt),
        render: (text, record) => listRenderer.shortDateFormat(record.createdAt),
      },
    ],
    middle: [
      {
        title: "Encadrant⋅e",
        dataIndex: "steward",
        sorter: (a, b) => listSorter.text(personName(a.steward), personName(b.steward)),
        render: (text, record) => (
          <>
            {record.steward ? (
              <Space>
                <Link to={`../stewards/${record.steward._id}`}>{personName(record.steward)}</Link>

                <Tooltip title="Modifier l'encadrant⋅e associé⋅e">
                  <Button
                    type="link"
                    icon={<EditOutlined />}
                    onClick={() => openStewardSelectionModal(record)}
                  />
                </Tooltip>

                <Tooltip title="Retirer l'encadrant⋅e associé⋅e">
                  <Button
                    danger
                    type="link"
                    icon={<UsergroupDeleteOutlined />}
                    onClick={() => removeSteward(record)}
                  />
                </Tooltip>
              </Space>
            ) : (
              <Tooltip title="Associer un⋅e encadrant⋅e">
                <Button
                  type="link"
                  icon={<UsergroupAddOutlined />}
                  onClick={() => openStewardSelectionModal(record)}
                />
              </Tooltip>
            )}
          </>
        ),
        searchText: (record) => personName(record.steward),
        searchable: true,
      },
    ],
    end: [
      {
        title: "Arrivé.e",
        dataIndex: "hasCheckedIn",
        render: (text, record) => (
          <Checkbox
            checked={record.hasCheckedIn}
            onChange={modifyBooleanFieldFn(record._id, "hasCheckedIn")}
          />
        ),
        filters: [
          {text: "Oui", value: true},
          {text: "Non", value: false},
        ],
        onFilter: (value, record) => record.hasCheckedIn === value,
        width: 95,
      },
      {
        title: "Date d'arrivée",
        dataIndex: "arrivalDateTime",
        render: (text, record) => {
          // If arrived, green. If not arrived and date is past, red. Else, no color
          const color = record.hasCheckedIn
            ? "green"
            : moment().isAfter(record.arrivalDateTime) && "red";
          return (
            <span style={{color}}>
              {listRenderer.longDateTimeFormat(record.arrivalDateTime, true)}
            </span>
          );
        },
        sorter: (a, b) => listSorter.date(a.arrivalDateTime, b.arrivalDateTime),
        searchable: true,
        searchText: (record) => listRenderer.longDateTimeFormat(record.arrivalDateTime, true),
        width: 155,
      },
      {
        title: "Date de départ",
        dataIndex: "departureDateTime",
        render: (text, record) => {
          // If arrived, green. If not arrived and date is past, red. Else, no color
          const color =
            record.hasCheckedIn && moment().isAfter(record.departureDateTime) && "green";
          return (
            <span style={{color}}>
              {listRenderer.longDateTimeFormat(record.departureDateTime, true)}
            </span>
          );
        },
        sorter: (a, b) => listSorter.date(a.departureDateTime, b.departureDateTime),
        searchable: true,
        searchText: (record) => listRenderer.longDateTimeFormat(record.departureDateTime, true),
        width: 155,
      },
      advancedMode && {
        title: "Caché⋅e",
        dataIndex: "hidden",
        render: (text, record) => (
          <Checkbox checked={record.hidden} onChange={modifyBooleanFieldFn(record._id, "hidden")} />
        ),
        filters: [
          {text: "Oui", value: true},
          {text: "Non", value: false},
        ],
        onFilter: (value, record) => record.hidden === value,
        width: 95,
      },
      {
        title: "Inscription",
        dataIndex: "everythingIsOk",
        render: (text, record) => (record.everythingIsOk ? "✔️" : "❌"),
        filters: [
          {text: "Oui", value: true},
          {text: "Non", value: false},
        ],
        onFilter: (value, record) => record.everythingIsOk === value,
        width: 115,
      },
      {
        title: "Bénévolat",
        dataIndex: "volunteering",
        render: (text, record) => <VolunteeringGauge registration={record} />,
        sorter: (a, b) => listSorter.number(a.voluntaryCounter, b.voluntaryCounter),
      },
      registrationDispoColumn,
      {
        key: "action",
        fixed: "right",
        render: (text, record) => (
          <div style={{margin: 4, textAlign: "right"}}>
            {record.user._id !== currentUser?._id && (
              <ChangeIdentityButton registration={record} envId={envId} />
            )}
          </div>
        ),
        width: 56,
      },
    ],
  });

  const columnsTickets = [
    {
      title: "Numéro de billet",
      dataIndex: "id",
    },
    project.ticketingMode === "helloAsso" && {
      title: "Nom",
      dataIndex: "username",
      render: (text, record) => personName(record.user),
    },
    {
      title: "Article",
      dataIndex: "name",
    },
    {
      title: "Montant",
      dataIndex: "amount",
      render: (text, record) => `${record?.amount} €`,
    },
  ].filter((el) => !!el);

  const usableStewards = stewards.filter(
    (s) => !project?.registrations?.map((r) => r?.steward?._id).includes(s._id)
  );
  const rowSelectionSteward = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      console.log(selectedRowObject);
      setSelectedSteward(selectedRowObject);
    },
    type: "radio",
  };

  useEffect(() => {
    dispatch(stewardsActions.loadList());
  }, []);

  const MemoizedRegistrationForm = useCallback(
    React.memo(
      (props) => (
        <LayoutElement.Suspense>
          <RegistrationForm {...props} />
        </LayoutElement.Suspense>
      ),
      (pp, np) =>
        JSON.stringify(pp.userFormResponses || {}) === JSON.stringify(np.userFormResponses || {})
    ),
    []
  );

  const resize = () => {
    try {
      const heightOffset =
        getFullHeightWithMargins(document.querySelector(".ant-table-header")) + // table header row
        getFullHeightWithMargins(document.querySelector(".ant-table-pagination")) + // table footer row
        getFullHeightWithMargins(document.querySelector(".tabs-page-header")) + // page title header
        getFullHeightWithMargins(document.querySelector(".ant-tabs-nav")) + // tabs nav bar
        parseInt(
          window
            .getComputedStyle(document.querySelector(".page-container"))
            .getPropertyValue("padding-top")
        ) + // top padding of the page above the header
        navbarHeight() + // The height of the navbar if we're in mobile mode
        3 - // Security margin
        15; // 15 px were removed from the top margin of the TabPane (cf. ParticipantMain)
      const tableBody = document.querySelector(".ant-table-body");
      tableBody.style.maxHeight = `calc(100vh - ${heightOffset}px)`;
      tableBody.style.minHeight = `calc(100vh - ${heightOffset}px)`;
    } catch {
      //Do nothing
    }
  };

  useEffect(resize);
  useEffect(() => setTimeout(resize, 3));

  return (
    <>
      <ListPage
        header={false}
        fullPage={false}
        rowClassName={(record) =>
          record.invitationToken
            ? "ant-table-row-selected"
            : record.everythingIsOk
            ? ""
            : "ant-table-row-danger"
        }
        entitiesName="Participant⋅es"
        editable={false}
        deletable={false}
        navigateFn={navigate}
        expandable={{
          expandedRowRender: (record) => (
            <div style={{width: contentWidth, position: "sticky", left: 8}}>
              <FormElement>
                <div className="container-grid two-per-row">
                  <CardElement greyedOut>
                    <div className="container-grid two-per-row">
                      <InputElement.Custom label="Tags">
                        {record.tags?.map((tagName) => (
                          <Tag>{tagName}</Tag>
                        ))}
                      </InputElement.Custom>
                      <InputElement.Text
                        label="Formulaire OK"
                        readOnly
                        value={record.formIsOk ? "✔️" : "❌"}
                      />
                    </div>
                  </CardElement>
                  {project.ticketingMode && (
                    <TableElement.WithTitle
                      title="Billets"
                      showHeader
                      fullWidth
                      columns={columnsTickets}
                      dataSource={record[`${project.ticketingMode}Tickets`]}
                    />
                  )}
                </div>
              </FormElement>
              <MemoizedRegistrationForm
                currentProject={project}
                userFormResponses={record.specific}
                formOptions={{readOnly: true}}
              />
            </div>
          ),
        }}
        columns={filterBlacklistedColumns(columns)}
        dataSource={registrationsFinal}
        settingsDrawerContent={
          <FormElement>
            <ColumnsBlacklistingSelector columns={columns} />
            <CardElement title="Affichage de la liste">
              <div className="container-grid">
                <InputElement.Switch
                  label="Afficher les personnes désinscrit⋅es"
                  checked={displayUnbookedUsers}
                  onChange={setDisplayUnbookedUsers}
                />
                <InputElement.Switch
                  label="Mode avancé"
                  checked={advancedMode}
                  onChange={setAdvancedMode}
                />
              </div>
            </CardElement>
          </FormElement>
        }
      />
      <LayoutElement.ElementSelectionModal
        title="Associer un⋅e encadrant⋅e"
        visible={showModalStewards}
        onOk={modifySteward}
        onCancel={closeStewardSelectionModal}
        customButtons={
          <Button type="link" onClick={() => setShowNewStewardModal(true)}>
            Créer un⋅e encadrant⋅e
          </Button>
        }
        showHeader
        searchInFields={["firstName", "lastName"]}
        rowSelection={rowSelectionSteward}
        selectedRowKeys={selectedSteward}
        setSelectedRowKeys={setSelectedSteward}
        columns={columnsStewards}
        dataSource={usableStewards}
      />

      <NewStewardModal />
    </>
  );
}
