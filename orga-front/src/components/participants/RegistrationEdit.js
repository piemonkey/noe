import React, {lazy, useMemo, useRef} from "react";
import {useDispatch, useSelector} from "react-redux";
import {InputElement} from "../common/InputElement";
import {EditPage, GetPdfPlanningButton, useNewElementModal} from "../common/EditPage";
import {currentProjectSelectors} from "../../features/currentProject";
import {useLoadEditing} from "../../helpers/editingUtilities";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {Alert, Button, Popconfirm} from "antd";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {teamsActions, teamsSelectors} from "../../features/teams";
import {
  generateSessionsColumns,
  generateColumnsTeams,
  generateSubscriptionInfo,
} from "../../helpers/tableUtilities";
import {TableElement} from "../common/TableElement";
import {stewardsActions, stewardsSelectors} from "../../features/stewards";
import {CardElement, LayoutElement} from "../common/LayoutElement";
import {roleTag} from "../../helpers/tableUtilities";
import {personName} from "../../helpers/utilities";
import {Availability} from "../utils/Availability";
import {StewardEdit} from "../stewards/StewardEdit";
import {WaitingInvitationTag} from "../../helpers/registrationsUtilities";
import {ChangeIdentityButton} from "./ParticipantList";
import {Link} from "@reach/router";
const RegistrationForm = lazy(() =>
  import(/* webpackPrefetch: true */ "../common/RegistrationForm").then((module) => ({
    default: module["RegistrationForm"],
  }))
);

export function RegistrationEdit({envId, id}) {
  const dispatch = useDispatch();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const registration = useSelector(registrationsSelectors.selectEditing);
  const registrations = useSelector(registrationsSelectors.selectList);
  const sessions = useSelector(sessionsSelectors.selectList);
  const teams = useSelector(teamsSelectors.selectList);
  const stewards = useSelector(stewardsSelectors.selectList);
  const stewardId = registration.steward?._id || registration.steward;
  const [setShowNewStewardModal, NewStewardModal] = useNewElementModal(StewardEdit);
  const dataRef = useRef();

  const registrationTickets = registration[`${currentProject.ticketingMode}Tickets`];
  const isHelloAssoTicketing = currentProject.ticketingMode === "helloAsso";

  const teamsRegistrations = teams.filter((team) =>
    registration.teamsSubscriptions?.find((ts) => ts.team._id === team._id)
  );
  const sessionsRegistrations = sessions.filter((session) =>
    registration.sessionsSubscriptions?.find((ts) => ts.session === session._id)
  );
  const stewardRegistrations = stewardId
    ? sessions
        .filter(
          (session) =>
            session.stewards.find((steward) => steward._id === stewardId) ||
            session.slots.find(
              (slot) =>
                !slot.stewardsSessionSynchro &&
                slot.stewards.find((steward) => steward._id === stewardId)
            )
        )
        .map((session) => ({...session, isSteward: true}))
    : [];

  const columnsTickets = [
    {
      title: "Numéro de billet",
      dataIndex: "id",
    },
    isHelloAssoTicketing && {
      title: "Nom",
      dataIndex: "username",
      render: (text, record) => personName(record.user),
    },
    {
      title: "Article",
      dataIndex: "name",
    },
    {
      title: "Montant",
      dataIndex: "amount",
      render: (text, record) => `${record?.amount} €`,
    },
  ].filter((el) => !!el);

  useLoadEditing(registrationsActions, id, () => {
    dispatch(sessionsActions.loadList());
    dispatch(teamsActions.loadList());
    dispatch(stewardsActions.loadList());
    dispatch(registrationsActions.loadList());
  });

  const columnsSessionsBase = useMemo(
    () => generateSessionsColumns("../..", currentProject.usePlaces, false, true),
    [currentProject]
  );

  const onValidation = (formData) =>
    dispatch(
      registrationsActions.persist({
        ...registration,
        ...formData,
        specific: dataRef.current?.data,
      })
    );

  const unregisterFromProject = () => dispatch(registrationsActions.unregister());

  const sessionSubscriptionInfoColumn = {
    title: "Inscription",
    width: "300px",
    render: (text, record) => {
      const sessionSubscription = registration.sessionsSubscriptions.find(
        (ss) => ss.session === record._id
      );
      return generateSubscriptionInfo(registration, sessionSubscription, registrations, true);
    },
  };

  const teamSubscriptionInfoColumn = {
    title: "Inscription",
    width: "300px",
    render: (text, record) => {
      const teamSubscription = registration.teamsSubscriptions.find(
        (ss) => ss.team._id === record._id
      );
      return generateSubscriptionInfo(registration, teamSubscription, registrations);
    },
  };

  const roleNotice = registration.role && (
    <>
      {personName(registration.user)} est{" "}
      <span style={{marginLeft: 3, marginRight: -4}}>{roleTag(registration.role)}</span> de
      l'événement.
    </>
  );

  return (
    <>
      <EditPage
        editTitle="Modifier un⋅e participant⋅e"
        onValidation={onValidation}
        elementsActions={registrationsActions}
        customButtons={
          <>
            {registration.booked && (
              <>
                <GetPdfPlanningButton elementsActions={registrationsActions} />

                <Popconfirm
                  title={
                    <>
                      Cela désinscrira également la personne de <br />
                      ses sessions et équipes si elle en a.
                    </>
                  }
                  okText="Je désinscris la personne"
                  okButtonProps={{danger: true}}
                  cancelText="Annuler"
                  onConfirm={unregisterFromProject}>
                  <Button danger>Désinscrire</Button>
                </Popconfirm>
              </>
            )}
            <ChangeIdentityButton registration={registration} envId={envId} />
          </>
        }
        record={registration}
        initialValues={{...registration, steward: stewardId}}
        forceModifButtonActivation
        fieldsThatNeedReduxUpdate={["steward"]}>
        {registration.hidden && (
          <Alert
            style={{marginBottom: 26}}
            message="Participant⋅e caché⋅e"
            description="Cela signifie que l'inscription pour cet⋅te utilisateur⋅ice sera invisible. Iel ne verra pas l'événement dans sa liste d'événements et ne pourra pas accéder à ses informations avec son compte."
          />
        )}
        {registration.booked === false && (
          <Alert
            style={{marginBottom: 26}}
            type="warning"
            message="Participant⋅e désinscrit⋅e"
            description="La personne a été désinscrite de l'événement."
          />
        )}
        {registration.invitationToken && (
          <Alert
            showIcon
            icon={WaitingInvitationTag}
            style={{marginBottom: 26}}
            message={
              <>
                Cette personne a été invitée sur NOÉ, mais ne s'est pas encore créé de compte. Vous
                pouvez gérer les invitations{" "}
                <Link to="../../config#members">
                  dans l'onglet "Membres" de la page Configuration
                </Link>
                .
              </>
            }
          />
        )}
        {roleNotice && <Alert showIcon style={{marginBottom: 26}} message={roleNotice} />}

        <div className="container-grid two-thirds-one-third">
          <CardElement greyedOut>
            <div className="container-grid two-per-row">
              <InputElement.Text
                label="Nom"
                value={personName(registration.user)}
                readOnly
                title="Ce champ n'est pas modifiable"
              />
              <InputElement.Text
                label="Email"
                value={registration.user?.email}
                readOnly
                title="Ce champ n'est pas modifiable"
              />
            </div>
          </CardElement>

          <CardElement>
            <InputElement.WithEntityLinks
              endpoint="stewards"
              entity={registration.steward}
              createButtonText="Créer un⋅e encadrant⋅e"
              setShowNewEntityModal={setShowNewStewardModal}>
              <InputElement.Select
                label="Encadrant⋅e associé⋅e"
                name="steward"
                placeholder="sélectionnez un⋅e encadrant⋅e"
                options={[
                  {value: null, label: "- Pas d'encadrant⋅e -"},
                  ...stewards.map((d) => ({value: d._id, label: personName(d)})),
                ]}
                showSearch
              />
            </InputElement.WithEntityLinks>
          </CardElement>
        </div>

        <div className={currentProject.ticketingMode && "container-grid two-thirds-one-third"}>
          {currentProject.ticketingMode && (
            <TableElement.WithTitle
              title="Billets"
              showHeader
              fullWidth
              columns={columnsTickets}
              dataSource={registrationTickets}
            />
          )}
          <Availability title="Dates de présence" disabled entity={registration} />
        </div>

        <CardElement>
          <div className="container-grid three-per-row">
            <InputElement.Switch label="Arrivé.e" name="hasCheckedIn" />

            <InputElement.TextArea
              label="Notes privées pour les orgas"
              name="notes"
              placeholder="notes privées"
              tooltip="Ces notes ne sont pas affichées aux participant⋅es, elles ne sont disponibles que pour les organisateur⋅ices de l'événement"
            />

            <InputElement.TagsSelect
              label="Tags"
              name="tags"
              elementsSelectors={registrationsSelectors}
            />
          </div>
        </CardElement>

        {currentProject.useTeams && (
          <TableElement.WithTitle
            title="Inscriptions aux équipes"
            showHeader
            fullWidth
            navigableRootPath="../teams"
            columns={[...generateColumnsTeams("../.."), teamSubscriptionInfoColumn]}
            dataSource={teamsRegistrations}
          />
        )}

        <TableElement.WithTitle
          title="Inscriptions aux sessions"
          subtitle={
            stewardRegistrations.length > 0 &&
            `En bleu clair figurent les sessions encadrées par ${personName(registration.user)}`
          }
          showHeader
          fullWidth
          navigableRootPath="../sessions"
          rowClassName={(record) => record.isSteward && "ant-table-row-selected"}
          columns={[...columnsSessionsBase, sessionSubscriptionInfoColumn]}
          dataSource={[...stewardRegistrations, ...sessionsRegistrations]}
        />

        <CardElement title="Formulaire">
          <LayoutElement.Suspense>
            <RegistrationForm
              currentProject={currentProject}
              userFormResponses={registration.specific}
              dataRef={dataRef}
            />
          </LayoutElement.Suspense>
        </CardElement>
      </EditPage>

      <NewStewardModal />
    </>
  );
}
