import React, {lazy} from "react";
import {useSelector} from "react-redux";
import {projectsSelectors} from "../../features/projects.js";
import {addKeyToItemsOfList} from "../../helpers/tableUtilities";
import {TableElement} from "../common/TableElement";
import moment from "moment";
import {Alert, Collapse} from "antd";
import {listRenderer, listSorter, removeDuplicates} from "../../helpers/listUtilities";
import {CsvExportButton} from "../../helpers/filesUtilities";
import {CardElement, LayoutElement} from "../common/LayoutElement";
const DualAxes = lazy(() =>
  import(/* webpackPrefetch: true */ "@ant-design/plots").then((module) => ({
    default: module["DualAxes"],
  }))
);

const {Panel} = Collapse;

const meals = [
  {name: "matin", field: "breakfast"},
  {name: "midi", field: "lunch"},
  {name: "soir", field: "dinner"},
];

const initializeGraphDates = (registrations, getStartDate, getEndDate, initializeStats): Array => {
  let minStartDate;
  let maxEndDate;

  registrations.forEach((r) => {
    // Compute the min and max dates to display the graph
    const startDate = getStartDate(r);
    const endDate = getEndDate(r);

    if ((startDate && minStartDate === undefined) || startDate.isBefore(minStartDate)) {
      minStartDate = startDate.clone();
    }
    if ((endDate && maxEndDate === undefined) || endDate.isAfter(maxEndDate)) {
      maxEndDate = endDate.clone();
    }
  });

  const arrayOfDates = [];

  // // Get the total number of days we should display on the graph
  const totalNumberOfDays = maxEndDate.startOf("day").diff(minStartDate.startOf("day"), "days") + 1;

  for (let i = 0; i < totalNumberOfDays; i++) {
    arrayOfDates.push(minStartDate.clone().add(i, "day"));
  }

  // Initialize data
  arrayOfDates.forEach((dateToDisplay) => {
    const dateRender = {
      momentDate: dateToDisplay,
      dateShort: dateToDisplay.format("D MMM"),
      date: listRenderer.longDateFormat(dateToDisplay),
    };
    initializeStats(dateRender);
  });
};

const columnsDOPMealStats = [
  {
    title: "Jour",
    dataIndex: "date",
    sorter: (a, b) => listSorter.date(a.momentDate, b.momentDate),
  },
  {
    title: "Participant⋅es sur place",
    dataIndex: "participantsOnSite",
    sorter: (a, b) => listSorter.number(a.participantsOnSite, b.participantsOnSite),
  },
  {
    title: "Participant⋅es mangeant le matin",
    dataIndex: "breakfast",
    sorter: (a, b) => listSorter.number(a.breakfast, b.breakfast),
  },
  {
    title: "Participant⋅es mangeant le midi",
    dataIndex: "lunch",
    sorter: (a, b) => listSorter.number(a.lunch, b.lunch),
  },
  {
    title: "Participant⋅es mangeant le soir",
    dataIndex: "dinner",
    sorter: (a, b) => listSorter.number(a.dinner, b.dinner),
  },
];

const columnsRegistrationsStats = [
  {
    title: "Jour",
    dataIndex: "date",
    sorter: (a, b) => listSorter.date(a.momentDate, b.momentDate),
  },
  {
    title: "Nouvelles inscriptions",
    dataIndex: "newRegistrations",
    sorter: (a, b) => listSorter.number(a.newRegistrations, b.newRegistrations),
  },
  {
    title: "Inscriptions complètes",
    dataIndex: "completedRegistrations",
    sorter: (a, b) => listSorter.number(a.completedRegistrations, b.completedRegistrations),
  },
  {
    title: "Nouvelles inscriptions (Total)",
    dataIndex: "newRegistrationsTotal",
    sorter: (a, b) => listSorter.number(a.newRegistrations, b.newRegistrations),
  },
  {
    title: "Inscriptions complétées (Total)",
    dataIndex: "completedRegistrationsTotal",
    sorter: (a, b) => listSorter.number(a.completedRegistrations, b.completedRegistrations),
  },
];

export function ParticipantDashboard({registrations}) {
  const project = useSelector(projectsSelectors.selectEditing);

  const registrationsBooked = registrations.filter((r) => r.availabilitySlots?.length > 0);

  // Don't calculate if we have no participants at all
  const noParticipantsAtAll = registrationsBooked.length === 0;
  if (noParticipantsAtAll) {
    return (
      <Alert
        message="Vous n'avez pas encore de personnes inscrites à votre événement."
        description="Dès qu'une personne s'inscrira, vous aurez accès aux données de participation."
      />
    );
  }

  ///////////////////////////////////////////
  // FIRST GRAPH: DAYS OF PRESENCE + MEALS //
  ///////////////////////////////////////////

  const mealsAndFreqStats = [];

  const tagsOptions = removeDuplicates(
    registrationsBooked?.reduce((acc, a) => (a.tags ? [...a.tags, ...acc] : acc), [])
  );
  const baseTagsContainer = Object.fromEntries(tagsOptions.map((tag) => [tag, 0]));

  // Initialize and format the dates
  initializeGraphDates(
    registrationsBooked,
    (registration) =>
      registration.daysOfPresence.reduce((accumulator, day) => {
        const start = moment(day.start);
        return accumulator ? moment.min(start, accumulator) : start;
      }, undefined),
    (registration) =>
      registration.daysOfPresence.reduce((accumulator, day) => {
        const end = moment(day.end);
        return accumulator ? moment.max(end, accumulator) : end;
      }, undefined),
    (dateRender) => {
      mealsAndFreqStats.push({
        ...dateRender,
        participantsOnSite: 0,
        breakfast: 0,
        lunch: 0,
        dinner: 0,
        tags: {...baseTagsContainer},
      });
    }
  );

  // For each registration...
  registrationsBooked.forEach((registration) => {
    // ...and each day of presence for this registration
    registration.daysOfPresence.forEach((dayOfPresence) => {
      const currentDay = moment(dayOfPresence.start);

      // Get the stat for this day and increment the number of participants on site
      const mealsAndFreqStatForDay = mealsAndFreqStats.find((st) =>
        st.momentDate.isSame(currentDay, "day")
      );
      mealsAndFreqStatForDay.participantsOnSite += 1;

      // And also increment for each meal that is taken this day
      meals.forEach((meal) => {
        if (dayOfPresence[meal.field]) {
          mealsAndFreqStatForDay[meal.field] += 1;
        }
      });

      // Fetch also all the tags from those participants
      registration.tags?.forEach((tag) => {
        mealsAndFreqStatForDay.tags[tag] += 1;
      });
    });
  });

  // Create the stats objects for the Ant design plot
  const plottedMealsStats = mealsAndFreqStats.reduce(
    (acc, stat) => [
      ...acc,
      {dateShort: stat.dateShort, count: stat.breakfast, name: "Petits-déjeuners à prévoir"},
      {
        dateShort: stat.dateShort,
        count: stat.lunch,
        name: "Déjeuners à prévoir",
      },
      {dateShort: stat.dateShort, count: stat.dinner, name: "Dîners à prévoir"},
    ],
    []
  );

  // Create the stats objects for the Ant design plot
  const plottedFreqStats = mealsAndFreqStats.map((stat) => ({
    dateShort: stat.dateShort,
    participantsOnSite: stat.participantsOnSite,
  }));

  // Create the stats objects for the Ant design plot
  const plottedTagsStats = mealsAndFreqStats.reduce(
    (acc, stat) => [
      ...acc,
      ...Object.entries(stat.tags).map(([tagName, count]) => ({
        dateShort: stat.dateShort,
        count,
        name: tagName,
      })),
    ],
    []
  );

  /////////////////////////////////////////////
  // 2ND GRAPH: REGISTRATION DATES EVOLUTION //
  /////////////////////////////////////////////

  let registrationsStats = [];

  // Initialize and format the dates
  initializeGraphDates(
    registrationsBooked,
    (r) => moment(r.createdAt),
    (r) => moment(r.createdAt),
    (dateRender) => {
      registrationsStats.push({
        ...dateRender,
        newRegistrations: 0,
        completedRegistrations: 0,
      });
    }
  );

  registrationsBooked.forEach((registration) => {
    // Increment +1 the date at which the current registration was created.
    const statForCreationDate = registrationsStats.find((st) =>
      st.momentDate.isSame(registration.createdAt, "day")
    );
    statForCreationDate.newRegistrations += 1;

    // If the registration is valid, also add it to the completed registrations
    if (registration.everythingIsOk) statForCreationDate.completedRegistrations += 1;
  });

  // Make the sums of new and completed Registrations
  for (let i = 0; i < registrationsStats.length - 1; i++) {
    registrationsStats[i].newRegistrationsTotal =
      (registrationsStats[i - 1]?.newRegistrationsTotal || 0) +
      registrationsStats[i].newRegistrations;
    registrationsStats[i].completedRegistrationsTotal =
      (registrationsStats[i - 1]?.completedRegistrationsTotal || 0) +
      registrationsStats[i].completedRegistrations;
  }

  // Create the stats object for the Ant design plot
  const plottedRegistrationsStats = registrationsStats.reduce(
    (acc, stat) => [
      ...acc,
      {dateShort: stat.dateShort, count: stat.newRegistrations, name: "Nouvelles inscriptions"},
      {
        dateShort: stat.dateShort,
        count: stat.completedRegistrations,
        name: "Inscriptions complètes",
      },
    ],
    []
  );

  const plottedRegistrationsTotalStats = registrationsStats.reduce(
    (acc, stat) => [
      ...acc,
      {
        dateShort: stat.dateShort,
        count: stat.newRegistrationsTotal,
        name: "Nouvelles inscriptions (Total)",
      },
      {
        dateShort: stat.dateShort,
        count: stat.completedRegistrationsTotal,
        name: "Inscriptions complètes (Total)",
      },
    ],
    []
  );

  const columnsTagsStats = [
    {
      title: "Jour",
      dataIndex: "date",
      sorter: (a, b) => listSorter.date(a.momentDate, b.momentDate),
    },
    ...removeDuplicates(
      mealsAndFreqStats.reduce((acc, stat) => [...acc, ...Object.keys(stat.tags)], [])
    ).map((tag) => ({dataIndex: ["tags", tag], title: tag})),
  ];

  const RawDataCollapsibleTable = ({dataSource, columns, exportTitle}) => (
    <Collapse style={{marginTop: 26}}>
      <Panel
        header={
          <div className="containerH" style={{justifyContent: "space-between"}}>
            <span>Données brutes</span>
            <CsvExportButton
              size="small"
              dataExportFunction={() => dataSource}
              getExportName={() =>
                `Données de fréquentation [${exportTitle}] - ${project.name} - ${moment().format(
                  "DD-MM-YYYY HH[h]mm"
                )}.csv`
              }>
              Exporter
            </CsvExportButton>
          </div>
        }>
        <TableElement.Simple
          showHeader
          columns={columns}
          dataSource={dataSource}
          scroll={{y: 700}}
        />
      </Panel>
    </Collapse>
  );

  // Show 10 days before and after project start and end dates
  const daysOfPresenceSliderDates = {
    start: Math.max(
      0,
      (mealsAndFreqStats.findIndex((stat) => stat.momentDate.isSame(moment(project.start), "day")) -
        10) /
        mealsAndFreqStats.length
    ),
    end: Math.min(
      1,
      (mealsAndFreqStats.findIndex((stat) => stat.momentDate.isSame(moment(project.end), "day")) +
        10) /
        mealsAndFreqStats.length
    ),
  };

  return (
    <>
      <Alert
        description="Ces données prennent en compte à la fois les inscriptions complètes et incomplètes."
        style={{marginBottom: 26}}
      />

      <CardElement title="Jours de présence et repas">
        <LayoutElement.Suspense>
          <DualAxes
            data={[plottedFreqStats, plottedMealsStats]}
            height={280}
            xField="dateShort"
            yField={["participantsOnSite", "count"]}
            geometryOptions={[
              {geometry: "column"},
              {geometry: "line", smooth: true, seriesField: "name"},
            ]}
            meta={{
              // Use sync to synchronize both axis
              participantsOnSite: {alias: "Participant⋅es sur place", sync: "count"},
              count: {sync: true},
            }}
            // Don't display the second axis, as they are sync
            yAxis={{count: false, min: 10}}
            slider={daysOfPresenceSliderDates}
          />
        </LayoutElement.Suspense>
        <RawDataCollapsibleTable
          exportTitle="Jours de présence et repas"
          columns={columnsDOPMealStats}
          dataSource={addKeyToItemsOfList(mealsAndFreqStats)}
        />
      </CardElement>

      <CardElement title="Comptabilisation des tags par jour">
        <LayoutElement.Suspense>
          <DualAxes
            data={[plottedFreqStats, plottedTagsStats]}
            height={280}
            xField="dateShort"
            yField={["participantsOnSite", "count"]}
            yAxis={[{min: 0}, {min: 0}]}
            geometryOptions={[
              {geometry: "column"},
              {geometry: "line", smooth: true, seriesField: "name"},
            ]}
            meta={{
              // Use sync to synchronize both axis
              participantsOnSite: {alias: "Participant⋅es sur place"},
            }}
            slider={daysOfPresenceSliderDates}
          />
        </LayoutElement.Suspense>
        <RawDataCollapsibleTable
          exportTitle="Comptabilisation des tags par jour"
          columns={columnsTagsStats}
          dataSource={addKeyToItemsOfList(mealsAndFreqStats)}
        />
      </CardElement>

      <CardElement title="Suivi des inscriptions">
        <LayoutElement.Suspense>
          <DualAxes
            data={[plottedRegistrationsStats, plottedRegistrationsTotalStats]}
            height={280}
            xField="dateShort"
            yField={["count", "count"]}
            geometryOptions={[
              {
                geometry: "line",
                smooth: true,
                seriesField: "name",
              },
              {
                geometry: "column",
                color: ["#B0ADFF", "#B5FFF6"],
                seriesField: "name",
              },
            ]}
            slider={{start: 0, end: 1}}
          />
        </LayoutElement.Suspense>
        <RawDataCollapsibleTable
          exportTitle="Suivi des inscriptions"
          columns={columnsRegistrationsStats}
          dataSource={addKeyToItemsOfList(registrationsStats)}
        />
      </CardElement>
    </>
  );
}
