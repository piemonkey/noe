import React, {useEffect, useState} from "react";
import {Tabs} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {TabsPage} from "../common/TabsPage";
import {ParticipantList} from "./ParticipantList";
import {ParticipantDashboard} from "./ParticipantDashboard";
import {projectsSelectors} from "../../features/projects";
import moment from "moment";
import {CsvExportButton} from "../../helpers/filesUtilities";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {cleanAnswer} from "../../helpers/tableUtilities";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {
  getElementsFromListInSlot,
  getSessionName,
  slotNumberString,
} from "../../helpers/agendaUtilities";
import {teamsActions, teamsSelectors} from "../../features/teams";
import {listRenderer, removeDuplicates} from "../../helpers/listUtilities";
import {personName} from "../../helpers/utilities";
import {displayLoadingMessage, killLoadingMessage} from "../../helpers/reduxUtilities";

const {TabPane} = Tabs;
const now = moment();

const getSessionsPlanningForRegistration = (registration, allSlots, teams, allPlanningColumns?) =>
  registration.sessionsSubscriptions
    .map((ss) => {
      const slot = allSlots.find((slot) => slot.session._id === ss.session);
      if (!slot) return;
      const title = slotNumberString(slot) + getSessionName(slot.session, teams);
      const location = getElementsFromListInSlot("places", slot, (el) => el.name).join(", ");
      const dateTimeRange = listRenderer.longDateTimeRangeFormat(slot.start, slot.end, true);
      const categoryName = slot.session.activity?.category.name;

      // Add the slot date to the dates columns if needed
      allPlanningColumns && allPlanningColumns.push(moment(slot.start));

      return {
        startDate: slot.start,
        endDate: slot.end,
        sessionId: slot.session._id,
        value: `${dateTimeRange} : [${categoryName}] ${title} (${location})`,
      };
    })
    .filter((slotInfo) => slotInfo !== undefined)
    .sort((a, b) => moment(a.startDate).diff(b.startDate));

export const getNextOrCurrentSession = (registration, allSlots, teams) =>
  allSlots &&
  teams &&
  getSessionsPlanningForRegistration(registration, allSlots, teams).find(
    (sessionPlanned) =>
      now.isBefore(sessionPlanned.startDate) ||
      (now.isAfter(sessionPlanned.startDate) && now.isBefore(sessionPlanned.endDate))
  );

export function ParticipantMain({envId, navigate}) {
  const dispatch = useDispatch();
  const [displayUnbookedUsers, setDisplayUnbookedUsers] = useState(false);
  const [advancedMode, setAdvancedMode] = useState(false);
  // We need to destructure it so we can then modify it in the getRegistrationMetadata function
  const project = useSelector(projectsSelectors.selectEditing);
  const registrationsWithMetadata = useSelector(registrationsSelectors.selectListWithMetadata);
  const allSlots = useSelector(sessionsSelectors.selectSlotsList);
  const teams = useSelector(teamsSelectors.selectList);

  useEffect(() => {
    displayLoadingMessage(); // Display the message so we keep the continuity between registrations and sessions/teams loading
    dispatch(registrationsActions.loadList()).then(() => {
      // Load registrations first, because it's critical, then the rest can be loaded
      dispatch(sessionsActions.loadList());
      dispatch(teamsActions.loadList());
      killLoadingMessage();
    });
  }, []);

  const buildParticipantExport = () => {
    const ticketsFieldName = project.ticketingMode && `${project.ticketingMode}Tickets`;

    const baseColumns = [
      "Prénom",
      "Nom",
      "Email",
      "Dates OK",
      "Formulaire OK",
      "Billetterie OK",
      "Inscription commencée",
      "Arrivé.e",
      `Détail billets (${project.ticketingMode || " - "})`,
      "Montant total payé",
      "Encadrant⋅e lié⋅e",
      "Équipes",
      "Date d'arrivée",
      "Date de départ",
      "Dates avancées",
      "Nombre de jours comptabilisés",
      "Nombre de jours total",
      "Jauge de bénévolat (minutes/jour)",
      "Rôle",
    ];

    let allFormColumns = [],
      allPlanningColumns = [];

    const exportedData = registrationsWithMetadata
      .map((registration) => {
        // Compute min and max arrival and departure date
        const earliestArrivalDate = registration.availabilitySlots.reduce(
          (accumulator, currentValue) => {
            const start = moment(currentValue.start);
            return !accumulator || start < accumulator ? start : accumulator;
          },
          undefined
        );
        const latestLeavingDate = registration.availabilitySlots.reduce(
          (accumulator, currentValue) => {
            const end = moment(currentValue.end);
            return !accumulator || end > accumulator ? end : accumulator;
          },
          undefined
        );

        const sessionsPlanning = getSessionsPlanningForRegistration(
          registration,
          allSlots,
          teams,
          allPlanningColumns
        );

        // Format it to a single total string
        const sessionPlanningTotal = sessionsPlanning.map((slotInfo) => slotInfo.value).join(" | ");
        // And also group it by day
        const sessionPlanningByDate = sessionsPlanning.reduce(
          (groupByDayAcc, {startDate, value}) => {
            const formattedDate = listRenderer.longDateFormat(startDate, true);
            return {
              ...groupByDayAcc,
              [formattedDate]:
                groupByDayAcc[formattedDate]?.length > 0
                  ? groupByDayAcc[formattedDate] + " | " + value
                  : value,
            };
          },
          {}
        );

        // Clean the form answers
        const cleanedFormAnswers =
          registration.specific &&
          Object.entries(registration.specific).reduce((acc, [key, value]) => {
            return {...acc, [key]: cleanAnswer(value, ";")};
          }, {});
        // Add the fields to the form columns
        cleanedFormAnswers && allFormColumns.push(...Object.keys(cleanedFormAnswers));

        // Return all the registration data
        return {
          createdAt: moment(registration.createdAt),
          INFOS: ">>",
          Prénom: registration.user?.firstName,
          Nom: registration.user?.lastName,
          Email: registration.user?.email,
          "Dates OK": registration.firstSlotIsOk,
          "Formulaire OK": registration.formIsOk,
          "Billetterie OK": registration.ticketingIsOk,
          "Inscription commencée": registration.booked,
          "Arrivé.e": registration.hasCheckedIn,
          [`Détail billets (${project.ticketingMode || " - "})`]:
            project.ticketingMode &&
            registration[ticketsFieldName]?.map((ticket) => ticket.id).join(", "),
          "Montant total payé":
            project.ticketingMode &&
            registration[ticketsFieldName]?.reduce((acc, ticket) => acc + ticket.amount, 0) / 100,
          "Encadrant⋅e lié⋅e": personName(registration.steward),
          Équipes: registration.teamsSubscriptionsNames,
          "Date d'arrivée": earliestArrivalDate?.format("LLL"),
          "Date de départ": latestLeavingDate?.format("LLL"),
          "Dates avancées": registration.availabilitySlots?.length > 1,
          "Nombre de jours comptabilisés": registration.numberOfDaysOfPresence,
          "Nombre de jours total": registration.daysOfPresence.length,
          Rôle: registration.role,
          "Jauge de bénévolat (minutes/jour)": Math.round(registration.voluntaryCounter),
          PLANNING: ">>",
          "Tout l'événement": sessionPlanningTotal,
          ...sessionPlanningByDate,
          "FORMULAIRE D'INSCRIPTION": ">>",
          ...cleanedFormAnswers,
        };
      })
      .sort((a, b) => a.createdAt.diff(b.createdAt)); // Sort it by creation date

    // Compute the final columns layout of the CSV file
    allPlanningColumns = allPlanningColumns
      .sort((a, b) => a.diff(b))
      .map((startDate) => listRenderer.longDateFormat(startDate, true));

    const columnsLayout = [
      "createdAt",
      "INFOS",
      ...baseColumns,
      "PLANNING",
      "Tout l'événement",
      ...removeDuplicates(allPlanningColumns),
      "FORMULAIRE D'INSCRIPTION",
      ...removeDuplicates(allFormColumns),
    ];

    // Return the data with the column layout
    return [columnsLayout, ...exportedData];
  };

  return (
    <TabsPage
      title="Participant⋅es"
      fullWidth
      customButtons={
        <CsvExportButton
          tooltip="Exporter les participant⋅es au format CSV"
          getExportName={() =>
            `Export des participant⋅es - ${project.name} - ${moment().format(
              "DD-MM-YYYY HH[h]mm"
            )}.csv`
          }
          withFields
          dataExportFunction={buildParticipantExport}>
          Exporter
        </CsvExportButton>
      }>
      <TabPane tab="Liste" key="list" style={{marginTop: -15}}>
        <ParticipantList
          envId={envId}
          displayUnbookedUsers={displayUnbookedUsers}
          setDisplayUnbookedUsers={setDisplayUnbookedUsers}
          advancedMode={advancedMode}
          setAdvancedMode={setAdvancedMode}
          registrations={registrationsWithMetadata}
          navigate={navigate}
        />
      </TabPane>
      <TabPane tab="Données de fréquentation" key="dashboard" className="with-margins">
        <ParticipantDashboard registrations={registrationsWithMetadata} />
      </TabPane>
    </TabsPage>
  );
}
