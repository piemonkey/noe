import React from "react";
import {useSelector} from "react-redux";
import {projectsSelectors} from "../../features/projects";
import {Form, FormBuilder as FormIoBuilder} from "@formio/react";

export const RegistrationForm = ({currentProject, userFormResponses, dataRef, formOptions}) => (
  <Form
    form={{components: currentProject.formComponents}}
    onChange={(submission) => {
      if (submission.changed && dataRef) {
        dataRef.current = submission;
      }
    }}
    submission={{data: userFormResponses}}
    options={formOptions}
  />
);

export const FormBuilder = React.memo(
  ({setIsModified, formRef, delaySaveTipMessage, messageNotYetDisplayed}) => {
    const project = useSelector(projectsSelectors.selectEditing);

    const getChildrenIdsAndComponents = (parentCompononents) => {
      return parentCompononents.map((childComp) => {
        if (childComp.components?.length > 0) {
          return [childComp.id, ...getChildrenIdsAndComponents(childComp.components)];
        } else {
          return childComp.id;
        }
      });
    };

    const onFormIOChange = (schema) => {
      const idsProject = getChildrenIdsAndComponents(project.formComponents || []);
      const idsForm = getChildrenIdsAndComponents(schema.components);
      if (JSON.stringify(idsProject) !== JSON.stringify(idsForm)) {
        setIsModified(true);
        formRef.current = schema.components;
        messageNotYetDisplayed.current = true;
        delaySaveTipMessage();
      }
    };

    return (
      <FormIoBuilder
        form={{
          display: "form",
          components: JSON.parse(JSON.stringify(project.formComponents || [])),
        }}
        options={{noDefaultSubmitButton: true}}
        onChange={onFormIOChange}
      />
    );
  },
  (pp, np) => true
);
