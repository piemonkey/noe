import {
  Alert,
  Button,
  Card,
  Carousel,
  Divider,
  Image,
  Menu as AntMenu,
  Modal,
  Progress,
  Spin,
  Tooltip,
} from "antd";
import React, {useEffect, useState, Suspense} from "react";
import {navigate, useNavigate} from "@reach/router";
import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  FacebookOutlined,
  GitlabOutlined,
  InstagramOutlined,
  MailOutlined,
  QuestionOutlined,
} from "@ant-design/icons";
import {displayNotification} from "../../helpers/notificationUtilities";
import {TableElement} from "./TableElement";
import {paginationPageSizes} from "../../features/view";
import {defaultSpinnerLogo} from "../../app/configuration";
import {useDebounce} from "../../helpers/viewUtilities";
import {searchInObjectsList} from "../../helpers/listUtilities";
import Search from "antd/es/input/Search";
import {useLocalStorageState} from "../../helpers/localStorageUtilities";

var demoNotifHasBeenClosed = false;

export const displayStagingInfoNotification = () => {
  // Display notif if not in production or in local development mode
  if (process.env.REACT_APP_MODE === "DEMO" && !demoNotifHasBeenClosed) {
    displayNotification("warning", "envDemoNotification", {
      message: (
        <>
          Attention : vous êtes en mode <strong>démonstration</strong>.
        </>
      ),
      onClose: () => (demoNotifHasBeenClosed = true),
      description: (
        <>
          <p>
            Cette plate-forme est <strong style={{color: "darkorange"}}>à vocation de test</strong>,
            et les données stockées ici peuvent être altérées ou supprimées à tout moment.
          </p>
          <p>
            Pour accéder à l’application officielle, rendez-vous sur{" "}
            <strong>
              <a href={process.env.REACT_APP_PROD_URL}>
                {process.env.REACT_APP_PROD_URL?.replace(/(http|https):\/\//, "")}
              </a>
            </strong>
            .
          </p>
          Vous voulez utiliser NOÉ pour votre événément ? Contactez-nous à l'adresse suivante :
          <a
            href={`mailto:${process.env.REACT_APP_CONTACT_US_EMAIL}?subject=J'aimerais%20en%20savoir%20plus%20sur%20NOÉ`}
            rel="noreferrer">
            {process.env.REACT_APP_CONTACT_US_EMAIL}
          </a>
        </>
      ),
    });
  }
};

export const LayoutElement = () => null;

LayoutElement.Carousel = ({children, ...carouselProps}) => {
  const CarouselArrow = (props) => (
    <Button
      size="large"
      shape="circle"
      className={`${props.className} carousel-arrow-button`}
      onClick={props.onClick}
      icon={props.icon}
    />
  );

  return (
    <Carousel
      style={{margin: "0 25px"}}
      arrows
      {...carouselProps}
      nextArrow={<CarouselArrow icon={<ArrowRightOutlined />} />}
      prevArrow={<CarouselArrow icon={<ArrowLeftOutlined />} />}>
      {children}
    </Carousel>
  );
};

LayoutElement.HelpCarousel = ({content, baseUrl}) => {
  const mappedContent = content.map(({title, description, src}, index) => {
    return (
      <div>
        <div className="carousel-container">
          <div
            style={{
              flexGrow: 4,
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              width: "400px",
            }}>
            {title && (
              <h4>
                {index + 1}. {title}
              </h4>
            )}
            {description && <p>{description}</p>}
          </div>
          {src && (
            <div style={{flexGrow: 6, width: "500px"}}>
              <Image src={`${baseUrl}/${src}`} />
            </div>
          )}
        </div>
      </div>
    );
  });

  return <LayoutElement.Carousel dots={false}>{mappedContent}</LayoutElement.Carousel>;
};

LayoutElement.HelpToast = ({tooltipMessage, ...buttonProps}) => (
  <div style={{position: "fixed", bottom: 10, right: 10, zIndex: 110}}>
    <Tooltip placement="leftBottom" title={tooltipMessage}>
      <Button type="primary" shape="circle" size="large" className="shadow" {...buttonProps}>
        <QuestionOutlined />
      </Button>
    </Tooltip>
  </div>
);

LayoutElement.NOESocialIcons = ({style}) => (
  <>
    <a
      href="https://gitlab.com/alternatiba/noe/-/blob/master/CONTRIBUTING.md"
      target="_blank"
      rel="noreferrer">
      <GitlabOutlined style={style} />
    </a>
    <a href="https://www.facebook.com/noeappli" target="_blank" rel="noreferrer">
      <FacebookOutlined style={style} />
    </a>
    <a href="https://www.instagram.com/noeappli" target="_blank" rel="noreferrer">
      <InstagramOutlined style={style} />
    </a>
    <a
      href={`mailto:${process.env.REACT_APP_CONTACT_US_EMAIL}?subject=J'aimerais%20en%20savoir%20plus%20sur%20NOÉ`}
      rel="noreferrer">
      <MailOutlined style={style} />
    </a>
  </>
);

LayoutElement.SidebarDivider = ({color}) => (
  <Divider
    style={{
      borderTop: "1px solid " + color,
      margin: "15pt 30% 0 30%",
      minWidth: "unset",
      width: "unset",
    }}
  />
);

LayoutElement.Menu = ({
  disabled,
  disabledMessage,
  title,
  selectedItem,
  children,
  items,
  rootNavUrl = "",
}) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const navigate = useNavigate();
  const color = disabled ? "rgba(255,255, 255, 0.3)" : "rgba(255, 255, 255, 0.8)";

  // If there are some items, then add the navigation. If the url is explicitely set to false,
  // and there is no already defined onClick action, then don't add the onClick function
  items?.forEach((item) => {
    if (item && item.url !== false && item.onClick === undefined)
      item.onClick = ({key}) => navigate(item.url || rootNavUrl + key);
    else delete item?.url;
  });

  return (
    <div title={disabled ? disabledMessage : undefined}>
      {title && (
        <>
          <LayoutElement.SidebarDivider color={color} />
          <p className="text-center" style={{color: color, marginTop: "25px"}}>
            {title.toUpperCase()}
          </p>
        </>
      )}
      {items ? <AntMenu theme="dark" selectedKeys={[selectedItem]} items={items} /> : children}
    </div>
  );
};

LayoutElement.PageHeading = ({
  title,
  buttonTitle,
  onButtonClick,
  customButtons,
  className,
  backButton = false,
  forceModifButtonActivation = true,
}) => (
  <div className={`header-space-between ${className}`}>
    {/*flexGrow is forced to 1000 to give better rendering in mobile view mode. No better solution found up to now.*/}
    <div style={{flexGrow: 1000, paddingBottom: 5}}>
      {backButton && (
        <Button
          type="link"
          onClick={() => navigate(-1)}
          style={{paddingRight: "20pt", paddingLeft: 5, display: "inline"}}>
          <ArrowLeftOutlined style={{fontSize: "20pt"}} />
        </Button>
      )}
      <h1 style={{margin: 0, display: "inline"}}>{title}</h1>
    </div>
    <div className="containerH buttons-container" style={{flexGrow: 1, alignItems: "center"}}>
      {customButtons}
      {buttonTitle && (
        <Button type="primary" disabled={!forceModifButtonActivation} onClick={onButtonClick}>
          {buttonTitle}
        </Button>
      )}
    </div>
  </div>
);

const ElementSelectionModal = ({
  title,
  visible,
  onOk,
  onCancel,
  subtitle,
  large,
  customButtons,
  searchInFields = [],
  dataSource,
  ...tableProps
}) => {
  const [filteredDataSource, setFilteredDataSource] = useState([]);
  const [searchValue, setSearchValue] = useState("");

  const debouncedSetFilteredDataSource = useDebounce((searchValue, dataSource) =>
    setFilteredDataSource(searchInObjectsList(searchValue, dataSource, searchInFields))
  );

  useEffect(
    () => debouncedSetFilteredDataSource(searchValue, dataSource),
    [dataSource, searchValue]
  );

  return (
    <Modal
      className="element-selection-modal"
      title={title}
      visible={visible}
      width={large && "98%"}
      centered
      footer={
        <>
          {customButtons}
          <Button onClick={onCancel}>Annuler</Button>
          <Button type="primary" onClick={onOk}>
            OK
          </Button>
        </>
      }
      onCancel={onCancel}
      bodyStyle={{padding: 0}}>
      {(subtitle || searchInFields.length > 0) && (
        <div style={{padding: "10px 24px"}}>
          {subtitle}
          {searchInFields.length > 0 && (
            <Search
              allowClear
              autoFocus
              onChange={(event) => setSearchValue(event.target.value)}
              placeholder="Chercher..."
            />
          )}
        </div>
      )}

      <TableElement.Simple
        showHeader
        scroll={{
          x: (tableProps.columns.length - 1) * 160 + 70,
          y: window.innerHeight - 260 - (subtitle ? 45 : 0) - (searchInFields.length > 0 ? 45 : 0),
        }}
        pagination={{
          position: ["bottomCenter"],
          pageSize: 40,
          size: "small",
          pageSizeOptions: paginationPageSizes,
          showSizeChanger: true,
        }}
        dataSource={filteredDataSource}
        {...tableProps}
      />
    </Modal>
  );
};
LayoutElement.ElementSelectionModal = ElementSelectionModal;

LayoutElement.Pending = ({spinnerLogo = defaultSpinnerLogo, className, minHeight = "100vh"}) => (
  <div
    className={`containerH fade-in ${className}`}
    style={{justifyContent: "center", minHeight: minHeight, overflow: "visible"}}>
    <div className="containerV" style={{justifyContent: "center", overflow: "visible"}}>
      <Spin indicator={<img className="spin" src={spinnerLogo} />} />
    </div>
  </div>
);

LayoutElement.Suspense = ({children}) => (
  <Suspense fallback={<LayoutElement.Pending minHeight={300} />}>
    <div className="fade-in">{children}</div>
  </Suspense>
);

LayoutElement.Progress = (props) => (
  <Progress
    id="progress-bar"
    percent={props.position}
    status={props.status}
    format={(percent) => `${props.state}: ${percent}%`}
  />
);

export const CardElement = ({
  title,
  subtitle,
  customButtons,
  children,
  borderless,
  greyedOut,
  style,
  bodyStyle,
  ...otherProps
}) => (
  <Card
    title={
      title ? (
        <div className="header-space-between" style={{gap: "10px 0px"}}>
          <div style={{flexGrow: 1000}}>{title}</div>
          {customButtons && <div className="containerH buttons-container">{customButtons}</div>}
        </div>
      ) : undefined
    }
    style={{
      flexGrow: 1,
      marginBottom: 26,
      backgroundColor: greyedOut ? "#fafafa" : undefined,
      ...style,
    }}
    bodyStyle={{
      padding: borderless ? 0 : undefined,
      ...bodyStyle,
    }}
    {...otherProps}>
    {subtitle && (borderless ? <div style={{padding: "10px 24px"}}>{subtitle}</div> : subtitle)}
    {children}
  </Card>
);

export const ClosableAlert = ({localStorageKey, ...props}) => {
  if (!localStorageKey) throw Error("Need key to display ClosableAlert");
  const [visible, setVisible] = useLocalStorageState("closable-alert-" + localStorageKey, true);
  return visible ? (
    <Alert closable afterClose={() => setVisible(false)} {...props} closeText="Ne plus montrer" />
  ) : null;
};
