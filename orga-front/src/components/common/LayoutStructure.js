import React, {useState} from "react";
import {Alert, Avatar, Badge, Button, Drawer, Layout, Result, Tag} from "antd";
import {MyAccount} from "../myAccount/MyAccount";
import {
  CloseOutlined,
  DownloadOutlined,
  EditOutlined,
  LoginOutlined,
  MenuOutlined,
  SettingOutlined,
  UserOutlined,
  WarningOutlined,
  WifiOutlined,
} from "@ant-design/icons";
import {LayoutElement} from "./LayoutElement";
import {arrivalSpinnerLogo, defaultBackgroundClassName} from "../../app/configuration";
import {useWindowDimensions} from "../../helpers/viewUtilities";
import {ErrorBoundary} from "@sentry/react";
import {OFFLINE_MODE} from "../../helpers/offlineModeUtilities";
import {useOnlineStatus} from "../../helpers/onlineStatusCheckerUtilities";
import {personName} from "../../helpers/utilities";

const {Sider, Header, Content} = Layout;

export const setAppTheme = ({bg, accent1, accent2} = {}) => {
  const theme = {
    "--noe-bg": bg || null,
    "--noe-accent-1": accent1 || null,
    "--noe-accent-1-90": accent1 || null,
    "--noe-accent-1-85": accent1 || null,
    "--noe-accent-2": accent2 || null,
    "--noe-accent-2-85": accent2 || null,
  };
  const setVariables = (vars) => {
    const bodyStyle = document.getElementsByTagName("html")[0].style;
    Object.entries(vars).forEach((v) =>
      v[1] !== null ? bodyStyle.setProperty(v[0], v[1]) : bodyStyle.removeProperty(v[0])
    );
  };
  setVariables(theme);
};

/**
 * SIDEBAR
 */

const Sidebar = ({title, children, collapsed, ribbon}) => {
  const {isMobileView} = useWindowDimensions();

  const sider = (
    <Sider
      className="bg-noe"
      collapsed={collapsed}
      breakpoint="md"
      style={{overflow: "auto", height: "100vh"}}
      width={210}>
      <div className="sidebar-container">
        <h3
          className="ant-typography text-white text-center"
          style={{
            padding: "5pt",
            paddingTop: "15pt",
            flexGrow: 0,
          }}>
          {title}
        </h3>
        {children}
      </div>
    </Sider>
  );
  return isMobileView ? null : (
    <div style={{position: "sticky", top: 0, height: "100vh", zIndex: 100}}>
      {ribbon ? (
        <Badge.Ribbon text={ribbon} color="#6cdac5">
          {sider}
        </Badge.Ribbon>
      ) : (
        sider
      )}
    </div>
  );
};

const SidebarBannerText = ({displayCondition, children, className}) => (
  <div
    className={`text-white d-flex justify-content-center align-items-center ${className}`}
    style={{
      minHeight: displayCondition ? 25 : 0,
      maxHeight: displayCondition ? 25 : 0,
      opacity: displayCondition ? 1 : 0,
      overflow: "hidden",
      transition: "all 0.8s ease-in-out",
    }}>
    <strong>{children}</strong>
  </div>
);

const ProfileSideBarMenuItem = ({user, setShowProfile, collapsedSidebar}) =>
  user?._id ? (
    <div
      onClick={() => setShowProfile(true)}
      style={{
        cursor: "pointer",
        paddingTop: 7,
        paddingBottom: 5,
        paddingLeft: !collapsedSidebar && 16,
        textAlign: collapsedSidebar && "center",
        opacity: 0.9,
        overflowX: "hidden",
        whiteSpace: "nowrap",
      }}>
      <Badge
        style={{marginTop: 27, marginRight: 2}}
        count={
          <SettingOutlined
            style={{backgroundColor: "white", borderRadius: "50%", padding: 2, fontSize: 12}}
          />
        }>
        <Avatar
          style={{display: "inline-block", outline: "2px solid white"}}
          className="bg-noe-gradient"
          icon={<UserOutlined />}
        />
      </Badge>

      {!collapsedSidebar && (
        <div style={{display: "inline-block", fontWeight: "bold", color: "white", paddingLeft: 14}}>
          {personName(user)}
        </div>
      )}
    </div>
  ) : (
    <LayoutElement.Menu
      items={[
        {label: "Se connecter", key: "login", icon: <LoginOutlined />},
        {label: "Créer un compte", key: "signup", icon: <EditOutlined />},
      ]}
    />
  );

/**
 * MOBILE NAVBAR
 */

const Navbar = ({title, children, displayButtonBadge, ribbon}) => {
  const [visible, setVisible] = useState(false);

  const online = useOnlineStatus();

  const IconContainer = ({className, style, children}) => (
    <div
      className={className}
      style={{
        borderRadius: 50,
        border: "1px solid var(--noe-bg)",
        minHeight: 32,
        minWidth: 32,
        marginBottom: 3,
        marginRight: 8,
        paddingLeft: 6,
        paddingTop: 6,
        ...style,
      }}>
      {children}
    </div>
  );

  const header = (
    <Header className="containerH mobile-navbar-container">
      <h4
        style={{
          fontSize: "18px",
          color: "white",
          display: "flex",
          alignItems: "baseline",
          whiteSpace: "wrap",
          margin: "auto",
          paddingTop: 5,
          fontWeight: "bold",
        }}>
        {OFFLINE_MODE && ( // Offline mode activated icon
          <IconContainer className="bg-info" style={{zIndex: 1}}>
            <DownloadOutlined />
          </IconContainer>
        )}
        {OFFLINE_MODE &&
          online && ( // Available internet connection while in offline mode
            <IconContainer className="bg-success" style={{marginLeft: -20}}>
              <WifiOutlined />
            </IconContainer>
          )}

        {process.env.REACT_APP_MODE === "DEMO" && <Tag color="#ff4d4f">DÉMO</Tag>}

        {title}
      </h4>
    </Header>
  );

  return (
    <>
      <Badge
        className="navbar-menu-button"
        dot
        count={displayButtonBadge && !visible ? 1 : 0}
        style={{top: 5, right: 5, width: 10, height: 10}}>
        <Button
          type="primary"
          size="large"
          icon={visible ? <CloseOutlined /> : <MenuOutlined />}
          onClick={() => setVisible(!visible)}
          // Make the button yellow if no connection while not in offline mode
          className={"shadow" + (online || OFFLINE_MODE ? "" : " warning-button")}
        />
      </Badge>
      <div style={{overflow: "hidden"}}>
        {ribbon ? (
          <Badge.Ribbon text={<span style={{marginRight: 5}}>{ribbon}</span>} color="#6cdac5">
            {header}
          </Badge.Ribbon>
        ) : (
          header
        )}
      </div>
      <Drawer
        className="mobile-sidebar-drawer"
        placement="left"
        zIndex={99}
        onClick={() => setVisible(false)}
        visible={visible}
        headerStyle={{background: "var(--noe-bg)", borderRadius: 0}}
        bodyStyle={{
          background: "var(--noe-bg)",
          padding: "50px 0 0 0",
        }}>
        <div className="sidebar-container bg-noe">{children}</div>
      </Drawer>
    </>
  );
};

/**
 * GLOBAL LAYOUT STRUCTURE
 */

export function LayoutStructure({
  title,
  ribbon,
  menu,
  fadeIn,
  showSocialIcons = true,
  children,
  profileUser,
  displayButtonBadge,
  collapsedSidebar,
}) {
  const online = useOnlineStatus();
  const {isMobileView} = useWindowDimensions();

  // Profile modal state
  const [showProfile, setShowProfile] = useState(false);

  menu = (
    <>
      {/* Online/offline small banner */}
      <div
        style={{
          position: "sticky",
          zIndex: 1000,
          top: 0,
          flexGrow: 0,
          flexShrink: 0,
          marginBottom: 5,
          display: "flex",
          flexDirection: "column",
          gap: 3,
        }}>
        {/* Offline mode banners */}
        <SidebarBannerText displayCondition={OFFLINE_MODE} className={"bg-info"}>
          <DownloadOutlined />
          {!collapsedSidebar && " Mode offline activé"}
        </SidebarBannerText>
        <SidebarBannerText displayCondition={online && OFFLINE_MODE} className={"bg-success"}>
          <WifiOutlined />
          {!collapsedSidebar && " Connexion disponible"}
        </SidebarBannerText>
        <SidebarBannerText displayCondition={!online && !OFFLINE_MODE} className={"bg-warning"}>
          <WarningOutlined />
          {!collapsedSidebar && " Vous êtes hors-ligne"}
        </SidebarBannerText>

        {/* Demo mode banner */}
        <SidebarBannerText
          displayCondition={process.env.REACT_APP_MODE === "DEMO"}
          className={"bg-error"}>
          MODE DÉMO
        </SidebarBannerText>
      </div>

      <div className="sidebar-top">
        {menu.top}

        {showSocialIcons && (
          <>
            <LayoutElement.SidebarDivider color="rgba(255,255,255,0.9)" />
            <div className="social-icons-container" style={{textAlign: "center", marginTop: 30}}>
              <LayoutElement.NOESocialIcons style={{fontSize: 16, margin: 8}} />
            </div>
          </>
        )}
      </div>

      <div className="sidebar-footer" style={{marginTop: "15px"}}>
        {menu.footer}
        <ProfileSideBarMenuItem
          user={profileUser}
          setShowProfile={setShowProfile}
          collapsedSidebar={collapsedSidebar}
        />
      </div>
    </>
  );
  // Check if there is a title (means that the project is loaded). If yes, then display the layout, else just wait with loading screen
  return title ? (
    <>
      <Layout>
        {/*The overlay div that makes a great transition when we open the app*/}
        {fadeIn && (
          <div
            className={`${defaultBackgroundClassName} fade-out`}
            style={{
              position: "fixed",
              width: "100%",
              height: "100%",
              zIndex: 10000,
            }}
          />
        )}

        {isMobileView && (
          <Navbar displayButtonBadge={displayButtonBadge} title={title} ribbon={ribbon}>
            {menu}
          </Navbar>
        )}
        {/* Page content: sidebar and main content */}
        <Layout style={{background: "white"}}>
          <Sidebar
            collapsed={collapsedSidebar}
            title={
              collapsedSidebar
                ? title
                    ?.split(" ")
                    .map((word) => word.charAt(0))
                    .join("")
                : title
            }
            ribbon={ribbon}>
            {menu}
          </Sidebar>

          <CustomErrorBoundary>
            <Content className="page-content">
              {OFFLINE_MODE && (
                <div className="full-width-content">
                  <Alert
                    closable
                    type="info"
                    banner
                    style={{margin: 5}}
                    message="Mode offline activé : les données affichées ne sont peut-être pas complètes."
                  />
                </div>
              )}
              {children}
            </Content>
          </CustomErrorBoundary>
        </Layout>

        <MyAccount visibleState={[showProfile, setShowProfile]} />
      </Layout>
    </>
  ) : (
    <LayoutElement.Pending
      spinnerLogo={arrivalSpinnerLogo}
      className={defaultBackgroundClassName}
    />
  );
}

/**
 * ERROR BOUNDARY
 */

export const CustomErrorBoundary = ({children, onError, extra}) => (
  <ErrorBoundary
    onError={onError}
    fallback={() => (
      <Result
        style={{margin: "auto"}}
        status="404"
        title="Oups ! Vous avez probablement trouvé un bug... 😁"
        subTitle={
          <>
            <p>Pour nous aider à corriger ce petit "couac", vous pouvez :</p>

            <p>
              <a
                href={`mailto:${process.env.REACT_APP_CONTACT_US_EMAIL}?subject=Bug%20NOÉ%20(${window.location.href})`}
                rel="noreferrer">
                Nous envoyer un email
              </a>
            </p>
            <p>
              <a href="https://m.me/noeappli" target="_blank" rel="noreferrer">
                Nous contacter sur les réseaux
              </a>
            </p>
            <p>Ça ne vous prend qu'une minute ou deux, mais ça nous aide énormément !</p>
            <p>Merci 🥰</p>
          </>
        }
        extra={extra}
      />
    )}>
    {children}
  </ErrorBoundary>
);
