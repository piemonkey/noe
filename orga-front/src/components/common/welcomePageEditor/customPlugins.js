import {ColorPickerField} from "@react-page/editor";

// The text editor plugin
import slate, {pluginFactories} from "@react-page/plugins-slate";
import "@react-page/plugins-slate/lib/index.css";

// The image plugin
import {imagePlugin} from "@react-page/plugins-image";
import type {ImageUploadType} from "@react-page/plugins-image";
import "@react-page/plugins-image/lib/index.css";

// The background plugin
import background, {ModeEnum} from "@react-page/plugins-background";
import "@react-page/plugins-background/lib/index.css";

/**
 * Add color customization in Slate's, Text plugin toolbar
 * https://react-page.github.io/docs/#/slate?id=example-simple-color-plugin
 */
export const slateTextColor = pluginFactories.createComponentPlugin({
  addHoverButton: true,
  addToolbarButton: true,
  type: "SetColor",
  object: "mark",
  icon: <span>Color</span>,
  label: "Set Color",
  Component: "span",
  getStyle: ({color}) => ({color}),
  controls: {
    type: "autoform",
    schema: {
      required: ["color"],
      properties: {
        color: {
          type: "string",
          default: "rgba(0,0,255,1)",
          uniforms: {component: ColorPickerField},
        },
      },
    },
  },
});
export const slateP = slate((def) => ({
  ...def,
  plugins: {
    ...def.plugins,
    custom: {custom1: slateTextColor},
  },
}));

const fakeImageUploadService: () => ImageUploadType = () => (file, reportProgress) => {
  return new Promise((resolve, reject) => {
    let counter = 0;
    const interval = setInterval(() => {
      counter++;
      reportProgress(counter * 10);
      if (counter > 9) {
        clearInterval(interval);
        alert(
          "Image has not actually been uploaded to a server. Check documentation for information on how to provide your own upload function."
        );
        resolve({url: URL.createObjectURL(file)});
      }
    }, 100);
  });
};
export const imageP = imagePlugin({imageUpload: fakeImageUploadService()});
export const backgroundP = background({
  imageUpload: fakeImageUploadService(),
  enabledModes: ModeEnum.COLOR_MODE_FLAG | ModeEnum.IMAGE_MODE_FLAG | ModeEnum.GRADIENT_MODE_FLAG,
});
