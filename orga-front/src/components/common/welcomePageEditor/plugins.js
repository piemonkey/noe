import {ColorPickerField} from "@react-page/editor";

// The divider plugin
import divider from "@react-page/plugins-divider";

// The spacer plugin
import spacer from "@react-page/plugins-spacer";
import "@react-page/plugins-spacer/lib/index.css";

// The video plugin
import video from "@react-page/plugins-video";
import "@react-page/plugins-video/lib/index.css";

// Customized plugins
import {imageP, slateP, backgroundP} from "./customPlugins";

const plugins = [slateP, backgroundP, spacer, divider, imageP, video];

const addStylesAndMarginDefaultControl = (plugins) =>
  plugins.map((plugin) => ({
    ...plugin,
    cellStyle: (data) => ({
      paddingLeft: data.paddingLeft,
      paddingRight: data.paddingRight,
      paddingTop: data.paddingTop,
      paddingBottom: data.paddingBottom,
      marginLeft: data.marginLeft,
      marginRight: data.marginRight,
      marginTop: data.marginTop,
      marginBottom: data.marginBottom,
      height: `calc(100% - calc(${data.marginTop}px + ${data.marginBottom}px))`,
      width: `calc(100% - calc(${data.marginLeft}px + ${data.marginRight}px))`,
      background: data.background,
      color: `${data.textColor} !important`,
      border: data.borderWidth > 0 ? `${data.borderWidth}px solid ${data.borderColor}` : undefined,
      borderRadius: data.borderRadius,
    }),
    cellSpacing: (data) => ({x: data.cellSpacingX, y: data.cellSpacingY}),
    controls: [
      {title: "Contrôles", controls: plugin.controls},
      {
        title: "Marges",
        controls: {
          type: "autoform",
          columnCount: 3,
          schema: {
            properties: {
              marginTop: {
                title: "Marge extérieure haute",
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              marginBottom: {
                title: "Marge extérieure basse",
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              marginLeft: {
                title: "Marge extérieure gauche",
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              marginRight: {
                title: "Marge extérieure droite",
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              paddingTop: {
                title: "Marge intérieure haute",
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              paddingBottom: {
                title: "Marge intérieure basse",
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              paddingLeft: {
                title: "Marge intérieure gauche",
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              paddingRight: {
                title: "Marge intérieure droite",
                type: "number",
                default: 20,
                multipleOf: 1,
              },
              cellSpacingX: {
                title: "Espacement horizontal entre les cellules",
                type: "number",
                default: 0,
                multipleOf: 1,
              },
              cellSpacingY: {
                title: "Espacement vertical entre les cellules",
                type: "number",
                default: 0,
                multipleOf: 1,
              },
            },
          },
        },
      },
      {
        title: "Styles",
        controls: {
          type: "autoform",
          columnCount: 3,
          schema: {
            properties: {
              borderWidth: {title: "Largeur de bordure", type: "number", default: 0, multipleOf: 1},
              borderRadius: {title: "Rayon de bordure", type: "number", default: 0, multipleOf: 1},
              borderColor: {
                title: "Couleur de bordure",
                type: "string",
                default: "#000000",
                uniforms: {component: ColorPickerField},
              },
              textColor: {
                title: "Couleur de texte",
                type: "string",
                default: "#000000",
                uniforms: {component: ColorPickerField},
              },
              backgroundColor: {
                title: "Couleur d'arrière plan",
                type: "string",
                default: "#ffffff",
                uniforms: {component: ColorPickerField},
              },
            },
          },
        },
      },
    ],
    bottomToolbar: {dark: true},
  }));

export default addStylesAndMarginDefaultControl(plugins);
