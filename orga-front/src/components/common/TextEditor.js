import React, {useEffect, useMemo, useRef} from "react";
import ReactQuill, {Quill} from "react-quill";
import "react-quill/dist/quill.snow.css";
import {Form, Modal} from "antd";
import {FormElement} from "./FormElement";
import {InputElement} from "./InputElement";
import {trySeveralTimes} from "../../helpers/agendaUtilities";

const toolbar = [
  [{size: []}],
  ["bold", "italic", "underline"],
  ["blockquote"],
  [{list: "ordered"}, {list: "bullet"}],
  ["link", "image", "video"],
  [{color: []}, {background: []}],
  [{align: []}],
];

export const TextDisplayer = ({value}) => (
  <div className="ql-snow">
    <div className="ql-editor ql-editor-readonly" dangerouslySetInnerHTML={{__html: value}} />
  </div>
);

export const TextEditor = ({onChange, placeholder, value}) => {
  const reactQuillRef = useRef();
  const [form] = Form.useForm();

  // **** Image Handler stuff ****
  const imageHandler = async () => {
    const quillEditor = reactQuillRef.current.editor;
    const range = quillEditor.getSelection();

    const onOk = async () => {
      await form.validateFields();
      const {url, width, height} = form.getFieldsValue();
      quillEditor.clipboard.dangerouslyPasteHTML(
        range.index,
        `<img src="${url}" width="${width}%" height="${height}px">`,
        Quill.sources.USER
      );
    };

    const ImageHandlerModalContent = () => {
      useEffect(() => {
        trySeveralTimes(() => document.getElementsByClassName("url-input")[0].focus());
      }, []);

      return (
        <FormElement
          form={form}
          validateAction={async () => {
            await onOk();
            Modal.destroyAll();
          }}>
          <div className="container-grid">
            <InputElement.Text
              className="url-input"
              label="URL de l'image"
              name="url"
              placeholder="url"
              rules={[{type: "url", required: true}]}
            />
            <div className="container-grid two-per-row">
              <InputElement.Number
                label="Largeur"
                name="width"
                bordered
                placeholder="largeur"
                addonAfter="%"
              />
              <InputElement.Number
                label="Hauteur"
                name="height"
                bordered
                placeholder="hauteur"
                addonAfter="px"
              />
            </div>
          </div>
        </FormElement>
      );
    };

    Modal.confirm({icon: false, content: <ImageHandlerModalContent />, onOk});
  };

  // **** Quill Editor ****
  return useMemo(
    () => (
      <ReactQuill
        ref={reactQuillRef}
        theme="snow"
        onChange={
          onChange
            ? (value, ...rest) => {
                if (value === "<p><br></p>") value = ""; // When the text is blank, then just remove everything
                onChange(value, ...rest);
              }
            : undefined
        }
        modules={{
          toolbar: {
            container: toolbar,
            handlers: {image: imageHandler},
          },
        }}
        placeholder={placeholder}
        value={value}
      />
    ),
    []
  );
};
