import {Form} from "antd";
import React from "react";
import isHotkey from "is-hotkey";
const isCtrlSPressed = isHotkey("mod+S");
const isCtrlEnterPressed = isHotkey("mod+Enter");

export const FormElement = ({validateAction, children, ...otherProps}) => {
  return (
    <Form
      layout={"vertical"}
      onKeyDown={
        (e) =>
          validateAction && // There is a validation action
          (isCtrlSPressed(e) || // Event was a press on Ctrl + S
            isCtrlEnterPressed(e)) && // Event was a press on Ctrl + Enter
          validateAction(isCtrlSPressed(e)) // ... Then, validate and give the info and if Ctrl+S, then pass a true value
      }
      {...otherProps}
      validateMessages={{
        // eslint-disable-next-line no-template-curly-in-string
        required: "'${label}' est obligatoire.",
        types: {
          email: "Merci de rentrer un email valide.",
          url: "Merci de rentrer une URL valide.",
        },
        string: {
          // eslint-disable-next-line no-template-curly-in-string
          min: "'${label}' doit faire au moins ${min} caractères.",
        },
      }}
      requiredMark={false}>
      {children}
    </Form>
  );
};
