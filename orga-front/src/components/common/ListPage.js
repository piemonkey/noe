import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Table, Tooltip, message, Upload, Modal, Alert} from "antd";
import {
  DoubleRightOutlined,
  EditOutlined,
  FileExcelOutlined,
  ImportOutlined,
  InboxOutlined,
  SelectOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import {CardElement, LayoutElement} from "./LayoutElement";
import {InputElement} from "./InputElement";

import {
  getFullHeightWithMargins,
  navbarHeight,
  useSavedPagination,
  useWindowDimensions,
} from "../../helpers/viewUtilities";
import {paginationPageSizes, viewActions, viewSelectors} from "../../features/view";
import {DeleteButton} from "./EditPage";
import {useCopyColumns, useSearchInColumns} from "../../helpers/tableUtilities";
import {listSorter, useDrawer} from "../../helpers/listUtilities";
import {navigate, useMatch} from "@reach/router";
import {fetchWithMessages} from "../../helpers/reduxUtilities";
import {currentProjectSelectors} from "../../features/currentProject";
import {TableElement} from "./TableElement";
import {CsvExportButton} from "../../helpers/filesUtilities";
import {FormElement} from "./FormElement";
import {useLocalStorageState} from "../../helpers/localStorageUtilities";

const useEntitySchema = (filterSchema) => {
  const [entitySchema, setEntitySchema] = useState([]);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const {endpoint} = useMatch("/:envId/:endpoint");

  useEffect(() => {
    fetchWithMessages(`projects/${currentProject._id}/${endpoint}/schema`, {
      method: "GET",
    }).then((schema) => setEntitySchema(schema.filter(filterSchema || (() => true))));
  }, []);

  return [entitySchema, endpoint];
};

const GroupEditionButton = ({selectedRowKeys}) => {
  const [visible, setVisible] = useState(false);
  const {useAI, usePlaces, useTeams} = useSelector(currentProjectSelectors.selectProject);
  const [selectedFields, setSelectedFields] = useState([]);
  const [entitySchema, endpoint] = useEntitySchema((field) => {
    if (
      (!useAI && field.useAI) ||
      (!usePlaces && field.usePlaces) ||
      (!useTeams && field.useTeams) ||
      field.noGroupEditing
    ) {
      return false;
    } else {
      return true;
    }
  });

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setSelectedFields(selectedRowObject);
    },
  };

  return (
    <>
      <Tooltip title="Édition groupée">
        <Button
          icon={<EditOutlined />}
          type="link"
          disabled={selectedRowKeys.length === 0}
          onClick={() => setVisible(true)}
        />
      </Tooltip>

      <LayoutElement.ElementSelectionModal
        title="Choisissez les champs que vous souhaitez modifier en groupe"
        subtitle="Ces champs seront les seuls à être modifiés. Les autres ne seront pas affectés."
        visible={visible}
        onOk={() => {
          navigate(`${endpoint}/groupedit`, {
            state: {
              groupEditing: {
                fieldsToUpdate: selectedFields,
                elements: selectedRowKeys,
              },
            },
          });
        }}
        onCancel={() => setVisible(false)}
        rowSelection={rowSelection}
        selectedRowKeys={selectedFields}
        rowKey="key"
        size="small"
        showHeader={false}
        setSelectedRowKeys={setSelectedFields}
        columns={[
          {
            dataIndex: "label",
            defaultSortOrder: "ascend",
            sorter: (a, b) => listSorter.text(a.label, b.label),
          },
        ]}
        dataSource={entitySchema}
      />
    </>
  );
};

const CsvGroupImportButton = ({elementsActions, elementsName}) => {
  const dispatch = useDispatch();
  const [modalVisible, setModalVisible] = useState(false);
  const [elementsToImport, setElementsToImport] = useState([]);
  const [entitySchema, endpoint] = useEntitySchema();
  const allowedFields = entitySchema.map((field) => field.key);
  const schemaColumns = entitySchema.map((field) => ({dataIndex: field.key, title: field.label}));
  const parseJsonFromCsvStringEntry = (string: string) => {
    if (string[0] === "{" || string[0] === "[") {
      string = string.replace(/[“”]/g, '"').replace(/[‘’]/g, "'");
      return JSON.parse(string);
    } else {
      return string;
    }
  };

  const parseCsvFile = (file) => {
    let reader = new FileReader();
    setElementsToImport([]);
    reader.onload = async function (e) {
      const tmpElements = [];

      // Import fast-css dynamically
      const {parseString} = await import(/* webpackPrefetch: true */ "@fast-csv/parse");

      parseString(e.target.result, {headers: true})
        .on("error", (error) =>
          message.error(
            `Oups, il semblerait que votre fichier d'import soit malformé ${error.message}`
          )
        )
        .on("data", (row) =>
          tmpElements.push(
            Object.fromEntries(
              Object.entries(row)
                .filter(([key, val]) => allowedFields.includes(key) && val !== "")
                .map(([key, val]) => [
                  key,
                  parseJsonFromCsvStringEntry(val), // If the string is JSON, then parse it
                ])
            )
          )
        )
        .on("end", (rowCount: number) => {
          setElementsToImport(tmpElements);
        });
    };
    reader.readAsText(file);
  };

  return (
    <>
      <Tooltip title="Importer à partir d'un fichier CSV">
        <Button type="link" icon={<ImportOutlined />} onClick={() => setModalVisible(true)} />
      </Tooltip>
      <Modal
        centered
        title="Importer en CSV"
        visible={modalVisible}
        onCancel={() => setModalVisible(false)}
        onOk={() => {
          elementsToImport.forEach((el) => dispatch(elementsActions.persist({_id: "new", ...el})));
          setElementsToImport([]);
          setModalVisible(false);
        }}
        width="98%"
        okText="Importer les éléments">
        <Alert
          type="warning"
          message="L'import CSV est une fonctionnalité expérimentale."
          description="Faites attention à ce que vous faites ! :) Notamment, les champs qui ne ont pas du texte simple ne sont pas encore supportés."
          showIcon
          style={{marginBottom: 26}}
        />
        <div style={{marginBottom: 26}}>
          <Upload.Dragger
            beforeUpload={(file) => {
              parseCsvFile(file);
              return false;
            }}
            maxCount={1}
            accept="text/csv"
            showUploadList={true}>
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">Cliquez ou déposez un fichier</p>
          </Upload.Dragger>
        </div>
        {elementsToImport.length > 0 ? (
          <TableElement.WithTitle
            title="Aperçu des élements à importer"
            dataSource={elementsToImport}
            columns={schemaColumns}
            scroll={{
              x: (schemaColumns.length - 1) * 160,
              y: window.innerHeight - 500,
            }}
            showHeader
          />
        ) : (
          <Alert
            type="info"
            message="Besoin d'aide ?"
            description={
              <>
                <p>
                  Vous pouvez télécharger un fichier d'exemple d'import pour les{" "}
                  {elementsName.toLowerCase()}.
                </p>
                <CsvExportButton
                  getExportName={() => `Exemple de fichier d'import - ${elementsName}.csv`}
                  dataExportFunction={() => [
                    entitySchema.reduce((acc, field) => {
                      acc[field.key] = `[exemple de '${field.label.toLowerCase()}']`;
                      return acc;
                    }, {}),
                  ]}
                  icon={<FileExcelOutlined />}
                  type="primary"
                  style={{marginBottom: 15}}>
                  Télécharger le fichier d'exemple
                </CsvExportButton>
                <p>
                  Dans l'import, vous pouvez référencer des listes d'objets en utilisant l'ID des
                  objets à référencer, que vous trouverez dans l'URL de leur page dédiée :{" "}
                  <tt>{`/<id_de_votre_événement>/${endpoint}/<id_de_l'objet>`}</tt>
                </p>
                <p>
                  Pour créer des listes d'éléments, vous pouvez utiliser les conventions JSON pour
                  remplir les champs. Par exemple :{" "}
                  <tt>["62fe708c59422416df44f4ac", "62fe708c59422416df44f4a6"]</tt> ou{" "}
                  <tt>{`{"aaa": "aaa test", "bbb": {"ccc": "ccc test"}}`}</tt>
                </p>
              </>
            }
            style={{marginBottom: 26}}
          />
        )}
      </Modal>
    </>
  );
};

export function ListPage({
  title,
  entitiesName,
  subtitle,
  customButtons,
  multipleActionsButtons: MultipleActionsButtons,
  buttonTitle,
  settingsDrawerContent,

  header = true,
  fullPage = true,

  elementsActions,
  columns: rawColumns,
  dataSource,

  editable = true,
  deletable = true,
  clickable,
  navigateFn,
  navigable = false,
  groupEditable = false,
  groupImportable = false,
  rowClassName,
  expandable,
}) {
  const dispatch = useDispatch();
  const [pagination, setPagination] = useSavedPagination();
  const {isMobileView} = useWindowDimensions();
  const sorting = useSelector(viewSelectors.selectSorting);
  const [displaySize, setDisplaySize] = useLocalStorageState(
    "listDisplaySize",
    isMobileView ? "small" : "middle"
  );
  const [setSettingsDrawerVisible, SettingsDrawer] = useDrawer({
    width: isMobileView ? undefined : "700px",
  });

  /*********** Multiple selection **************************/

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [enableMultipleSelection, setEnableMultipleSelection] = useState(false);

  const rowSelection = enableMultipleSelection && {
    selectedRowKeys,
    onChange: (newSelectedRowKeys) => setSelectedRowKeys(newSelectedRowKeys),
  };

  const onBatchDelete = async () => {
    const notDeleted = [];
    for (const id of selectedRowKeys) {
      try {
        await dispatch(elementsActions.remove(id));
      } catch {
        notDeleted.push(id);
      }
    }
    setSelectedRowKeys(notDeleted);
  };

  /*********** Sorting / Filtering memorization ************/

  let {columns, handleDisplayConfigChange, currentDataSource} = useCopyColumns(
    dataSource,
    rawColumns,
    (pagination, filters, sorter, extra) => {
      dispatch(viewActions.changeSorting({filteredInfo: filters, sortedInfo: sorter}));
    }
  );

  const clearDisplayConfig = () => {
    dispatch(viewActions.changeSorting({}));
  };

  columns = columns.map((column) => {
    const savedSortingOrder =
      sorting?.sortedInfo?.column?.dataIndex === column.dataIndex && sorting?.sortedInfo?.order;
    return {
      // The column data
      ...column,
      // Add the saved sorting order if found
      sortOrder: savedSortingOrder || column.sortOrder,
      filteredValue: sorting?.filteredInfo?.[column.dataIndex],
    };
  });

  columns = useSearchInColumns(columns);

  if (editable || deletable || navigable) {
    columns.push({
      key: "action",
      width: 9 + (editable ? 32 + 9 : 0) + (deletable ? 32 + 9 : 0) + (navigable ? 32 + 9 : 0),
      render: (text, record) => (
        <div
          className="containerH buttons-container"
          style={{justifyContent: "flex-end", flexWrap: "nowrap"}}>
          {navigable ? (
            <Button
              style={{flexGrow: 0}}
              type="link"
              icon={<DoubleRightOutlined />}
              onClick={() => navigateFn(record._id)}
            />
          ) : (
            <>
              {editable && (
                <Button
                  style={{flexGrow: 0}}
                  type="link"
                  icon={<EditOutlined />}
                  onClick={() => navigateFn(record.slug || record._id)}
                />
              )}
              {deletable && (
                <DeleteButton onConfirm={() => dispatch(elementsActions.remove(record._id))} />
              )}
            </>
          )}
        </div>
      ),
    });
  }

  const resize = () => {
    if (header) {
      try {
        const heightOffset =
          getFullHeightWithMargins(document.querySelector(".ant-table-header")) + // table header row
          getFullHeightWithMargins(document.querySelector(".subtitle")) + // subtitle
          getFullHeightWithMargins(document.querySelector(".ant-table-pagination")) + // table footer row
          getFullHeightWithMargins(document.querySelector(".list-page-header")) + // page title header
          navbarHeight() + // The height of the navbar if we're in mobile mode
          3; // Security margin
        const tableBody = document.querySelector(".ant-table-body");
        tableBody.style.maxHeight = `calc(100vh - ${heightOffset}px)`;
        tableBody.style.minHeight = `calc(100vh - ${heightOffset}px)`;
      } catch {
        //Do nothing
      }
    }
  };
  useEffect(resize);
  useEffect(() => setTimeout(resize, 3));

  return (
    <div
      className={`containerV ${fullPage ? "full-width-content" : ""}`}
      style={{background: "#fafafa"}}>
      {header && (
        <LayoutElement.PageHeading
          className="list-page-header"
          title={title}
          customButtons={
            <>
              {!enableMultipleSelection && customButtons}

              {sorting?.sortedInfo?.column && ( // There is a sorter activated
                <Button type="link" onClick={clearDisplayConfig}>
                  Effacer les filtres
                </Button>
              )}

              {(deletable || MultipleActionsButtons || groupEditable) &&
                (enableMultipleSelection ? (
                  <>
                    <span style={{marginRight: 8}}>{selectedRowKeys.length} sélectionnés.</span>

                    {MultipleActionsButtons && (
                      <MultipleActionsButtons
                        selectedRowKeys={selectedRowKeys}
                        setSelectedRowKeys={setSelectedRowKeys}
                      />
                    )}

                    {groupEditable && <GroupEditionButton selectedRowKeys={selectedRowKeys} />}

                    {deletable && (
                      <DeleteButton
                        onConfirm={onBatchDelete}
                        disabled={selectedRowKeys.length === 0}
                      />
                    )}

                    <Button
                      type="link"
                      danger
                      onClick={() => {
                        setEnableMultipleSelection(false);
                        setSelectedRowKeys([]);
                      }}>
                      Annuler
                    </Button>
                  </>
                ) : (
                  <Tooltip title="Sélection multiple">
                    <Button
                      type="link"
                      style={{marginRight: 12}}
                      icon={<SelectOutlined />}
                      onClick={() => setEnableMultipleSelection(true)}
                    />
                  </Tooltip>
                ))}

              {groupImportable && (
                <CsvGroupImportButton elementsActions={elementsActions} elementsName={title} />
              )}
            </>
          }
          buttonTitle={buttonTitle}
          onButtonClick={() => navigateFn("new")}
        />
      )}
      {subtitle && <div className="subtitle with-margins">{subtitle}</div>}
      <div>
        <div style={{position: "fixed", right: 10, zIndex: 3}}>
          <Tooltip title="Paramètres d'affichage de la liste" placement="topRight">
            <Button
              icon={<SettingOutlined />}
              style={{transform: "translateY(8px)"}}
              className="shadow"
              onClick={() => setSettingsDrawerVisible(true)}
            />
          </Tooltip>
        </div>

        <Table
          scroll={{
            y: "100vh",
            x: (columns.length - 1) * 160 + 70,
          }}
          columns={columns}
          rowClassName={rowClassName || ((record) => record.updatedAt && "ant-table-row-updated")}
          pagination={{
            position: ["bottomCenter"],
            current: pagination.current,
            pageSize: pagination.pageSize,
            pageSizeOptions: paginationPageSizes,
            total: dataSource.length,
            showSizeChanger: true,
            showTotal: (total, range) =>
              `${range[0]}-${range[1]} sur ${currentDataSource?.length || total} ${(!isMobileView
                ? entitiesName || title
                : ""
              ).toLowerCase()} `,
            onChange: setPagination,
          }}
          onRow={
            clickable !== false &&
            navigateFn &&
            ((record) => ({onDoubleClick: () => navigateFn(record.slug || record._id)}))
          }
          expandable={expandable}
          size={displaySize}
          rowKey="_id"
          dataSource={dataSource}
          rowSelection={rowSelection}
          onChange={handleDisplayConfigChange}
        />
      </div>

      <SettingsDrawer>
        <h3 style={{marginBottom: 20}}>Paramètres</h3>
        {settingsDrawerContent}
        <CardElement title="Confort visuel">
          <FormElement>
            <InputElement.Select
              label="Densité d'affichage"
              onChange={setDisplaySize}
              defaultValue={displaySize}
              options={[
                {value: "small", label: "Compact"},
                {value: "middle", label: "Confort"},
              ]}
            />
          </FormElement>
        </CardElement>
      </SettingsDrawer>
    </div>
  );
}
