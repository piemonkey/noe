import React from "react";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "../../features/currentProject";
import {Progress, Tooltip} from "antd";
import {listRenderer} from "../../helpers/listUtilities";

export function VolunteeringGauge({registration, style}) {
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  if (currentProject && registration) {
    const {minMaxVolunteering} = currentProject;
    const {voluntaryCounter, numberOfDaysOfPresence} = registration;
    if (minMaxVolunteering && (voluntaryCounter || voluntaryCounter === 0)) {
      const [minVolunteering, maxVolunteering] = minMaxVolunteering;
      const max = minVolunteering + maxVolunteering;

      let color, contextualMessage;
      if (voluntaryCounter < minVolunteering) {
        color = "red";
        contextualMessage = "Inscris toi à plus d'activités de bénévolat pour remplir ta jauge.";
      } else {
        if (voluntaryCounter > maxVolunteering) {
          color = "dodgerblue";
          contextualMessage =
            "Tu fais beaucoup de bénévolat... Pense à prendre du temps pour toi !";
        } else {
          color = "#52c41a"; // Same color as success buttons
          contextualMessage = "Ni trop, ni pas assez : ta jauge de bénévolat est parfaite.";
        }
      }

      const tooltipMessage = (
        <>
          <p>
            Tu comptabilises {numberOfDaysOfPresence} jours de présence, et ton total de bénévolat
            s'élève à {listRenderer.durationFormat(voluntaryCounter * numberOfDaysOfPresence)}. Par
            jour, cela fait donc {listRenderer.durationFormat(voluntaryCounter)}.
          </p>
          <p>
            Le nombre d'heures par jour conseillé se trouve entre{" "}
            {listRenderer.durationFormat(minVolunteering)} et{" "}
            {listRenderer.durationFormat(maxVolunteering)}.
          </p>
          <p>{contextualMessage}</p>
        </>
      );

      let gaugePercentage = (100 * voluntaryCounter) / max;
      if (gaugePercentage < 1) gaugePercentage = 3; // Always see a little bit of red, even if the jauge is empty

      return (
        <Tooltip title={tooltipMessage} placement="right">
          <Progress
            style={style}
            percent={gaugePercentage}
            strokeWidth={15}
            trailColor="#d9d9d9ee"
            showInfo={false}
            strokeColor={color}
          />
        </Tooltip>
      );
    }
    // Fallback: don't display anything if there is no loaded project
    return null;
  }
}
