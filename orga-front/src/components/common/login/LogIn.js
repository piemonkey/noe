import React from "react";
import {useSelector, useDispatch} from "react-redux";
import {currentUserSelectors, currentUserActions} from "../../../features/currentUser.js";
import {Button, Form} from "antd";
import {InputElement} from "../InputElement";
import {ConnectionPage} from "../ConnectionPage";
import {viewSelectors} from "../../../features/view";

export function LogIn({navigate, envId, subtitle}) {
  const [form] = Form.useForm();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const searchParamsInfo = useSelector(viewSelectors.selectSearchParams);
  const dispatch = useDispatch();

  const onChange = (changedFields, allFields) => {
    dispatch(currentUserActions.changeLogin(allFields[0].value));
    dispatch(currentUserActions.changePassword(allFields[1].value));
  };

  const logIn = () => {
    form.validateFields().then(() => {
      dispatch(currentUserActions.changeConnectionError(undefined));
      dispatch(currentUserActions.changeConnectionNotice(undefined));
      dispatch(currentUserActions.logIn());
    });
  };

  const goToSignInPage = () => {
    // Flush alerts
    dispatch(currentUserActions.changeConnectionError(undefined));
    dispatch(currentUserActions.changeConnectionNotice(undefined));
    navigate("../signup");
  };

  const goToForgotPasswordPage = () => {
    dispatch(currentUserActions.changeConnectionError(undefined));
    dispatch(currentUserActions.changeConnectionNotice(undefined));
    navigate("../forgotpassword");
  };

  return (
    <ConnectionPage
      form={form}
      className="fade-in"
      envId={envId}
      subtitle={subtitle}
      initialValues={{
        email: currentUser.email || searchParamsInfo.email,
        password: currentUser.password,
      }}
      onFieldsChange={onChange}
      validateAction={logIn}
      buttons={
        <>
          <Button type="primary" onClick={logIn}>
            Se connecter
          </Button>
          <Button onClick={goToSignInPage}>Créer un compte</Button>
          <Button type="link" onClick={goToForgotPasswordPage}>
            Mot de passe oublié ?
          </Button>
        </>
      }>
      <InputElement.Text
        label="Email"
        name="email"
        size="large"
        placeholder="email"
        autoComplete="email"
        rules={[{required: true}, {type: "email"}]}
      />
      <InputElement.Password
        label="Mot de passe"
        name="password"
        size="large"
        placeholder="mot de passe"
        autoComplete="current-password"
        rules={[{required: true}]}
      />
    </ConnectionPage>
  );
}
