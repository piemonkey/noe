import React, {useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import {currentUserSelectors, currentUserActions} from "../../../features/currentUser.js";
import {Button, Form} from "antd";
import {InputElement} from "../InputElement";
import {navigate, useLocation} from "@reach/router";
import {ConnectionPage, validatePassword, checkPasswordsAreSame} from "../ConnectionPage";

export function ResetPassword() {
  const [form] = Form.useForm();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const connectionError = useSelector(currentUserSelectors.selectConnectionError);
  const dispatch = useDispatch();
  let query = new URLSearchParams(useLocation().search);
  const token = query.get("token");

  useEffect(() => {
    dispatch(currentUserActions.checkPasswordResetToken(token));
  }, [token]);

  const onChange = (changedFields, allFields) => {
    dispatch(currentUserActions.changePassword(allFields[0].value));
  };

  const resetPassword = () => {
    form.validateFields().then(() => {
      dispatch(currentUserActions.changeConnectionError(undefined));
      dispatch(currentUserActions.changeConnectionNotice(undefined));
      dispatch(currentUserActions.resetPassword(token));
    });
  };

  const goToForgotPasswordPage = () => {
    // Flush alerts
    dispatch(currentUserActions.changeConnectionError(undefined));
    dispatch(currentUserActions.changeConnectionNotice(undefined));
    navigate("../forgotpassword");
  };

  return (
    <ConnectionPage
      form={form}
      subtitle="Nouveau mot de passe"
      fields={[
        {
          name: "password",
          value: currentUser.password,
        },
      ]}
      onFieldsChange={onChange}
      validateAction={resetPassword}
      buttons={
        connectionError ? (
          <Button onClick={goToForgotPasswordPage}>Renvoyer le lien de mot de passe oublié</Button>
        ) : (
          <Button type="primary" onClick={resetPassword}>
            Valider le changement de mot de passe
          </Button>
        )
      }>
      <InputElement.Password
        label="Nouveau mot de passe"
        name="password"
        size="large"
        placeholder="mot de passe"
        autoComplete="new-password"
        rules={[{required: true}, {validator: validatePassword(form)}, {min: 8}]}
      />

      <InputElement.Password
        label="Confirmer le mot de passe"
        name="confirmPassword"
        size="large"
        placeholder="mot de passe"
        autoComplete="new-password"
        dependencies={["password"]}
        rules={[{required: true}, {validator: checkPasswordsAreSame(form)}]}
      />
    </ConnectionPage>
  );
}
