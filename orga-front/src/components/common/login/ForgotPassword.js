import React from "react";
import {useSelector, useDispatch} from "react-redux";
import {currentUserSelectors, currentUserActions} from "../../../features/currentUser.js";
import {Button, Form} from "antd";
import {InputElement} from "../InputElement";
import {navigate} from "@reach/router";
import {ConnectionPage} from "../ConnectionPage";
import {viewSelectors} from "../../../features/view";

export function ForgotPassword() {
  const [form] = Form.useForm();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const searchParamsInfo = useSelector(viewSelectors.selectSearchParams);
  const dispatch = useDispatch();

  const onChange = (changedFields, allFields) => {
    dispatch(currentUserActions.changeLogin(allFields[0].value));
  };

  const sendPasswordRecoveryMail = () => {
    form.validateFields().then(() => {
      dispatch(currentUserActions.changeConnectionError(undefined));
      dispatch(currentUserActions.changeConnectionNotice(undefined));
      dispatch(currentUserActions.forgotPassword());
    });
  };

  const goToLogInPage = () => {
    // Flush alerts
    dispatch(currentUserActions.changeConnectionError(undefined));
    dispatch(currentUserActions.changeConnectionNotice(undefined));
    navigate("./login");
  };

  return (
    <ConnectionPage
      form={form}
      subtitle="Mot de passe oublié"
      initialValues={{
        email: currentUser.email || searchParamsInfo.email,
      }}
      onFieldsChange={onChange}
      validateAction={sendPasswordRecoveryMail}
      buttons={
        <>
          <Button type="primary" onClick={sendPasswordRecoveryMail} htmlType="submit">
            Envoyer l'email de récupération
          </Button>
          <Button type="link" onClick={goToLogInPage}>
            Retour à la connexion
          </Button>
        </>
      }>
      <InputElement.Text
        label="Email de récupération"
        name="email"
        size="large"
        placeholder="email"
        rules={[{required: true}, {type: "email"}]}
      />
    </ConnectionPage>
  );
}
