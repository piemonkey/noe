import React, {useEffect, useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import {currentUserSelectors, currentUserActions} from "../../../features/currentUser.js";
import {Button, Form} from "antd";
import {InputElement} from "../InputElement";
import {navigate} from "@reach/router";
import {ConnectionPage, validatePassword} from "../ConnectionPage";
import {fieldToData, ROLES_MAPPING} from "../../../helpers/tableUtilities";
import {viewSelectors} from "../../../features/view";
import {fetchWithMessages} from "../../../helpers/reduxUtilities";
import {currentProjectSelectors} from "../../../features/currentProject";

export function SignUp({envId}) {
  const [form] = Form.useForm();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const searchParamsInfo = useSelector(viewSelectors.selectSearchParams);
  const [invitedRegistration, setInvitedRegistration] = useState();
  const dispatch = useDispatch();

  useEffect(() => {
    if (searchParamsInfo.invitationToken && currentProject._id) {
      fetchWithMessages(
        `projects/${currentProject._id}/registrations/byInvitationToken/${searchParamsInfo.invitationToken}`,
        {noAuthNeeded: true, method: "GET"},
        {},
        false
      )
        .then((invitedRegistration) => {
          setInvitedRegistration(invitedRegistration);
          form.setFieldsValue(invitedRegistration.user);
        })
        .catch(() =>
          dispatch(
            currentUserActions.changeConnectionError(
              "Oups ! Le jeton d'invitation ne semble plus valide."
            )
          )
        );
    }
  }, [currentProject._id]);

  // Don't render if there is an invitation token and we haven't fetched the invited registration yet
  if (searchParamsInfo.invitationToken && !invitedRegistration) return null;

  const onChange = (changedFields, allFields) =>
    dispatch(currentUserActions.changeUser(fieldToData(allFields)));

  const signUp = () =>
    form
      .validateFields()
      .then(() => {
        dispatch(currentUserActions.changeConnectionError(undefined));
        dispatch(currentUserActions.signUp());
      })
      .catch(() => {
        /*do nothing*/
      });

  const goToLogInPage = () => {
    // Flush alerts
    dispatch(currentUserActions.changeConnectionError(undefined));
    navigate("./login");
  };

  const roleLabel = invitedRegistration?.role && (
    <strong>
      {ROLES_MAPPING.find((r) => r.value === invitedRegistration.role).label.toLowerCase()}
    </strong>
  );
  const stewardLabel = invitedRegistration?.steward && <strong>encadrant⋅e</strong>;
  const greetingName =
    invitedRegistration?.user.firstName || invitedRegistration?.steward?.firstName;

  return (
    <ConnectionPage
      form={form}
      envId={envId}
      subtitle={
        invitedRegistration ? (
          <>
            {greetingName && <p>Bonjour {greetingName} !</p>}
            <p>
              Vous avez été invité⋅e en tant{" "}
              {roleLabel ? (
                stewardLabel ? (
                  <>
                    qu'{stewardLabel} et {roleLabel}
                  </>
                ) : (
                  <>que {roleLabel}</>
                )
              ) : (
                <>qu'{stewardLabel}</>
              )}{" "}
              sur l'organisation de l'événement <strong>{currentProject.name}</strong>.
            </p>
            Renseignez un prénom, un nom et un mot de passe pour vous créer un compte.
          </>
        ) : (
          "Nouveau compte"
        )
      }
      initialValues={{
        firstName: currentUser.firstName || searchParamsInfo.firstName,
        lastName: currentUser.lastName || searchParamsInfo.lastName,
        email: currentUser.email || searchParamsInfo.email,
        password: currentUser.password,
      }}
      onFieldsChange={onChange}
      validateAction={signUp}
      buttons={
        <>
          <Button type="primary" onClick={signUp}>
            Créer un compte
          </Button>
          <Button onClick={goToLogInPage}>J'ai déjà un compte</Button>
        </>
      }>
      <InputElement.Text
        label="Prénom"
        name="firstName"
        size="large"
        placeholder="prénom"
        autoComplete="given-name"
        rules={[{required: true}]}
      />
      <InputElement.Text
        label="Nom"
        name="lastName"
        size="large"
        placeholder="nom"
        autoComplete="family-name"
        rules={[{required: true}]}
      />
      <InputElement.Text
        label="Email"
        name="email"
        size="large"
        placeholder="email"
        autoComplete="email"
        disabled={invitedRegistration}
        rules={[{required: true}, {type: "email"}]}
      />
      <InputElement.Password
        label="Mot de passe"
        name="password"
        size="large"
        placeholder="mot de passe"
        autoComplete="new-password"
        rules={[{required: true}, {validator: validatePassword(form)}, {min: 8}]}
      />
    </ConnectionPage>
  );
}
