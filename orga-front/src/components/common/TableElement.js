import React from "react";
import {Button, Table, Tooltip} from "antd";
import {DeleteOutlined, EditOutlined, QuestionCircleOutlined} from "@ant-design/icons";
import {useCopyColumns, useSearchInColumns} from "../../helpers/tableUtilities";
import {CardElement} from "./LayoutElement";
import {navigate} from "@reach/router";

export const TableElement = () => null;

const SimpleTable = ({
  onEdit,
  onDelete,
  navigableRootPath,
  showHeader = false,
  columns: rawColumns,
  selectedRowKeys,
  setSelectedRowKeys,
  rowSelection,
  pagination = false,
  handleDisplayConfigChange: handleDisplayConfigChangeFn,
  ...otherProps
}) => {
  let {columns, handleDisplayConfigChange} = useCopyColumns(
    otherProps.dataSource,
    rawColumns,
    handleDisplayConfigChangeFn
  );
  columns = useSearchInColumns(columns);

  if (onEdit || onDelete) {
    columns.push({
      key: "action",
      width: 15 + (onEdit ? 32 + 15 : 0) + (onDelete ? 32 + 15 : 0),
      render: (text, record) => (
        <div className="containerH buttons-container" style={{justifyContent: "flex-end"}}>
          {onEdit && (
            <Button
              style={{flexGrow: 0}}
              type="link"
              icon={<EditOutlined />}
              onClick={(e) => onEdit(record)}
            />
          )}
          {onDelete && (
            <Button
              style={{flexGrow: 0}}
              danger
              type="link"
              icon={<DeleteOutlined />}
              onClick={(e) => onDelete(record)}
            />
          )}
        </div>
      ),
    });
  }

  let onRowActions;
  if (selectedRowKeys && setSelectedRowKeys && rowSelection) {
    // If all the needed elements for a selection are given, activate the selection by clicking on the row

    const rowKey = otherProps.rowKey;
    const selectRow = (record) => {
      let newSelectedRowKeys;
      if (rowSelection.type === "radio") {
        // If we have radio buttons (able to select only one row)
        newSelectedRowKeys = [record];
      } else {
        // If we have checkboxes (able to select only multiple rows)
        newSelectedRowKeys = [...selectedRowKeys];
        const recordIndex = newSelectedRowKeys.findIndex(
          (el) => (el._id || el.key || el[rowKey]) === (record._id || record.key || record[rowKey])
        );
        if (recordIndex >= 0) {
          newSelectedRowKeys.splice(recordIndex, 1);
        } else {
          newSelectedRowKeys.push(record);
        }
      }

      setSelectedRowKeys(newSelectedRowKeys);
    };

    onRowActions = (record) => ({
      onDoubleClick: () => onEdit && onEdit(record),
      onClick: () => selectRow(record),
    });

    rowSelection.selectedRowKeys = selectedRowKeys.map((el) => el._id || el.key || el[rowKey]);
  } else {
    onRowActions = (record) => ({
      onDoubleClick: () =>
        onEdit
          ? onEdit(record)
          : navigableRootPath && navigate(`${navigableRootPath}/${record._id}`),
    });
  }

  return (
    <Table
      columns={columns}
      scroll={{x: (rawColumns.length - 1) * 160 + 70}}
      onRow={onRowActions}
      showHeader={showHeader}
      pagination={pagination}
      rowSelection={rowSelection}
      rowKey="_id"
      handleDisplayConfigChange={handleDisplayConfigChange}
      {...otherProps}
    />
  );
};
TableElement.Simple = SimpleTable;

TableElement.WithTitle = ({
  onClickButton,
  buttonTitle,
  title,
  subtitle,
  tooltip,
  ...otherProps
}) => (
  <CardElement
    title={
      tooltip ? (
        <>
          {title}{" "}
          <Tooltip title={tooltip}>
            <QuestionCircleOutlined
              style={{color: "rgba(46, 48, 56, 0.6)", cursor: "help", fontSize: 14}}
            />
          </Tooltip>
        </>
      ) : (
        title
      )
    }
    subtitle={subtitle}
    customButtons={
      onClickButton &&
      buttonTitle && (
        <Tooltip title={buttonTitle}>
          <Button onClick={onClickButton}>{buttonTitle.split(" ")[0]}</Button>
        </Tooltip>
      )
    }
    style={{overflow: "hidden"}}
    borderless>
    <TableElement.Simple {...otherProps} style={{...otherProps.style, height: "100%"}} />
  </CardElement>
);
