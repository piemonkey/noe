import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {categoriesActions, categoriesSelectors} from "../../features/categories.js";
import {ListPage} from "../common/ListPage";
import {listSorter} from "../../helpers/listUtilities";
import {ColorDot} from "./CategoryEdit";

export function CategoryList({navigate}) {
  const categories = useSelector(categoriesSelectors.selectList);
  const dispatch = useDispatch();

  const columns = [
    {
      title: "Couleur",
      dataIndex: "color",
      width: "80pt",
      render: (text, record) => <ColorDot color={record.color} />,
    },
    {
      title: "Nom",
      dataIndex: "name",
      sorter: (a, b) => listSorter.text(a.name, b.name),
      searchable: true,
    },
  ];

  // const [user, setUser] = useState({login:'toto',password:'password'});
  useEffect(() => {
    dispatch(categoriesActions.loadList());
  }, []);

  return (
    <ListPage
      title="Catégories"
      buttonTitle="Créer une catégorie"
      elementsActions={categoriesActions}
      navigateFn={navigate}
      columns={columns}
      dataSource={categories}
      groupEditable
      groupImportable
    />
  );
}
