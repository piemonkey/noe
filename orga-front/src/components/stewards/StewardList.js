import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {stewardsActions, stewardsSelectors} from "../../features/stewards.js";
import {ListPage} from "../common/ListPage";
import {listRenderer, listSorter} from "../../helpers/listUtilities";
import {GetPdfPlanningButton} from "../common/EditPage";
import {getRegistrationsLinkedToSteward} from "./StewardEdit";
import {Link} from "@reach/router";
import {personName} from "../../helpers/utilities";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {WaitingInvitationTag} from "../../helpers/registrationsUtilities";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";

export function StewardList({navigate}) {
  const stewards = useSelector(stewardsSelectors.selectList);
  const sessions = useSelector(sessionsSelectors.selectList);
  const registrations = useSelector(registrationsSelectors.selectList);
  const dispatch = useDispatch();

  // Not counting sessions that are desynchronized
  const getNumberOfSessionsForSteward = (steward) =>
    sessions.filter((session) => session.stewards.find((s) => s._id === steward._id)).length;

  const columns = [
    {
      width: 48,
      render: (text, record) => (
        <GetPdfPlanningButton elementsActions={stewardsActions} id={record._id} noText />
      ),
    },
    {
      title: "Prénom",
      dataIndex: "firstName",
      sorter: (a, b) => listSorter.text(a.firstName, b.firstName),
      searchable: true,
    },
    {
      title: "Nom",
      dataIndex: "lastName",
      sorter: (a, b) => listSorter.text(a.lastName, b.lastName),
      searchable: true,
    },
    {
      title: "Participant⋅e lié⋅e",
      dataIndex: "registrationsLinkedToSteward",
      sorter: (a, b) =>
        listSorter.text(
          listRenderer.listOfClickableElements(
            getRegistrationsLinkedToSteward(a, registrations),
            (r) => personName(r.user)
          ),
          listRenderer.listOfClickableElements(
            getRegistrationsLinkedToSteward(b, registrations),
            (r) => personName(r.user)
          )
        ),
      searchable: true,
      searchText: (record) =>
        getRegistrationsLinkedToSteward(record, registrations)
          .map((r) => personName(r.user))
          .join(" "),
      render: (text, record) =>
        listRenderer.listOfClickableElements(
          getRegistrationsLinkedToSteward(record, registrations),
          (el, index) => (
            <Link to={`../participants/${el._id}`} key={index}>
              {el.invitationToken && <WaitingInvitationTag />}
              {personName(el.user)}
            </Link>
          )
        ),
    },
    {
      title: "Email participant⋅e",
      dataIndex: "registrationsLinkedToStewardEmail",
      sorter: (a, b) =>
        listSorter.text(
          listRenderer.listOfClickableElements(
            getRegistrationsLinkedToSteward(a, registrations),
            (r) => r.user.email
          ),
          listRenderer.listOfClickableElements(
            getRegistrationsLinkedToSteward(b, registrations),
            (r) => r.user.email
          )
        ),
      searchable: true,
      searchText: (record) =>
        getRegistrationsLinkedToSteward(record, registrations)
          .map((r) => r.user.email)
          .join(" "),
      render: (text, record) =>
        listRenderer.listOfClickableElements(
          getRegistrationsLinkedToSteward(record, registrations),
          (el, index) => el.user.email
        ),
    },
    {
      title: "Nb. sessions",
      render: (text, record) => getNumberOfSessionsForSteward(record) || "",
      sorter: (a, b) =>
        listSorter.number(getNumberOfSessionsForSteward(a), getNumberOfSessionsForSteward(b)),
      width: 125,
    },
  ];

  useEffect(() => {
    dispatch(stewardsActions.loadList());
    dispatch(registrationsActions.loadList());
    dispatch(sessionsActions.loadList());
  }, []);

  return (
    <ListPage
      title="Encadrant⋅es"
      buttonTitle="Créer un⋅e encadrant⋅e"
      elementsActions={stewardsActions}
      customButtons={
        <GetPdfPlanningButton
          elementsActions={stewardsActions}
          id="all"
          tooltip="Exporter tous les plannings encadrants en une seule fois."
        />
      }
      multipleActionsButtons={({selectedRowKeys}) => (
        <GetPdfPlanningButton
          elementsActions={stewardsActions}
          id={selectedRowKeys}
          noText
          tooltip={`Exporter les plannings des ${selectedRowKeys.length} encadrants en une seule fois.`}
        />
      )}
      navigateFn={navigate}
      columns={columns}
      dataSource={stewards}
      groupEditable
      groupImportable
    />
  );
}
