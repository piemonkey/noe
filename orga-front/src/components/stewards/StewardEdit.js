import React, {useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import {stewardsSelectors, stewardsActions} from "../../features/stewards.js";
import {InputElement} from "../common/InputElement";
import {Availability} from "../utils/Availability.js";
import {EditPage, GetPdfPlanningButton} from "../common/EditPage";
import {currentProjectSelectors} from "../../features/currentProject";
import {Button, List, Popconfirm} from "antd";
import {useLoadEditing, isValidObjectId} from "../../helpers/editingUtilities";
import {Link} from "@reach/router";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {CardElement} from "../common/LayoutElement";
import {personName} from "../../helpers/utilities";
import {useRegistrationEditModal} from "../config/Members";
import {MailOutlined} from "@ant-design/icons";
import {WaitingInvitationTag} from "../../helpers/registrationsUtilities";

export const getRegistrationsLinkedToSteward = (steward, registrations) =>
  registrations?.filter((r) => r.steward?._id === steward._id);

export function StewardEdit({envId, id, location, asModal, modalVisible, setModalVisible}) {
  const dispatch = useDispatch();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const steward = useSelector(stewardsSelectors.selectEditing);
  const registrations = useSelector(registrationsSelectors.selectList);
  const [isModified, setIsModified] = useState(false);
  const registrationsLinkedToSteward = getRegistrationsLinkedToSteward(steward, registrations);
  const [onNewRegistration, _, RegistrationEditModal] = useRegistrationEditModal(
    registrations.filter((r) => r.invitationToken && !r.steward),
    {
      steward: steward._id,
      firstName: steward?.firstName,
      lastName: steward?.lastName,
    }
  );

  const persistLinkedParticipantRegistration = (selectedRegistration) => {
    dispatch(
      registrationsActions.persist({
        _id: registrations.find((r) => r.user._id === selectedRegistration.user._id)?._id || "new", // If no id found, it means it's a new one
        steward: steward?._id,
      })
    );
  };

  const checkPhone = (rule, value) => {
    const acceptedValue = /^((\+|00)\d\d|0)\s*[1-9]([\s.-]*\d{2}){4}$/;
    // Don't raise warning if there is no value: the {required: true} will already raise an error for that
    if (!value || acceptedValue.exec(value)) {
      return Promise.resolve();
    } else {
      return Promise.reject("Merci de rentrer un numéro de téléphone valide");
    }
  };

  const groupEditing = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(
    stewardsActions,
    id,
    () => dispatch(registrationsActions.loadList()),
    clonedElement
  );

  const importProjectAvailabilities = () => {
    dispatch(
      stewardsActions.replaceAllAvailabilitySlots(
        currentProject.availabilitySlots.map((as) => {
          const {_id, ...slot} = as;
          return slot;
        })
      )
    );
    setIsModified(true);
  };

  return (
    <>
      <EditPage
        createTitle="Créer un⋅e encadrant⋅e"
        editTitle="Modifier un⋅e encadrant⋅e"
        groupEditingTitle="Édition groupée d'encadrant⋅es"
        cloningTitle="Cloner des encadrant⋅es"
        clonable
        clonedElement={clonedElement}
        asModal={asModal}
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
        deletable
        elementsActions={stewardsActions}
        customButtons={
          <GetPdfPlanningButton
            elementsActions={stewardsActions}
            tooltip="Si l'encadrant⋅e est lié⋅e à un⋅e participant⋅e, les inscriptions de ce⋅tte participant⋅e seront aussi intégrées dans l'export."
          />
        }
        record={steward}
        initialValues={steward}
        forceModifButtonActivation={isModified}
        outerChildren={
          <Availability
            title="Disponibilités"
            disableDatesIfOutOfProject // We grey out all the dates out of the project scope, but we allow to select outside of them
            setIsModified={setIsModified}
            customButtons={
              <Popconfirm
                title="Les disponibilités existantes seront écrasées."
                okText="Importer quand même"
                okButtonProps={{danger: true}}
                cancelText="Annuler"
                onConfirm={importProjectAvailabilities}>
                <Button type="link">Importer les plages de l'événement</Button>
              </Popconfirm>
            }
            entity={steward}
            actions={stewardsActions}
          />
        }
        groupEditing={groupEditing}>
        <div className={`container-grid ${isValidObjectId(id) ? " two-thirds-one-third" : ""}`}>
          <div className="container-grid">
            <CardElement>
              <div className="container-grid two-per-row">
                <InputElement.Text
                  name="firstName"
                  label="Prénom"
                  placeholder="prénom"
                  rules={[{required: true}]}
                />

                <InputElement.Text name="lastName" label="Nom" placeholder="nom" />

                <InputElement.Text
                  name="phoneNumber"
                  label="Téléphone"
                  placeholder="téléphone"
                  rules={[{validator: checkPhone}]}
                />
              </div>
            </CardElement>
          </div>
          {isValidObjectId(id) && (
            <CardElement greyedOut>
              <InputElement.Custom label="Participant⋅e associé⋅e">
                <div style={{paddingLeft: 10}}>
                  {registrationsLinkedToSteward.length > 0 ? (
                    <List
                      rowKey="_id"
                      dataSource={registrationsLinkedToSteward}
                      renderItem={(r) => (
                        <div>
                          <Link to={`../../participants/${r._id}`}>
                            {r.invitationToken && <WaitingInvitationTag />}
                            {personName(r.user).length > 0 ? personName(r.user) : r.user.email}
                          </Link>
                        </div>
                      )}
                    />
                  ) : (
                    <Button icon={<MailOutlined />} onClick={onNewRegistration}>
                      Inviter un⋅e encadrant⋅e par email
                    </Button>
                  )}
                </div>
              </InputElement.Custom>
            </CardElement>
          )}
        </div>
        <CardElement>
          <div className="container-grid">
            <InputElement.TextArea name="summary" label="Résumé" placeholder="Résumé" />

            <InputElement.Editor
              name="notes"
              label="Notes privées pour les orgas"
              placeholder="notes privées"
              tooltip="Ces notes ne sont pas affichées aux participant⋅es, elles ne sont disponibles que pour les organisateur⋅ices de l'événement"
            />
          </div>
        </CardElement>
      </EditPage>

      <RegistrationEditModal
        persistRegistration={persistLinkedParticipantRegistration}
        addNewRegistrationTitle="Associer l'encadrant⋅e"
      />
    </>
  );
}
