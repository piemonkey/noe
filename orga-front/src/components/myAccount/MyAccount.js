import React, {useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import {currentUserActions} from "../../features/currentUser.js";
import {usersSelectors, usersActions} from "../../features/users.js";
import {Button, Modal, Form, Collapse, Alert, Popconfirm, message} from "antd";
import {InputElement} from "../common/InputElement";
import {EditPage} from "../common/EditPage";
import {checkPasswordsAreSame, validatePassword} from "../common/ConnectionPage";
import {DEFAULT_HEADERS} from "../../helpers/reduxUtilities";
import {EyeInvisibleOutlined} from "@ant-design/icons";
import {CardElement} from "../common/LayoutElement";
import {DisplayOfflineModeFeatureSwitch} from "../../helpers/offlineModeUtilities";

const {Panel} = Collapse;

const checkOldPasswordIsNotTheSameValidator = ({getFieldValue}) => ({
  validator(_, value) {
    const newPasswordGiven = getFieldValue("oldPassword");
    const oldPasswordGiven = value;

    console.log(newPasswordGiven, oldPasswordGiven);

    return newPasswordGiven.length && !oldPasswordGiven.length
      ? Promise.reject(new Error("Vous devez saisir votre ancien mot de passe pour le changer."))
      : newPasswordGiven === oldPasswordGiven
      ? Promise.reject(new Error("Le nouveau mot de passe doit être différent du précédent."))
      : Promise.resolve();
  },
});

export function MyAccount({visibleState}) {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const user = useSelector(usersSelectors.selectEditing);
  const [showModal, setShowModal] = visibleState;

  const onValidation = (formData) => {
    dispatch(usersActions.persist({...user, ...formData}));
    form.resetFields(["oldPassword", "password", "confirmPassword"]);
    return Promise.resolve();
  };

  const DeleteAccountZone = () => {
    const [canDeleteAccount, setCanDeleteAccount] = useState();
    return (
      <CardElement
        style={{borderColor: "red", marginBottom: 0}}
        headStyle={{borderColor: "red"}}
        title={<strong style={{color: "red"}}>Zone de danger</strong>}>
        <Alert
          style={{marginBottom: 26}}
          type="error"
          message="La suppression du compte est irréversible."
          description="Vous serez désinscrit⋅e de tous les événements auxquels vous êtes inscrite et perdrez irrémédiablement vos données personnelles sur NOÉ."
        />

        <InputElement.Text
          style={{marginBottom: 20}}
          label={'Écrivez "Je veux supprimer mon compte" pour pouvoir cliquer sur le bouton :'}
          onChange={(event) =>
            setCanDeleteAccount(event.target.value === "Je veux supprimer mon compte")
          }
          placeholder="Je veux supprimer mon compte"
        />
        <Popconfirm
          visible={canDeleteAccount ? undefined : false}
          title="Êtes-vous sûr⋅e ?"
          onConfirm={() => dispatch(currentUserActions.deleteAccount())}
          okText="Oui, je supprime mon compte"
          okButtonProps={{danger: true}}
          cancelText="Annuler">
          <Button danger disabled={!canDeleteAccount} type="primary">
            Supprimer mon compte
          </Button>
        </Popconfirm>
      </CardElement>
    );
  };

  return (
    <Modal
      visible={showModal}
      width={"min(90vw, 1000pt)"}
      footer={false}
      onCancel={() => setShowModal(false)}>
      <div className="modal-page-content">
        <EditPage
          formOverride={form}
          editTitle="Mon compte"
          navigateAfterValidation={false}
          backButton={false}
          customButtons={
            <Button
              type="primary"
              danger
              onClick={() => {
                dispatch(currentUserActions.logOut());
                setShowModal(false);
                message.success("Déconnexion réussie.");
              }}>
              Se déconnecter
            </Button>
          }
          onValidation={onValidation}
          record={user}
          initialValues={user}>
          <CardElement>
            <div className="container-grid two-per-row">
              <InputElement.Text label="Prénom" name="firstName" placeholder="prénom" />
              <InputElement.Text label="Nom" name="lastName" placeholder="nom" />
              <InputElement.Text label="Email" name="email" placeholder="email" disabled />
            </div>
          </CardElement>

          <Collapse>
            <Panel header="Changer le mot de passe">
              <div className="container-grid">
                <InputElement.Password
                  label="Mot de passe actuel"
                  name="oldPassword"
                  placeholder="mot de passe"
                  autoComplete="current-password"
                  rules={[{required: true}]}
                />

                <div className="container-grid two-per-row">
                  <InputElement.Password
                    label="Nouveau mot de passe"
                    name="password"
                    dependencies={["oldPassword"]}
                    placeholder="mot de passe"
                    autoComplete="new-password"
                    rules={[
                      checkOldPasswordIsNotTheSameValidator,
                      {validator: validatePassword(form)},
                      {min: 8},
                    ]}
                  />

                  <InputElement.Password
                    label="Confirmer le nouveau mot de passe"
                    name="confirmPassword"
                    placeholder="mot de passe"
                    autoComplete="new-password"
                    dependencies={["password"]}
                    rules={[{validator: checkPasswordsAreSame(form)}]}
                  />
                </div>
              </div>
            </Panel>
          </Collapse>

          <Collapse style={{marginTop: 26}}>
            <Panel header="Jeton API">
              <p>
                Vous pouvez utiliser ce jeton pour vous connecter à NOÉ via des API. Il suffit, dans
                les headers de votre requête, d'inclure les élements suivants:
              </p>
              <pre>
                {JSON.stringify(
                  {...DEFAULT_HEADERS, Authorization: `JWT xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx`},
                  null,
                  2
                )}
              </pre>
              <p>
                Cliquez sur l'icône <EyeInvisibleOutlined /> pour le révéler.
              </p>
              <Alert
                style={{marginBottom: 15}}
                type="warning"
                showIcon
                message="Ne donnez ce jeton à personne."
                description={
                  <>
                    Toute personne possédant ce jeton pourra se connecter à votre compte et
                    bénéficier de tous les droits que vous avez, dans tous vos événements. Gardez-le
                    précieusement, à l'abri des regards.
                  </>
                }
              />
              <InputElement.Password
                label="Jeton d'authentification JWT"
                value={localStorage.getItem("token")}
              />
            </Panel>
          </Collapse>

          <Collapse style={{marginTop: 26}}>
            <Panel header="Mode offline">
              <DisplayOfflineModeFeatureSwitch />
            </Panel>
          </Collapse>

          <Collapse style={{marginTop: 26}}>
            <Panel header="Gestion du compte">
              <DeleteAccountZone />
            </Panel>
          </Collapse>
        </EditPage>
      </div>
    </Modal>
  );
}
