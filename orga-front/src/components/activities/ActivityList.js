import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {activitiesActions, activitiesSelectors} from "../../features/activities.js";
import {ListPage} from "../common/ListPage";
import {Tag} from "antd";
import {listRenderer, listSorter} from "../../helpers/listUtilities";
import {currentProjectSelectors} from "../../features/currentProject";
import {Link} from "@reach/router";
import {editableTagsColumn} from "../../helpers/tableUtilities";
import {personName} from "../../helpers/utilities";
import {useColumnsBlacklistingSelector} from "../../helpers/viewUtilities";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";

export function ActivityList({navigate}) {
  const activities = useSelector(activitiesSelectors.selectList);
  const sessions = useSelector(sessionsSelectors.selectList);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelector();
  const dispatch = useDispatch();

  const getNumberOfSessionsForActivity = (activity) =>
    sessions.filter((session) => session.activity._id === activity._id).length;

  const getCategoryTitle = (record) => record.category?.name;
  const columns = [
    {
      title: "Catégorie",
      dataIndex: "category",
      sorter: (a, b) => listSorter.text(getCategoryTitle(a), getCategoryTitle(b)),
      render: (text, record) => (
        <Link to={`../categories/${record.category?._id}`}>
          <Tag
            style={{
              cursor: "pointer",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              overflow: "hidden",
              maxWidth: 125,
            }}
            title={getCategoryTitle(record)}
            color={record.category?.color}>
            {getCategoryTitle(record)}
          </Tag>
        </Link>
      ),
      searchable: true,
      width: 140,
      searchText: getCategoryTitle,
    },
    {
      title: "Nom",
      dataIndex: "name",
      sorter: (a, b) => listSorter.text(a.name, b.name),
      searchable: true,
    },
    {
      title: "Encadrant⋅es éligibles",
      dataIndex: "stewards",
      render: (text, record) =>
        listRenderer.listOfClickableElements(record.stewards, (el, index) => (
          <Link to={`../stewards/${el._id}`} key={index}>
            {personName(el)}
          </Link>
        )),
      sorter: (a, b) =>
        listSorter.text(
          a.stewards.map(personName).join(", "),
          b.stewards.map(personName).join(", ")
        ),
      searchable: true,
      ellipsis: true,
      searchText: (record) => record.stewards.map(personName).join(", "),
    },
    currentProject.usePlaces && {
      title: "Espaces éligibles",
      dataIndex: "places",
      render: (text, record) =>
        listRenderer.listOfClickableElements(record.places, (el, index) => (
          <Link to={`../places/${el._id}`} key={index}>
            {el.name}
          </Link>
        )),
      sorter: (a, b) =>
        listSorter.text(
          a.places.map((el) => el.name).join(", "),
          b.places.map((el) => el.name).join(", ")
        ),
      searchable: true,
      ellipsis: true,
      searchText: (record) => record.places.map((el) => el.name).join(", "),
    },
    editableTagsColumn("tags", "Tags", activitiesActions, activitiesSelectors),
    editableTagsColumn(
      "secondaryCategories",
      "Catégories secondaires",
      activitiesActions,
      activitiesSelectors
    ),
    currentProject.usePlaces &&
      currentProject.useAI && {
        title: "Blocage espaces",
        dataIndex: "nonBlockingActivity",
        sorter: (a, b) => listSorter.number(a.nonBlockingActivity, b.nonBlockingActivity),
        searchable: true,
        render: (text, record) => (record.nonBlockingActivity ? "Ne pas bloquer" : "Bloquer"),
        width: 180,
      },
    {
      title: "Max. participant⋅es",
      dataIndex: "maxNumberOfParticipants",
      sorter: (a, b) => listSorter.number(a.maxNumberOfParticipants, b.maxNumberOfParticipants),
      width: 165,
    },
    {
      title: "Nb. sessions",
      render: (text, record) => getNumberOfSessionsForActivity(record) || "",
      sorter: (a, b) =>
        listSorter.number(getNumberOfSessionsForActivity(a), getNumberOfSessionsForActivity(b)),
      width: 125,
    },
  ].filter((el) => !!el);

  useEffect(() => {
    dispatch(activitiesActions.loadList()).then(() => dispatch(sessionsActions.loadList(false)));
  }, []);

  return (
    <ListPage
      title="Activités"
      buttonTitle="Créer une activité"
      elementsActions={activitiesActions}
      settingsDrawerContent={<ColumnsBlacklistingSelector columns={columns} />}
      navigateFn={navigate}
      columns={filterBlacklistedColumns(columns)}
      dataSource={activities}
      groupEditable
      groupImportable
    />
  );
}
