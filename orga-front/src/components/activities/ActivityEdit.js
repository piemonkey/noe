import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {activitiesActions, activitiesSelectors} from "../../features/activities.js";
import {stewardsActions, stewardsSelectors} from "../../features/stewards.js";
import {placesActions, placesSelectors} from "../../features/places.js";
import {categoriesActions, categoriesSelectors} from "../../features/categories.js";
import {Alert, Button, Collapse, Form, Modal} from "antd";
import {InputElement} from "../common/InputElement";
import {FormElement} from "../common/FormElement";
import {TableElement} from "../common/TableElement";
import {CardElement, LayoutElement} from "../common/LayoutElement";
import {EditPage, useNewElementModal} from "../common/EditPage";
import {listRenderer} from "../../helpers/listUtilities";
import {
  addKeyToItemsOfList,
  columnsPlaces,
  columnsStewards,
  dataToFields,
  fieldToData,
  generateSessionsColumns,
} from "../../helpers/tableUtilities";
import {useLoadEditing} from "../../helpers/editingUtilities";
import {currentProjectSelectors} from "../../features/currentProject";
import {navigate} from "@reach/router";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {CategoryEdit, ColorDot} from "../categories/CategoryEdit";
import {useLocalStorageState} from "../../helpers/localStorageUtilities";
import {StewardEdit} from "../stewards/StewardEdit";
import {PlaceEdit} from "../places/PlaceEdit";

export function ActivityEdit({id, location, asModal, modalVisible, setModalVisible}) {
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const activity = useSelector(activitiesSelectors.selectEditing);
  const stewards = useSelector(stewardsSelectors.selectList);
  const places = useSelector(placesSelectors.selectList);
  const categories = useSelector(categoriesSelectors.selectList);
  const sessions = useSelector(sessionsSelectors.selectList);
  const dispatch = useDispatch();
  const [showModalSteward, setShowModalSteward] = useState(false);
  const [showModalPlace, setShowModalPlace] = useState(false);
  const [showModalSlot, setShowModalSlot] = useState(false);
  const [setShowNewCategoryModal, NewCategoryModal] = useNewElementModal(CategoryEdit);
  const [setShowNewStewardModal, NewStewardModal] = useNewElementModal(StewardEdit);
  const [setShowNewPlaceModal, NewPlaceModal] = useNewElementModal(PlaceEdit);
  const [addingSteward, setAddingSteward] = useState([]);
  const [addingPlace, setAddingPlace] = useState([]);
  const [currentSlot, setCurrentSlot] = useState();
  const [slotForm] = Form.useForm();
  const [isModified, setIsModified] = useState(false);
  const [activitySessionsHelpCollapseKeys, setActivitySessionsHelpCollapseKeys] =
    useLocalStorageState("activitySessionsHelpCollapseKeys", true);

  const activitySessions = sessions.filter((s) => s.activity?._id === activity._id);

  const insufficientNumberOfStewards =
    activity && activity.stewards?.length < activity.minNumberOfStewards;

  const groupEditing = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(
    activitiesActions,
    id,
    () => {
      dispatch(stewardsActions.loadList());
      currentProject.usePlaces && dispatch(placesActions.loadList());
      dispatch(categoriesActions.loadList());
      dispatch(sessionsActions.loadList());
      dispatch(activitiesActions.loadList()); // For tags
    },
    clonedElement
  );

  const stewardsAddable =
    activity.stewards === undefined
      ? []
      : stewards.filter((s) => activity.stewards.filter((as) => as._id === s._id).length === 0);
  const placesAddable =
    activity.places === undefined
      ? []
      : places.filter((p) => activity.places.filter((ap) => ap._id === p._id).length === 0);

  const activitySlotsTable = addKeyToItemsOfList(activity.slots);
  const columnsSlots = [{dataIndex: "duration", render: listRenderer.durationFormat}];

  const rowSelectionSteward = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setAddingSteward(selectedRowObject);
    },
  };
  const rowSelectionPlace = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setAddingPlace(selectedRowObject);
    },
  };

  const addStewards = () => {
    dispatch(activitiesActions.addStewardsToEditing(addingSteward));
    setAddingSteward([]);
    setIsModified(true);
  };
  const addPlaces = () => {
    dispatch(activitiesActions.addPlacesToEditing(addingPlace));
    setAddingPlace([]);
    setIsModified(true);
  };

  const removeSteward = (steward) => {
    dispatch(activitiesActions.removeStewardToEditing(steward));
    setAddingSteward([]);
    setIsModified(true);
  };
  const removePlace = (place) => {
    dispatch(activitiesActions.removePlaceToEditing(place));
    setAddingPlace([]);
    setIsModified(true);
  };

  const removeSlot = (slot) => {
    let slots = [...activitySlotsTable.filter((r) => r.key !== slot.key)];
    dispatch(activitiesActions.changeSlotsToEditing(slots));
    setIsModified(true);
  };

  const onChangeSlot = (changedFields, allFields) =>
    setCurrentSlot({...currentSlot, ...fieldToData(allFields)});

  const onValidateSlot = () => {
    slotForm.validateFields().then(() => {
      let slots;
      if (currentSlot.key === undefined) {
        slots = [...activitySlotsTable, currentSlot];
      } else {
        slots = [...activitySlotsTable];
        slots[slots.findIndex((r) => r.key === currentSlot.key)] = currentSlot;
      }
      dispatch(activitiesActions.changeSlotsToEditing(slots));

      setCurrentSlot(undefined);
      slotForm.resetFields(["duration"]);
      setShowModalSlot(false);
      setIsModified(true);
    });
  };

  const activityFieldToData = (fields) => {
    let data = fieldToData(fields);
    const categoryIndex = categories.map((d) => d._id).indexOf(data.category);
    if (categoryIndex >= 0) {
      data.category = {...categories[categoryIndex]};
    }
    return data;
  };

  return (
    <>
      <EditPage
        createTitle="Créer une activité"
        editTitle="Modifier une activité"
        groupEditingTitle="Édition groupée d'activités"
        cloningTitle="Cloner des activités"
        clonable
        clonedElement={clonedElement}
        asModal={asModal}
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
        deletable
        elementsActions={activitiesActions}
        record={activity}
        initialValues={{...activity, category: activity.category?._id}}
        forceModifButtonActivation={isModified}
        fieldsThatNeedReduxUpdate={["category"]}
        fieldToData={activityFieldToData}
        groupEditing={groupEditing}>
        <CardElement>
          <div className="container-grid three-per-row">
            <InputElement.Text
              label="Nom de l'activité"
              name="name"
              placeholder="nom"
              rules={[
                {required: true},
                {
                  max: 75,
                  message:
                    "Votre nom d'activité semble un petit peu long... Et si on essayait plus concis ?",
                  warningOnly: true,
                },
              ]}
            />

            <InputElement.WithEntityLinks
              endpoint="categories"
              entity={activity.category}
              createButtonText="Créer une nouvelle catégorie"
              setShowNewEntityModal={setShowNewCategoryModal}>
              <InputElement.Select
                label="Catégorie"
                name="category"
                placeholder="catégorie"
                rules={[{required: true}]}
                options={categories.map((m) => ({
                  value: m._id,
                  label: (
                    <div className="containerH" style={{alignItems: "center"}}>
                      <ColorDot
                        color={m.color}
                        size={15}
                        style={{marginBottom: 1, marginRight: 8}}
                      />
                      {m.name}
                    </div>
                  ),
                }))}
                showSearch
              />
            </InputElement.WithEntityLinks>

            <InputElement.TagsSelect
              label="Catégories secondaires"
              name="secondaryCategories"
              elementsSelectors={activitiesSelectors}
            />

            <InputElement.Switch
              label="Autoriser les inscriptions en même temps"
              name="allowSameTime"
            />

            <InputElement.Number
              label="Coefficient de bénévolat de l'activité"
              name="volunteeringCoefficient"
              step={0.1}
              placeholder="coef"
              min={0}
              max={5}
            />

            <InputElement.Number
              label="Coefficient de bénévolat pour les encadrants"
              name="stewardVolunteeringCoefficient"
              step={0.1}
              defaultValue={1.5}
              min={0}
              max={5}
            />

            <InputElement.Number
              label="Nombre maximum de participant⋅es"
              name="maxNumberOfParticipants"
              min={0}
              placeholder="max"
            />
          </div>
        </CardElement>

        <CardElement>
          <div className="container-grid two-thirds-one-third">
            <InputElement.Editor
              label="Description détaillée"
              tooltip={
                'Elle sera affichée seulement dans la vue "pleine page" de l\'activité. ' +
                "C'est ici que vous pouvez rédiger une description plus longue de ce qui attend les participant⋅e.es"
              }
              name="description"
              placeholder="description de l'activité"
            />

            <InputElement.TextArea
              label="Résumé"
              name="summary"
              tooltip="Il sera affiché dans la liste des sessions et devra être très court."
              placeholder="résumé court (idéalement une quinzaine de mots)"
              rules={[
                {
                  max: 250,
                  message: (
                    <>
                      Votre résumé est trop long. ne sera pas affiché de manière optimale dans
                      l'interface utilisateur⋅ice. Considérez le résumé comme un sous-titre court
                      qui décrit en quelques mots ce qu'est l'activité. Si vous voulez donner des
                      détails sur l'activité, utilisez la <strong>Description détaillée</strong>.
                    </>
                  ),
                },
              ]}
            />

            <InputElement.Editor
              label="Notes privées pour les orgas"
              name="notes"
              placeholder="notes privées"
              tooltip="Ces notes ne sont pas affichées aux participant⋅es, elles ne sont disponibles que pour les organisateur⋅ices de l'événement"
            />

            <InputElement.TagsSelect
              label="Tags"
              name="tags"
              elementsSelectors={activitiesSelectors}
            />
          </div>
        </CardElement>

        {currentProject.useAI && (
          <CardElement title="Créer avec l'IA">
            <div className="container-grid three-per-row">
              <InputElement.Number
                label="Nombre de sessions voulues"
                name="numberOfSessions"
                min={1}
                placeholder="sessions"
                rules={[{required: true}]}
              />
              <InputElement.Number
                label="Nombre minimum d'encadrant⋅es"
                name="minNumberOfStewards"
                min={0}
                placeholder="min"
              />
              <InputElement.Switch label="Ne pas bloquer les espaces" name="nonBlockingActivity" />
            </div>
          </CardElement>
        )}

        <Collapse
          style={{marginBottom: 26}}
          onChange={setActivitySessionsHelpCollapseKeys}
          activeKey={currentProject.useAI ? ["sessions-help"] : activitySessionsHelpCollapseKeys}>
          <Collapse.Panel header="Aide à la création des sessions" key="sessions-help">
            <p>
              Ces renseignements vous aideront à créer plus vite des sessions de cette activité. Ils
              sont facultatifs.
              {currentProject.useAI &&
                " Ils sont utilisés également pour la création du planning avec l'IA."}
            </p>

            <div
              style={{marginBottom: -26}}
              className={
                "container-grid " + (currentProject.usePlaces ? "three-per-row" : "two-per-row")
              }>
              <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                <TableElement.WithTitle
                  title="Plages par défaut"
                  buttonTitle="Ajouter une plage"
                  tooltip="Lorsqu'une session de cette activité sera créée, les plages de la session seront pré-remplies avec les durées des plages par défaut."
                  onClickButton={() => {
                    setCurrentSlot(undefined);
                    setShowModalSlot(true);
                  }}
                  onEdit={(record) => {
                    setCurrentSlot(record);
                    setShowModalSlot(true);
                  }}
                  onDelete={removeSlot}
                  columns={columnsSlots}
                  dataSource={activitySlotsTable}
                />
              </div>
              <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                <TableElement.WithTitle
                  title="Encadrant⋅es éligibles"
                  buttonTitle="Ajouter un⋅e encadrant⋅e"
                  subtitle={
                    insufficientNumberOfStewards && (
                      <Alert
                        message="Le nombre d'encadrant⋅es est inférieur au nombre minimal."
                        type="warning"
                        showIcon
                      />
                    )
                  }
                  tooltip="Lorsque vous sélectionnez les encadrant⋅es d'une session de cette activité, les encadrant⋅es éligibles seront pré-sélectionné⋅es pour vous faciliter la manipulation."
                  onClickButton={() => setShowModalSteward(true)}
                  onDelete={removeSteward}
                  columns={columnsStewards}
                  navigableRootPath="../stewards"
                  dataSource={activity.stewards}
                />
              </div>
              {currentProject.usePlaces && (
                <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                  <TableElement.WithTitle
                    title="Espaces éligibles"
                    buttonTitle="Ajouter un espace"
                    tooltip="Lorsque vous sélectionnez les espaces d'une session de cette activité, les espaces éligibles seront pré-sélectionnés pour vous faciliter la manipulation."
                    onClickButton={() => setShowModalPlace(true)}
                    onDelete={removePlace}
                    columns={columnsPlaces}
                    navigableRootPath="../places"
                    dataSource={activity.places}
                  />
                </div>
              )}
            </div>
          </Collapse.Panel>
        </Collapse>

        {activity._id !== "new" && (
          <TableElement.WithTitle
            title="Sessions de l'activité"
            showHeader
            buttonTitle="Créer une session de cette activité"
            onClickButton={() => {
              dispatch(
                sessionsActions.changeEditing({
                  _id: "new",
                  activity,
                  places: [],
                  stewards: [],
                  slots: [],
                })
              );
              navigate("../sessions/new");
            }}
            navigableRootPath="../sessions"
            columns={generateSessionsColumns("../..", currentProject.usePlaces, true, true)}
            dataSource={activitySessions}
          />
        )}
      </EditPage>

      {/* MODALS */}

      <Modal
        title="Éditer une plage"
        visible={showModalSlot}
        onOk={onValidateSlot}
        onCancel={() => {
          slotForm.resetFields(["duration"]);
          setShowModalSlot(false);
          setCurrentSlot(undefined);
        }}>
        <FormElement
          form={slotForm}
          fields={dataToFields(currentSlot)}
          onFieldsChange={onChangeSlot}
          validateAction={onValidateSlot}>
          <div className="container-grid">
            <InputElement.Number
              autoFocus={true}
              label="Durée (minutes)"
              name="duration"
              min={1}
              placeholder="durée"
              rules={[{required: true}, {type: "number"}]}
            />
            {currentSlot?.duration && (
              <div className="fade-in">
                Ce qui représente {listRenderer.durationFormat(currentSlot.duration)}
              </div>
            )}
          </div>
        </FormElement>
      </Modal>

      <LayoutElement.ElementSelectionModal
        title="Ajouter des encadrant⋅es pouvant prendre en charge cette activité"
        visible={showModalSteward}
        customButtons={
          <Button type="link" onClick={() => setShowNewStewardModal(true)}>
            Créer un⋅e encadrant⋅e
          </Button>
        }
        onOk={() => {
          addStewards();
          setShowModalSteward(false);
          setIsModified(true);
        }}
        onCancel={() => setShowModalSteward(false)}
        rowSelection={rowSelectionSteward}
        searchInFields={["firstName", "lastName"]}
        selectedRowKeys={addingSteward}
        setSelectedRowKeys={setAddingSteward}
        columns={columnsStewards}
        dataSource={stewardsAddable}
      />

      <LayoutElement.ElementSelectionModal
        title="Ajouter des espaces où peut se dérouler cette activité"
        visible={showModalPlace}
        customButtons={
          <Button type="link" onClick={() => setShowNewPlaceModal(true)}>
            Créer un espace
          </Button>
        }
        onOk={() => {
          addPlaces();
          setShowModalPlace(false);
          setIsModified(true);
        }}
        onCancel={() => setShowModalPlace(false)}
        rowSelection={rowSelectionPlace}
        searchInFields={["name"]}
        selectedRowKeys={addingPlace}
        setSelectedRowKeys={setAddingPlace}
        columns={columnsPlaces}
        dataSource={placesAddable}
      />

      <NewCategoryModal />
      <NewStewardModal />
      {currentProject.usePlaces && <NewPlaceModal />}
    </>
  );
}
