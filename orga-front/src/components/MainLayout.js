import React from "react";
import {useSelector} from "react-redux";
import {CalendarOutlined, PlaySquareOutlined} from "@ant-design/icons";
import {currentUserSelectors} from "../features/currentUser.js";
import {LayoutElement} from "./common/LayoutElement";
import {LayoutStructure, setAppTheme} from "./common/LayoutStructure";
import {useBrowserTabTitle} from "../helpers/viewUtilities";
import {useRedirectToProjectIfOffline} from "../helpers/offlineModeUtilities";
import {instanceName, URLS} from "../app/configuration";
import {useTranslation} from "react-i18next";

export const MainLayout = ({page, children}) => {
  const {t} = useTranslation();

  const currentUser = useSelector(currentUserSelectors.selectUser);

  useRedirectToProjectIfOffline();

  // ****** TAB TITLE ******

  useBrowserTabTitle(instanceName, page, {projects: "Mes événements"}, "ORGA");

  // ***** RESET STUFF ******

  // On Main layout, reset app theme
  setAppTheme(undefined);

  // ****** SIDE MENU ******

  const menu = {
    top: (
      <LayoutElement.Menu
        selectedItem={page}
        items={[
          // User events
          {label: t("projects:labelMyProjects"), key: "projects", icon: <CalendarOutlined />},
        ]}
      />
    ),
    footer: (
      <LayoutElement.Menu
        selectedItem={page}
        items={[
          // Inscription front
          {
            label: t("common:pagesNavigation.inscriptionFront"),
            key: `${URLS.INSCRIPTION_FRONT}/projects`,
            icon: <PlaySquareOutlined />,
          },
        ]}
      />
    ),
  };

  return (
    <LayoutStructure
      title={instanceName}
      ribbon={t("common:orgaRibbon")}
      menu={menu}
      profileUser={currentUser}>
      {children}
    </LayoutStructure>
  );
};
