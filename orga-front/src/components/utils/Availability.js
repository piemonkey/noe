import {useDispatch} from "react-redux";
import React, {useEffect, useState} from "react";
import {Button, ConfigProvider, Form, Input, message, Modal} from "antd";
import moment from "moment";
import {TableElement} from "../common/TableElement";
import {listRenderer, listSorter} from "../../helpers/listUtilities";
import {FormElement} from "../common/FormElement";
import {InputElement} from "../common/InputElement";
import {CardElement} from "../common/LayoutElement";

export const slotEndIsBeforeBeginning = (slot) =>
  moment(slot.start).toDate() > moment(slot.end).toDate();

export const slotOverlapsAnOtherOne = (slot, availabilitySlots, ignoreThisSlot = undefined) => {
  if (!availabilitySlots) return false;

  for (let existingSlot of availabilitySlots) {
    if (
      ignoreThisSlot &&
      existingSlot.start === ignoreThisSlot.start &&
      existingSlot.end === ignoreThisSlot.end
    ) {
      continue;
    }

    // Check if the slot overlaps an other slot
    if (
      moment(slot.start).toDate() < moment(existingSlot.end).toDate() &&
      moment(slot.end).toDate() > moment(existingSlot.start).toDate()
    ) {
      return true;
    }
  }
  return false;
};

export function Availability({
  entity,
  customButtons,
  defaultPickerValue, // Doesn't work with rangePicker, but hopefully a new version of AntDesign will solve the issue
  disableDatesIfOutOfProject, // True: We grey out all the dates out of the project scope / False: only dates in the past
  blockDisabledDates, // We prevent from choosing grayed out dates
  actions: entityActions,
  setIsModified,
  disabled,
  title,
  subtitle,
}) {
  const dispatch = useDispatch();
  const [generalAddSlotButton, setGeneralAddSlotButton] = useState(false);
  const [disableAddSlotButton, setDisableAddSlotButton] = useState(true);
  const [disableUpdateSlotButton, setDisableUpdateSlotButton] = useState(false);
  const [showModalUpdateSlot, setShowModalUpdateSlot] = useState(false);
  const [currentSlot, setCurrentSlot] = useState();
  const [removedSlot, setRemovedSlot] = useState();
  const [updateSlot, setUpdateSlot] = useState();
  const [form] = Form.useForm();
  const [modalForm] = Form.useForm();

  const notifyChanges = () => setIsModified && setIsModified(true);

  const validateSlot =
    (setDisableButton, ignoreThisSlot = undefined) =>
    (_, value) => {
      // If null, OK
      if (value == null) {
        setDisableButton(true);
        return Promise.resolve();
      }

      const slot = formatSlotFromField(value);

      // Make various checks
      if (slotOverlapsAnOtherOne(slot, entity.availabilitySlots, ignoreThisSlot)) {
        setDisableButton(true);
        return Promise.reject("Cette plage en chevauche une autre.");
      }
      if (slotEndIsBeforeBeginning(slot)) {
        setDisableButton(true);
        return Promise.reject("La date de fin ne peut pas être avant la date de début.");
      }

      // If it passes, it's ok
      setDisableButton(false);
      return Promise.resolve();
    };

  const formatSlotFromField = (fields) => {
    return {
      start: moment(fields[0].toDate()).seconds(0).format(),
      end: moment(fields[1].toDate()).seconds(0).format(),
    };
  };

  useEffect(() => {
    if (currentSlot) {
      dispatch(entityActions.createAvailabilitySlotToEditing(currentSlot));
      form.resetFields();
    }
  }, [currentSlot]);

  useEffect(() => {
    if (removedSlot) {
      dispatch(entityActions.removeAvailabilitySlotToEditing(removedSlot));
    }
  }, [removedSlot]);

  useEffect(() => {
    if (updateSlot) {
      dispatch(entityActions.updateAvailabilitySlotToEditing(updateSlot));
      modalForm.resetFields();
    }
  }, [updateSlot]);

  const updateTimeSlot = () => {
    if (disableUpdateSlotButton) {
      message.error("Oups, la date n'est pas bonne !");
      return;
    }
    let values = modalForm.getFieldsValue().slot;

    if (values !== undefined) {
      let oldSlot = modalForm.getFieldsValue().oldSlot;
      let slot = formatSlotFromField(values);
      setUpdateSlot({newSlot: slot, oldSlot: oldSlot});
    }

    setShowModalUpdateSlot(false);
    notifyChanges();
  };

  const addTimeSlot = () => {
    if (disableAddSlotButton) {
      message.error("Oups, la date n'est pas bonne !");
      return;
    }
    let values = form.getFieldsValue().slot;

    if (values !== undefined) {
      let slot = formatSlotFromField(values);
      setCurrentSlot(slot);
      setDisableAddSlotButton(true);
      notifyChanges();
    }
  };

  const columns = [
    {
      title: "Début – Fin",
      key: "dateRange",
      sorter: (a, b) => listSorter.date(a.start, b.start),
      defaultSortOrder: "ascend",
      render: (text, record, index) =>
        listRenderer.longDateTimeRangeFormat(record.start, record.end),
    },
  ];

  const showTitle = title || !disabled;

  return (
    <>
      <CardElement
        title={showTitle ? title : undefined}
        subtitle={subtitle}
        borderless
        customButtons={
          <>
            {customButtons}
            {!disabled && (
              <FormElement form={form} style={{flexGrow: 1}}>
                <div className="containerH buttons-container">
                  {generalAddSlotButton && (
                    <>
                      <InputElement.DateTimeRange
                        autoFocus
                        formItemProps={{style: {margin: 0}}}
                        name="slot"
                        defaultPickerValue={defaultPickerValue}
                        disableDatesIfOutOfProject={disableDatesIfOutOfProject}
                        blockDisabledDates={blockDisabledDates}
                        rules={[{validator: validateSlot(setDisableAddSlotButton)}]}
                      />

                      <Button onClick={addTimeSlot}>Ajouter</Button>
                    </>
                  )}
                  {entity.availabilitySlots?.length > 0 && !generalAddSlotButton && (
                    <Button onClick={() => setGeneralAddSlotButton(true)}>
                      Ajouter de nouvelles plages horaires
                    </Button>
                  )}
                </div>
              </FormElement>
            )}
          </>
        }>
        <ConfigProvider
          renderEmpty={
            !generalAddSlotButton &&
            !disabled &&
            (() => (
              <Button onClick={() => setGeneralAddSlotButton(true)}>
                Ajouter de nouvelles plages horaires
              </Button>
            ))
          }>
          <TableElement.Simple
            columns={columns}
            rowKey="start"
            scroll={{y: "50vh", x: 400}}
            dataSource={entity.availabilitySlots}
            onEdit={
              !disabled &&
              ((record) => {
                modalForm.setFieldsValue({
                  slot: [moment(record.start), moment(record.end)],
                  oldSlot: record,
                });
                setShowModalUpdateSlot(true);
              })
            }
            onDelete={
              !disabled &&
              ((record) => {
                setRemovedSlot(record);
                notifyChanges();
              })
            }
          />
        </ConfigProvider>
      </CardElement>

      <Modal
        title="Modifier une plage horaire"
        visible={showModalUpdateSlot}
        onOk={updateTimeSlot}
        onCancel={() => setShowModalUpdateSlot(false)}>
        <FormElement form={modalForm}>
          <div
            className="containerH"
            style={{
              flexWrap: "wrap",
            }}>
            <InputElement.DateTimeRange
              label="Plage horaire"
              name="slot"
              defaultPickerValue={defaultPickerValue}
              disableDatesIfOutOfProject={disableDatesIfOutOfProject}
              blockDisabledDates={blockDisabledDates}
              rules={[
                {
                  validator: validateSlot(
                    setDisableUpdateSlotButton,
                    modalForm.getFieldValue("oldSlot")
                  ),
                },
              ]}
            />
            <div style={{display: "none"}}>
              <Form.Item hidden name="oldSlot">
                <Input />
              </Form.Item>
            </div>
          </div>
        </FormElement>
      </Modal>
    </>
  );
}
