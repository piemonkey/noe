import {useDispatch, useSelector} from "react-redux";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import React, {lazy, useEffect, useState} from "react";
import {projectsActions, projectsSelectors} from "../../features/projects";
import {CardElement, ClosableAlert, LayoutElement} from "../common/LayoutElement";
import {Button, Select} from "antd";
import {EditPage} from "../common/EditPage";
import {AreaChartOutlined, ShareAltOutlined} from "@ant-design/icons";
import Paragraph from "antd/es/typography/Paragraph";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {categoriesActions, categoriesSelectors} from "../../features/categories";
import {activitiesActions, activitiesSelectors} from "../../features/activities";
import {placesActions, placesSelectors} from "../../features/places";
import {stewardsActions, stewardsSelectors} from "../../features/stewards";
import {getVolunteeringCoefficient} from "../../helpers/sessionsUtilities";
import {listRenderer} from "../../helpers/listUtilities";
import {InputElement} from "../common/InputElement";
import moment from "moment";
import {InsightCard} from "./InsightCard";
import {groupBy} from "lodash";
import {Link} from "@reach/router";
import {URLS} from "../../app/configuration";
const Pie = lazy(() =>
  import(/* webpackPrefetch: true */ "@ant-design/plots").then((module) => ({
    default: module["Pie"],
  }))
);
const Sunburst = lazy(() =>
  import(/* webpackPrefetch: true */ "@ant-design/plots").then((module) => ({
    default: module["Sunburst"],
  }))
);

const ShareEventButton = () => {
  const project = useSelector(projectsSelectors.selectEditing);
  const [shareLinkEndPoint, setShareLinkEndPoint] = useState("welcome");
  const [displayShareLink, setDisplayShareLink] = useState(false);
  return displayShareLink ? (
    <div className="containerH buttons-container">
      <Select value={shareLinkEndPoint} onChange={setShareLinkEndPoint}>
        <Select.Option value="welcome">Page d'accueil</Select.Option>
        <Select.Option value="login">Page de connexion</Select.Option>
      </Select>
      <Paragraph copyable style={{fontFamily: "monospace"}}>
        {`${URLS.INSCRIPTION_FRONT}/${project._id}/${shareLinkEndPoint}`}
      </Paragraph>
    </div>
  ) : (
    <Button onClick={() => setDisplayShareLink(true)} icon={<ShareAltOutlined />} type="link">
      Partager l'événement
    </Button>
  );
};

export const ChartViz = React.memo(
  ({chart: Chart, config}) => {
    config = Object.assign(
      {interactions: [{type: "element-selected"}, {type: "element-active"}]},
      config
    );
    return (
      <LayoutElement.Suspense>
        <Chart {...config} />
      </LayoutElement.Suspense>
    );
  },
  (pp, np) => JSON.stringify(pp) === JSON.stringify(np)
);

export interface Insight<Element> {
  name: string;
  elements: Array<Element>;
  isPlot?: boolean;
  filter?: (element: Element) => boolean;
  noAvailableData?: boolean;
  calculateOnEach?: (element: Element) => any;
  calculateOnAll?: (elements: Array<Element>) => any;
  statType?: string;
  isGood?: (result: any) => 1 | -1 | 0;
  suffix?: string;
  precisions?: Array<Insight>;
  onClick?: boolean | ((event: MouseEvent) => void);
}

export function Cockpit() {
  const dispatch = useDispatch();
  const project = useSelector(projectsSelectors.selectEditing);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

  const sessions = useSelector(sessionsSelectors.selectList);
  const activities = useSelector(activitiesSelectors.selectList);
  const categories = useSelector(categoriesSelectors.selectList);
  const places = useSelector(placesSelectors.selectList);
  const stewards = useSelector(stewardsSelectors.selectList);
  const registrations = useSelector(registrationsSelectors.selectListWithMetadata);

  const registrationsWithDaysOfPresence = registrations.filter(
    (registration) => registration.availabilitySlots?.length > 0
  );
  const currentUserAloneInTheProject =
    registrations.filter((el) => el._id !== currentRegistration._id).length === 0;

  const volunteeringSessionsWithMetadata = sessions
    .map((el) => ({
      ...el,
      duration: el.slots.reduce((acc, slot) => acc + slot.duration, 0),
      volunteeringCoefficient: getVolunteeringCoefficient(el, activities),
    }))
    .filter((el) => el.volunteeringCoefficient > 0);

  const totalAmountOfVolunteering = volunteeringSessionsWithMetadata.reduce(
    (acc, el) => acc + el.duration * el.computedMaxNumberOfParticipants,
    0
  );

  /**
   * REGISTRATIONS STATS
   */

  const registrationsGroupedByTypeAndRole = Object.entries(
    groupBy(registrations, (el) =>
      el.invitationToken
        ? "Invitations"
        : !el.everythingIsOk
        ? el.booked === false
          ? "Désinscriptions"
          : "Incomplètes"
        : "Finalisées"
    )
  ).map(([name, value]) => ({
    name,
    value: value.length,
    children: Object.entries(groupBy(value, (el) => (el.role ? "Orga" : "Non orga"))).map(
      ([name, value]) => ({name, value: value.length, children: null})
    ),
  }));

  const averageNumberOfDaysOfPresence =
    registrationsWithDaysOfPresence
      .map((el) => el.numberOfDaysOfPresence)
      .reduce((acc, el) => acc + el, 0) / registrationsWithDaysOfPresence.length;

  const registrationsInsights = [
    {
      name: "Inscriptions",
      isPlot: true,
      calculateOnAll: () => ({name: "Inscriptions", children: registrationsGroupedByTypeAndRole}),
      noAvailableData: currentUserAloneInTheProject,
      formatter: (data) => (
        <ChartViz
          chart={Sunburst}
          config={{
            data,
            innerRadius: 0.3,
            label: {formatter: ({value}) => value, autoRotate: false},
            color: (item) => {
              switch (item["ancestor-node"]) {
                case "Finalisées":
                  return "green";
                case "Incomplètes":
                  return "red";
                case "Désinscriptions":
                  return "darkred";
                case "Invitations":
                  return "#86c4ff";
                case "Orga":
                  return "grey";
                case "Non orga":
                  return "darkgrey";
                default:
                  return "grey";
              }
            },
          }}
        />
      ),
      precisions: [
        {
          name: "Finalisées",
          endpoint: "participants",
          noAvailableData: currentUserAloneInTheProject,
          calculateOnAll: () =>
            registrationsGroupedByTypeAndRole.find(({name}) => name === "Finalisées")?.value,
        },
        {
          name: "Incomplètes",
          endpoint: "participants",
          noAvailableData: currentUserAloneInTheProject,
          calculateOnAll: () =>
            registrationsGroupedByTypeAndRole.find(({name}) => name === "Incomplètes")?.value,
        },
        {
          name: "Finalisées + Incomplètes",
          endpoint: "participants",
          elements: registrations,
          filter: (el) => el.booked !== false && !el.invitationToken, // Remove also people who unregistered + with invitation token
          noAvailableData: currentUserAloneInTheProject,
          statType: "length",
        },
      ],
    },
    {
      name: "Durée moyenne du séjour sur place",
      endpoint: "participants",
      noAvailableData: registrationsWithDaysOfPresence.length === 0,
      calculateOnAll: () => averageNumberOfDaysOfPresence,
      suffix: "jours",
    },
    {
      name: "Remplissage moyen des sessions (bénévolat exclu)",
      endpoint: "sessions",
      elements: sessions,
      filter: (el) =>
        el.computedMaxNumberOfParticipants > 0 && getVolunteeringCoefficient(el, activities) === 0,
      calculateOnEach: (el) => (el.numberParticipants / el.computedMaxNumberOfParticipants) * 100,
      statType: "average",
      suffix: "%",
    },
  ];

  /**
   * VOLUNTEERING STATS
   */
  const volunteeringInsights = [
    {
      name: "Remplissage moyen des sessions de bénévolat",
      endpoint: "sessions",
      elements: volunteeringSessionsWithMetadata,
      filter: (el) => el.computedMaxNumberOfParticipants > 0, // only sessions with at least one participant allowed
      calculateOnEach: (el) => (el.numberParticipants / el.computedMaxNumberOfParticipants) * 100,
      statType: "average",
      suffix: "%",
      isGood: (val) => (val > 90 ? 1 : -1),
    },
    {
      name: "Quantité de bénévolat à pourvoir dans l'événement",
      endpoint: "sessions",
      noAvailableData: volunteeringSessionsWithMetadata.length === 0,
      calculateOnAll: () => totalAmountOfVolunteering,
      formatter: (val) => listRenderer.durationFormat(val, true),
      precisions: [
        {
          name: "Déjà pourvu",
          elements: volunteeringSessionsWithMetadata,
          noAvailableData: volunteeringSessionsWithMetadata.length === 0,
          calculateOnEach: (el) => el.duration * el.numberParticipants,
          statType: "sum",
          formatter: (val) => listRenderer.durationFormat(val, true),
        },
        {
          name: "Déjà pourvu (%)",
          elements: volunteeringSessionsWithMetadata,
          noAvailableData: volunteeringSessionsWithMetadata.length === 0,

          calculateOnEach: (el) => el.duration * el.numberParticipants,
          statType: "sum",
          formatter: (val) => Math.round((val / totalAmountOfVolunteering) * 100),
          suffix: "%",
        },
      ],
    },
    {
      name: "Durée moyenne de bénévolat par jour et par participant⋅e (orgas et encadrant.es inclus, pondéré)",
      endpoint: "participants",
      elements: registrations,
      noAvailableData: registrations.filter((el) => el.everythingIsOk).length === 0,
      filter: (el) => el.everythingIsOk,
      calculateOnEach: (el) => el.voluntaryCounter,
      statType: "average",
      formatter: (val) => listRenderer.durationFormat(val, true),
      isGood: (val) =>
        val <= project.minMaxVolunteering[0]
          ? -1 // Red if below
          : val >= project.minMaxVolunteering[1]
          ? 0 // Neutral if above
          : 1, // Green if inside
      suffix: "/ jour",
    },
    {
      name: "Durée optimale de bénévolat par participant⋅e (compte tenu de la quantité de bénévolat de l'événement, et de la durée de séjour moyenne des participant⋅es)",
      endpoint: "participants",
      noAvailableData:
        volunteeringSessionsWithMetadata.length === 0 ||
        registrationsWithDaysOfPresence.length === 0,
      calculateOnAll: () => {
        return (
          totalAmountOfVolunteering /
          (registrationsWithDaysOfPresence.length * averageNumberOfDaysOfPresence)
        );
      },
      formatter: (val) => listRenderer.durationFormat(val, true),
    },
  ];

  /**
   * SCHEDULING STATS
   */
  const schedulingInsights = [
    {
      name: "Catégories",
      isPlot: true,
      elements: categories,
      calculateOnEach: (el) => {
        const numberOfSessions = sessions.filter(
          (session) => session.activity.category._id === el._id
        ).length;
        const numberOfSessionsPercentage = numberOfSessions / sessions.length;
        return {...el, numberOfSessions, numberOfSessionsPercentage};
      },
      calculateOnAll: (elements) => {
        const sorted = elements.sort((a, b) => (a.numberOfSessions > b.numberOfSessions ? -1 : 1));
        return [
          ...sorted.slice(0, 6),
          {
            name: "Autres",
            numberOfSessions: sorted.slice(6).reduce((acc, el) => acc + el.numberOfSessions, 0),
            color: "#555555",
          },
        ];
      },
      noAvailableData: categories.length === 0,
      formatter: (data) => (
        <ChartViz
          chart={Pie}
          config={{
            data,
            radius: 0.85,
            height: 200,
            angleField: "numberOfSessions",
            colorField: "name",
            color: (el) => {
              return data.find((category) => category.name === el.name).color;
            },
          }}
        />
      ),
    },
    {
      name: "Activités",
      endpoint: "activities",
      elements: activities,
      statType: "length",
      precisions: [
        {
          name: "Ayant au moins une session créée",
          elements: activities,
          filter: (el) => sessions.find((session) => session.activity._id === el._id),
          statType: "length",
        },
      ],
    },
    {
      name: "Encadrant⋅es",
      endpoint: "stewards",
      elements: stewards,
      statType: "length",
      precisions: [
        {
          name: "Associé⋅es à un⋅e participant⋅e",
          elements: stewards,
          filter: (el) =>
            registrations.find((registration) => registration.steward?._id === el._id),
          statType: "length",
        },
      ],
    },
    {name: "Sessions", endpoint: "sessions", elements: sessions, statType: "length"},
    {name: "Espaces", endpoint: "places", elements: places, statType: "length"},
  ];

  useEffect(() => {
    Promise.all([
      dispatch(sessionsActions.loadList()),
      dispatch(categoriesActions.loadList()),
      dispatch(activitiesActions.loadList()),
      dispatch(stewardsActions.loadList()),
      project.usePlaces && dispatch(placesActions.loadList()),
      dispatch(registrationsActions.loadList()),
    ]);
  });

  const currentHour = moment().hour();

  return (
    <EditPage
      noSaveButton
      editTitle={
        (currentHour > 19 || currentHour < 4 ? "Bonsoir !" : "Bonjour !") +
        " Bienvenue dans le cockpit."
      }
      elementsActions={projectsActions}
      navigateAfterValidation={false}
      record={project}
      fieldsThatNeedReduxUpdate={["notes"]}
      initialValues={project}
      backButton={false}
      customButtons={<ShareEventButton />}>
      <ClosableAlert
        type="success"
        localStorageKey="cockpit-welcome"
        style={{marginBottom: 26}}
        message="Bienvenue sur l'espace orga de NOÉ !"
        description={
          <>
            Vous êtes sur votre <strong>tableau de bord</strong>. Pour l'instant, il n'y a pas grand
            chose d'affiché, mais au fur et à mesure que vous construirez votre événement, les
            chiffres et graphiques apparaîtront...
          </>
        }
      />
      <div className="container-grid three-per-row">
        <CardElement title="Notes à destination des orgas" readOnly>
          <InputElement.Editor name="notes" placeholder="notes à destination des orgas" />
        </CardElement>
        <CardElement
          className="fade-in"
          title="Inscriptions"
          subtitle={
            <p>
              <Link to="../participants#dashboard">
                <AreaChartOutlined style={{marginRight: 8}} />
                Voir plus de graphiques sur vos données de fréquentation
              </Link>
            </p>
          }>
          {registrationsInsights?.map((insight, index) => (
            <InsightCard key={index} {...insight} />
          ))}
        </CardElement>
        <CardElement className="fade-in" title="Bénévolat">
          {volunteeringInsights?.map((insight, index) => (
            <InsightCard key={index} {...insight} />
          ))}
        </CardElement>
        <CardElement className="fade-in" title="Programmation & Encadrement">
          <div className="container-grid three-per-row">
            {schedulingInsights?.map((insight, index) => (
              <InsightCard key={index} {...insight} />
            ))}
          </div>
        </CardElement>
      </div>
    </EditPage>
  );
}
