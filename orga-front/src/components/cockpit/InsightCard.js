import {Link} from "@reach/router";
import {CardElement} from "../common/LayoutElement";
import {Statistic} from "antd";
import React from "react";
import type {Insight} from "./Cockpit";

const computeInsight = ({
  elements,
  calculateOnEach,
  calculateOnAll,
  filter,
  statType,
  suffix,
  ...displayProps
}: Insight) => {
  let result = elements;
  let noAvailableData = false;

  // Filter
  if (filter) result = elements.filter(filter);

  // Compute on each
  if (calculateOnEach) result = result.map(calculateOnEach);

  // Compute on all
  if (calculateOnAll) result = calculateOnAll(result);

  // Apply any statType of results needed.
  switch (statType) {
    case "length":
      result = result.length;
      break;

    case "sum":
      result = result.reduce((acc, el) => (acc += el), 0);
      break;

    case "average":
      if (result.length > 0) {
        result = result.reduce((a, b) => a + b, 0) / result.length;
      } else {
        noAvailableData = true;
      }
      break;

    case "percentage":
      if (elements.length > 0) {
        result = (result.length / elements.length) * 100;
        suffix = "%";
      } else {
        noAvailableData = true;
      }
      break;

    default:
      break;
  }

  return {result, suffix, noAvailableData, ...displayProps};
};

export const InsightCard = ({
  name,
  endpoint,
  isPlot = false,
  isGood,
  precisions,
  cardProps,
  ...insight
}) => {
  const {result, noAvailableData, ...displayProps} = computeInsight(insight);

  const showLoading = result === undefined || noAvailableData;

  const card = (
    <CardElement className="fade-in" hoverable={endpoint} {...cardProps}>
      {isPlot && !showLoading ? (
        <div className="ant-statistic">
          <div className="ant-statistic-title">{name}</div>
          {displayProps.formatter(result)}
        </div>
      ) : (
        <Statistic
          loading={showLoading}
          title={name}
          value={result}
          valueStyle={{
            color:
              isGood?.(result) === 1 ? "#3f8600" : isGood?.(result) === -1 ? "#cf1322" : undefined,
          }}
          precision={0}
          {...displayProps}
        />
      )}

      {precisions?.length > 0 && (
        <div className="container-grid two-per-row" style={{marginBottom: -10, marginTop: 16}}>
          {precisions.map((precision, index) => (
            <InsightCard
              {...precision}
              key={index}
              cardProps={{size: "small", style: {marginBottom: 10}}}
            />
          ))}
        </div>
      )}
    </CardElement>
  );

  return endpoint ? <Link to={`../${endpoint}`}>{card}</Link> : card;
};
