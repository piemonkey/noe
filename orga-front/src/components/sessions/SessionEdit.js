import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {sessionsActions, sessionsSelectors} from "../../features/sessions.js";
import {stewardsActions, stewardsSelectors} from "../../features/stewards.js";
import {placesActions, placesSelectors} from "../../features/places.js";
import {currentProjectSelectors} from "../../features/currentProject.js";
import {activitiesActions, activitiesSelectors} from "../../features/activities.js";
import {
  addKeyToItemsOfList,
  columnsPlaces,
  columnsStewards,
  dataToFields as baseDataToFields,
  fieldToData,
  generateRegistrationsColumns,
  generateSubscriptionInfo,
  registrationDispoColumn,
} from "../../helpers/tableUtilities";
import {Alert, Button, Checkbox, Collapse, Form, message, Modal, Tooltip} from "antd";
import {InfoCircleOutlined} from "@ant-design/icons";
import moment from "moment";
import {InputElement} from "../common/InputElement";
import {FormElement} from "../common/FormElement";
import {TableElement} from "../common/TableElement";
import {CardElement, LayoutElement} from "../common/LayoutElement";
import {EditPage, useNewElementModal} from "../common/EditPage";
import {useLoadEditing} from "../../helpers/editingUtilities";
import {Link} from "@reach/router";
import {getMaxParticipantsBasedOnPlaces} from "../../helpers/sessionsUtilities";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {listRenderer} from "../../helpers/listUtilities";
import {teamsActions, teamsSelectors} from "../../features/teams";
import {viewSelectors} from "../../features/view";
import {personName, pick} from "../../helpers/utilities";
import {ActivityEdit} from "../activities/ActivityEdit";
import {StewardEdit} from "../stewards/StewardEdit";
import {PlaceEdit} from "../places/PlaceEdit";
import {TeamEdit} from "../teams/TeamEdit";
import {useColumnsBlacklistingSelector} from "../../helpers/viewUtilities";

// Shared accross the two frontends
export const SessionFilling = ({
  computedMaxNumberOfParticipants,
  numberOfParticipants,
  showLabel = true,
  ...otherProps
}) => {
  // cf. getMaxParticipantsBasedOnPlaces() function in backend
  const noMaxNumberOfParticipants = computedMaxNumberOfParticipants === null;
  const noParticipantsAtAll = computedMaxNumberOfParticipants === 0;

  const fillingText =
    numberOfParticipants !== undefined && computedMaxNumberOfParticipants
      ? numberOfParticipants + "/" + computedMaxNumberOfParticipants
      : 0;

  const textIndic =
    (noMaxNumberOfParticipants || noParticipantsAtAll) &&
    `${noMaxNumberOfParticipants ? "Inscription libre" : "Fermé aux participant⋅es"} (${
      numberOfParticipants || 0
    } pers.)`;

  return textIndic ? (
    <InputElement.Text
      label={showLabel ? "Remplissage" : undefined}
      formItemProps={{style: {maxWidth: 250, marginBottom: showLabel ? undefined : 0}}}
      title={textIndic}
      value={`${!showLabel ? (noMaxNumberOfParticipants ? "✔️" : "❌") : textIndic} (${
        numberOfParticipants || 0
      } pers.)`}
      readOnly
    />
  ) : (
    <InputElement.Filling
      formItemProps={{style: {maxWidth: 250, marginBottom: showLabel ? undefined : 0}}}
      label={showLabel ? "Remplissage" : undefined}
      percent={(numberOfParticipants / computedMaxNumberOfParticipants) * 100}
      status={numberOfParticipants > computedMaxNumberOfParticipants ? "exception" : ""}
      text={fillingText}
      {...otherProps}
    />
  );
};

const columnsSlots = [
  {
    dataIndex: "start",
    render: (text, record, index) =>
      listRenderer.longDateTimeRangeFormat(record.start, record.end, true),
  },
  {
    dataIndex: "duration",
    render: listRenderer.durationFormat,
  },
];

export function SessionEdit({id, location, asModal, modalVisible, setModalVisible}) {
  const [filterBlacklistedParticipantsColumns] = useColumnsBlacklistingSelector("participants");
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const session = useSelector(sessionsSelectors.selectEditing);
  const stewards = useSelector(stewardsSelectors.selectList);
  const places = useSelector(placesSelectors.selectList);
  const activities = useSelector(activitiesSelectors.selectList);
  const teams = useSelector(teamsSelectors.selectList);
  const registrations = useSelector(registrationsSelectors.selectList);
  const dispatch = useDispatch();
  const [slotEditingForm] = Form.useForm();
  const [showModalStewards, setShowModalStewards] = useState(false);
  const [addingSteward, setAddingSteward] = useState([]);
  const [showModalPlaces, setShowModalPlaces] = useState(false);
  const [addingPlace, setAddingPlace] = useState([]);
  const [showModalRegistrations, setShowModalRegistrations] = useState(false);
  const [setShowNewActivityModal, NewActivityModal] = useNewElementModal(ActivityEdit);
  const [setShowNewStewardModal, NewStewardModal] = useNewElementModal(StewardEdit);
  const [setShowNewPlaceModal, NewPlaceModal] = useNewElementModal(PlaceEdit);
  const [setShowNewTeamModal, NewTeamModal] = useNewElementModal(TeamEdit);
  const [addingRegistrations, setAddingRegistrations] = useState([]);
  const [showModalSlot, setShowModalSlot] = useState(false);
  const [currentSlot, setCurrentSlot] = useState();
  const [isModified, setIsModified] = useState(false);

  const groupEditing = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(
    sessionsActions,
    id,
    () => {
      dispatch(stewardsActions.loadList());
      dispatch(activitiesActions.loadList());
      currentProject.useTeams && dispatch(teamsActions.loadList());
      currentProject.usePlaces && dispatch(placesActions.loadList());
      dispatch(registrationsActions.loadList());
      dispatch(sessionsActions.loadList()); // For tags
    },
    clonedElement
  );

  const insufficientNumberOfStewards =
    session.activity && session?.stewards?.length < session?.activity?.minNumberOfStewards;

  const sessionMax = session.maxNumberOfParticipants;
  const activityMax = session.activity?.maxNumberOfParticipants;
  const sessionMaxIsDefined = sessionMax !== undefined && sessionMax !== null;
  const maxParticipantsBasedOnPlaces = getMaxParticipantsBasedOnPlaces(session);
  const numberOfParticipantsSetIsAbovePlacesCapacity =
    sessionMax > maxParticipantsBasedOnPlaces || activityMax > maxParticipantsBasedOnPlaces;

  const sessionRegistrationsColumns = [
    ...filterBlacklistedParticipantsColumns(generateRegistrationsColumns(currentProject)),
    registrationDispoColumn,
  ];
  const additionalRegistrationsColumns = [
    {
      title: "Présent⋅e",
      dataIndex: "hasCheckedIn",
      render: (text, record) => {
        const sessionSubscription = record.sessionsSubscriptions.find(
          (ss) => ss.session === session._id
        );

        const checkedInBy =
          sessionSubscription?.hasCheckedIn &&
          registrations.find((r) => r.user._id === sessionSubscription.hasCheckedIn);

        const toggle = (event) => {
          console.log(event.target.checked);
          dispatch(
            registrationsActions.checkInSession(record._id, session._id, event.target.checked)
          );
        };

        return (
          <Tooltip
            title={
              checkedInBy && (
                <>
                  Coché par{" "}
                  <Link
                    to={`../../participants/${checkedInBy._id}`}
                    style={{color: "white", textDecoration: "underline"}}>
                    {personName(checkedInBy.user)}
                  </Link>
                </>
              )
            }>
            <Checkbox checked={sessionSubscription?.hasCheckedIn} onClick={toggle} />
          </Tooltip>
        );
      },
    },
    {
      title: "Inscription",
      render: (text, record) => {
        const sessionSubscription = record.sessionsSubscriptions.find(
          (ss) => ss.session === session._id
        );
        return generateSubscriptionInfo(record, sessionSubscription, registrations, true);
      },
    },
  ];

  const getCommonStuff = (
    entityName,
    entities,
    addingEntity,
    setAddingEntity,
    setShowModalEntities
  ) => {
    const entitiesAddable = () => {
      if (currentSlot && !currentSlot[`${entityName}SessionSynchro`]) {
        if (currentSlot[entityName]) {
          return entities.filter(
            (t) => currentSlot[entityName].filter((el) => el._id === t._id).length === 0
          );
        } else {
          return entities;
        }
      } else {
        if (session[entityName]) {
          return entities.filter(
            (t) => session[entityName].filter((el) => el._id === t._id).length === 0
          );
        } else {
          return entities;
        }
      }
    };

    const entitiesAddableTable = addKeyToItemsOfList(entitiesAddable());
    const sessionEntitiesTable = addKeyToItemsOfList(session[entityName]);

    const currentSlotEntities = () => {
      if (currentSlot && currentSlot[`${entityName}SessionSynchro`]) {
        return sessionEntitiesTable;
      } else if (currentSlot && currentSlot[entityName]) {
        return currentSlot[entityName];
      } else {
        return [];
      }
    };

    const currentSlotEntitiesTable = addKeyToItemsOfList(currentSlotEntities(), "_id");

    const rowSelection = {
      onChange: (selectedRowKeys, selectedRowObject) => {
        setAddingEntity(selectedRowObject);
      },
    };

    const addEntities = () => {
      if (currentSlot) {
        if (currentSlot[entityName]) {
          currentSlot[entityName] = [...currentSlot[entityName], ...addingEntity];
        } else {
          currentSlot[entityName] = [...addingEntity];
        }
      } else {
        dispatch(sessionsActions.addToEditing({payload: addingEntity, field: entityName}));
        dispatch(sessionsActions.checkInconsistencies());
      }
      setAddingEntity([]);
    };

    const removeEntity = (entity) => {
      if (currentSlot) {
        const index = currentSlot[entityName].map((s) => s._id).indexOf(entity._id);
        currentSlot[entityName] = [
          ...currentSlot[entityName].slice(0, index),
          ...currentSlot[entityName].slice(index + 1),
        ];
      } else {
        dispatch(sessionsActions.removeToEditing({payload: entity, field: entityName}));
        dispatch(sessionsActions.checkInconsistencies());
      }

      setAddingEntity([]);
      setIsModified(true);
    };

    const openModalEntities = () => {
      if (session.activity) {
        if (currentSlot && !currentSlot[`${entityName}SessionSynchro`]) {
          const activityEntities = entitiesAddableTable.filter((p) =>
            session.activity[entityName].map((ap) => ap._id).includes(p._id)
          );
          setAddingEntity(activityEntities);
        } else {
          const activityEntities = entitiesAddableTable.filter((p) =>
            session.activity[entityName].map((ap) => ap._id).includes(p._id)
          );
          setAddingEntity(activityEntities);
        }
      }
      setShowModalEntities(true);
    };

    return [
      entitiesAddableTable,
      sessionEntitiesTable,
      currentSlotEntitiesTable,
      rowSelection,
      addEntities,
      removeEntity,
      openModalEntities,
    ];
  };

  // STEWARDS STUFF

  const [
    stewardsAddableTable,
    sessionStewardsTable,
    currentSlotStewardsTable,
    rowSelectionSteward,
    addStewards,
    removeSteward,
    openModalStewards,
  ] = getCommonStuff("stewards", stewards, addingSteward, setAddingSteward, setShowModalStewards);

  // PLACES STUFF

  const [
    placesAddableTable,
    sessionPlacesTable,
    currentSlotPlacesTable,
    rowSelectionPlace,
    addPlaces,
    removePlace,
    openModalPlaces,
  ] = getCommonStuff("places", places, addingPlace, setAddingPlace, setShowModalPlaces);

  // SLOTS STUFF

  const openModalSlots = () => {
    setCurrentSlot({
      duration: undefined,
      start: session.end || session.start,
      end: session.end || session.start,
      placesSessionSynchro: true,
      stewardsSessionSynchro: true,
    });
    setShowModalSlot(true);
  };

  const editSlot = (record) => {
    setCurrentSlot(record);
    setShowModalSlot(true);
  };

  const sessionSlotsTable = addKeyToItemsOfList(session.slots);

  const removeSlot = (key) => {
    dispatch(sessionsActions.removeSlotToEditing(key));
    setIsModified(true);
  };

  const onChangeSlot = (changedFields, allFields) => {
    const newCurrentSlot = {...currentSlot, ...slotFieldToData(allFields)};
    if (newCurrentSlot.start && newCurrentSlot.duration) {
      newCurrentSlot.end = moment(newCurrentSlot.start)
        .add(newCurrentSlot.duration, "minutes")
        .format();
    }
    if (newCurrentSlot.placesSessionSynchro === true) {
      newCurrentSlot.places = session.places;
    }
    if (newCurrentSlot.stewardsSessionSynchro === true) {
      newCurrentSlot.stewards = session.stewards;
    }
    setCurrentSlot(newCurrentSlot);
  };

  const onValidateSlot = () =>
    slotEditingForm.validateFields().then(() => {
      if (currentSlot.key === undefined) {
        dispatch(sessionsActions.createSlotToEditing(currentSlot));
      } else {
        dispatch(sessionsActions.updateSlotToEditing(currentSlot));
      }
      dispatch(sessionsActions.checkInconsistencies());

      setCurrentSlot(undefined);
      setShowModalSlot(false);
      setIsModified(true);
    });

  // REGISTRATIONS STUFF

  const sessionRegistrations = registrations.filter((r) =>
    r.sessionsSubscriptions?.find((ss) => ss.session === session._id)
  );
  const registrationsAddable = registrations.filter(
    (r) => !session.registrations?.find((selected) => selected._id === r._id)
  );
  const rowSelectionRegistration = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setAddingRegistrations(selectedRowObject);
    },
  };

  useEffect(() => {
    if (session._id)
      dispatch(sessionsActions.changeEditing({...session, registrations: sessionRegistrations}));
  }, [JSON.stringify(sessionRegistrations)]);

  const addRegistrations = () => {
    dispatch(
      sessionsActions.changeEditing({
        ...session,
        registrations: [...(session.registrations || []), ...addingRegistrations],
      })
    );
    setAddingRegistrations([]);
    setIsModified(true);
    setShowModalRegistrations(false);
  };
  const removeRegistration = (registration) => {
    dispatch(
      sessionsActions.changeEditing({
        ...session,
        registrations: session.registrations?.filter((r) => r._id !== registration._id),
      })
    );
    setAddingRegistrations([]);
    setIsModified(true);
  };

  // GLOBAL STUFF

  const onValidation = async (formData, fieldsKeysToUpdate) => {
    const payload = {...session, ...formData};

    if (fieldsKeysToUpdate) {
      // If picked slots, then pick also start and end because they go together
      if (fieldsKeysToUpdate.includes("slots")) {
        fieldsKeysToUpdate = [...fieldsKeysToUpdate, "start", "end"];
      }
      const onlyFieldsToUpdate = pick(payload, fieldsKeysToUpdate);

      for (const entityId of groupEditing.elements) {
        await dispatch(sessionsActions.persist({...onlyFieldsToUpdate, _id: entityId}));
      }
    } else {
      if (payload.slots?.length > 0) {
        return dispatch(sessionsActions.persist(payload));
      } else {
        message.error("Vous devez créer au moins une plage pour créer cette session.");
      }
    }
  };

  const sessionFieldToData = (fields) => {
    let data = fieldToData(fields);
    if (data.start) {
      data.start = data.start.format();
    }
    if (data.end) {
      data.end = data.end.format();
    }
    if (typeof data.activity === "string") {
      data.activity = activities.find((activity) => activity._id === data.activity);
    }
    return data;
  };

  const slotFieldToData = (fields) => {
    let data = fieldToData(fields);
    data = {
      ...data,
      key: currentSlot.key,
      project: {_id: currentProject._id},
    };
    if (data.start) {
      data.start = data.start.format();
    }
    if (data.end) {
      data.end = data.end.format();
    }
    return data;
  };

  const dataToFields = (data) =>
    baseDataToFields(data, (clone) => {
      clone.start = moment(clone.start);
      clone.end = clone.end && moment(clone.end);
      clone.activity = clone.activity?._id;
      clone.team = clone.team?._id;
      clone.project = undefined;
    });

  return (
    <>
      <EditPage
        createTitle="Créer une session"
        editTitle="Modifier une session"
        groupEditingTitle="Édition groupée de sessions"
        cloningTitle="Cloner des sessions"
        onValidation={onValidation}
        clonable
        clonedElement={clonedElement}
        asModal={asModal}
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
        deletable
        elementsActions={sessionsActions}
        record={session}
        initialValues={{
          ...session,
          start: session.start && moment(session.start),
          end: session.end && moment(session.end),
          activity: session.activity?._id,
          team: session.team?._id,
        }}
        forceModifButtonActivation={isModified}
        fieldsThatNeedReduxUpdate={["activity", "team", "maxNumberOfParticipants"]}
        fieldToData={sessionFieldToData}
        groupEditing={groupEditing}>
        <div className="container-grid two-thirds-one-third">
          <div className="container-grid">
            <CardElement>
              <div className="container-grid two-per-row">
                <InputElement.WithEntityLinks
                  endpoint="activities"
                  entity={session.activity}
                  createButtonText="Créer une nouvelle activité"
                  setShowNewEntityModal={setShowNewActivityModal}>
                  <InputElement.Select
                    label="Activité"
                    name="activity"
                    options={activities.map((d) => ({value: d._id, label: d.name}))}
                    placeholder="sélectionnez une activité"
                    rules={[{required: true}]}
                    showSearch
                  />
                </InputElement.WithEntityLinks>

                {currentProject.useTeams && (
                  <InputElement.WithEntityLinks
                    endpoint="teams"
                    entity={session.team}
                    setShowNewEntityModal={setShowNewTeamModal}
                    createButtonText="Créer une nouvelle équipe">
                    <InputElement.Select
                      label="Équipe"
                      name="team"
                      placeholder="sélectionnez une équipe"
                      options={[
                        {value: null, label: "- Pas d'équipe -"},
                        ...teams.map((d) => ({
                          value: d._id,
                          label: d.name + (d.activity?.name ? ` (${d.activity?.name})` : ""),
                        })),
                      ]}
                      showSearch
                    />
                  </InputElement.WithEntityLinks>
                )}
                <InputElement.Number
                  label="Coefficient de bénévolat de la session"
                  name="volunteeringCoefficient"
                  step={0.1}
                  placeholder="coef"
                  min={0}
                  max={5}
                />

                <InputElement.Number
                  label="Nombre maximum de participant⋅es"
                  name="maxNumberOfParticipants"
                  min={0}
                  placeholder="max"
                />
              </div>
            </CardElement>

            <CardElement>
              <div className="container-grid two-per-row">
                <InputElement.Editor
                  label="Notes privées pour les orgas"
                  name="notes"
                  placeholder="notes privées"
                  tooltip="Ces notes ne sont pas affichées aux participant⋅es, elles ne sont disponibles que pour les organisateur⋅ices de l'événement"
                />

                <InputElement.TagsSelect
                  label="Tags"
                  name="tags"
                  elementsSelectors={sessionsSelectors}
                />
              </div>
            </CardElement>
          </div>

          <div className="container-grid">
            <CardElement>
              <InputElement.Text label="Nom de la session" name="name" placeholder="nom" />
            </CardElement>

            <CardElement greyedOut>
              <div className="container-grid">
                <InputElement.Text
                  label="Horaires"
                  value={listRenderer.longDateTimeRangeFormat(session.start, session.end)}
                  readOnly
                />

                <SessionFilling
                  computedMaxNumberOfParticipants={session.computedMaxNumberOfParticipants}
                  numberOfParticipants={session.registrations?.length}
                />
              </div>
            </CardElement>

            {currentProject.useAI && (
              <CardElement title="Créer avec l'IA">
                <InputElement.Switch label="Session figée" name="isScheduleFrozen" />
              </CardElement>
            )}
          </div>
        </div>

        <div
          className={
            "container-grid " + (currentProject.usePlaces ? "three-per-row" : "two-per-row")
          }>
          <div className="containerV" style={{flexBasis: "33%"}}>
            <TableElement.WithTitle
              title="Plages"
              buttonTitle="Ajouter une plage"
              onClickButton={openModalSlots}
              columns={columnsSlots}
              onEdit={editSlot}
              onDelete={removeSlot}
              dataSource={sessionSlotsTable}
            />
          </div>

          <div className="containerV" style={{flexBasis: "33%"}}>
            <TableElement.WithTitle
              title="Encadrant⋅es"
              buttonTitle="Ajouter un⋅e encadrant⋅e"
              subtitle={
                insufficientNumberOfStewards && (
                  <Alert
                    message="Le nombre d'encadrant⋅es est inférieur au nombre minimal."
                    type="warning"
                    showIcon
                  />
                )
              }
              onClickButton={openModalStewards}
              columns={columnsStewards}
              navigableRootPath="../stewards"
              onDelete={removeSteward}
              dataSource={sessionStewardsTable}
            />
          </div>

          {currentProject.usePlaces && (
            <div className="containerV" style={{flexBasis: "33%"}}>
              <TableElement.WithTitle
                title="Espaces"
                subtitle={
                  numberOfParticipantsSetIsAbovePlacesCapacity && (
                    <Alert
                      message={`La jauge finale de la session est contrainte par la taille des espaces définis dans la session.
                 La jauge de participants définie dans ${
                   sessionMaxIsDefined ? "la session" : "l'activité"
                 } est contrainte, et la jauge finale sera de ${maxParticipantsBasedOnPlaces}.`}
                      type="warning"
                      showIcon
                    />
                  )
                }
                buttonTitle="Ajouter un espace"
                onClickButton={openModalPlaces}
                onDelete={removePlace}
                columns={columnsPlaces}
                navigableRootPath="../places"
                dataSource={sessionPlacesTable}
              />
            </div>
          )}
        </div>

        <div className="container-grid">
          <div className="containerV" style={{flexBasis: "33%", overflow: "hidden"}}>
            <TableElement.WithTitle
              title="Participant⋅es"
              subtitle="En rouge clair figurent les participant⋅es qui ne sont pas encore arrivé⋅es à l'événement."
              showHeader
              buttonTitle="Ajouter un⋅e participant⋅e"
              onClickButton={() => setShowModalRegistrations(true)}
              onDelete={removeRegistration}
              rowClassName={(record) =>
                record.hasCheckedIn ? "" : " ant-table-row-danger ant-table-row-italic"
              }
              navigableRootPath="../participants"
              columns={[...sessionRegistrationsColumns, ...additionalRegistrationsColumns]}
              dataSource={addKeyToItemsOfList(session.registrations)}
            />
          </div>
          <LayoutElement.ElementSelectionModal
            title="Ajouter des participant⋅es à la session"
            visible={showModalRegistrations}
            large
            onOk={addRegistrations}
            onCancel={() => setShowModalRegistrations(false)}
            searchInFields={(registration) => [
              registration.user.firstName,
              registration.user.lastName,
              ...registration.tags,
            ]}
            rowSelection={rowSelectionRegistration}
            selectedRowKeys={addingRegistrations}
            setSelectedRowKeys={setAddingRegistrations}
            columns={sessionRegistrationsColumns}
            dataSource={addKeyToItemsOfList(registrationsAddable)}
          />
        </div>
      </EditPage>

      <LayoutElement.ElementSelectionModal
        title="Ajouter des encadrant⋅es en charge de cette session"
        visible={showModalStewards}
        subtitle={
          session.activity?.stewards?.length > 0 ? (
            <>
              <InfoCircleOutlined /> Les encadrant⋅es éligibles de l'activité ont été
              pré-sélectionné⋅es pour plus de facilités.
            </>
          ) : undefined
        }
        customButtons={
          <Button type="link" onClick={() => setShowNewStewardModal(true)}>
            Créer un⋅e encadrant⋅e
          </Button>
        }
        onOk={() => {
          addStewards();
          setShowModalStewards(false);
          setIsModified(true);
        }}
        onCancel={() => setShowModalStewards(false)}
        searchInFields={["firstName", "lastName"]}
        rowSelection={rowSelectionSteward}
        selectedRowKeys={addingSteward}
        setSelectedRowKeys={setAddingSteward}
        columns={columnsStewards}
        dataSource={stewardsAddableTable}
      />
      <LayoutElement.ElementSelectionModal
        title="Ajouter les espaces utilisés pour cette session"
        visible={showModalPlaces}
        subtitle={
          session.activity?.places?.length > 0 ? (
            <>
              <InfoCircleOutlined /> Les espaces éligibles de l'activité ont été pré-sélectionnés
              pour plus de facilités.
            </>
          ) : undefined
        }
        customButtons={
          <Button type="link" onClick={() => setShowNewPlaceModal(true)}>
            Créer un espace
          </Button>
        }
        onOk={() => {
          addPlaces();
          setShowModalPlaces(false);
          setIsModified(true);
        }}
        onCancel={() => setShowModalPlaces(false)}
        searchInFields={["name"]}
        rowSelection={rowSelectionPlace}
        selectedRowKeys={addingPlace}
        setSelectedRowKeys={setAddingPlace}
        columns={columnsPlaces}
        dataSource={placesAddableTable}
      />
      <Modal
        title="Éditer une plage"
        visible={showModalSlot}
        onOk={onValidateSlot}
        onCancel={() => {
          setCurrentSlot(undefined);
          setShowModalSlot(false);
        }}
        width={"min(90vw, 1200px)"}
        zIndex={100} /* cause otherwise the ElementsSelectionModals are behind it */
      >
        <FormElement
          form={slotEditingForm}
          fields={dataToFields(currentSlot)}
          onFieldsChange={onChangeSlot}
          validateAction={onValidateSlot}>
          <div className="container-grid two-thirds-one-third">
            <CardElement>
              <div className="container-grid two-per-row">
                <InputElement.DateTime
                  disableDatesIfOutOfProject // We grey out all the dates out of the project scope, but we allow to select outside of them
                  label="Début"
                  name="start"
                  rules={[{required: true}]}
                />

                <div>
                  <InputElement.Number
                    label="Durée (minutes)"
                    name="duration"
                    min="1"
                    placeholder="durée"
                    rules={[{required: true}, {type: "number"}]}
                  />
                  {currentSlot?.duration && (
                    <div className="fade-in">
                      Ce qui représente {listRenderer.durationFormat(currentSlot.duration)}
                    </div>
                  )}
                </div>
              </div>
            </CardElement>
            <InputElement.Custom label="Récapitulatif des horaires">
              {currentSlot?.start && currentSlot?.end && currentSlot?.duration ? (
                listRenderer.longDateTimeRangeFormat(currentSlot.start, currentSlot.end)
              ) : (
                <div style={{color: "grey"}}>
                  Veuillez renseigner une date de début et une durée.
                </div>
              )}{" "}
            </InputElement.Custom>
          </div>

          <Collapse
            defaultActiveKey={
              !currentSlot?.stewardsSessionSynchro || !currentSlot?.placesSessionSynchro
                ? ["1"]
                : []
            }>
            <Collapse.Panel header="Synchronisation des encadrant⋅es et espaces (avancé)" key="1">
              <div className="container-grid two-per-row">
                <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                  <InputElement.Switch
                    label="Synchroniser les encadrant⋅es"
                    name="stewardsSessionSynchro"
                  />
                  {!currentSlot?.stewardsSessionSynchro && (
                    <TableElement.WithTitle
                      title="Encadrant⋅es"
                      buttonTitle="Ajouter"
                      onClickButton={
                        currentSlot && !currentSlot.stewardsSessionSynchro && openModalStewards
                      }
                      columns={columnsStewards}
                      onDelete={currentSlot && !currentSlot.stewardsSessionSynchro && removeSteward}
                      dataSource={currentSlotStewardsTable}
                    />
                  )}
                </div>

                <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                  <InputElement.Switch
                    label="Synchroniser les espaces"
                    name="placesSessionSynchro"
                  />
                  {!currentSlot?.placesSessionSynchro && (
                    <TableElement.WithTitle
                      title="Espaces"
                      buttonTitle="Ajouter"
                      onClickButton={
                        currentSlot && !currentSlot.placesSessionSynchro && openModalPlaces
                      }
                      columns={columnsPlaces}
                      onDelete={currentSlot && !currentSlot.placesSessionSynchro && removePlace}
                      dataSource={currentSlotPlacesTable}
                    />
                  )}
                </div>
              </div>
            </Collapse.Panel>
          </Collapse>
        </FormElement>
      </Modal>

      <NewActivityModal />
      <NewStewardModal />
      {currentProject.usePlaces && <NewPlaceModal />}
      {currentProject.useTeams && <NewTeamModal />}
    </>
  );
}
