import {Card, Tag} from "antd";
import React from "react";
import Paragraph from "antd/es/typography/Paragraph";
import {listRenderer} from "../../helpers/listUtilities";
import {getVolunteeringCoefficient} from "../../helpers/sessionsUtilities";
import {SessionFilling} from "./SessionEdit";
import {getSessionName} from "../../helpers/agendaUtilities";
import {EnvironmentOutlined, TeamOutlined, UserOutlined} from "@ant-design/icons";
import {DeleteButton} from "../common/EditPage";
import {personName} from "../../helpers/utilities";

const CategoryTagWithVolunteeringMajoration = ({
  volunteeringCoefficient,
  category,
  children,
  ...props
}) => {
  let volunteeringMajoration;
  if (volunteeringCoefficient > 0 && volunteeringCoefficient !== 1) {
    const percent = Math.round((volunteeringCoefficient - 1) * 100);
    volunteeringMajoration = `${Math.sign(percent) === 1 ? "+" : "-"}${Math.abs(percent)}%`;
  }

  return children || volunteeringMajoration ? (
    <Tag
      title={
        volunteeringMajoration
          ? `Ce créneau de bénévolat compte ${volunteeringMajoration} !`
          : undefined
      }
      color={category.color}
      {...props}>
      {children}
      {volunteeringMajoration && <strong style={{fontSize: 15}}> {volunteeringMajoration}</strong>}
    </Tag>
  ) : null;
};

export function SessionShowSmall({session, style, onClick, withMargins, registrations, onDelete}) {
  const volunteeringCoefficient = getVolunteeringCoefficient(session);
  const stewardsNames = session.stewards?.map(personName);
  const placesNames = session.places?.map((p) => p.name);
  const registrationsNames = registrations
    ?.filter((r) => r.sessionsSubscriptions.find((ss) => ss.session === session._id))
    .map((r) => personName(r.user));

  const elementsListing = (Icon, elements) =>
    elements.length > 0 && (
      <div className="containerH" style={{marginTop: 6}}>
        <Icon style={{marginTop: 3, marginRight: 5}} />
        <div className="containerV">
          {elements?.map((name) => (
            <div key={name}>{name}</div>
          ))}
        </div>
      </div>
    );

  return (
    <div
      className="containerV"
      style={{height: "100%", flexGrow: 1, padding: withMargins && "15px 15px"}}>
      <div style={{position: "relative", zIndex: 1}}>
        <CategoryTagWithVolunteeringMajoration
          volunteeringCoefficient={volunteeringCoefficient}
          category={session.activity?.category}
          style={{
            position: "absolute",
            top: 4,
            right: -4,
          }}
        />
      </div>
      <Card
        title={
          <div style={{textAlign: "center"}}>
            <div
              style={{
                // Allow activity name to go on 3 lines not more
                whiteSpace: "normal",
                fontWeight: "bold",
                overflow: "hidden",
                display: "-webkit-box",
                WebkitLineClamp: "3",
                WebkitBoxOrient: "vertical",
              }}>
              {getSessionName(session)}
            </div>
            <CategoryTagWithVolunteeringMajoration
              category={session.activity?.category}
              style={{
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
                overflow: "hidden",
                maxWidth: 300,
                margin: "-5px 0 -10px 0",
                opacity: 0.85,
              }}>
              {session.activity?.category.name}
            </CategoryTagWithVolunteeringMajoration>
          </div>
        }
        style={{
          transition: "opacity 0.3s ease-in-out",
          border: volunteeringCoefficient > 0 && `2px solid ${session.activity.category.color}`,
          overflow: "hidden",
          ...style,
        }}
        headStyle={{
          background: session.activity?.category.color,
          color: "white",
          borderRadius: 0,
        }}
        onClick={onClick}>
        <div style={{textAlign: "center", marginBottom: 15}}>
          {listRenderer.longDateTimeRangeFormat(session.start, session.end)}
        </div>
        {session.activity?.summary && (
          <Paragraph ellipsis={{rows: 2}} style={{color: "grey"}}>
            {session.activity.summary}
          </Paragraph>
        )}
        <div className="containerH buttons-container" style={{flexWrap: "nowrap"}}>
          <strong style={{flexGrow: 0, marginBottom: 15}}>Remplissage:</strong>
          <SessionFilling
            showLabel={false}
            computedMaxNumberOfParticipants={session.computedMaxNumberOfParticipants}
            numberOfParticipants={session.numberParticipants}
          />
        </div>
        <div
          className="containerH"
          style={{
            alignItems: "baseline",
            flexWrap: "wrap",
            margin: 5,
            paddingBottom: 7,
            rowGap: 8,
            justifyContent: "center",
          }}>
          {session.activity?.secondaryCategories?.map((tagName, index) => (
            <Tag key={index}>{tagName}</Tag>
          ))}
        </div>
        {elementsListing(EnvironmentOutlined, placesNames)}
        {elementsListing(UserOutlined, stewardsNames)}
        {elementsListing(TeamOutlined, registrationsNames)}
      </Card>
      <div style={{textAlign: "center", marginTop: 10}}>
        <DeleteButton block onConfirm={onDelete} />
      </div>
    </div>
  );
}
