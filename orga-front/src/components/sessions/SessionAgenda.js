import React, {useCallback, useEffect, useMemo, useState} from "react";
import moment from "moment";
import {navigate} from "@reach/router";
import {useDispatch, useSelector} from "react-redux";
import {Button, Drawer, Dropdown, Menu, message, Popover, Tooltip} from "antd";
import {CalendarOutlined, CloseOutlined, DownOutlined} from "@ant-design/icons";
import {
  EditingState,
  GroupingState,
  IntegratedEditing,
  IntegratedGrouping,
  ViewState,
} from "@devexpress/dx-react-scheduler";
import {
  AppointmentForm,
  Appointments,
  CurrentTimeIndicator,
  DateNavigator,
  DayView,
  DragDropProvider,
  GroupingPanel,
  Resources,
  Scheduler,
  Toolbar,
} from "@devexpress/dx-react-scheduler-material-ui";
import {SessionShowSmall} from "./SessionShow";
import {LayoutElement} from "../common/LayoutElement";
import {InputElement} from "../common/InputElement";
import {sessionsActions, sessionsSelectors} from "../../features/sessions.js";
import {projectsActions, projectsSelectors} from "../../features/projects.js";
import {placesActions, placesSelectors} from "../../features/places.js";
import {stewardsActions, stewardsSelectors} from "../../features/stewards";
import {categoriesActions, categoriesSelectors} from "../../features/categories";
import {slotOverlapsAnOtherOne} from "../utils/Availability";
import {navbarHeight, useDetectHotKey, useWindowDimensions} from "../../helpers/viewUtilities";
import {
  AgendaControls,
  AgendaSettingsDrawer,
  CELL_DURATION_MINUTES,
  DEFAULT_AGENDA_PARAMS,
  displayLoadingMessage,
  filterAppointments,
  getAgendaDisplayParams,
  getNumberOfDaysToDisplay,
  getTimeScaleLabelComponent,
  midnightDateManager,
  NavButton,
  RECOMMENDED_LIMIT_OF_RESOURCE_COLUMNS,
  ResourceFilterSelector,
  synchronizeTicksOnSideOfAgenda,
  useInitialScrollToProjectAvailabilities,
  useKillLoadingMessage,
} from "../../helpers/agendaUtilities";
import {viewActions, viewSelectors} from "../../features/view";
import {EnvironmentOutlined, ReloadOutlined, TeamOutlined, UserOutlined} from "@ant-design/icons";
import {teamsActions} from "../../features/teams";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {listRenderer} from "../../helpers/listUtilities";
import {personName} from "../../helpers/utilities";
import {CustomErrorBoundary} from "../common/LayoutStructure";
import {currentProjectSelectors} from "../../features/currentProject";

const resourcesMapping = {
  places: "placeId",
  stewards: "stewardId",
  category: "categoryId",
};

export function SessionAgenda() {
  const dispatch = useDispatch();

  const {isMobileView, windowWidth, windowHeight} = useWindowDimensions();

  const cloningFeatureActivated = useDetectHotKey("mod+shift");
  const [currentSessionPreview, setCurrentSessionPreview] = useState();
  const schedulerHeight = windowHeight - navbarHeight();

  // *********************** //
  // **** DATA FETCHING **** //
  // *********************** //

  const allSlots = useSelector(sessionsSelectors.selectSlotsListForAgenda);
  const places = useSelector(placesSelectors.selectList);
  const stewards = useSelector(stewardsSelectors.selectList);
  const categories = useSelector(categoriesSelectors.selectList);
  const registrations = useSelector(registrationsSelectors.selectList);

  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const agendaParams = useSelector(viewSelectors.selectAgendaParams);

  useEffect(() => {
    // Load crucial resources first (sessions + project that is blocking places and teams)
    Promise.all([dispatch(sessionsActions.loadList())]).then(() =>
      Promise.all([
        dispatch(stewardsActions.loadList()),
        dispatch(categoriesActions.loadList()),
        dispatch(registrationsActions.loadList()),
      ])
    );
  }, [currentProject._id]);

  // Load assets that depend on the project
  useEffect(() => {
    if (!currentProject._id) return;
    currentProject.usePlaces && dispatch(placesActions.loadList());
    currentProject.useTeams && dispatch(teamsActions.loadList());
  }, [currentProject]);

  // ************************ //
  // ******** STATES ******** //
  // ************************ //

  const {
    // Set the cell display height layout
    cellDisplayHeight,
    // Start date for the agenda display
    currentAgendaDate,
    // Place Events on top of each other or next to each other
    slotsOnEachOther,

    // Group by day then by resource, or the opposite
    groupByDaysFirst,

    // Default slot duration when creating a session from the agenda view
    defaultNewSlotDuration,

    // Show the resources availabilities on the agenda or not
    showResourcesAvailabilities,

    // Show or not info on the cards in the agenda view
    showResourcesListingOnAgendaCards,

    // Selected resources to make the groupings. Initial value set asynchronously in useEffect
    selectedResources,
    // Filters for resources and appointments
    resourcesFilterSelections,

    // Categories filter
    categoriesFilter,
  } = agendaParams;

  const resourcesListingsToDisplay = {
    places: showResourcesListingOnAgendaCards.includes("places"),
    stewards: showResourcesListingOnAgendaCards.includes("stewards"),
    registrations: showResourcesListingOnAgendaCards.includes("registrations"),
    maxNumberOfParticipants: showResourcesListingOnAgendaCards.includes("maxNumberOfParticipants"),
  };

  // Number of days to display. Will be set asynchronously in useEffect depending on the number of places and other params
  const numberOfDaysDisplayed =
    agendaParams?.numberOfDaysDisplayed || getNumberOfDaysToDisplay(windowWidth, currentProject);

  const [searchBarValue, setSearchBarValue] = useState("");

  const setAgendaParams = (params) =>
    dispatch(viewActions.changeAgendaParams({projectId: currentProject._id, params}));

  // ************************ //
  // **** MEMOIZED DATA **** //
  // ************************ //
  // everything that can easily be computed only once //

  const agendaDisplayParams = useMemo(
    () => getAgendaDisplayParams(allSlots, currentProject),
    [allSlots, currentProject]
  );

  // The appointments (ie. the slots from all the sessions, formatted in the way that the Agenda accepts it
  const filteredAppointments = useMemo(
    () =>
      filterAppointments(allSlots, searchBarValue, categoriesFilter, registrations)?.map(
        midnightDateManager.dataToDisplay
      ),
    [allSlots, places, searchBarValue, categoriesFilter]
  );

  const resources = useMemo(
    () =>
      [
        currentProject.usePlaces && {
          fieldName: "placeId",
          resourceName: "places",
          title: "Espaces",
          instances: places.map((place) => ({
            text: place.name,
            id: place._id,
            availabilitySlots: place.availabilitySlots,
            color: "transparent",
          })),
          allowMultiple: true,
          hasAvailabilities: true,
        },
        {
          fieldName: "stewardId",
          resourceName: "stewards",
          title: "Encadrant⋅es",
          instances: stewards.map((steward) => ({
            text: personName(steward),
            id: steward._id,
            availabilitySlots: steward.availabilitySlots,
          })),
          allowMultiple: true,
          hasAvailabilities: true,
        },
        {
          fieldName: "categoryId",
          resourceName: "category",
          title: "Catégories",
          instances: categories.map((category) => ({
            text: category.name,
            id: category._id,
            color: category.color, // Color given to the appointment slots on the page
          })),
          allowMultiple: false,
        },
      ].filter((el) => !!el),
    [places, categories, stewards]
  );

  // ************************* //
  // ***** ASYNC EFFECTS ***** //
  // ************************* //

  // APPEARANCE TWEAKS

  useInitialScrollToProjectAvailabilities(agendaDisplayParams, cellDisplayHeight);
  useEffect(
    () => synchronizeTicksOnSideOfAgenda("timescale-cell", cellDisplayHeight),
    [cellDisplayHeight, agendaDisplayParams, currentAgendaDate]
  );
  useKillLoadingMessage();

  // ************************* //
  // ** APPOINTMENTS BLOCKS ** //
  // ************************* //

  // Appointment popover when we hover the block
  const SessionPreview = ({session, fullWidth}) => (
    <SessionShowSmall
      onClick={() => navigate(session._id)}
      style={{width: fullWidth ? "100%" : 350}}
      session={session}
      registrations={registrations}
      onDelete={() => {
        dispatch(sessionsActions.remove(session._id));
        setCurrentSessionPreview(undefined);
      }}
    />
  );

  // Generic appointment component
  const CustomAppointment = useCallback(
    React.memo(
      ({style, children, component: Component, opacity = 1, data, ...restProps}) => {
        return (
          // Component variable must have an uppercase letter to be considered as a component by React
          <Component
            {...restProps}
            data={{...data}}
            className={restProps.className}
            style={{...style, color: "white", backgroundColor: data.color, opacity: opacity}}>
            <div style={{marginLeft: 8, marginTop: 5}}>
              <div
                style={{
                  lineHeight: 1.5,
                  whiteSpace: "normal",
                  overflow: "hidden",
                  display: "-webkit-box",
                  WebkitLineClamp: "3",
                  WebkitBoxOrient: "vertical",
                  fontWeight: "bold",
                }}>
                {data.title}
              </div>
              <div style={{overflow: "hidden", whiteSpace: "nowrap", paddingTop: 1}}>
                {listRenderer.timeRangeFormat(data.start, data.end)}
              </div>
              <div
                style={{marginTop: 4, lineHeight: 1.2, opacity: 0.8, gap: 5}}
                className="containerV">
                {currentProject.usePlaces &&
                  resourcesListingsToDisplay.places &&
                  data?.location.length > 0 && (
                    <div>
                      <EnvironmentOutlined /> {data?.location}
                    </div>
                  )}
                {resourcesListingsToDisplay.stewards && data?.stewardsNames.length > 0 && (
                  <div>
                    <UserOutlined /> {data?.stewardsNames}
                  </div>
                )}
                {((resourcesListingsToDisplay.registrations &&
                  data?.participantsNames.length > 0) ||
                  resourcesListingsToDisplay.maxNumberOfParticipants) && (
                  <div>
                    <TeamOutlined />{" "}
                    {resourcesListingsToDisplay.maxNumberOfParticipants &&
                      (data?.session.computedMaxNumberOfParticipants === null
                        ? `${data?.session.numberParticipants}`
                        : data?.session.computedMaxNumberOfParticipants === 0
                        ? `✖${data?.session.numberParticipants}`
                        : `${data?.session.numberParticipants}/${data?.session.computedMaxNumberOfParticipants}`)}{" "}
                    {resourcesListingsToDisplay.registrations && data?.participantsNames}
                  </div>
                )}
              </div>
            </div>
          </Component>
        );
      },
      (prevProps, nextProps) => JSON.stringify(prevProps.data) === JSON.stringify(nextProps.data)
    ),
    [currentProject, JSON.stringify(resourcesListingsToDisplay)]
  );

  // Renders the appointment block in the agenda view.
  // useCallback(React.memo()) helps avoiding approx 300% of re-rendering on start and 30% after
  const Appointment = useCallback(
    React.memo(
      (props) => {
        const appointmentComp = (
          <CustomAppointment
            component={Appointments.Appointment}
            onClick={isMobileView && (() => setCurrentSessionPreview(props.data.session))}
            {...props}
          />
        );

        return isMobileView ? (
          appointmentComp
        ) : (
          // zIndex=100 to put the popover above the Sidebar, but below the SessionShow modal
          <Popover
            zIndex={100}
            content={<SessionPreview session={props.data.session} />}
            mouseEnterDelay={0.6}>
            {appointmentComp}
          </Popover>
        );
      },
      (prevProps, nextProps) =>
        JSON.stringify(prevProps.data.session) === JSON.stringify(nextProps.data.session)
    ),
    // Don't put the currentProject as a dependency cause it's not necessary and causes a whole re-render for
    // nothing on each session subscribe/unsubscribe
    [registrations, JSON.stringify(resourcesListingsToDisplay)]
  );

  // When we drag and drop, , renders the appointment block that is dragged
  const DraftAppointment = ({style, ...props}) => (
    <CustomAppointment
      component={DragDropProvider.DraftAppointment}
      {...props}
      style={
        // Only show cloning option if there is only one slot in the session
        props.data.session.slots.length === 1 && cloningFeatureActivated
          ? {boxShadow: "0 0 3px 3px green", outline: "1.6px solid white", zIndex: 1000, ...style}
          : style
      }
    />
  );

  // When we drag and drop, renders the appointment at the original location
  const SourceAppointment = (props) => (
    <CustomAppointment
      component={DragDropProvider.SourceAppointment}
      // Only show cloning option if there is only one slot in the session
      opacity={props.data.session.slots.length === 1 && cloningFeatureActivated ? 1 : 0.5}
      {...props}
    />
  );

  // ************************* //
  // **** DATA DEFINITION **** //
  // ************************* //

  // Determine if grouping is enabled
  const groupByResourcesEnabled = selectedResources?.length > 0;

  // The resources and grouping data given to the agenda with just what it needs, not more
  const resourcesData = resources.filter((resource) =>
    selectedResources?.includes(resource.fieldName)
  );
  const groupingData = selectedResources?.map((fieldName) => ({resourceName: fieldName}));

  // Apply filter to resources
  const filteredResources = resourcesData.map((resource) => {
    const filterSelection = resourcesFilterSelections[resource.resourceName] || [];
    const filteredInstances = resource.instances.filter((instance) =>
      filterSelection.includes(instance.id)
    );
    return {...resource, instances: filteredInstances};
  });

  // Useful to calculate overlaps with different availabilities from the resources
  const filteredResourcesWithAvailabilities = filteredResources.filter(
    (resource) => resource.hasAvailabilities
  );

  // ************************** //
  // *** GROUPING/FILTERING *** //
  // ************************** //

  // Called by the grouping controls to change the state of the grouping
  const changeGrouping = (resource, value, newResourcesFilterSelections) => {
    const length = resource.instances.length;
    if (length === 0) {
      message.warn(
        `Vous devez déjà avoir créé des ${resource.title.toLowerCase()} pour effectuer cette action.`
      );
    } else {
      const selectedResourcesWithoutCurrent = selectedResources.filter(
        (t) => t !== resource.fieldName
      );
      const newSelectedResources = value
        ? [...selectedResourcesWithoutCurrent, resource.fieldName]
        : selectedResourcesWithoutCurrent;

      const displayNumberOfColumnsTooBig =
        value &&
        Object.entries(newResourcesFilterSelections)
          .map(([resourceName, resourcesSelected]) => [
            resourcesMapping[resourceName],
            resourcesSelected,
          ])
          .filter(([fieldName, resourcesSelected]) => selectedResources?.includes(fieldName))
          .reduce((acc, [_, resourcesSelected]) => resourcesSelected.length * acc, 1) *
          numberOfDaysDisplayed >
          RECOMMENDED_LIMIT_OF_RESOURCE_COLUMNS;

      if (displayNumberOfColumnsTooBig) {
        message.warn(
          `Vous avez beaucoup d'éléments sélectionnés. ` +
            `Pour éviter les ralentissements, décochez quelques éléments à afficher, ou abaissez le nombre de jours.`,
          6
        );
      }
      displayLoadingMessage().then(() => {
        const params = {selectedResources: newSelectedResources};
        if (newResourcesFilterSelections)
          params.resourcesFilterSelections = newResourcesFilterSelections;
        setAgendaParams(params);
      });
    }
  };

  // ************************** //
  // ***** AGENDA ACTIONS ***** //
  // ************************** //

  // Action on update or deletion
  const commitChanges = async ({deleted, changed}) => {
    // Actions if the slot is updated (by drag and drop for example)
    if (changed) {
      for (const [slotId, newSlotData] of Object.entries(changed)) {
        // Get the existing slot and create a new one
        const existingSlot = allSlots.find((slot) => slot._id === slotId);

        const newSlot = {...existingSlot};

        // Synchronize the category
        if (
          selectedResources.includes("categoryId") &&
          newSlotData.categoryId !== existingSlot.session.activity?.category?._id
        ) {
          message.info("Vous ne pouvez pas modifier la catégorie dans la vue agenda.");
        }

        // Synchronize places
        if (selectedResources.includes("placeId") && newSlotData.placeId) {
          newSlot.places = places.filter((place) => newSlotData.placeId.includes(place._id));
        }

        // Synchronize stewards
        if (selectedResources.includes("stewardId") && newSlotData.stewardId) {
          newSlot.stewards = stewards.filter((steward) =>
            newSlotData.stewardId.includes(steward._id)
          );
        }

        // Update dates
        newSlot.start = moment(newSlotData.startDate).set("seconds", 0).format();
        newSlot.end = midnightDateManager
          .displayToData(existingSlot.endDate, newSlotData.endDate)
          .set("seconds", 0)
          .format();

        // Select the session and put it in editing mode, then update the slot from the session
        await dispatch(sessionsActions.selectEditingByIdFromList(existingSlot.session._id));

        // If the cloning key was pressed during the drop, clone the element instead of moving it.
        // Else, just take the existing session and update the slot inside it.
        if (cloningFeatureActivated && existingSlot.session.slots.length === 1) {
          // If the slot is part of a session that has only one slot, clone the whole session
          await dispatch(
            sessionsActions.changeEditing({...existingSlot.session, _id: "new", slots: [newSlot]})
          );
          // Persist data and notify about successful creation
          await dispatch(sessionsActions.persist(undefined, true));
        } else {
          await dispatch(sessionsActions.updateSlotToEditing(newSlot));
          // Persist data silently
          await dispatch(sessionsActions.persist(undefined, true));
        }
        dispatch(sessionsActions.setEditing({}));
      }
    }
  };

  // Action on double click or when clicking the edit button: navigates to the existing session
  // (also when drag and drop, but not supported yet)
  const onEditingAppointmentChange = (changes) => {
    if (changes) {
      if (["resize-end", "resize-start"].includes(changes.type)) {
        console.log(changes.type);
      } else if (changes.type !== "vertical") {
        navigate(changes.session._id);
      }
    }
  };

  // Action on double click on an empty cell: navigates to a new session form
  const onAddedAppointmentChange = (appointmentData) => {
    const start = moment(appointmentData.startDate).set("seconds", 0).format();
    const end = moment(appointmentData.startDate).add(defaultNewSlotDuration, "minutes").format();
    console.log("added", start);
    dispatch(
      sessionsActions.setEditing({
        _id: "new",
        slots: [
          {
            start,
            end,
            duration: defaultNewSlotDuration,
            stewardsSessionSynchro: true,
            placesSessionSynchro: true,
          },
        ],
        start,
        end,
        places: appointmentData.placeId?.map((id) => places.find((p) => p._id === id)) || [],
        stewards: appointmentData.stewardId?.map((id) => stewards.find((s) => s._id === id)) || [],
      })
    );
    navigate("new");
  };

  // ************************** //
  // ***** SUB-COMPONENTS ***** //
  // ************************** //

  // Renders a cell of the Agenda day view
  // useCallback(React.memo()) helps avoiding approx 300% of re-rendering on start and cuts all the rendering after that
  const TimeTableCell = useCallback(
    React.memo((props) => {
      // Get the cell start and end hour
      const cellSlot = {start: props.startDate, end: props.endDate};
      const overlapsWithElements = [];

      // Check if the cell is out of project availabilities
      if (!slotOverlapsAnOtherOne(cellSlot, currentProject.availabilitySlots)) {
        overlapsWithElements.push("project");
      }

      // Check if the cell is out of the other entities availabilities (only for resources with availabilities
      for (const resourceInfo of filteredResourcesWithAvailabilities) {
        if (showResourcesAvailabilities[resourceInfo.fieldName]) {
          const resourceGrouping = props.groupingInfo?.find(
            (info) => info.fieldName === resourceInfo.fieldName
          );
          if (resourceGrouping) {
            const resource = resourceInfo.instances.find((res) => res.id === resourceGrouping.id);
            if (!slotOverlapsAnOtherOne(cellSlot, resource?.availabilitySlots)) {
              overlapsWithElements.push(resourceInfo.resourceName);
            }
          }
        }
      }

      // Add the appropriate classes to the cell, so it can be painted if there are some overlaps
      const className =
        overlapsWithElements.length > 0
          ? `${overlapsWithElements.join("-")}-disabled-date`
          : undefined;
      return (
        <DayView.TimeTableCell
          {...props}
          style={{height: cellDisplayHeight}}
          className={className}
        />
      );
    }),
    [
      JSON.stringify(showResourcesAvailabilities),
      JSON.stringify(filteredResourcesWithAvailabilities),
      cellDisplayHeight,
    ]
  );

  const DayScaleCell = (props) => (
    <DayView.DayScaleCell
      style={{cursor: "pointer"}}
      onDoubleClick={() =>
        setAgendaParams({
          currentAgendaDate: props.startDate.getTime(),
          numberOfDaysDisplayed: 1,
        })
      }
      {...props}
    />
  );

  // Agenda date navigation arrows and selector
  const DateNavigatorRoot = useCallback(
    React.memo((props) => {
      const goToProjectStartDate = moment(currentProject.start).isAfter(moment());

      // Extract month, capitalize first letter, and trim if on mobile device
      let dateText = props.navigatorText.replace(/\d+(-\d+)? ?/g, "");
      dateText = dateText.charAt(0).toUpperCase() + dateText.slice(1); // capitalize day
      if (isMobileView && dateText.length > 3)
        dateText = dateText.length !== 4 ? dateText.slice(0, 3) + "." : dateText;

      return (
        <div
          ref={props.rootRef}
          className="containerH buttons-container"
          style={{flexShrink: 0, alignItems: "center"}}>
          <div>
            <NavButton type="back" onClick={() => props.onNavigate("back")} />
            <NavButton type="forward" onClick={() => props.onNavigate("forward")} />
          </div>
          <Tooltip title="Sélectionner une date">
            <Button onClick={props.onVisibilityToggle}>{dateText}</Button>
          </Tooltip>
          <Tooltip
            title={
              goToProjectStartDate ? "Aller au commencement de l'événement" : "Aller à aujourd'hui"
            }>
            <Button
              type="link"
              icon={<CalendarOutlined />}
              onClick={() =>
                setAgendaParams({
                  currentAgendaDate: goToProjectStartDate
                    ? moment(currentProject.start).unix() * 1000
                    : moment().unix() * 1000,
                })
              }>
              {!isMobileView && (goToProjectStartDate ? "Début" : "Aujourd'hui")}
            </Button>
          </Tooltip>
        </div>
      );
    }),
    [isMobileView]
  );

  const GroupByControls = () => {
    const ResSelect = ({resource}) => (
      <ResourceFilterSelector
        key={resource.fieldName}
        resource={resource}
        selectedResources={selectedResources}
        resourcesFilterSelections={resourcesFilterSelections}
        changeGrouping={changeGrouping}
        agendaParams={agendaParams}
        setAgendaParams={setAgendaParams}
        showResourcesAvailabilities={showResourcesAvailabilities}
      />
    );

    return (
      <Dropdown
        trigger={["click"]}
        overlay={
          <Menu
            items={resources.map((resource) => ({
              label: <ResSelect resource={resource} />,
            }))}
          />
        }>
        <Button>
          <span style={{marginRight: -8}}>
            <span style={{marginRight: 8}}>Groupage</span>
            {selectedResources.length > 0 &&
              resources
                .filter((resource) => selectedResources.includes(resource.fieldName))
                .map((resource) => <ResSelect resource={resource} />)}
          </span>
          <DownOutlined />
        </Button>
      </Dropdown>
    );
  };

  // Where all the agenda controls are
  const AgendaControlsBar = useCallback(
    React.memo(() => {
      const FullSettingsDrawer = () => (
        <AgendaSettingsDrawer
          buttonStyle={{flexGrow: 0}}
          cellDisplayHeight={cellDisplayHeight}
          showResourcesListingOnAgendaCards={showResourcesListingOnAgendaCards}
          slotsOnEachOther={slotsOnEachOther}
          setAgendaParams={setAgendaParams}>
          {/*Select if we want to filter by date then by entities, or the inverse. */}
          {/*Disabled if there is no selected grouping.*/}
          <InputElement.Select
            label="Méthode de groupage"
            onChange={(value) =>
              displayLoadingMessage().then(() => setAgendaParams({groupByDaysFirst: value}))
            }
            defaultValue={groupByDaysFirst}
            options={[
              {value: true, label: "Jours, puis Groupage d'élements"},
              {value: false, label: "Groupage d'élements, puis Jours"},
            ]}
            tooltip="Lorsque le groupage est activé, grouper d'abord par jour puis par entité, ou l'inverse."
          />

          <InputElement.Number
            label="Durée par défaut pour les nouvelles sessions"
            defaultValue={defaultNewSlotDuration}
            step={30}
            onChange={(value) => setAgendaParams({defaultNewSlotDuration: value})}
          />
        </AgendaSettingsDrawer>
      );

      return (
        <AgendaControls
          isMobileView={isMobileView}
          GroupByControls={GroupByControls}
          FullSettingsDrawer={FullSettingsDrawer}
          searchBarValue={searchBarValue}
          setSearchBarValue={setSearchBarValue}
          numberOfDaysDisplayed={numberOfDaysDisplayed}
          setNumberOfDaysDisplayed={(value) => setAgendaParams({numberOfDaysDisplayed: value})}
          categoriesFilter={categoriesFilter}
          setCategoriesFilter={(value) => setAgendaParams({categoriesFilter: value})}
        />
      );
    }),
    [
      places,
      stewards,
      categories,
      selectedResources,
      isMobileView,
      numberOfDaysDisplayed,
      categoriesFilter,
    ]
  );

  // ************************** //
  // **** RENDER FUNCTION ***** //
  // ************************** //

  // If everything isn't loaded yet, stop here, no need to go further. Display a pending spinner.
  return currentProject._id !== undefined &&
    filteredAppointments &&
    selectedResources &&
    numberOfDaysDisplayed ? (
    <div className="full-width-content scheduler-wrapper">
      {/*Error boundary to return back to normal state if it is buggy with slotsOnEachOther*/}
      <CustomErrorBoundary
        onError={() => setAgendaParams(DEFAULT_AGENDA_PARAMS)}
        extra={
          <Button
            icon={<ReloadOutlined />}
            onClick={() => {
              setAgendaParams(DEFAULT_AGENDA_PARAMS);
              window.location.reload();
            }}>
            Recharger la page
          </Button>
        }>
        {/*The big agenda component*/}
        <Scheduler data={filteredAppointments} locale="fr-FR" height={schedulerHeight}>
          {/*State management*/}
          <ViewState
            defaultCurrentDate={currentAgendaDate}
            currentDate={currentAgendaDate}
            onCurrentDateChange={(date) => setAgendaParams({currentAgendaDate: date.getTime()})}
            defaultCurrentViewName="Day"
          />
          <EditingState
            onCommitChanges={commitChanges}
            onEditingAppointmentChange={onEditingAppointmentChange}
            onAddedAppointmentChange={onAddedAppointmentChange}
          />

          {groupByResourcesEnabled && (
            <GroupingState grouping={groupingData} groupByDate={() => groupByDaysFirst} />
          )}

          {/*Views management*/}
          <DayView
            timeTableCellComponent={TimeTableCell}
            timeScaleLabelComponent={getTimeScaleLabelComponent(cellDisplayHeight)}
            startDayHour={agendaDisplayParams.start}
            endDayHour={agendaDisplayParams.end}
            cellDuration={CELL_DURATION_MINUTES}
            intervalCount={numberOfDaysDisplayed}
            dayScaleCellComponent={DayScaleCell}
          />

          {/*Other tools*/}
          <Toolbar
            flexibleSpaceComponent={AgendaControlsBar} /*The toolbar with all the controls*/
          />
          <DateNavigator /*The controls to navigate to different days*/
            rootComponent={DateNavigatorRoot}
          />
          <Appointments
            appointmentComponent={Appointment} /*Displays the appointments on the angenda*/
            placeAppointmentsNextToEachOther={!slotsOnEachOther}
          />
          {groupByResourcesEnabled && (
            <Resources data={filteredResources} /*Infos about the resources*/ />
          )}
          <IntegratedEditing />
          {groupByResourcesEnabled && <IntegratedGrouping />}
          {groupByResourcesEnabled && <GroupingPanel />}
          <DragDropProvider
            sourceAppointmentComponent={SourceAppointment}
            draftAppointmentComponent={DraftAppointment}
          />
          <AppointmentForm visible={false} /*Makes double click possible everywhere*/ />
          <CurrentTimeIndicator />

          {/* Session Preview drawer on mobile view */}
          {isMobileView && (
            <Drawer
              className="session-preview-drawer shadow"
              mask={false}
              placement="bottom"
              zIndex={99}
              height={300}
              bodyStyle={{
                display: "flex",
                alignItems: "stretch",
                padding: 0,
                margin: 0,
                scroll: "auto",
                justifyContent: "center",
              }}
              style={{margin: 0, padding: 0}}
              onClose={() => setCurrentSessionPreview(undefined)}
              closeIcon={<CloseOutlined style={{color: "white"}} />}
              visible={currentSessionPreview}>
              {currentSessionPreview && (
                <SessionPreview session={currentSessionPreview} fullWidth />
              )}
            </Drawer>
          )}
        </Scheduler>
      </CustomErrorBoundary>
    </div>
  ) : (
    <LayoutElement.Pending />
  );
}
