import React, {useEffect, useMemo} from "react";
import {useDispatch, useSelector} from "react-redux";
import {sessionsActions, sessionsSelectors} from "../../features/sessions.js";
import {ListPage} from "../common/ListPage";
import {listRenderer, listSorter} from "../../helpers/listUtilities";
import moment from "moment";
import {CsvExportButton} from "../../helpers/filesUtilities";
import {currentProjectSelectors} from "../../features/currentProject";
import {generateSessionsColumns} from "../../helpers/tableUtilities";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {viewActions, viewSelectors} from "../../features/view";
import {InputElement} from "../common/InputElement";

import {personName} from "../../helpers/utilities";
import {useColumnsBlacklistingSelector} from "../../helpers/viewUtilities";
import {getSessionName} from "../../helpers/agendaUtilities";

export function SessionList({navigate}) {
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelector();
  const sessions = useSelector(sessionsSelectors.selectList);
  const registrations = useSelector(registrationsSelectors.selectList);
  const filter = useSelector(viewSelectors.selectFilter);
  const columnsBlacklist = useSelector(viewSelectors.selectColumnsBlacklist);
  const dispatch = useDispatch();

  const columns = useMemo(
    () =>
      generateSessionsColumns(
        "..",
        currentProject.usePlaces,
        currentProject.useTeams,
        false,
        false,
        registrations
      ),
    [currentProject, registrations, columnsBlacklist]
  );

  useEffect(() => {
    dispatch(sessionsActions.loadList());
    dispatch(registrationsActions.loadList());
  }, []);

  const filteredSessions = sessions.filter((session) => {
    if (filter === "hidePastSessions") {
      return moment().isBefore(session.end); // Show only ongoing sessions and in the future
    } else if (filter === "showNextSessions") {
      return moment().isBefore(session.start);
    } else if (filter === "showOngoingSessions") {
      return moment().isBefore(session.end) && moment().isAfter(session.start);
    } else {
      return true;
    }
  });

  const buildSessionsExport = () => {
    const exportedData = sessions
      .map((session) => {
        return {
          "ID Session": session._id,
          "ID Activité": session.activity._id,
          Catégorie: session.activity.category.name,
          "Couleur de catégorie": session.activity.category.color,
          "Catégories secondaires": session.activity.secondaryCategories?.join(", "),
          "Nom de la session": getSessionName(session),
          Résumé: session.activity.summary,
          "Description détaillée": session.activity.description,
          "Début (heure)": listRenderer.timeFormat(session.start),
          "Fin  (heure)": listRenderer.timeFormat(session.end),
          "Début (jour + heure)": listRenderer.longDateTimeFormat(session.start),
          "Fin  (jour + heure)": listRenderer.longDateTimeFormat(session.end),
          "Début - Fin (heure)": listRenderer.timeRangeFormat(session.start, session.end),
          "Début - Fin (jour + heure, court)": listRenderer.longDateTimeRangeFormat(
            session.start,
            session.end,
            true
          ),
          "Début - Fin (jour + heure, long)": listRenderer.longDateTimeRangeFormat(
            session.start,
            session.end,
            false
          ),
          "Début (Format ISO)": session.start,
          "Fin (Format ISO)": session.end,
          "Durée totale": session.slots
            ?.map((s) =>
              listRenderer.durationFormat(moment(s.end).diff(moment(s.start), "minutes"), true)
            )
            .join(" + "),
          "Durée totale (brute)": session.slots
            ?.map((s) => moment(s.end).diff(moment(s.start), "minutes"))
            .reduce((acc, duration) => acc + duration, 0),
          "Participant.es inscrit.es": session.numberParticipants,
          "Jauge max. de participant.es": session.computedMaxNumberOfParticipants,
          "Encadrant.es": session.stewards.map(personName).join(", "),
          Espaces: session.places.map((place) => place.name).join(", "),
        };
      })
      .sort((a, b) => listSorter.date(a["Début (Format ISO)"], b["Début (Format ISO)"]));
    return exportedData;
  };

  return (
    <ListPage
      title="Sessions"
      buttonTitle="Créer une session"
      elementsActions={sessionsActions}
      settingsDrawerContent={<ColumnsBlacklistingSelector columns={columns} />}
      navigateFn={navigate}
      columns={filterBlacklistedColumns(columns)}
      customButtons={
        <>
          <InputElement.Select
            placeholder="Filtrer les sessions"
            defaultValue={filter}
            formItemProps={{style: {marginBottom: 0, minWidth: 100}}}
            options={[
              {value: null, label: "- Aucun filtrage -"},
              {value: "hidePastSessions", label: "Cacher les sessions passées"},
              {value: "showOngoingSessions", label: "Sessions en cours"},
              {value: "showNextSessions", label: "Sessions à venir"},
            ]}
            onChange={(value) => dispatch(viewActions.changeFilter(value))}
          />
          <CsvExportButton
            tooltip="Exporter les sessions au format CSV"
            getExportName={() =>
              `Export des sessions - ${currentProject.name} - ${moment().format(
                "DD-MM-YYYY HH[h]mm"
              )}.csv`
            }
            dataExportFunction={buildSessionsExport}>
            Exporter
          </CsvExportButton>
        </>
      }
      dataSource={filteredSessions}
      groupEditable
      groupImportable
    />
  );
}
