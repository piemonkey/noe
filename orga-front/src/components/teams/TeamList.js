import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {teamsActions, teamsSelectors} from "../../features/teams.js";
import {ListPage} from "../common/ListPage";
import {generateColumnsTeams} from "../../helpers/tableUtilities";

export function TeamList({navigate}) {
  const teams = useSelector(teamsSelectors.selectList);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(teamsActions.loadList());
  }, []);

  return (
    <ListPage
      title="Équipes"
      buttonTitle="Créer une équipe"
      elementsActions={teamsActions}
      navigateFn={navigate}
      columns={generateColumnsTeams("..")}
      dataSource={teams}
      groupEditable
      groupImportable
    />
  );
}
