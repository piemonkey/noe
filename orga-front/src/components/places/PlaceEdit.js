import React, {useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import {placesSelectors, placesActions} from "../../features/places.js";
import {currentProjectSelectors} from "../../features/currentProject.js";
import {Availability} from "../utils/Availability.js";
import {InputElement} from "../common/InputElement";
import {EditPage} from "../common/EditPage";
import {Button, Popconfirm} from "antd";
import {useLoadEditing} from "../../helpers/editingUtilities";
import {CardElement} from "../common/LayoutElement";

export function PlaceEdit({id, location, asModal, modalVisible, setModalVisible}) {
  const place = useSelector(placesSelectors.selectEditing);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const [isModified, setIsModified] = useState(false);
  const dispatch = useDispatch();

  const groupEditing = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(placesActions, id, undefined, clonedElement);

  const importProjectAvaibility = (e) => {
    dispatch(
      placesActions.replaceAllAvailabilitySlots(
        currentProject.availabilitySlots.map((as) => {
          const {_id, ...slot} = as;
          return slot;
        })
      )
    );
    setIsModified(true);
  };

  return (
    <EditPage
      createTitle="Créer un espace"
      editTitle="Modifier un espace"
      groupEditingTitle="Édition groupée d'espaces"
      cloningTitle="Cloner des espaces"
      clonable
      clonedElement={clonedElement}
      asModal={asModal}
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      deletable
      elementsActions={placesActions}
      record={place}
      forceModifButtonActivation={isModified}
      initialValues={place}
      outerChildren={
        <Availability
          title="Disponibilités"
          disableDatesIfOutOfProject // We grey out all the dates out of the project scope, but we allow to select outside of them
          setIsModified={setIsModified}
          customButtons={
            <Popconfirm
              title="Les disponibilités existantes seront écrasées."
              okText="Importer quand même"
              okButtonProps={{danger: true}}
              cancelText="Annuler"
              onConfirm={importProjectAvaibility}>
              <Button type="link">Importer les plages de l'événement</Button>
            </Popconfirm>
          }
          entity={place}
          actions={placesActions}
        />
      }
      groupEditing={groupEditing}>
      <CardElement>
        <div className="container-grid two-per-row">
          <InputElement.Text
            label="Nom de l'espace"
            name="name"
            placeholder="nom"
            rules={[{required: true}]}
          />

          <InputElement.Number
            label="Nombre maximum de personnes"
            name="maxNumberOfParticipants"
            min={1}
            placeholder="max"
            rules={[{required: true}]}
          />
        </div>
      </CardElement>

      <CardElement>
        <div className="container-grid">
          <InputElement.TextArea label="Informations" name="summary" placeholder="informations" />

          <InputElement.Editor
            label="Notes privées pour les orgas"
            name="notes"
            placeholder="notes privées"
            tooltip="Ces notes ne sont pas affichées aux participant⋅es, elles ne sont disponibles que pour les organisateur⋅ices de l'événement"
          />
        </div>
      </CardElement>
    </EditPage>
  );
}
