import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {placesActions, placesSelectors} from "../../features/places.js";

import {ListPage} from "../common/ListPage";
import {listSorter} from "../../helpers/listUtilities";

export function PlaceList({navigate}) {
  const places = useSelector(placesSelectors.selectList);
  const dispatch = useDispatch();

  const columns = [
    {
      title: "Nom",
      dataIndex: "name",
      sorter: (a, b) => listSorter.text(a.name, b.name),
      searchable: true,
    },
    {
      title: "Nombre maximum de personnes",
      dataIndex: "maxNumberOfParticipants",
      sorter: (a, b) => listSorter.number(a.maxNumberOfParticipants, b.maxNumberOfParticipants),
      searchable: true,
    },
  ];

  // const [user, setUser] = useState({login:'toto',password:'password'});
  useEffect(() => {
    dispatch(placesActions.loadList());
  }, []);

  return (
    <ListPage
      title="Espaces"
      buttonTitle="Créer un espace"
      elementsActions={placesActions}
      navigateFn={navigate}
      columns={columns}
      dataSource={places}
      groupEditable
      groupImportable
    />
  );
}
