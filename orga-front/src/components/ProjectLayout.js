import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {SessionList} from "./sessions/SessionList.js";
import {SessionEdit} from "./sessions/SessionEdit.js";
import {SessionAgenda} from "./sessions/SessionAgenda.js";
import {ConfigEdit} from "./config/ConfigEdit.js";
import {ParticipantMain} from "./participants/ParticipantMain.js";
import {ActivityList} from "./activities/ActivityList.js";
import {ActivityEdit} from "./activities/ActivityEdit.js";
import {StewardList} from "./stewards/StewardList.js";
import {StewardEdit} from "./stewards/StewardEdit.js";
import {PlaceList} from "./places/PlaceList.js";
import {PlaceEdit} from "./places/PlaceEdit.js";
import {CategoryList} from "./categories/CategoryList.js";
import {CategoryEdit} from "./categories/CategoryEdit.js";
import {RunAI} from "./ai/RunAI.js";
import {Menu} from "antd";
import {LayoutElement} from "./common/LayoutElement";
import {
  CalendarOutlined,
  ClusterOutlined,
  DownloadOutlined,
  EnvironmentOutlined,
  DoubleLeftOutlined,
  FileOutlined,
  PlaySquareOutlined,
  SnippetsOutlined,
  TagOutlined,
  TeamOutlined,
  ToolOutlined,
  UserOutlined,
  DashboardOutlined,
} from "@ant-design/icons";
import {Redirect, Router} from "@reach/router";
import {currentUserSelectors} from "../features/currentUser.js";
import {currentProjectActions, currentProjectSelectors} from "../features/currentProject.js";
import {LayoutStructure, setAppTheme} from "./common/LayoutStructure";
import {useBrowserTabTitle} from "../helpers/viewUtilities";
import {RegistrationEdit} from "./participants/RegistrationEdit";
import {registrationsActions, registrationsSelectors} from "../features/registrations";
import {TeamEdit} from "./teams/TeamEdit";
import {TeamList} from "./teams/TeamList";
import {
  OFFLINE_MODE,
  OnlineOfflineSwitch,
  DISPLAY_OFFLINE_MODE_FEATURE,
  useOfflineMode,
} from "../helpers/offlineModeUtilities";
import {Cockpit} from "./cockpit/Cockpit";
import {instanceName, URLS} from "../app/configuration";

export const ProjectLayout = ({"*": page, envId, navigate}) => {
  const dispatch = useDispatch();

  // ****** SELECTORS & STATES ******

  const currentUser = useSelector(currentUserSelectors.selectUser);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  // Set app theme accordingly
  if (currentProject.theme) setAppTheme(currentProject.theme);

  // ****** DATA FETCHING ******

  // Load project and project registration
  useEffect(() => {
    if (currentUser._id) {
      dispatch(currentProjectActions.load(envId)).catch(() => navigate("/projects"));
    }
  }, [currentUser, envId]);
  useEffect(() => {
    //(on orga front, the registration is independent from the project)
    dispatch(registrationsActions.loadCurrent(envId));
  }, [envId]);

  useOfflineMode(currentRegistration?._id);

  // ****** DATA DEFINITION ******

  const projectAndUserValid = currentUser._id && currentProject?._id && currentRegistration?._id;

  // If there is a loaded registration, but no role, then the user doesn't have the right to access the project
  if (projectAndUserValid && !currentRegistration?.role) navigate("/projects");

  // ****** TAB TITLE ******

  useBrowserTabTitle(
    currentProject.name,
    page,
    {
      config: "Configuration",
      categories: "Catégories",
      activities: "Activités",
      teams: "Équipes",
      stewards: "Encadrant⋅es",
      places: "Espaces",
      sessions: "Sessions",
      "sessions/agenda": "Sessions",
      participants: "Participant⋅es",
    },
    (instanceName !== "NOÉ" ? `${instanceName} ` : "") + "ORGA"
  );

  // ****** SIDE MENU ******

  const collapsedSidebar = page?.includes("agenda");

  const menu = {
    top: (
      <>
        <LayoutElement.Menu
          rootNavUrl={`/${envId}/`}
          selectedItem={page}
          items={[
            {
              label: "Cockpit",
              key: "cockpit",
              icon: <DashboardOutlined />,
            },
            {
              label: "Configuration",
              key: "config",
              icon: <ToolOutlined />,
            },
            {
              label: "Participant⋅es",
              key: "participants",
              icon: <TeamOutlined />,
            },
          ]}
        />

        <LayoutElement.Menu
          rootNavUrl={`/${envId}/`}
          selectedItem={page}
          title={collapsedSidebar ? "prog" : "programmation"}
          items={[
            {
              label: "Catégories",
              key: "categories",
              icon: <TagOutlined />,
            },
            currentProject.usePlaces && {
              label: "Espaces",
              key: "places",
              icon: <EnvironmentOutlined />,
            },
            {
              label: "Encadrant⋅es",
              key: "stewards",
              icon: <UserOutlined />,
            },
            currentProject.useTeams && {
              label: "Équipes",
              key: "teams",
              icon: <TeamOutlined />,
            },
            {
              label: "Activités",
              key: "activities",
              icon: <FileOutlined />,
            },
          ]}
        />

        <LayoutElement.Menu
          rootNavUrl={`/${envId}/`}
          selectedItem={page}
          title="planning"
          items={[
            currentProject.useAI && {
              label: "Créer avec l'IA",
              key: "ai",
              icon: <ClusterOutlined />,
            },
            {
              label: "Sessions",
              key: "sessions",
              icon: <SnippetsOutlined />,
            },
            {
              label: "Planning des sessions",
              key: "sessions/agenda",
              icon: <CalendarOutlined />,
            },
          ]}
        />
      </>
    ),

    footer: (
      <LayoutElement.Menu
        selectedItem={page}
        items={[
          // Offline mode
          DISPLAY_OFFLINE_MODE_FEATURE && {
            label: <OnlineOfflineSwitch />,
            icon: <DownloadOutlined />,
            url: false,
          },

          // Back to events page
          {
            label: "Mes événements",
            url: "/projects",
            icon: <DoubleLeftOutlined />,
            disabled: OFFLINE_MODE,
          },

          // Inscription front
          {
            label: "Interface participant⋅e",
            url: `${URLS.INSCRIPTION_FRONT}/${envId}/${page}`,
            icon: <PlaySquareOutlined />,
          },
        ]}
      />
    ),
  };

  return (
    <LayoutStructure
      title={currentProject.name}
      ribbon="ORGA"
      menu={menu}
      profileUser={currentUser}
      showSocialIcons={false}
      fadeIn
      collapsedSidebar={collapsedSidebar}>
      {projectAndUserValid && (
        <>
          <Router style={{height: "100%"}}>
            <Cockpit path="/cockpit" />
            <ConfigEdit path="/config" />
            <ActivityList path="/activities" />
            <ActivityEdit path="/activities/:id" />
            <StewardList path="/stewards" />
            <StewardEdit path="/stewards/:id" />
            {currentProject.usePlaces && <PlaceList path="/places" />}
            {currentProject.usePlaces && <PlaceEdit path="/places/:id" />}
            <CategoryList path="/categories" />
            <CategoryEdit path="/categories/:id" />
            <ParticipantMain path="/participants" />
            <RegistrationEdit path="/participants/:id" />
            <SessionList path="/sessions" />
            <SessionEdit path="/sessions/:id" />
            <SessionAgenda path="/sessions/agenda" />
            {currentProject.useTeams && <TeamList path="/teams" />}
            {currentProject.useTeams && <TeamEdit path="/teams/:id" />}
            {currentProject.useAI && <RunAI path="/ai" />}
            <Redirect noThrow from="/*" to="./cockpit" />
          </Router>
          <LayoutElement.HelpToast
            href={process.env.REACT_APP_FAQ_URL}
            target="_blank" // Redirect in a new tab with target = "_blank"
            tooltipMessage="Besoin d'aide ? Cliquez ici pour accéder à la FAQ et à la documentation de l'application."
          />
        </>
      )}
    </LayoutStructure>
  );
};
