import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {message, Steps} from "antd";
import {InputElement} from "../common/InputElement";
import {FormElement} from "../common/FormElement";
import moment from "moment";
import {OpeningState, projectsActions, projectsSelectors} from "../../features/projects.js";
import {dataToFields, fieldToData} from "../../helpers/tableUtilities";
import {CardElement} from "../common/LayoutElement";
import {listRenderer} from "../../helpers/listUtilities";

const {Step} = Steps;

export function EssentialInformation({setIsModified}) {
  const dispatch = useDispatch();
  const project = useSelector(projectsSelectors.selectEditing);
  const projectIsNotOpened = project.openingState === "notOpened";

  const onChange = (changedFields, allFields) => {
    const formData = fieldToData(allFields);
    dispatch(projectsActions.changeEditing(formData));
    setIsModified(true);
  };

  const projectDataToFields = dataToFields(project, (clone) => {
    clone.breakfastTime = moment(clone.breakfastTime);
    clone.lunchTime = moment(clone.lunchTime);
    clone.dinnerTime = moment(clone.dinnerTime);
  });

  const onOpeningStateChange = (newStateIndex) => {
    const newState = Object.keys(OpeningState).find((key) => OpeningState[key] === newStateIndex);
    const projectData =
      newStateIndex >= 2 ? {openingState: newState, useAI: false} : {openingState: newState};
    if (newStateIndex >= 2 && project.useAI) {
      message.warning(
        "Vous lancez vos inscriptions : par sécurité, nous désactivons automatiquement l'IA. Vous pouvez la réactiver dans les paramètres avancés.",
        15
      );
    }
    dispatch(projectsActions.changeEditing(projectData));
    setIsModified(true);
  };

  return (
    <FormElement fields={projectDataToFields} onFieldsChange={onChange}>
      <div className="container-grid two-per-row">
        <div className="container-grid">
          <CardElement>
            <InputElement.Text label="Nom de l'événement" name="name" placeholder="nom" />
          </CardElement>

          <CardElement title="Gestion de l'événément">
            <div className="container-grid">
              <InputElement.Custom label="Étapes de l'événement">
                <Steps
                  current={OpeningState[project.openingState]}
                  direction={"vertical"}
                  onChange={onOpeningStateChange}>
                  <Step
                    title="Fermé"
                    key={0}
                    description="Ni ouvert aux inscriptions, ni ouvert aux préinscriptions."
                  />
                  <Step
                    title="Pré-inscriptions"
                    key={1}
                    description="Les personnes peuvent se pré-inscrire en remplissant leurs dates de présence, le formulaire d'inscription et en payant."
                  />
                  <Step
                    title={
                      <span style={{whiteSpace: "normal"}}>Inscriptions des encadrant⋅es</span>
                    }
                    key={2}
                    description="Les encadrant⋅es peuvent s'inscire aux sessions en avance, mais les participant⋅es n'ont encore qu'accès aux pré-inscriptions."
                  />
                  <Step
                    title="Inscriptions pour tous"
                    key={3}
                    description="Tout le monde peut s'inscrire aux sessions sur l'événement."
                  />
                </Steps>
              </InputElement.Custom>

              <InputElement.Switch
                label="L'événement est complet (bloquer les nouvelles inscriptions)"
                tooltip="Permet de bloquer les nouvelles inscription, tout en laissant la possibilité aux personnes déjà inscrites de continuer à accéder à leur inscription."
                name="full"
                disabled={projectIsNotOpened}
              />

              <div className="container-grid two-per-row">
                <InputElement.Switch
                  label="Rendre l'événement public"
                  tooltip="L'événement sera visible dans la liste de tous les événements publics de la plateforme, côté participant⋅e."
                  name="isPublic"
                />

                <InputElement.Switch
                  label="Programmation secrète"
                  tooltip="Rendre le planning et la page d'accueil secrètes, c'est à dire accessibles seulement par les participant.es dont l'inscription est totalement validée."
                  name="secretSchedule"
                />
              </div>
            </div>
          </CardElement>
        </div>

        <div className="container-grid">
          <CardElement title="Gestions des inscriptions">
            <div className="container-grid">
              <InputElement.Switch
                label="Interdire les inscriptions sur plusieurs sessions qui se chevauchent"
                name="notAllowOverlap"
              />

              <InputElement.Switch
                label="Bloquer toutes les inscriptions et désinscriptions"
                tooltip="Les participant⋅es pourront consulter leur planning sans le modifier. Les orgas peuvent toujours s'inscrire ou se désinscrire."
                name="blockSubscriptions"
              />

              <InputElement.Switch
                label="Bloquer la désinscription des sessions de bénévolat lorsqu'elles ont lieu dans moins de 48 heures"
                disabled={project.blockSubscriptions}
                tooltip="Si non bloqué, un message de confirmation avec un avertissement apparaîtra si la personne essaie de se désinscrire."
                name="blockVolunteeringUnsubscribeIfBeginsSoon"
              />

              <InputElement.Slider
                label="Nombre d'heures minimum / maximum de bénévolat par jour conseillé"
                name="minMaxVolunteering"
                range
                tipFormatter={(val) => listRenderer.durationFormat(val, true)}
                step={15}
                max={480}
                marks={{
                  0: "0h",
                  60: "1h",
                  120: "2h",
                  180: "3h",
                  240: "4h",
                  300: "5h",
                  360: "6h",
                  420: "7h",
                  480: "8h",
                }}
              />
            </div>
          </CardElement>

          <CardElement title="Heure des repas">
            <div className="container-grid">
              <p>
                Pour calculer les <strong>jours de présence</strong>, NOÉ se base sur les heures de
                repas du midi et du soir, en partant du principe que si une personne est présente
                pour au moins un des deux repas du jour, elle est considérée comme "présente" ce
                jour-là.
              </p>
              <div className="container-grid three-per-row">
                <InputElement.Time label="Petit déjeuner" name="breakfastTime" />
                <InputElement.Time label="Déjeuner" name="lunchTime" />
                <InputElement.Time label="Dîner" name="dinnerTime" />
              </div>
            </div>
          </CardElement>
        </div>
      </div>
    </FormElement>
  );
}
