import React, {useRef, useState} from "react";
import {Button, message, Upload} from "antd";
import {ExportOutlined, ImportOutlined} from "@ant-design/icons";
import {projectsActions, projectsSelectors} from "../../features/projects";
import {useDispatch, useSelector} from "react-redux";
import {InputElement} from "../common/InputElement";
import {FormElement} from "../common/FormElement";
import {navigate} from "@reach/router";
import {CardElement} from "../common/LayoutElement";
import moment from "moment";

export function ImportExport() {
  const project = useSelector(projectsSelectors.selectEditing);
  const [importExportWithRegistrations, setImportExportWithRegistrations] = useState(false);
  const dispatch = useDispatch();
  const downloadButton = useRef();

  const exportToFile = () => {
    dispatch(projectsActions.export(importExportWithRegistrations))
      .then((dataToExport) => {
        const file = new File([JSON.stringify(dataToExport)], "export", {
          type: "application/json",
        });

        let exportUrl = URL.createObjectURL(file);

        downloadButton.current.setAttribute("href", exportUrl);
        downloadButton.current.setAttribute(
          "download",
          `Export du projet - ${project.name} - ${moment().format("HH-mm DD-MM-YYYY")}.json`
        );
        downloadButton.current.click();
      })
      .catch(() => message.error("Le projet n'a pas pu être exporté."));
  };

  const importFile = (file, additiveImport) => {
    let reader = new FileReader();
    reader.onload = function (e) {
      try {
        let data = JSON.parse(e.target.result);
        dispatch(projectsActions.import(data, additiveImport, importExportWithRegistrations))
          .then(() => message.success("Import réussi !"))
          .catch(() => message.error("L'import a échoué."));
      } catch (e) {
        message.error("Oups, il semblerait que votre fichier d'import soit malformé !");
      }
    };
    reader.readAsText(file);
  };

  return (
    <>
      <a ref={downloadButton} style={{display: "none"}} />

      <CardElement>
        <p>
          Vous pouvez choisir d'exporter et importer les inscriptions et utilisateur⋅ices qui sont
          liées à votre projet.
        </p>
        <FormElement>
          <InputElement.Switch
            onChange={() => setImportExportWithRegistrations(!importExportWithRegistrations)}
            checked={importExportWithRegistrations}
            label="Importer / Exporter aussi les inscriptions et les utilisateur⋅ices"
          />
        </FormElement>
      </CardElement>

      <div className="container-grid two-per-row">
        <CardElement title="Exporter l'événement en JSON">
          <h5>Export du projet entier</h5>
          <p>
            Tout est exporté (catégories, activités, encadrant⋅es, espaces, sessions, et
            configuration générale) <strong>à l'exception</strong> :
          </p>
          <ul>
            <li>des participant⋅es et e leurs inscriptions</li>
            <li>des inscriptions des participant⋅es aux sessions et équipes</li>
            <li>des droits de chaque utilisateur⋅ice sur l'événement</li>
          </ul>
          <Button onClick={exportToFile} icon={<ExportOutlined />}>
            Exporter
          </Button>
          <h5 style={{marginTop: 25}}>Autres exports</h5>
          <p>D'autres exports sont disponibles sur d'autres pages (bouton en haut à droite) :</p>

          <div>
            <Button
              type="link"
              onClick={() => navigate("./participants")}
              icon={<ExportOutlined />}>
              Participant⋅es & stats
            </Button>
            <Button type="link" onClick={() => navigate("./sessions")} icon={<ExportOutlined />}>
              Sessions
            </Button>
          </div>
        </CardElement>

        <CardElement title="Importer un événement depuis un fichier JSON">
          <h5>Import additif</h5>
          <p>
            Importer les données{" "}
            <strong style={{color: "green"}}>en addition des données existantes</strong>. Seule la
            configuration de l'événement sera remplacée, c'est à dire:
          </p>
          <ul>
            <li>Informations essentielles</li>
            <li>Formulaire d'inscription</li>
            <li>Page d'accueil</li>
            <li>Paramétrage Hello Asso</li>
          </ul>
          <p>Le nom du projet et les droits des personnes sur le projet restent inchangés.</p>
          <p>
            Si vous souhaitez importer seulement une partie de la configuration (par exemple
            seulement le formulaire d'inscription), vous pouvez éditer manuellement le fichier JSON
            d'import pour enlever les données que vous ne voulez pas ré-importer.
          </p>
          <Upload
            beforeUpload={(file) => {
              importFile(file, true);
              return false;
            }}
            accept="application/json"
            showUploadList={false}>
            <Button icon={<ImportOutlined />}>Importer et ajouter à l'existant</Button>
          </Upload>

          <h5 style={{marginTop: 25}}>Import destructif</h5>
          <p>
            Importer les données en remplacement des données existantes.{" "}
            <strong style={{color: "red"}}>Tout l'événement sera écrasé.</strong>
          </p>
          <p>Les participant⋅es à l'événement ne seront pas impactés.</p>
          <Upload
            beforeUpload={(file) => {
              importFile(file, false);
              return false;
            }}
            accept="application/json"
            showUploadList={false}>
            <Button danger icon={<ImportOutlined />} style={{margin: 4}}>
              Importer et tout remplacer
            </Button>
          </Upload>
        </CardElement>
      </div>
    </>
  );
}
