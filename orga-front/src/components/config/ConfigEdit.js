import React, {useEffect, useRef, useState, lazy} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Alert, Button, Form, Input, message, Select, Tabs, Tooltip} from "antd";
import {InputElement} from "../common/InputElement";
import {FormElement} from "../common/FormElement";
import {TabsPage} from "../common/TabsPage";
import {MinusCircleOutlined, PlusOutlined, ShareAltOutlined} from "@ant-design/icons";
import {projectsActions, projectsSelectors} from "../../features/projects.js";
import {Availability} from "../utils/Availability";
import {Advanced} from "./Advanced";
import {Members} from "./Members";
import {debounce} from "../../helpers/viewUtilities";
import {registrationsSelectors} from "../../features/registrations";
import {CardElement, LayoutElement} from "../common/LayoutElement";
import {flatFormInputs} from "../../helpers/registrationsUtilities";
import {EssentialInformation} from "./EssentialInformation";
import {Ticketing} from "./Ticketing";
const WelcomePageEditor = lazy(() =>
  import(/* webpackPrefetch: true */ "../common/welcomePageEditor/WelcomePageEditor")
);
const FormBuilder = lazy(() =>
  import(/* webpackPrefetch: true */ "../common/RegistrationForm").then((module) => ({
    default: module["FormBuilder"],
  }))
);

const {Option} = Select;
const {TabPane} = Tabs;

export function ConfigEdit() {
  const dispatch = useDispatch();
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const [isModified, setIsModified] = useState(false);
  const formBuilderRef = useRef();
  const [mappingForm] = Form.useForm();
  const messageNotYetDisplayed = useRef();
  const project = useSelector(projectsSelectors.selectEditing);
  const [shareLinkEndPoint, setShareLinkEndPoint] = useState("welcome");
  const [displayShareLink, setDisplayShareLink] = useState(false);
  const isUserAdminOnThisProject = currentRegistration.role === "admin";

  const saveFormData = () => {
    // Save formIO data
    if (formBuilderRef.current) {
      dispatch(
        projectsActions.changeEditing({
          formComponents: formBuilderRef.current.map((c) => ({...c})),
        })
      );
    }
  };

  useEffect(() => {
    // When unmounting the component, save the form data not dispatched in Redux
    return saveFormData;
  }, []);

  const setIsModifiedAndSaveForm = (isModified) => {
    saveFormData();
    setIsModified(isModified);
    isModified && delaySaveTipMessage();
  };

  const onConfigSaveButtonClick = () => {
    saveFormData();
    mappingForm
      .validateFields()
      .catch(() => message.error("Attention, le mapping des champs de formulaire est incomplet."));
    isModified && delaySaveTipMessage();
    dispatch(projectsActions.persist());
    message.destroy("notSaved");
    setIsModified(false);
  };

  const delaySaveTipMessage = debounce((triggerNewMessage) => {
    if (messageNotYetDisplayed.current) {
      message.open({
        key: "notSaved",
        content: (
          <div className="containerH buttons-container">
            <span>N'oublie pas de sauver tes modifications :)</span>
            <Button type="primary" onClick={onConfigSaveButtonClick}>
              Enregistrer
            </Button>
          </div>
        ),
        duration: 10,
      });
      messageNotYetDisplayed.current = false;
    }
  }, 1000 * 60);

  const changeMapping = (changedFields, {formMapping}) => {
    dispatch(projectsActions.changeEditing({formMapping}));
    formMapping.length > 0 && setIsModifiedAndSaveForm(true);
  };

  const onChangeWelcomePage = (value) => {
    setIsModifiedAndSaveForm(true);
    messageNotYetDisplayed.current = false;
    dispatch(projectsActions.changeEditing({content: value}));
  };

  return (
    <TabsPage
      title="Configuration de l'événement"
      fullWidth
      forceModifButtonActivation={isModified}
      onValidation={onConfigSaveButtonClick}>
      <TabPane tab="Informations essentielles" key="main" className="with-margins">
        <EssentialInformation setIsModified={setIsModified} />
      </TabPane>

      <TabPane tab="Plages d'ouverture" key="opening-times" className="with-margins">
        <Availability
          title="Plages d'ouverture de l'événement"
          setIsModified={setIsModified}
          entity={project}
          actions={projectsActions}
        />
      </TabPane>

      <TabPane tab="Formulaire d'inscription" key="form" className="with-margins">
        <CardElement title="Mapping des champs de formulaire">
          <Alert
            style={{marginBottom: 26}}
            description={
              <>
                Le mapping des champs de formulaire vous permet d'afficher certaines réponses aux
                questions de votre formulaire d'inscription dans la liste des participant⋅es partout
                dans NOÉ.
              </>
            }
          />
          {project.formComponents?.length > 0 ? (
            <FormElement form={mappingForm} onValuesChange={changeMapping} initialValues={project}>
              <Form.List name="formMapping">
                {(fields, {add, remove}) => (
                  <>
                    {fields.map(({key, name, ...restField}) => (
                      <div
                        key={key}
                        className="containerH"
                        style={{
                          gap: "0 8px",
                          alignItems: "baseline",
                          flexWrap: "wrap",
                          marginBottom: 10,
                        }}>
                        <Button
                          type="link"
                          style={{flexGrow: 0, flexShrink: 0}}
                          danger
                          icon={<MinusCircleOutlined />}
                          onClick={() => {
                            remove(name);
                            setIsModifiedAndSaveForm(true);
                          }}
                        />
                        <strong>La réponse</strong>
                        <InputElement.Select
                          name={[name, "formField"]}
                          placeholder="nom du champ de formulaire"
                          rules={[{required: true}]}
                          formItemProps={{style: {marginBottom: 0, minWidth: 100}}}
                          options={flatFormInputs(project.formComponents).map((formInput) => ({
                            value: formInput.key,
                            label: formInput.label,
                          }))}
                          showSearch
                        />
                        <strong>sera affichée dans la colonne</strong>
                        <InputElement.Text
                          name={[name, "columnName"]}
                          placeholder="nom de la colonne"
                          formItemProps={{style: {marginBottom: 0}}}
                          rules={[{required: true}]}
                        />
                        <InputElement.Select
                          name={[name, "inPdfExport"]}
                          placeholder="Ajouter aux exports PDF ?"
                          formItemProps={{style: {marginBottom: 0}}}
                          options={[
                            {value: false, label: "Ne pas ajouter aux exports PDF"},
                            {value: true, label: "Ajouter aux exports PDF"},
                          ]}
                        />
                      </div>
                    ))}
                    <Form.Item>
                      <Button type="dashed" onClick={() => add()} icon={<PlusOutlined />}>
                        Ajouter un mapping
                      </Button>
                    </Form.Item>
                  </>
                )}
              </Form.List>
            </FormElement>
          ) : (
            <div style={{color: "grey"}}>
              Vous n'avez pas encore créé de questions dans votre formulaire d'inscription. Créez
              des questions ci-dessous pour pouvoir utiliser le mapping.
            </div>
          )}
        </CardElement>

        <CardElement title="Formulaire d'inscription">
          <Alert
            showIcon
            style={{marginBottom: 26}}
            description={
              <>
                <p>
                  Les participant⋅es se créent un compte en renseignant leur{" "}
                  <strong>nom, prénom et email</strong>. Il est donc inutile de leur redemander ces
                  informations dans le formulaire d'inscription.
                </p>
                Aussi, NOÉ n'est pas habilité à héberger les données sensibles des participant⋅es
                (données médicales, notamment). Si pour une raison ou une autre, vous devez en
                collecter, la responsabilité de NOÉ ne pourra être engagée en cas de problème.
              </>
            }
          />
          <LayoutElement.Suspense>
            <FormBuilder
              formRef={formBuilderRef}
              setIsModified={setIsModified}
              delaySaveTipMessage={delaySaveTipMessage}
              messageNotYetDisplayed={messageNotYetDisplayed}
            />
          </LayoutElement.Suspense>
        </CardElement>
      </TabPane>

      <TabPane tab="Page d'accueil" key="welcome-page">
        <LayoutElement.Suspense>
          <WelcomePageEditor value={project.content} onChange={onChangeWelcomePage} />
        </LayoutElement.Suspense>
        <div style={{minHeight: 400}}></div>
      </TabPane>

      <TabPane tab="Membres" key="members" className="with-margins">
        <Members isAdmin={isUserAdminOnThisProject} />
      </TabPane>

      {isUserAdminOnThisProject && (
        <>
          <TabPane tab="Billetterie" key="ticketing" className="with-margins">
            <Ticketing isModified={isModified} setIsModified={setIsModified} />
          </TabPane>

          <TabPane tab="Avancé" key="advanced" className="with-margins">
            <Advanced setIsModified={setIsModified} />
          </TabPane>
        </>
      )}
    </TabsPage>
  );
}
