import React from "react";
import {Alert, Popconfirm} from "antd";
import {projectsActions, projectsSelectors} from "../../features/projects";
import {useDispatch, useSelector} from "react-redux";
import {InputElement} from "../common/InputElement";

import {CardElement} from "../common/LayoutElement";

export const AdditionalFeatures = ({project, toggle, subtitle}) => (
  <CardElement subtitle={subtitle} title="Fonctionnalités supplémentaires">
    <div className="container-grid two-per-row">
      <InputElement.Switch
        tooltip={
          <>
            {project.useAI && <p>Les espaces doivent être activés si vous utilisez l'IA.</p>}
            Quand vous créez des activités et des sessions, dites où elles ont lieu, et profitez de
            l'intelligence de NOÉ et son système de jauges de personnes.
          </>
        }
        disabled={project.useAI}
        onChange={() => toggle("usePlaces")}
        checked={project.usePlaces || project.useAI}
        label="Utiliser les espaces"
      />
      <InputElement.Switch
        tooltip="Besoin de ranger vos participant⋅es par groupes de personnes, ou de gérer leurs inscriptions aux différentes sessions par lots ? Les équipes semblent faites pour vous."
        onChange={() => toggle("useTeams")}
        checked={project.useTeams}
        label="Utiliser les équipes"
      />
    </div>
  </CardElement>
);

export function FeaturesFormContent({setIsModified}) {
  const project = useSelector(projectsSelectors.selectEditing);
  const dispatch = useDispatch();

  const registerStarted =
    project.openingState === "registerForStewardsOnly" || project.openingState === "registerForAll";

  const toggle = (key) => {
    setIsModified(true);
    dispatch(projectsActions.changeEditing({[key]: !project[key]}));
  };

  return (
    <>
      <CardElement title="Créer des sessions avec l'IA">
        {registerStarted && (
          <Alert
            style={{marginBottom: 10}}
            message="Les inscriptions ont commencé"
            description="Il est fortement déconseillé d'utiliser l'IA si des personnes se sont inscrites à des sessions, car l'IA peut les supprimer et désinscrire les participant⋅es."
            showIcon
            type="warning"
          />
        )}
        <Alert
          style={{marginBottom: 26}}
          message={
            <>
              L'activation de l'onglet de création de sessions par IA vous donnera accès à{" "}
              <strong>la génération de planning par IA</strong>. Celle-ci n'est pas forcément utile,
              et pourra impacter voire supprimer vos sessions déjà créées.
              <br />
              <strong>
                Il est fortement déconseillé de l'activer si vous ne savez pas ce que vous faites
              </strong>{" "}
              car utiliser la génération automatique effacerait toutes les inscriptions aux
              sessions.
            </>
          }
          type="info"
        />
        <Popconfirm
          title="Si vous ne savez pas ce que vous faites, ne le faites pas."
          onConfirm={() => {
            // If we wanna activate IA but places are not activated, then also activate places
            if (!project.useAI && !project.usePlaces) toggle("usePlaces");
            toggle("useAI");
          }}
          okText={
            project.useAI
              ? "Désactiver l'IA ? (Conseillé)"
              : "Je suis sûr⋅e et j'ai conscience de la dangerosité potentielle de l'IA"
          }
          okButtonProps={{danger: !project.useAI}}
          cancelText="Annuler">
          <InputElement.Switch
            initialValue={false}
            formItemProps={{style: {maxWidth: 150}}}
            checked={project.useAI}
            label="Activation de l'IA"
          />
        </Popconfirm>
      </CardElement>
      <AdditionalFeatures
        subtitle={
          <Alert
            style={{marginBottom: 26}}
            message="Vous devez recharger la page lors de la modification de ces fonctionnalités."
            description="L'application a besoin de charger de nouvelles données une fois ces changements effectuées. Rechargez la page pour éviter les bugs."
            showIcon
            type="warning"
          />
        }
        project={project}
        toggle={toggle}
      />
    </>
  );
}
