import React from "react";
import {Alert, Button, Form, Popconfirm, Tabs} from "antd";
import {DeleteOutlined, WarningOutlined} from "@ant-design/icons";
import {projectsActions, projectsSelectors} from "../../features/projects";
import {useDispatch, useSelector} from "react-redux";
import {InputElement} from "../common/InputElement";
import {FormElement} from "../common/FormElement";
import {ImportExport} from "./ImportExport";
import {FeaturesFormContent} from "./FeaturesFormContent";
import {Ticketing} from "./Ticketing";
import {ModificationsHistoryTimeline} from "../common/EditPage";

const {TabPane} = Tabs;

export function Advanced({setIsModified}) {
  const project = useSelector(projectsSelectors.selectEditing);
  const dispatch = useDispatch();
  const [rollbackForm] = Form.useForm();

  const rollback = () => {
    dispatch(projectsActions.rollback(rollbackForm.getFieldValue("rollbackPeriod")));
  };

  const deleteProject = () => {
    dispatch(projectsActions.remove());
  };

  return (
    <Tabs>
      <TabPane tab="Export & Import" key="export-import">
        <ImportExport />
      </TabPane>
      <TabPane tab="Fonctionnalités" key="features">
        <FormElement>
          <FeaturesFormContent setIsModified={setIsModified} />s
        </FormElement>
      </TabPane>
      <TabPane tab="Oops!" key="rollback">
        <FormElement form={rollbackForm}>
          <Alert
            style={{marginBottom: 10}}
            message="Fonctionnalité expérimentale."
            description="Cela peut faire dysfonctionner votre projet. Le résultat n'est pas garanti."
            showIcon
            type="warning"
          />
          <Alert
            style={{marginBottom: 10}}
            message="Ici, vous pouvez récupérer des données supprimées. Il suffit de mettre jusqu'à combien de minutes en arrière vous souhaitez retourner."
            type="info"
          />
          <InputElement.Number
            min={0}
            name="rollbackPeriod"
            defaultValue={0}
            placeholder="minutes"
            label="Nombre de minutes à revenir en arrière"
          />
          <Popconfirm
            title="Si vous ne savez pas ce que vous faites, ne le faites pas."
            onConfirm={rollback}
            okText="Je suis conscient⋅e des risques"
            okButtonProps={{danger: true}}
            cancelText="Annuler">
            <Button type="primary" danger>
              Récupérer les éléments supprimés
            </Button>
          </Popconfirm>
        </FormElement>
      </TabPane>
      <TabPane tab="Historique de modification" key="history">
        <ModificationsHistoryTimeline
          elementsActions={projectsActions}
          record={project}
          root=".."
        />
      </TabPane>
      <TabPane tab="Suppression du projet" key="delete">
        <p style={{marginTop: 10}}>
          <WarningOutlined /> Attention, <strong>la suppression du projet est irréversible</strong>{" "}
          et engendrera également la suppression des catégories, espaces, encadrant⋅es, activités et
          sessions liés au projet.
        </p>
        <Popconfirm
          title={"Ëtes-vous sûr de vouloir supprimer l'entièreté du projet " + project.name + " ?"}
          onConfirm={deleteProject}
          okText="Oui, je veux définitivement supprimer le projet"
          cancelText="Non, je veux revenir en arrière">
          <Button type="primary" danger icon={<DeleteOutlined />} style={{marginTop: "10px"}}>
            Supprimer le projet
          </Button>
        </Popconfirm>
      </TabPane>
    </Tabs>
  );
}
