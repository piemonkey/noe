import React, {useCallback, useEffect, useState} from "react";
import {Alert, Avatar, Button, Form, Modal, Popconfirm, Tag, Tooltip} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {TableElement} from "../common/TableElement";
import {listSorter} from "../../helpers/listUtilities";
import {FormElement} from "../common/FormElement";
import {InputElement} from "../common/InputElement";
import {fieldToData} from "../../helpers/tableUtilities";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {CardElement} from "../common/LayoutElement";
import {roleTag, ROLES_MAPPING} from "../../helpers/tableUtilities";
import {debounce} from "../../helpers/viewUtilities";
import {fetchWithMessages} from "../../helpers/reduxUtilities";
import {MailOutlined, UserOutlined} from "@ant-design/icons";
import {personName} from "../../helpers/utilities";
import {stewardsActions, stewardsSelectors} from "../../features/stewards";
import {WaitingInvitationTag} from "../../helpers/registrationsUtilities";

const columns = [
  {
    title: "Prénom",
    dataIndex: "firstName",
    render: (text, record) => record.user.firstName,
    sorter: (a, b) => listSorter.text(a.user.firstName, b.user.firstName),
    searchable: true,
    searchText: (record) => record.user.firstName,
  },
  {
    title: "Nom",
    dataIndex: "lastName",
    render: (text, record) => record.user.lastName,
    sorter: (a, b) => listSorter.text(a.user.lastName, b.user.lastName),
    defaultSortOrder: "ascend",
    searchable: true,
    searchText: (record) => record.user.lastName,
  },
  {
    title: "Email",
    dataIndex: "email",
    render: (text, record) => (
      <>
        {record.invitationToken && <WaitingInvitationTag />}
        {record.user.email}
      </>
    ),
    searchable: true,
    searchText: (record) => record.user.email,
  },
  {
    title: "Droits",
    dataIndex: "role",
    sorter: (a, b) => listSorter.text(a.role, b.role),
    render: (text) => roleTag(text),
  },
];

export const useRegistrationEditModal = (
  registrationsEligibleToModification,
  newRegistrationTemplate = {}
) => {
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const [showModalRegistrationMode, setShowModalRegistrationMode] = useState(); // edit | new | undefined (=closed)

  // Save the current edited registration in the Redux "editing"
  const setSelectedRegistration = (value) => dispatch(registrationsActions.setEditing(value));

  ////// MODAL ACTIONS //////

  // Called to display the modal to create/add a new registration/invitation
  const onNewRegistration = () => {
    setSelectedRegistration(newRegistrationTemplate);
    form.setFieldsValue(newRegistrationTemplate);
    setShowModalRegistrationMode("new");
  };

  // Called to display the modal to modify an existing registration/invitation
  const onEditRegistration = (record) => {
    const registrationToBeEdited = {
      ...record,
      steward: record.steward?._id,
    };
    setSelectedRegistration(registrationToBeEdited);
    form.setFieldsValue(registrationToBeEdited);
    setShowModalRegistrationMode("edit");
  };

  ////// MODAL COMPONENT //////

  const RegistrationEditModal = ({children, persistRegistration, addNewRegistrationTitle}) => {
    const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

    // The registration/invitation that is being edited
    const selectedRegistration = useSelector(registrationsSelectors.selectEditing);

    // Stuff about the current user
    const isAdmin = currentRegistration.role === "admin";
    const userIsModifyingItsOwnRegistration = selectedRegistration._id === currentRegistration._id;

    // If the user iss undefined, it means that the email given is not correct, so we haven't earched for a user yet
    const emailSearchIsCorrect = selectedRegistration?.user !== undefined;
    // If the email is correct and the user is not equl to false, then it means that a user already exists for this email
    const userAlreadyExists = emailSearchIsCorrect && selectedRegistration?.user !== false;

    // Called by the FormElement when a change occurs
    const onChangeSelectedRegistration = (changedFields, allFields) => {
      const data = fieldToData(allFields);
      setSelectedRegistration({...selectedRegistration, ...data});
    };

    // Modal actions
    const registrationModalButtons = {
      closeModal: async () => {
        await setSelectedRegistration({});
        setShowModalRegistrationMode(undefined);
        // Reset fields only at the end because otherwise it will take the selectedRegistration values again
        form.resetFields();
      },
      ok: () =>
        form.validateFields().then((r) => {
          persistRegistration(selectedRegistration);
          registrationModalButtons.closeModal();
        }),
      sendInvitationEmail: () => {
        form.validateFields().then((r) => {
          dispatch(
            registrationsActions.sendInvitationEmail(
              {
                email: selectedRegistration.searchedEmail,
                firstName: selectedRegistration.firstName,
                lastName: selectedRegistration.lastName,
              },
              {role: selectedRegistration.role, steward: selectedRegistration.steward}
            )
          );
          registrationModalButtons.closeModal();
        });
      },
      removeRole: () => {
        persistRegistration({...selectedRegistration, role: null});
        registrationModalButtons.closeModal();
      },
      removeEntireRegistration: () => {
        dispatch(registrationsActions.remove(selectedRegistration._id));
        registrationModalButtons.closeModal();
      },
    };

    ////// USER SEARCH BY EMAIL //////

    const debouncedOnChangeUserEmail = useCallback(
      debounce(async (searchedEmail) => {
        const emailErrors = form.getFieldError(["searchedEmail"]);
        // If input is not a valid email, set user to "undefined"
        if (emailErrors.length > 0) {
          setSelectedRegistration({...selectedRegistration, user: undefined});
          return;
        }

        // Else, try to find if the email belongs to a user or not
        try {
          // Find a user already registered to the project
          let foundRegistration = registrationsEligibleToModification.find(
            (r) => r.user.email === searchedEmail
          );

          // If not found, then search in all users from the backend, and see if it can be edited
          let user;
          if (!foundRegistration) {
            user = await fetchWithMessages("users/" + searchedEmail, {method: "GET"}, false, false);

            foundRegistration = registrationsEligibleToModification.find(
              (r) => r.user._id === user._id
            );
          }

          // If we found an eligible user, edit it
          // (the user has an account, or at least an invitation pending in this project)
          if (foundRegistration) {
            onEditRegistration(foundRegistration);
          } else {
            // The user has an account, maybe has registration, but doesn't belong to the editable users.
            // We only set the registration in the Redux store
            setSelectedRegistration({...selectedRegistration, user});
          }
        } catch {
          // If the searched email is valid, but no user found in the whole NOÉ database
          // then initialize a new registration, with user=false as a way of telling that we haven't found a matching user.
          setSelectedRegistration({...selectedRegistration, searchedEmail, user: false});
        }
      }, 400),
      [JSON.stringify(registrationsEligibleToModification)]
    );

    const RegistrationCard = ({style}) => (
      <>
        {emailSearchIsCorrect && (
          <CardElement size="small" style={style}>
            {userAlreadyExists ? (
              // If a user is found, show a pretty avatar stuff to tell "hey, I recognized this user, look !"
              <>
                <Avatar
                  style={{marginRight: 10}}
                  className="bg-noe-gradient"
                  icon={<UserOutlined />}
                />
                {personName(selectedRegistration.user)} ({selectedRegistration.user.email}){" "}
                {selectedRegistration.invitationToken && <WaitingInvitationTag />}
              </>
            ) : (
              // If no existing user is found ==> ask for invitation email
              "Pas d'utilisateur⋅ice trouvé⋅e."
            )}
          </CardElement>
        )}
      </>
    );

    return (
      <Modal
        title={
          showModalRegistrationMode === "new"
            ? addNewRegistrationTitle
            : userIsModifyingItsOwnRegistration
            ? "Modifier vos droits"
            : selectedRegistration.invitationToken
            ? `Modifier l'invitation pour ${selectedRegistration.user.email}`
            : `Modifier les droits de ${personName(selectedRegistration.user)}`
        }
        onCancel={registrationModalButtons.closeModal}
        visible={showModalRegistrationMode}
        footer={
          <>
            {/* Delete registration button + remove rights buttons. Visible when:
                 - registration exists (not only an invitation)
                 - current user is admin, and the modified registration is not its own registration */}
            {isAdmin &&
              !selectedRegistration.invitationToken &&
              !userIsModifyingItsOwnRegistration &&
              showModalRegistrationMode === "edit" && [
                // Delete registration button
                <Popconfirm
                  title={
                    <>
                      <p>
                        <strong style={{color: "red"}}>
                          Ce bouton ne doit servir qu'à des fins de déboguage.
                        </strong>
                      </p>
                      <p>
                        La personne sera totalement désinscrite de l'événement et ne pourra plus y
                        accéder.
                        <br />
                        Si la personne est inscrite en tant que participant⋅e,{" "}
                        <strong>elle perdra toute son inscription</strong>.
                      </p>
                      <p>
                        Cette suppression est <strong>irréversible</strong>.
                      </p>
                    </>
                  }
                  okText="Oui, je désinscris cette personne"
                  okButtonProps={{danger: true}}
                  cancelText="Annuler"
                  onConfirm={registrationModalButtons.removeEntireRegistration}>
                  <Button style={{opacity: 0.5}} danger>
                    Supprimer l'inscription
                  </Button>
                </Popconfirm>,

                // Remove rights button
                <Tooltip title="La personne perdra ses droits existants et redeviendra un⋅e simple participant⋅e au projet.">
                  <Button danger onClick={registrationModalButtons.removeRole}>
                    Retirer les droits
                  </Button>
                </Tooltip>,
              ]}

            {/* Cancel button */}
            <Button onClick={registrationModalButtons.closeModal}>Annuler</Button>

            {/* Remove invitation button --> only if an invitation token is found */}
            {selectedRegistration.invitationToken && (
              <Button danger onClick={registrationModalButtons.removeEntireRegistration}>
                Supprimer l'invitation
              </Button>
            )}

            {/* Validate button --> if user exists, or anytime
            when the user has not given a correct email address yet */}
            {userAlreadyExists || !emailSearchIsCorrect ? (
              <>
                {userIsModifyingItsOwnRegistration ? (
                  <Popconfirm
                    title="Vous êtes en train de modifier vos propres droits. Êtes-vous sûr⋅e ?"
                    okText="Oui, je modifie mes droits"
                    okButtonProps={{danger: true}}
                    cancelText="Annuler"
                    onConfirm={registrationModalButtons.ok}>
                    <Button type="primary" danger>
                      OK
                    </Button>
                  </Popconfirm>
                ) : (
                  <Button type="primary" onClick={registrationModalButtons.ok}>
                    OK
                  </Button>
                )}
              </>
            ) : (
              <Button
                type="primary"
                className="bounce-in"
                icon={<MailOutlined />}
                onClick={registrationModalButtons.sendInvitationEmail}>
                Envoyer une invitation par email
              </Button>
            )}
          </>
        }>
        {/*
            Beginning of modal content
        */}
        {/* Info alert at the top of the modal */}
        {!userIsModifyingItsOwnRegistration && (
          <Alert
            style={{marginBottom: 26}}
            showIcon
            icon={<MailOutlined />}
            message={
              showModalRegistrationMode === "new" ? (
                <>
                  Saisissez l'email de la personne à inviter. Si la personne n'est pas inscrite sur
                  NOÉ, vous pouvez lui envoyer une invitation par email.
                </>
              ) : (
                <>
                  Lorsque vous donnez, modifiez ou retirez des droits, les personnes concernées
                  reçoivent automatiquement un email.
                </>
              )
            }
          />
        )}

        <FormElement form={form} onFieldsChange={onChangeSelectedRegistration}>
          <CardElement>
            <div className="container-grid">
              {/* Email search input in "new" mode */}
              {showModalRegistrationMode === "new" ? (
                <>
                  <InputElement.Text
                    label="Email"
                    name="searchedEmail"
                    rules={[{required: true}, {type: "email"}]}
                    onChange={(e) => debouncedOnChangeUserEmail(e.target.value)}
                    disabled={showModalRegistrationMode === "edit"}
                    readOnly={showModalRegistrationMode === "edit"}
                    placeholder="email"
                  />
                  <RegistrationCard style={{marginTop: -22, marginBottom: 15}} />
                </>
              ) : (
                // Or if "edit" mode, just display the user
                <InputElement.Custom label="Utilisateur⋅ice">
                  <RegistrationCard style={{marginBottom: 0}} />
                </InputElement.Custom>
              )}

              {emailSearchIsCorrect && (
                <div className="fade-in container-grid">
                  {!userAlreadyExists && showModalRegistrationMode === "new" && (
                    <>
                      <InputElement.Text
                        label="Prénom (optionnel)"
                        name="firstName"
                        placeholder="prénom"
                      />
                      <InputElement.Text
                        label="Nom (optionnel)"
                        name="lastName"
                        placeholder="nom"
                      />
                    </>
                  )}

                  {children}
                </div>
              )}
            </div>
          </CardElement>
        </FormElement>
      </Modal>
    );
  };

  return [onNewRegistration, onEditRegistration, RegistrationEditModal];
};

export function Members({isAdmin}) {
  const dispatch = useDispatch();

  const registrations = useSelector(registrationsSelectors.selectList);
  const invitedRegistrations = registrations.filter((r) => r.invitationToken);
  const stewards = useSelector(stewardsSelectors.selectList);
  const projectMembersRegistrations = registrations.filter((r) => r.role);

  const [onNewRegistration, onEditRegistration, RegistrationEditModal] = useRegistrationEditModal(
    projectMembersRegistrations
  );

  const persistRegistration = (selectedRegistration) => {
    dispatch(
      registrationsActions.persist({
        _id: registrations.find((r) => r.user._id === selectedRegistration.user._id)?._id || "new", // If no id found, it means it's a new one
        user: selectedRegistration.user._id,
        role: selectedRegistration.role,
        steward: selectedRegistration.steward,
      })
    );
  };

  ////// ASYNC LOAD //////

  useEffect(() => {
    dispatch(registrationsActions.loadList());
    dispatch(stewardsActions.loadList());
  });

  return (
    <>
      <TableElement.WithTitle
        title="Membres du projet"
        showHeader
        columns={columns}
        onEdit={isAdmin && onEditRegistration}
        dataSource={projectMembersRegistrations.filter((r) => !r.invitationToken)}
        buttonTitle={isAdmin ? "Ajouter de nouveaux membres" : undefined}
        onClickButton={isAdmin && onNewRegistration}
      />

      {invitedRegistrations.length > 0 && (
        <TableElement.WithTitle
          title="Invitations en attente"
          showHeader
          columns={[
            ...columns,
            {
              title: "Encadrant⋅e associé⋅e",
              dataIndex: "steward",
              render: (text, record) =>
                personName(stewards.find((s) => s._id === record?.steward?._id)),
            },
          ]}
          onEdit={isAdmin && onEditRegistration}
          dataSource={invitedRegistrations}
        />
      )}

      <CardElement title="Échelle des droits" className="mt-4">
        {ROLES_MAPPING.map((role) => (
          <p>
            <span>{roleTag(role.value)}</span>
            {role.explanation}
          </p>
        ))}
      </CardElement>

      <RegistrationEditModal
        persistRegistration={persistRegistration}
        addNewRegistrationTitle="Ajouter de nouveaux droits">
        <InputElement.Select
          rules={[atLeastOneRequired("role", "steward")]}
          label="Droits"
          name="role"
          placeholder="droits"
          options={ROLES_MAPPING}
        />

        <InputElement.Select
          rules={[atLeastOneRequired("role", "steward")]}
          label="Associer un.e encadrant.e"
          name="steward"
          placeholder="encadrant.e"
          options={stewards.map((steward) => ({label: personName(steward), value: steward._id}))}
          showSearch
        />
      </RegistrationEditModal>
    </>
  );
}

const atLeastOneRequired =
  (...fields) =>
  ({getFieldValue}) => ({
    validator(_, value) {
      const atLeastOneFilled = fields.find((field) => getFieldValue(field)?.length > 0);
      if (!atLeastOneFilled) return Promise.reject(`Au moins un des champs doit être rempli.`);

      return Promise.resolve();
    },
  });
