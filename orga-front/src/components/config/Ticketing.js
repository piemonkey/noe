import React, {useMemo} from "react";
import {Alert} from "antd";
import {projectsActions, projectsSelectors} from "../../features/projects";
import {useDispatch, useSelector} from "react-redux";
import {InputElement} from "../common/InputElement";
import {FormElement} from "../common/FormElement";
import {addKeyToItemsOfList} from "../../helpers/tableUtilities";
import {TableElement} from "../common/TableElement";
import {listSorter} from "../../helpers/listUtilities";
import moment from "moment";
import Paragraph from "antd/es/typography/Paragraph";
import {CardElement} from "../common/LayoutElement";
import {URLS} from "../../app/configuration";

const columnsHelloAssoEvents = [
  {
    title: "Nom de l'événement",
    dataIndex: "title",
    sorter: (a, b) => listSorter.text(a.title, b.title),
    defaultSortOrder: "descend",
    width: 500,
  },
  {
    title: "Début",
    dataIndex: "startDate",
    sorter: (a, b) => listSorter.date(a.startDate, b.startDate),
    render: (text, record) => (text !== undefined ? moment(text).format("LL") : text),
    width: 170,
  },
  {
    title: "Description",
    dataIndex: "description",
    width: 1000,
  },
];

export function Ticketing({isModified, setIsModified}) {
  const dispatch = useDispatch();
  const project = useSelector(projectsSelectors.selectEditing);
  const ticketingIsSetUp = {
    helloAsso:
      project.helloAsso?.selectedEvent &&
      project.helloAsso?.clientId &&
      project.helloAsso?.clientSecret,
    tiBillet: project.tiBillet?.serverUrl && project.tiBillet?.apiKey,
    customTicketing: project.customTicketing?.length > 0,
  };
  const ticketingIsOperational = project.ticketingMode && ticketingIsSetUp[project.ticketingMode];

  const setHelloAssoEvent = (selectedRowObject) => {
    dispatch(
      projectsActions.changeEditing({
        helloAsso: {...project.helloAsso, selectedEvent: selectedRowObject[0].key},
      })
    );
    setIsModified(true);
  };

  const onTicketingValuesConfigChange = (_, allValues) => {
    dispatch(projectsActions.changeEditing(allValues));
    setIsModified(true);
  };

  const rowSelectionHelloAssoEvent = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setHelloAssoEvent(selectedRowObject);
    },
    type: "radio",
  };

  const TicketingCardElement = useMemo(
    () =>
      ({ticketingMode, children, ...props}) =>
        project.ticketingMode === ticketingMode ? (
          <CardElement {...props} className="fade-in" style={{marginBottom: 0, marginTop: 16}}>
            <FormElement initialValues={project} onValuesChange={onTicketingValuesConfigChange}>
              {children}
            </FormElement>
          </CardElement>
        ) : null,
    [project?.ticketingMode]
  );

  return (
    <>
      {ticketingIsOperational && !isModified && (
        <Alert
          style={{marginBottom: 26}}
          message="La billetterie est activée."
          showIcon
          type="success"
        />
      )}

      <CardElement>
        <div className="container-grid">
          <FormElement initialValues={project} onValuesChange={onTicketingValuesConfigChange}>
            <InputElement.Select
              label="Type de billetterie"
              name="ticketingMode"
              options={[
                {value: null, label: "- Aucune -"},
                {value: "tiBillet", label: "TiBillet"},
                {value: "helloAsso", label: "Hello Asso"},
                {value: "customTicketing", label: "Validation par appel d'une URL"},
              ]}
            />
          </FormElement>

          <TicketingCardElement title="TiBillet" ticketingMode="tiBillet">
            <div className="container-grid three-per-row">
              <InputElement.Text
                label="URL de la billetterie TiBillet"
                name={["tiBillet", "serverUrl"]}
                placeholder="https://url-de-la-billetterie-tibillet.org"
                rules={[{type: "url"}]}
              />
              <InputElement.Text
                label="Slug de l'événement"
                name={["tiBillet", "eventSlug"]}
                placeholder="mon-evenemement-102123-xxxx"
              />
              <InputElement.Password
                label="Clé API"
                name={["tiBillet", "apiKey"]}
                placeholder="clé api"
              />
            </div>
          </TicketingCardElement>

          <TicketingCardElement
            title="Validation par appel d'une URL"
            ticketingMode="customTicketing">
            <Alert
              style={{marginBottom: 26}}
              message="Vous pouvez valider n'importe quel type de numéro de billet avec NOÉ."
              description={
                <>
                  <p>
                    NOÉ peut appeler une URL particulière pour valider des numéros de billets en
                    tous genres rentrés par les participant⋅es.
                  </p>
                  <Paragraph ellipsis={{rows: 3, expandable: true}}>
                    <p>La requête envoyée sera la suivante :</p>
                    <ul>
                      <li>
                        <strong>Type :</strong> POST
                      </li>
                      <li>
                        <strong>URL de destination :</strong> {project.customTicketing || " -"}
                      </li>
                      <li>
                        <strong>Corps de la requête :</strong>
                        <pre>{'{ ticketId: "XXXXXXX" }'}</pre>
                      </li>
                    </ul>
                    <p>
                      Toute réponse avec un <strong>code d'état HTTP égal à 200</strong> sera
                      interprété comme une validation du numéro de billet. Toute code d'état autre
                      que 200 sera interprétée comme une invalidation.
                    </p>
                    <p>Le corps de la réponse peut contenir deux arguments "name" et "amount" :</p>
                    <pre>
                      {"{"} name: <i>string</i>, amount: <i>number</i> {"}"}
                    </pre>
                    <p>
                      Vous pouvez faire des tests avec l'url suivante, qui validera n'importe quel
                      numéro de billet, sauf si l'indentifiant du billet vaut "
                      <span style={{fontFamily: "monospace"}}>fail</span>" :
                    </p>
                    <a href={`${URLS.API}/projects/${project._id}/ticketing/test`}>
                      {URLS.API}/projects/{project._id}/ticketing/test
                    </a>
                  </Paragraph>
                </>
              }
              type="info"
            />
            <InputElement.Text
              label="URL de validation des billets"
              name="customTicketing"
              placeholder="https://mon-url-de-validation-de-ticket.com"
              rules={[{type: "url"}]}
            />
          </TicketingCardElement>

          <TicketingCardElement title="Hello Asso" ticketingMode="helloAsso">
            {!project.helloAsso?.selectedEvent &&
              project.helloAsso?.organizationSlug &&
              !isModified && (
                <Alert
                  type="warning"
                  showIcon
                  style={{marginBottom: 26}}
                  className="bounce-in"
                  message="Plus qu'une étape pour finir la configuration !"
                  description="Veillez sélectionner une billetterie Hello Asso avec laquelle lier votre événement, puis Enregistrer les modifications."
                />
              )}
            {project.helloAsso?.organizationSlug && (
              <Alert
                showIcon
                style={{marginBottom: 26}}
                className="bounce-in"
                message="Envoyez des invitations automatiques à vos participant⋅es !"
                description={
                  <>
                    <p>
                      Vous pouvez demander à NOÉ d'envoyer un email d'invitation à toutes les
                      personnes qui ont pris un billet sur votre événement Hello Asso. Cet email
                      leur permet de s'inscrire à NOÉ très facilement, car tous les champs sont déjà
                      pré-remplis, et le numéro de billet est automatiquement ajouté lors de
                      l'inscription.
                    </p>
                    <p>
                      Pour cela, accédez à votre espace Hello Asso, puis allez dans l'onglet "Mon
                      Compte" > "Intégrations et API". Enfin, copiez-collez l'URL ci-dessous dans la
                      partie "Notifications".
                    </p>
                    <a
                      href={`${URLS.API}/projects/${project._id}/ticketing/sendInvitationEmailAfterHelloAssoOrder`}>
                      {URLS.API}/projects/{project._id}
                      /ticketing/sendInvitationEmailAfterHelloAssoOrder
                    </a>
                  </>
                }
              />
            )}
            <div className="container-grid two-per-row">
              <InputElement.Password
                label="Client Id"
                name={["helloAsso", "clientId"]}
                placeholder="client id"
              />
              <InputElement.Password
                label="Client Secret"
                name={["helloAsso", "clientSecret"]}
                placeholder="client secret"
              />
            </div>
            {project.helloAsso?.organizationSlug && (
              <>
                <div style={{marginBottom: 26}}>
                  <strong>Organisation trouvée :</strong> {project.helloAsso?.organizationSlug}
                </div>
                <TableElement.Simple
                  showHeader
                  scroll={{x: (columnsHelloAssoEvents.length - 1) * 160 + 70}}
                  rowSelection={rowSelectionHelloAssoEvent}
                  selectedRowKeys={[{key: project.helloAsso?.selectedEvent}]}
                  setSelectedRowKeys={setHelloAssoEvent}
                  columns={columnsHelloAssoEvents}
                  rowKey="formSlug"
                  dataSource={addKeyToItemsOfList(
                    project.helloAsso?.selectedEventCandidates,
                    "formSlug"
                  )}
                />
              </>
            )}
          </TicketingCardElement>
        </div>
      </CardElement>
    </>
  );
}
