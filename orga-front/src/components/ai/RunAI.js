import React from "react";
import {useSelector, useDispatch} from "react-redux";
import {Button, Tabs, Popconfirm, Card, List, Alert} from "antd";
import {TabsPage} from "../common/TabsPage";
import {computingActions, computingSelectors} from "../../features/computing";
import {LayoutElement} from "../common/LayoutElement";

const {TabPane} = Tabs;

const messageTitre = "Attention, vous risquez de supprimer vos données.";
const messageInfo =
  "Générer un planninng implique de créer de nouvelles sessions basées sur les activités créées et les informations associées. Cela supprime également les sessions non figées. Si vous souhaitez générer un planning assurer de figer les sessions que vous voulez garder.";

function AIDisplay() {
  const queueLength = useSelector(computingSelectors.selectQueueLength);
  const queuePosition = useSelector(computingSelectors.selectQueuePosition);
  const state = useSelector(computingSelectors.selectComputationState);
  const computationId = useSelector(computingSelectors.selectComputationId);
  let status = "active";
  if (state === "SUCCESS") {
    status = "success";
  } else if (state === "FAILED") {
    status = "exception";
  }
  if (computationId) {
    return (
      <div className="container-grid col-3">
        <span>État de votre calcul:</span>

        <LayoutElement.Progress
          length={queueLength}
          position={queuePosition}
          status={status}
          state={state}></LayoutElement.Progress>
      </div>
    );
  }
  return null;
}

function AIResult(props) {
  const result = props.result;
  const nbIdSkipped = result?.skippedId?.length;
  if (nbIdSkipped === 0) {
    return <h4>result.conclusion</h4>;
  } else if (result.conclusion === "Solution complète trouvée" && nbIdSkipped > 0) {
    return (
      <>
        <h4>Le calcul a été effectué sans problème !</h4>
        <p>
          Mais {nbIdSkipped} sessions n'ont pas été placées. Voir l'onglet{" "}
          <i>Sessions non placées</i> pour plus de détails.
        </p>
      </>
    );
  } else {
    return null;
  }
}

function AISessionsNotPlaced(props) {
  const result = props.result;
  const data = [];
  if (result) {
    for (const skipped of result?.skippedId) {
      for (const training of result?.data?.training) {
        if (training.key === skipped.id.slice(0, -3)) {
          data.push({
            name: training.title,
            id: training.key,
            numberSession: skipped.id.slice(-2, -1),
            reason: skipped.reason,
          });
        }
      }
    }
  }

  if (data.length > 0) {
    return (
      <>
        <h3>Sessions non placées</h3>
        <List
          itemLayout="horizontal"
          dataSource={data}
          renderItem={(item) => (
            <List.Item>
              <List.Item.Meta
                title={'Activité "' + item.name + '" - Numéro de session : ' + item.numberSession}
                description={item.reason}
              />
            </List.Item>
          )}
        />
      </>
    );
  }
  return null;
}

function AIDetails(props) {
  return (
    <>
      <h3>Informations sur le calcul</h3>
      <Card style={{backgroundColor: "#eee"}}>
        <pre>{JSON.stringify(props.result, null, 2)}</pre>
      </Card>
    </>
  );
}

export function RunAI() {
  const result = useSelector(computingSelectors.selectAIResult);
  const dispatch = useDispatch();

  return (
    <TabsPage title="Créations de sessions par IA">
      <TabPane tab="Lancement" key="run">
        <Alert
          style={{flexGrow: 100, border: "none"}}
          message={messageTitre}
          description={messageInfo}
          showIcon
          type="warning"
        />

        <div className="container-grid col-3">
          <Popconfirm
            title="Regénérer un planning va effacer toutes les sessions existantes qui n'ont pas été figées."
            onConfirm={() => dispatch(computingActions.runIA())}
            okText="Oui, je veux générer le planning"
            okButtonProps={{danger: true}}
            cancelText="Annuler">
            <Button type="danger">Générer le planning</Button>
          </Popconfirm>
        </div>
        <AIDisplay />
        {result && <AIResult result={result} />}
      </TabPane>

      <TabPane tab="Sessions non placées" key="unmatched-sessions">
        <AISessionsNotPlaced result={result} />
      </TabPane>
      <TabPane tab="Détails du calcul" key="details">
        <AIDetails result={result} />
      </TabPane>
    </TabsPage>
  );
}
