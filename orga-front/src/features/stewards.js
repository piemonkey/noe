import {createSlice} from "@reduxjs/toolkit";
import {
  createAvailabilitySlot,
  updateAvailabilitySlot,
  removeAvailabilitySlot,
  replaceAllAvailabilitySlots,
} from "./availability";
import {
  fetchWithMessages,
  loadEntityFromBackend,
  loadListFromBackend,
  persistEntityInBackend,
  removeEntityInBackend,
} from "../helpers/reduxUtilities";
import {sessionsActions} from "./sessions";
import {activitiesActions} from "./activities";
import {message} from "antd";
import {OFFLINE_MODE} from "../helpers/offlineModeUtilities";

export const stewardsSlice = createSlice({
  name: "stewards",
  initialState: {
    list: [],
    init: false,
    editing: {},
    cloning: undefined,
  },
  reducers: {
    addToList: (state, action) => {
      state.list = [action.payload, ...state.list];
    },
    updateInList: (state, action) => {
      state.list = [action.payload, ...state.list.filter((i) => i._id !== action.payload._id)];
    },
    initContext: (state, action) => {
      state.init = action.payload;
    },
    initList: (state, action) => {
      state.init = action.payload.project;
      state.list = action.payload.list;
    },
    changeEditing: (state, action) => {
      state.editing = {
        ...state.editing,
        ...action.payload,
      };
    },
    setEditing: (state, action) => {
      state.editing = action.payload;
    },
    removeAvailabilitySlotToEditing: removeAvailabilitySlot,
    createAvailabilitySlotToEditing: createAvailabilitySlot,
    updateAvailabilitySlotToEditing: updateAvailabilitySlot,
    replaceAllAvailabilitySlots: replaceAllAvailabilitySlots,
  },
});

const asyncActions = {
  loadList: () => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await loadListFromBackend(
      "stewards",
      projectId,
      state.stewards.init,
      () => dispatch(stewardsActions.initContext(projectId)),
      (data) => dispatch(stewardsActions.initList({list: data, project: projectId}))
    );
  },
  loadEditing: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    return loadEntityFromBackend(
      "stewards",
      entityId,
      projectId,
      state.stewards.editing,
      () => dispatch(stewardsActions.setEditing({_id: "new"})),
      (data) => dispatch(stewardsActions.setEditing(data)),
      {
        navigateBackIfFail: !OFFLINE_MODE,
        notFoundAction: () =>
          OFFLINE_MODE &&
          dispatch(stewardsActions.setEditing(state.stewards.list.find((s) => s._id === entityId))),
      }
    );
  },
  persist: (fieldsToUpdate?: any) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id || fieldsToUpdate.project; // If no project id, fll back on the fields given

    // If some fields are given as argument, directly take this to update the registration
    const payload = fieldsToUpdate || state.stewards.editing;

    return persistEntityInBackend(
      "stewards",
      {...payload, project: projectId},
      projectId,
      (data) => dispatch(stewardsActions.addToList(data)),
      (data) => {
        dispatch(stewardsActions.updateInList(data));
        // Update all dependencies next time
        dispatch(sessionsActions.initContext(false));
        dispatch(activitiesActions.initContext(false));
      }
    );
  },
  remove: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await removeEntityInBackend(
      "stewards",
      entityId,
      projectId,
      state.stewards.list,
      (newStewardsList) =>
        dispatch(
          stewardsActions.initList({
            list: newStewardsList,
            project: state.stewards.init,
          })
        )
    );
  },
  loadEditingHistory: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    !OFFLINE_MODE &&
      (await loadEntityFromBackend(
        "stewards",
        `${entityId}/history`,
        projectId,
        state.stewards.editing,
        null,
        (data) => dispatch(stewardsActions.changeEditing({history: data}))
      ));
  },
  getPdfPlanning: (id) => async (dispatch, getState) => {
    const state = getState();

    if (id === "all")
      message.loading({
        key: "fetchWithMessagesKey",
        content:
          "En fonction du nombre de plannings à générer, cela peut prendre jusqu'à une minute ou deux. Soyez patient⋅e :) Si c'est trop long, il se peut que la génération échoue.",
        duration: 8,
      });

    id = id || state.stewards.editing._id;
    try {
      return await fetchWithMessages(
        `projects/${state.currentProject.project._id}/pdf/stewardPlanning/${id}`,
        {isBlob: true, method: "GET"},
        {},
        false,
        true,
        id === "all" ? 7000 : 0
      );
    } catch {
      return Promise.reject();
    }
  },
};

export const stewardsSelectors = {
  selectList: (state) => state.stewards.list,
  selectEditing: (state) => state.stewards.editing,
};

export const stewardsReducer = stewardsSlice.reducer;

export const stewardsActions = {
  ...stewardsSlice.actions,
  ...asyncActions,
};
