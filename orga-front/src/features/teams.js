import {createSlice} from "@reduxjs/toolkit";
import {
  loadEntityFromBackend,
  loadListFromBackend,
  persistEntityInBackend,
  removeEntityInBackend,
} from "../helpers/reduxUtilities";
import {sessionsActions} from "./sessions";
import {registrationsActions} from "./registrations";
import {OFFLINE_MODE} from "../helpers/offlineModeUtilities";

const reloadUpdatedElement = (
  dispatch,
  projectId,
  sessionsToUpdateIds,
  registrationsToUpdateIds
) => {
  // Update all dependencies next time
  dispatch(sessionsActions.reloadElementsFromList(sessionsToUpdateIds));
  dispatch(registrationsActions.reloadElementsFromList(registrationsToUpdateIds));
};

export const teamsSlice = createSlice({
  name: "teams",
  initialState: {
    list: [],
    init: false,
    editing: {},
    cloning: undefined,
  },
  reducers: {
    addTeam: (state, action) => {
      state.list = [action.payload, ...state.list];
    },
    updateInList: (state, action) => {
      state.list = [action.payload, ...state.list.filter((i) => i._id !== action.payload._id)];
    },
    initContext: (state, action) => {
      state.init = action.payload;
    },
    initList: (state, action) => {
      // console.log('initList',action.payload);
      state.init = action.payload.project;
      state.list = action.payload.list;
    },
    changeEditing: (state, action) => {
      state.editing = {...state.editing, ...action.payload};
    },
    setEditing: (state, action) => {
      state.editing = action.payload;
    },
  },
});

const asyncActions = {
  loadList: () => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await loadListFromBackend(
      "teams",
      projectId,
      state.teams.init,
      () => dispatch(teamsActions.initContext(projectId)),
      (data) => dispatch(teamsActions.initList({list: data, project: projectId}))
    );
  },
  loadEditing: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    return loadEntityFromBackend(
      "teams",
      entityId,
      projectId,
      state.teams.editing,
      () =>
        dispatch(
          teamsActions.setEditing({
            _id: "new",
            stewards: [],
            places: [],
          })
        ),
      (data) => dispatch(teamsActions.setEditing(data)),
      {
        navigateBackIfFail: !OFFLINE_MODE,
        notFoundAction: () =>
          OFFLINE_MODE &&
          dispatch(teamsActions.setEditing(state.teams.list.find((s) => s._id === entityId))),
      }
    );
  },
  persist: (fieldsToUpdate?: any) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id || fieldsToUpdate.project; // If no project id, fll back on the fields given

    // If some fields are given as argument, directly take this to update the registration
    const payload = fieldsToUpdate || state.teams.editing;

    return persistEntityInBackend(
      "teams",
      {...payload, project: projectId},
      projectId,
      ({sessionsToUpdateIds, registrationsToUpdateIds, ...team}) => {
        dispatch(teamsActions.addTeam(team));
        reloadUpdatedElement(dispatch, projectId, sessionsToUpdateIds, registrationsToUpdateIds);
      },
      ({sessionsToUpdateIds, registrationsToUpdateIds, ...team}) => {
        dispatch(teamsActions.updateInList(team));
        reloadUpdatedElement(dispatch, projectId, sessionsToUpdateIds, registrationsToUpdateIds);
      }
    );
  },
  remove: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await removeEntityInBackend(
      "teams",
      entityId,
      projectId,
      state.teams.list,
      (newTeamsList, {sessionsToUpdateIds, registrationsToUpdateIds}) => {
        dispatch(
          teamsActions.initList({
            list: newTeamsList,
            project: state.teams.init,
          })
        );
        reloadUpdatedElement(dispatch, projectId, sessionsToUpdateIds, registrationsToUpdateIds);
      },
      true
    );
  },
  loadEditingHistory: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    !OFFLINE_MODE &&
      (await loadEntityFromBackend(
        "teams",
        `${entityId}/history`,
        projectId,
        state.teams.editing,
        null,
        (data) => dispatch(teamsActions.changeEditing({history: data}))
      ));
  },
};

export const teamsSelectors = {
  selectList: (state) => state.teams.list,
  selectEditing: (state) => state.teams.editing,
};

export const teamsReducer = teamsSlice.reducer;

export const teamsActions = {
  ...teamsSlice.actions,
  ...asyncActions,
};
