import {createSlice} from "@reduxjs/toolkit";
import moment from "moment";
import {
  fetchWithMessages,
  loadListFromBackend,
  loadEntityFromBackend,
  persistEntityInBackend,
  removeEntityInBackend,
} from "../helpers/reduxUtilities";
import {displaySessionsInconsistenciesNotifications} from "../helpers/notificationUtilities";
import {registrationsActions} from "./registrations";
import {OFFLINE_MODE} from "../helpers/offlineModeUtilities";
import {
  getElementsFromListInSlot,
  getSessionName,
  slotNumberString,
} from "../helpers/agendaUtilities";
import {personName} from "../helpers/utilities";

const computeSessionEnd = (session) => {
  if (session.slots) {
    return moment.max(...session.slots.map((s) => moment(s.end))).format();
  } else {
    return undefined;
  }
};

const computeSessionStart = (session) => {
  if (session.slots) {
    return moment.min(...session.slots.map((s) => moment(s.start))).format();
  } else {
    return undefined;
  }
};
const computeSessionStartAndEnd = (session) => {
  session.start =
    session.slots && session.slots.length > 0 ? computeSessionStart(session) : session.start;
  session.end =
    session.slots && session.slots.length > 0 ? computeSessionEnd(session) : session.end;
};

const validAvailability = (slot, direction, project) => {
  let availabilitySlots;

  if (slot.places[0] && slot.places[0].availabilitySlots) {
    availabilitySlots = slot.places[0].availabilitySlots.map((a) => ({
      start: moment(a.start),
      end: moment(a.end),
    }));
  } else {
    availabilitySlots = project.project.availabilitySlots.map((a) => ({
      start: moment(a.start),
      end: moment(a.end),
    }));
  }
  // let availabilitySlots = slot.places[0].availabilitySlots.map(a => ({start: moment(a.start), end: moment(a.end)}));
  availabilitySlots.sort((a, b) => a.start - b.start); //asc
  const momentSlot = {...slot, start: moment(slot.start), end: moment(slot.end)};
  let out;
  for (const [index, availabilitySlot] of availabilitySlots.entries()) {
    if (momentSlot.start >= availabilitySlot.start && momentSlot.end <= availabilitySlot.end) {
      //in
      out = momentSlot;
      // console.log('break IN');
      break;
    } else if (momentSlot.start < availabilitySlot.start && index === 0) {
      //trop top en limite
      let outEnd = availabilitySlot.start.clone();
      outEnd.add(momentSlot.duration, "minutes");
      out = {
        ...momentSlot,
        end: outEnd,
        start: availabilitySlot.start,
      };
      // console.log('break too early');
      break;
    } else if (momentSlot.end > availabilitySlot.end && index === availabilitySlots.length - 1) {
      //pas trop tard en limite
      let outStart = availabilitySlot.end.clone();
      outStart.subtract(momentSlot.duration, "minutes");
      out = {
        ...momentSlot,
        start: outStart,
        end: availabilitySlot.end,
      };
      // console.log('break too late');
      break;
    } else if (index !== availabilitySlots.length - 1) {
      const nextAvaibility = availabilitySlots[index + 1];
      if (momentSlot.end > availabilitySlot.end && momentSlot.start < nextAvaibility.start) {
        //entre deux
        let outEnd;
        if (direction === "backward") {
          outEnd = availabilitySlot.end.clone();
        } else if (direction === "forward") {
          outEnd = nextAvaibility.start.clone();
        }
        outEnd.add(momentSlot.duration, "minutes");
        out = {
          ...momentSlot,
          start: nextAvaibility.start,
          end: outEnd,
        };
      }
    }
  }
  return out;
};

const postponeSlotInPlacesOpenTimeRecursiv = (slot, project, direction) => {
  const existingSlots = project.slots.map((es) => ({
    ...es,
    start: moment(es.start),
    end: moment(es.end),
  }));
  const key = existingSlots.map((s) => s._id).indexOf(slot._id);

  const validAvabilityOnly = false;

  let validSlot = slot;
  if (validAvabilityOnly) {
    validSlot = validAvailability(slot, direction, project);
  }

  let postponeHitory;
  switch (direction) {
    case "backward":
      if (key !== 0) {
        let postponeCandidate = existingSlots[key - 1];
        if (postponeCandidate.end > validSlot.start) {
          let postponeCandidateStart = validSlot.start.clone();
          postponeCandidateStart.subtract(postponeCandidate.duration, "minutes");
          postponeCandidate = {
            ...postponeCandidate,
            end: validSlot.start.clone(),
            start: postponeCandidateStart,
          };
        }
        postponeHitory = postponeSlotInPlacesOpenTimeRecursiv(
          postponeCandidate,
          project,
          direction
        );
        const lastHistory = postponeHitory[postponeHitory.length - 1];
        if (lastHistory.end > validSlot.start) {
          let validSlotEnd = lastHistory.end.clone();
          validSlotEnd.add(validSlot.duration, "minutes");
          validSlot = {
            ...validSlot,
            start: lastHistory.end.clone(),
            end: validSlotEnd,
          };
        }
        if (validAvabilityOnly) {
          validSlot = validAvailability(validSlot, "forward", project);
        }

        postponeHitory.push(validSlot);
      } else {
        if (validAvabilityOnly) {
          return [validAvailability(slot, direction, project)];
        } else {
          return [slot];
        }
      }
      break;
    case "forward":
      if (key !== existingSlots.length - 1) {
        let postponeCandidate = existingSlots[key + 1];
        // console.log(JSON.stringify(postponeCandidate.start),JSON.stringify(validSlot.end));
        if (postponeCandidate.start < validSlot.end) {
          let postponeCandidateEnd = validSlot.end.clone();
          // console.log('postponeCandidateEnd');
          postponeCandidateEnd.add(postponeCandidate.duration, "minutes");
          postponeCandidate = {
            ...postponeCandidate,
            start: validSlot.end.clone(),
            end: postponeCandidateEnd,
          };
        }
        // console.log('postponeCandidate',JSON.stringify({start:postponeCandidate.start,end:postponeCandidate.end}));
        postponeHitory = postponeSlotInPlacesOpenTimeRecursiv(
          postponeCandidate,
          project,
          direction
        );
        const lastHistory = postponeHitory[postponeHitory.length - 1];
        if (lastHistory.start < validSlot.end) {
          let validSlotStart = lastHistory.start.clone();
          validSlotStart.subtract(validSlot.duration, "minutes");
          validSlot = {
            ...validSlot,
            end: lastHistory.start.clone(),
            start: validSlotStart,
          };
        }
        if (validAvabilityOnly) {
          validSlot = validAvailability(validSlot, "backward", project);
        }
        postponeHitory.push(validSlot);
      } else {
        if (validAvabilityOnly) {
          return [validAvailability(slot, direction, project)];
        } else {
          return [slot];
        }
      }
      break;
    default:
  }
  return postponeHitory;
};

const postponeSlotInPlacesOpenTime = (rawCurrentSlot, session) => {
  let currentSlot = {
    ...rawCurrentSlot,
    start: moment(rawCurrentSlot.start),
    end: moment(rawCurrentSlot.end),
  };

  const existingSlots = session.slots.map((es) => ({
    ...es,
    start: moment(es.start),
    end: moment(es.end),
  }));

  // Choose the direction. If we don't know the ID of the slot (ie the slot is new),
  // let's say 'forward'
  const oldCurrentSlotIndex = existingSlots.map((s) => s._id).indexOf(currentSlot._id);

  let oldCurrentSlot;
  if (oldCurrentSlotIndex !== -1) {
    // If found, get the old slot data in the existing slots array
    oldCurrentSlot = existingSlots[oldCurrentSlotIndex];
  } else {
    // If not found (= it's a new added slot), then use the current slot as the old slot
    oldCurrentSlot = currentSlot;
  }

  let direction;
  if (currentSlot.start.isBefore(oldCurrentSlot.start)) {
    direction = "backward";
  } else if (currentSlot.end.isAfter(oldCurrentSlot.end)) {
    direction = "forward";
  }

  let postponeResult;
  if (direction) {
    postponeResult = postponeSlotInPlacesOpenTimeRecursiv(currentSlot, session, direction);
  } else {
    postponeResult = [currentSlot];
  }

  const out = [];
  for (const postponeSlot of postponeResult) {
    if (typeof postponeSlot !== "object") {
      throw Error("Found dirty values in the postpone recommandations");
    }
    const existingSlot = existingSlots.find((es) => es._id == postponeSlot._id);
    if (existingSlot != undefined) {
      if (
        existingSlot.start.diff(postponeSlot.start) !== 0 ||
        existingSlot.end.diff(postponeSlot.end) !== 0
      ) {
        out.push(postponeSlot);
      }
    } else {
      out.push(postponeSlot);
    }
  }
  return out;
};

const displayInconsistencies = (sessionEditingState, projectId) =>
  // just check it with a little delay, cause it takes long
  setTimeout(
    () =>
      fetchWithMessages(`projects/${projectId}/sessions/checkInconsistencies`, {
        method: "POST",
        body: sessionEditingState,
      }).then((data) => displaySessionsInconsistenciesNotifications(data)),
    1500
  );

const updateInSessionListAndSlotList = (state) => {
  // Update session in session list
  const index = state.list.map((s) => s._id).indexOf(state.editing._id);
  if (index >= 0) state.list[index] = state.editing;

  // Update the slots list
  state.slotList = state.list
    .map((session) =>
      session.slots
        ? session.slots.map((slot) => ({...slot, session: session})) // Get all the slots...
        : []
    )
    .flat();
};

export const sessionsSlice = createSlice({
  name: "sessions",
  initialState: {
    init: false,
    list: [],
    editing: {},
    cloning: undefined,
    slotList: undefined,
    inconsistenciesList: [],
  },
  reducers: {
    addToList: (state, action) => {
      state.list = [action.payload, ...state.list];
      // TODO can be optimized
      state.slotList = state.list
        .map((session) =>
          session.slots.map((slot) => {
            return {...slot, session: session};
          })
        )
        .flat();
    },
    updateInList: (state, action) => {
      state.list[state.list.findIndex((session) => session._id === action.payload._id)] =
        action.payload;
      // TODO can be optimized
      state.slotList = state.list
        .map((session) =>
          session.slots.map((slot) => {
            return {...slot, session: session};
          })
        )
        .flat();
    },
    initContext: (state, action) => {
      state.init = action.payload;
    },
    initList: (state, action) => {
      state.init = action.payload.project;
      state.list = action.payload.list;
      state.slotList = state.list
        .map((session) => session.slots.map((slot) => ({...slot, session})))
        .flat();
    },
    selectEditingByIdFromList: (state, action) => {
      state.editing = state.list[state.list.map((session) => session._id).indexOf(action.payload)];
    },
    changeEditing: (state, action) => {
      let newSession = action.payload;
      const oldSession = state.editing;

      // Auto-update the default slots of the session when we change the activity
      if (
        newSession.activity?.slots?.length > 0 && // If an activity is selected and has some slots
        ((oldSession.activity?._id && newSession.activity._id !== oldSession.activity._id) || // There was an old activity, but it has changed, or...
          (!oldSession.activity?._id && newSession._id === "new")) // ... the session is new and the activity just got selected
      ) {
        // get the slot durations from the activity if there are some
        if (newSession.activity.slots?.length > 0) {
          newSession.slots = newSession.activity.slots.map((s) => ({
            duration: s.duration,
            stewardsSessionSynchro: true,
            placesSessionSynchro: true,
          }));

          // Create the slots based on the session start
          let timeTrace = moment(newSession.start);
          for (const slot of newSession.slots) {
            slot.start = timeTrace.format();
            timeTrace.add(slot.duration, "minutes");
            slot.end = timeTrace.format();
          }
        }
      }

      state.editing = {
        ...newSession,
        stewards: newSession.stewards || state.editing.stewards,
        places: newSession.places || state.editing.places,
        slots: newSession.slots || state.editing.slots,
      };

      computeSessionStartAndEnd(state.editing);
      updateInSessionListAndSlotList(state);
    },
    setEditing: (state, action) => {
      state.editing = action.payload;
    },
    removeSlotToEditing: (state, action) => {
      state.editing.slots.splice(
        state.editing.slots.map((s) => s._id).indexOf(action.payload._id),
        1
      );

      computeSessionStartAndEnd(state.editing);
      updateInSessionListAndSlotList(state);
    },
    createSlotToEditing: (state, action) => {
      let newSlot = action.payload;

      // Check if we have to postpone other slots of this session if some of them are overlapping
      const slotsPostponed = postponeSlotInPlacesOpenTime(newSlot, state.editing);
      // The new slot has no id, find it back in the postponed slots
      newSlot = slotsPostponed.find((sp) => sp._id === undefined);

      state.editing.slots.push(newSlot);

      computeSessionStartAndEnd(state.editing);
      updateInSessionListAndSlotList(state);
    },

    addToEditing: (state, action) => {
      const field = action.payload.field;
      state.editing[field] = [...(state.editing[field] || []), ...action.payload.payload];
      for (let slot of state.editing.slots) {
        if (slot[`${field}SessionSynchro`]) {
          slot[field] = state.editing[field];
        }
      }
    },
    removeToEditing: (state, action) => {
      const field = action.payload.field;
      state.editing[field] = state.editing[field].filter(
        (t) => t._id !== action.payload.payload._id
      );
      for (let slot of state.editing.slots) {
        if (slot[`${field}SessionSynchro`]) {
          slot[field] = state.editing[field];
        }
      }
    },
  },
});

const asyncActions = {
  loadList:
    (showLoading = true) =>
    async (dispatch, getState) => {
      const state = getState();
      const projectId = state.currentProject.project._id;

      await loadListFromBackend(
        "sessions",
        projectId,
        state.sessions.init,
        () => dispatch(sessionsActions.initContext(projectId)),
        (data) => dispatch(sessionsActions.initList({list: data.list, project: projectId})),
        showLoading
      );
    },
  reloadElementsFromList: (idsToReload) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    const allSessionsExceptTheOnesToReload = state.sessions.list.filter(
      (s) => !idsToReload.includes(s._id)
    );

    const data = await fetchWithMessages(
      `projects/${projectId}/sessions/selectiveLoad`,
      {
        method: "POST",
        body: {alreadyLoaded: allSessionsExceptTheOnesToReload.map((s) => s._id)},
      },
      undefined,
      undefined
    );
    dispatch(
      sessionsActions.initList({
        list: [...data.list, ...allSessionsExceptTheOnesToReload],
        project: projectId,
      })
    );
  },
  loadEditing: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    return loadEntityFromBackend(
      "sessions",
      entityId,
      projectId,
      state.sessions.editing,
      () =>
        dispatch(
          sessionsActions.setEditing({
            _id: "new",
            start: state.currentProject.project.start,
            stewards: [],
            places: [],
            slots: [],
          })
        ),
      (data) => dispatch(sessionsActions.setEditing(data)),
      {
        navigateBackIfFail: !OFFLINE_MODE,
        notFoundAction: () =>
          OFFLINE_MODE &&
          dispatch(sessionsActions.setEditing(state.sessions.list.find((s) => s._id === entityId))),
      }
    );
  },
  persist:
    (fieldsToUpdate?: any, checkInconsistencies = false) =>
    async (dispatch, getState) => {
      const state = getState();
      const projectId = state.currentProject.project._id || fieldsToUpdate.project; // If no project id, fll back on the fields given

      // If some fields are given as argument, directly take this to update the registration
      const payload = fieldsToUpdate || state.sessions.editing;

      return persistEntityInBackend(
        "sessions",
        {...payload, project: projectId},
        projectId,
        ({registrationsToUpdateIds, ...session}) => {
          dispatch(sessionsActions.addToList(session));
          dispatch(registrationsActions.reloadElementsFromList(registrationsToUpdateIds));
        },
        ({registrationsToUpdateIds, ...session}) => {
          dispatch(sessionsActions.updateInList(session));
          dispatch(registrationsActions.reloadElementsFromList(registrationsToUpdateIds));
        }
      );

      // if asked, check the inconsistencies
      checkInconsistencies &&
        displayInconsistencies(state.sessions.editing, state.currentProject.project._id);
    },
  remove: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await removeEntityInBackend(
      "sessions",
      entityId,
      projectId,
      state.sessions.list,
      (newSessionList, {registrationsToUpdateIds}) => {
        dispatch(
          sessionsActions.initList({
            list: newSessionList,
            project: state.sessions.init,
          })
        );
        dispatch(registrationsActions.reloadElementsFromList(registrationsToUpdateIds));
      },
      true
    );
  },
  loadEditingHistory: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    !OFFLINE_MODE &&
      (await loadEntityFromBackend(
        "sessions",
        `${entityId}/history`,
        projectId,
        state.sessions.editing,
        null,
        (data) => dispatch(sessionsActions.setEditing({...state.sessions.editing, history: data}))
      ));
  },
  checkInconsistencies: () => async (dispatch, getState) => {
    const state = getState();
    displayInconsistencies(state.sessions.editing, state.currentProject.project._id);
  },
  updateSlotToEditing: (newSlotData) => async (dispatch, getState) => {
    // Add duration to the slot data
    newSlotData.duration = moment(newSlotData.end).diff(moment(newSlotData.start), "minutes");

    const state = getState();

    // Get all the slots of the current session in the redux state
    const currentSession = {...state.sessions.editing};
    const existingSlots = currentSession.slots.map((s) => ({...s}));

    try {
      // Get all the slots that need to be postponed and for each of them, update them with new start and end dates
      // This manages all the setup for the start and end dates
      const slotsPostponed = postponeSlotInPlacesOpenTime(newSlotData, state.sessions.editing);
      for (const slotPostponed of slotsPostponed) {
        let slotToUpdate = existingSlots.find((es) => es._id === slotPostponed._id);
        if (slotToUpdate) {
          slotToUpdate.start = slotPostponed.start.format();
          slotToUpdate.end = slotPostponed.end.format();
        }
      }
    } catch (error) {
      console.log(error.message, "Stopping.");
      return;
    }

    // If the slot has places synchro with its session, change the places everywhere to keep the synchronisation
    if (newSlotData.places && newSlotData.placesSessionSynchro) {
      // Load the slot places into the session editing state
      currentSession.places = newSlotData.places;
      // Update all the other slots that are also synchronized
      for (const inspectedSlot of existingSlots) {
        if (inspectedSlot.placesSessionSynchro) {
          inspectedSlot.places = newSlotData.places;
        }
      }
    }

    // Same for the stewards! :)
    if (newSlotData.stewards && newSlotData.stewardsSessionSynchro) {
      // Load the slot stewards into the session editing state
      currentSession.stewards = newSlotData.stewards;
      // Update all the other slots that are also synchronized
      for (const inspectedSlot of existingSlots) {
        if (inspectedSlot.stewardsSessionSynchro) {
          inspectedSlot.stewards = newSlotData.stewards;
        }
      }
    }

    // Add all the rest. Don't include the start and end dates as they could have been changed by the function above.
    const {start, end, ...dataToUpdate} = newSlotData;
    let slotToUpdateKey = existingSlots.findIndex((es) => es._id == newSlotData._id);
    existingSlots[slotToUpdateKey] = {
      ...existingSlots[slotToUpdateKey],
      ...dataToUpdate,
    };

    //rebuild session
    currentSession.slots = existingSlots;

    // Compute the new start and end for the session based on the new data
    computeSessionStartAndEnd(currentSession);

    //Could be undefined : sessionsActions is declared after; updateSlotToEditing could't be trigger bafore sessionsActions delaration
    dispatch(sessionsActions.changeEditing(currentSession));
  },
};

export const sessionsSelectors = {
  selectList: (state) => state.sessions.list,
  selectEditing: (state) => state.sessions.editing,
  selectSlotsList: (state) => state.sessions.slotList,
  selectSlotsListForAgenda: (state) =>
    state.sessions.slotList?.map((slot) => ({
      ...slot,
      id: slot._id,
      title: slotNumberString(slot) + getSessionName(slot.session, state.teams.list),
      startDate: slot.start,
      endDate: slot.end,
      location: getElementsFromListInSlot("places", slot, (el) => el.name).join(", "),
      placeId: getElementsFromListInSlot("places", slot, (el) => el._id),
      categoryId: slot.session.activity?.category?._id,
      stewardsNames: getElementsFromListInSlot("stewards", slot, personName).join(", "),
      participantsNames: state.registrations.list
        ?.filter((r) => r.sessionsSubscriptions.find((ss) => ss.session === slot.session._id))
        .map((r) => personName(r.user))
        .join(", "),
      stewardId: getElementsFromListInSlot("stewards", slot, (el) => el._id),
      color: slot.session.activity?.category?.color,
    })),
};

export const sessionsReducer = sessionsSlice.reducer;

export const sessionsActions = {
  ...sessionsSlice.actions,
  ...asyncActions,
};
