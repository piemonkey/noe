import {createSlice} from "@reduxjs/toolkit";
import {navigate} from "@reach/router";
import {currentProjectActions} from "./currentProject.js";
import {usersActions} from "./users.js";
import {fetchWithMessages} from "../helpers/reduxUtilities";
import {projectsActions} from "./projects";
import {registrationsActions} from "./registrations";
import {viewActions} from "./view";
import {URLS} from "../app/configuration";

export const currentUserSlice = createSlice({
  name: "currentUser",
  initialState: {
    user: {},
    connected: undefined,
    connectionError: undefined,
    connectionNotice: undefined,
  },
  reducers: {
    changeLogin: (state, action) => {
      state.user.email = action.payload;
    },
    changeLastName: (state, action) => {
      state.user.lastName = action.payload;
    },
    changeFirstName: (state, action) => {
      state.user.firstName = action.payload;
    },
    changeUser: (state, action) => {
      state.user = action.payload;
    },
    changePassword: (state, action) => {
      state.user.password = action.payload;
    },
    changeConnected: (state, action) => {
      state.connected = action.payload;
    },
    changeConnectionError: (state, action) => {
      state.connectionError = action.payload;
    },
    changeConnectionNotice: (state, action) => {
      state.connectionNotice = action.payload;
    },
    reset: (state, action) => {
      state.user = {};
      state.connected = false;
    },
  },
});

const asyncActions = {
  signUp: () => async (dispatch, getState) => {
    const state = getState();

    fetchWithMessages(
      "users",
      {
        noAuthNeeded: true,
        method: "POST",
        body: state.currentUser.user,
      },
      {},
      false
    )
      .then(() => {
        navigate("./login");
        dispatch(currentUserActions.changeConnectionNotice("Compte créé avec succès."));
      })
      .catch(() =>
        dispatch(currentUserActions.changeConnectionError("Cet email correspond déjà à un compte."))
      );
  },
  logIn: () => async (dispatch, getState) => {
    const state = getState();
    try {
      const data = await fetchWithMessages(
        "auth/authenticate",
        {
          noAuthNeeded: true,
          method: "POST",
          body: state.currentUser.user,
        },
        {200: "Connexion réussie."},
        false
      );

      localStorage.setItem("token", data.jwt_token);
      dispatch(currentProjectActions.cleanProject());
      dispatch(currentUserActions.changeConnected(undefined));
    } catch (e) {
      dispatch(
        currentUserActions.changeConnectionError(
          "L'email et le mot de passe saisis ne sont pas pas reconnus."
        )
      );
    }
  },
  logOut: () => async (dispatch, getState) => {
    dispatch(currentProjectActions.cleanProject());
    dispatch(viewActions.setSearchParams({}));
    dispatch(projectsActions.cleanProjectList());
    localStorage.removeItem("token");
    dispatch(currentUserActions.reset());
    dispatch(registrationsActions.setCurrent(undefined));
  },
  refreshAuthTokens: () => async (dispatch, getState) => {
    // If no login token, then it means the user should disconnect, that's all
    if (!localStorage.getItem("token")) {
      dispatch(currentUserActions.changeConnected(false));
      return;
    }

    try {
      // If there is a login token, check it
      const data = await fetchWithMessages(
        "auth/refreshAuthTokens",
        {method: "GET"},
        {
          401: {
            type: "info",
            message:
              "Vous avez été déconnecté⋅e : cela peut arriver quand on fait une mise à jour :) Reconnectez-vous pour accéder à votre compte.",
          },
        }
      );

      // Change connection status and store the connection token
      localStorage.setItem("token", data.jwt_token);
      dispatch(currentUserActions.changeConnected(true));

      // Change the connected currentUser
      dispatch(currentUserActions.changeUser(data.user));
      dispatch(usersActions.changeEditing(data.user));
    } catch (e) {
      dispatch(currentUserActions.changeConnected(false));
    }
  },
  forgotPassword: () => async (dispatch, getState) => {
    const state = getState();

    fetchWithMessages("auth/password/sendResetEmail", {
      noAuthNeeded: true,
      noResponseData: true,
      method: "POST",
      queryParams: {lang: "fr"},
      body: {
        email: state.currentUser.user.email,
        url: URLS.CURRENT,
      },
    }).then(() => {
      dispatch(currentProjectActions.cleanProject());
      dispatch(
        currentUserActions.changeConnectionNotice(
          "Si cet email correspond à un compte, les instructions de récupération lui ont bien été envoyées. Si vous ne le recevez pas, pensez à regarder dans vos spams."
        )
      );
    });
  },
  checkPasswordResetToken: (token) => async (dispatch, getState) => {
    fetchWithMessages(`auth/password/checkResetToken/${token}`, {
      noAuthNeeded: true,
      noResponseData: true,
      method: "GET",
    })
      .then(() => {
        dispatch(currentProjectActions.cleanProject());
      })
      .catch(() =>
        dispatch(
          currentUserActions.changeConnectionError(
            "Le token de changement de mot de passe n'est pas valide. Merci de réessayer depuis le formulaire 'Mot de passe oublié'."
          )
        )
      );
  },
  resetPassword: (token) => async (dispatch, getState) => {
    const state = getState();

    fetchWithMessages("auth/password/reset", {
      noAuthNeeded: true,
      noResponseData: true,
      method: "POST",
      body: {
        token: token,
        password: state.currentUser.user.password,
      },
    })
      .then(() => {
        dispatch(currentProjectActions.cleanProject());
        dispatch(currentUserActions.changeConnectionNotice("Le mot de passe a bien été changé."));
        navigate("./login");
      })
      .catch(() =>
        dispatch(
          currentUserActions.changeConnectionError(
            "Erreur lors du changement du mot de passe. Merci de réessayer."
          )
        )
      );
  },
  deleteAccount: () => async (dispatch, getState) => {
    const state = getState();
    await fetchWithMessages(
      `users/${state.currentUser.user._id}`,
      {method: "DELETE", noResponseData: true},
      {200: "Suppression réussie."}
    );
    dispatch(currentUserActions.logOut());
  },
};

export const currentUserSelectors = {
  selectUser: (state) => state.currentUser.user,
  selectConnected: (state) => state.currentUser.connected,
  selectConnectionError: (state) => state.currentUser.connectionError,
  selectConnectionNotice: (state) => state.currentUser.connectionNotice,
};

export const currentUserReducer = currentUserSlice.reducer;

export const currentUserActions = {
  ...currentUserSlice.actions,
  ...asyncActions,
};
