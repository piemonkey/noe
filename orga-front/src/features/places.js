import {createSlice} from "@reduxjs/toolkit";
import {
  createAvailabilitySlot,
  updateAvailabilitySlot,
  removeAvailabilitySlot,
  replaceAllAvailabilitySlots,
} from "./availability";
import {
  loadEntityFromBackend,
  loadListFromBackend,
  persistEntityInBackend,
  removeEntityInBackend,
} from "../helpers/reduxUtilities";
import {sessionsActions} from "./sessions";
import {activitiesActions} from "./activities";
import {OFFLINE_MODE} from "../helpers/offlineModeUtilities";

export const placesSlice = createSlice({
  name: "places",
  initialState: {
    list: [],
    init: false,
    editing: {},
  },
  reducers: {
    addToList: (state, action) => {
      state.list = [action.payload, ...state.list];
    },
    updateInList: (state, action) => {
      state.list = [action.payload, ...state.list.filter((i) => i._id !== action.payload._id)];
    },
    initContext: (state, action) => {
      state.init = action.payload;
    },
    initList: (state, action) => {
      state.init = action.payload.project;
      state.list = action.payload.list;
    },
    changeEditing: (state, action) => {
      state.editing = {
        ...state.editing,
        ...action.payload,
      };
    },
    setEditing: (state, action) => {
      state.editing = action.payload;
    },
    removeAvailabilitySlotToEditing: removeAvailabilitySlot,
    createAvailabilitySlotToEditing: createAvailabilitySlot,
    updateAvailabilitySlotToEditing: updateAvailabilitySlot,
    replaceAllAvailabilitySlots: replaceAllAvailabilitySlots,
  },
});

const asyncActions = {
  loadList: () => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await loadListFromBackend(
      "places",
      projectId,
      state.places.init,
      () => dispatch(placesActions.initContext(projectId)),
      (data) => dispatch(placesActions.initList({list: data, project: projectId}))
    );
  },
  loadEditing: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    return loadEntityFromBackend(
      "places",
      entityId,
      projectId,
      state.places.editing,
      () => dispatch(placesActions.setEditing({_id: "new"})),
      (data) => dispatch(placesActions.setEditing(data)),
      {
        navigateBackIfFail: !OFFLINE_MODE,
        notFoundAction: () =>
          OFFLINE_MODE &&
          dispatch(placesActions.setEditing(state.places.list.find((s) => s._id === entityId))),
      }
    );
  },
  persist: (fieldsToUpdate?: any) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id || fieldsToUpdate.project; // If no project id, fll back on the fields given

    // If some fields are given as argument, directly take this to update the registration
    const payload = fieldsToUpdate || state.places.editing;

    return persistEntityInBackend(
      "places",
      {...payload, project: projectId},
      projectId,
      (data) => dispatch(placesActions.addToList(data)),
      (data) => {
        dispatch(placesActions.updateInList(data));
        // Update all dependencies next time
        dispatch(sessionsActions.initContext(false));
        dispatch(activitiesActions.initContext(false));
      }
    );
  },
  remove: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await removeEntityInBackend("places", entityId, projectId, state.places.list, (newPlacesList) =>
      dispatch(
        placesActions.initList({
          list: newPlacesList,
          project: state.places.init,
        })
      )
    );
  },
  loadEditingHistory: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    !OFFLINE_MODE &&
      (await loadEntityFromBackend(
        "places",
        `${entityId}/history`,
        projectId,
        state.places.editing,
        null,
        (data) => dispatch(placesActions.changeEditing({history: data}))
      ));
  },
};

export const placesSelectors = {
  selectEditing: (state) => state.places.editing,
  selectList: (state) => state.places.list,
};

export const placesReducer = placesSlice.reducer;

export const placesActions = {
  ...placesSlice.actions,
  ...asyncActions,
};
