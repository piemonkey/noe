import moment from "moment";

const computeStartEnd = (subject) => {
  subject.start = moment.min(subject.availabilitySlots.map((s) => moment(s.start))).format();
  subject.end = moment.max(subject.availabilitySlots.map((s) => moment(s.end))).format();
};

export function replaceAllAvailabilitySlots(state, action) {
  let newSlots = action.payload;

  state.editing.availabilitySlots = newSlots;
  computeStartEnd(state.editing);
}

export function removeAvailabilitySlot(state, action) {
  // Get remove slot
  let freeSlot = action.payload;

  // Remove Slot in editing
  state.editing.availabilitySlots = state.editing.availabilitySlots.filter(
    (s) => !(s.start == freeSlot.start && s.end == freeSlot.end)
  );
  computeStartEnd(state.editing);
}

export function createAvailabilitySlot(state, action) {
  // Get new slot
  let freeSlot = action.payload;
  if (!state.editing.availabilitySlots) {
    state.editing.availabilitySlots = [];
  }

  // Add slot in editing
  state.editing.availabilitySlots.push(freeSlot);

  computeStartEnd(state.editing);
}

export function updateAvailabilitySlot(state, action) {
  // Get update slot and old
  let newSlot = action.payload.newSlot;
  let oldSlot = action.payload.oldSlot;

  // Update Slot in editing
  state.editing.availabilitySlots = state.editing.availabilitySlots.map((s) =>
    s.start === oldSlot.start && s.end === oldSlot.end
      ? {...s, start: newSlot.start, end: newSlot.end}
      : s
  );

  computeStartEnd(state.editing);
}
