import {createSlice} from "@reduxjs/toolkit";
import {
  fetchWithMessages,
  loadEntityFromBackend,
  loadListFromBackend,
  persistEntityInBackend,
  removeEntityInBackend,
} from "../helpers/reduxUtilities";
import {sessionsActions, sessionsSelectors} from "./sessions";
import {OFFLINE_MODE} from "../helpers/offlineModeUtilities";
import {getRegistrationMetadata} from "../helpers/registrationsUtilities";
import moment from "moment";
import {getNextOrCurrentSession} from "../components/participants/ParticipantMain";
import {projectsSelectors} from "./projects";
import {teamsSelectors} from "./teams";

export const registrationsSlice = createSlice({
  name: "registrations",
  initialState: {
    list: [],
    init: false,
    current: undefined,
    editing: {},
  },
  reducers: {
    addToList: (state, action) => {
      state.list = [action.payload, ...state.list];
    },
    updateInList: (state, action) => {
      state.list = [action.payload, ...state.list.filter((i) => i._id !== action.payload._id)];
    },
    initContext: (state, action) => {
      state.init = action.payload;
    },
    initList: (state, action) => {
      state.init = action.payload.project;
      state.list = action.payload.list;
    },
    setCurrent: (state, action) => {
      state.current = action.payload;
    },
    changeCurrent: (state, action) => {
      state.current = {
        ...state.current,
        ...action.payload,
      };
    },
    changeEditing: (state, action) => {
      state.editing = {
        ...state.editing,
        ...action.payload,
      };
    },
    setEditing: (state, action) => {
      state.editing = action.payload;
    },
  },
});

const asyncActions = {
  loadList: () => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await loadListFromBackend(
      "registrations",
      projectId,
      state.registrations.init,
      () => dispatch(registrationsActions.initContext(projectId)),
      (data) => dispatch(registrationsActions.initList({list: data, project: projectId}))
    );
  },
  reloadElementsFromList: (idsToReload) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    const allRegistrationsExceptTheOnesToReload = state.registrations.list.filter(
      (s) => !idsToReload.includes(s._id)
    );

    const data = await fetchWithMessages(
      `projects/${projectId}/registrations/selectiveLoad`,
      {
        method: "POST",
        body: {alreadyLoaded: allRegistrationsExceptTheOnesToReload.map((s) => s._id)},
      },
      undefined,
      undefined
    );
    dispatch(
      registrationsActions.initList({
        list: [...data, ...allRegistrationsExceptTheOnesToReload],
        project: projectId,
      })
    );
  },
  loadEditing: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    return loadEntityFromBackend(
      "registrations",
      entityId,
      projectId,
      state.registrations.editing,
      null,
      (data) => dispatch(registrationsActions.setEditing(data)),
      {
        navigateBackIfFail: !OFFLINE_MODE,
        notFoundAction: () =>
          OFFLINE_MODE &&
          dispatch(
            registrationsActions.setEditing(
              state.registrations.list.find((s) => s._id === entityId)
            )
          ),
      }
    );
  },
  loadCurrent: (envId) => async (dispatch, getState) => {
    return loadEntityFromBackend("registrations", "current", envId, undefined, null, (data) =>
      dispatch(registrationsActions.changeCurrent(data))
    );
  },
  persist: (fieldsToUpdate?: any) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id || fieldsToUpdate.project; // If no project id, fll back on the fields given
    const registrationState = state.registrations;

    // If some fields are given as argument, directly take this to update the registration
    const payload = fieldsToUpdate || registrationState.editing;

    return persistEntityInBackend(
      "registrations",
      {...payload, project: projectId},
      projectId,
      (data) => dispatch(registrationsActions.addToList(data)),
      (data) => {
        dispatch(registrationsActions.updateInList(data));
        // If the updated registration is the user's own registration, then update it as well
        if (data.user._id === registrationState.current.user._id) {
          dispatch(registrationsActions.changeCurrent(data));
        }
        // Update all dependencies next time
        dispatch(sessionsActions.initContext(false));
      }
    );
  },
  remove: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await removeEntityInBackend(
      "registrations",
      entityId,
      projectId,
      state.registrations.list,
      (newRegistrationsList) =>
        dispatch(
          registrationsActions.initList({
            list: newRegistrationsList,
            project: state.registrations.init,
          })
        )
    );
  },
  loadEditingHistory: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    !OFFLINE_MODE &&
      (await loadEntityFromBackend(
        "registrations",
        `${entityId}/history`,
        projectId,
        state.registrations.editing,
        null,
        (data) => dispatch(registrationsActions.changeEditing({history: data}))
      ));
  },
  getPdfPlanning: (id) => async (dispatch, getState) => {
    const state = getState();

    id = id || state.registrations.editing._id;
    try {
      return await fetchWithMessages(
        `projects/${state.currentProject.project._id}/pdf/registrationPlanning/${id}`,
        {isBlob: true, method: "GET"},
        {},
        false,
        true
      );
    } catch {
      return Promise.reject();
    }
  },
  unregister: () => async (dispatch, getState) => {
    const state = getState();

    const currentProject = state.currentProject.project;
    const currentRegistration = state.registrations.editing;

    const updatedCurrentRegistration = await fetchWithMessages(
      `projects/${currentProject._id}/registrations/${currentRegistration._id}`,
      {
        method: "PATCH",
        body: {
          booked: false,
          availabilitySlots: [],
          specific: {},
          sessionsSubscriptions: [],
          teamsSubscriptions: [],
          steward: null,
          helloAssoTickets: [],
          customTicketingTickets: [],
        },
      },
      {200: "Désinscription réussie"}
    );

    // Update in list
    await dispatch(registrationsActions.updateInList(updatedCurrentRegistration));
  },
  checkInSession: (registrationId, sessionId, hasCheckedIn) => async (dispatch, getState) => {
    const state = getState();

    const currentProject = state.currentProject.project;

    const updatedCurrentRegistration = await fetchWithMessages(
      `projects/${currentProject._id}/registrations/${registrationId}/checkIn/${sessionId}`,
      {
        method: "POST",
        body: {hasCheckedIn: hasCheckedIn},
      },
      {200: "Modification réussie."}
    );

    // Update in list
    await dispatch(registrationsActions.updateInList(updatedCurrentRegistration));
  },
  sendInvitationEmail: (user, registrationAdditionalData) => async (dispatch, getState) => {
    const state = getState();

    try {
      const newInvitedRegistration = await fetchWithMessages(
        `projects/${state.currentProject.project._id}/registrations/sendInvitationEmail`,
        {method: "POST", body: {user, ...registrationAdditionalData}},
        {200: "L'email d'invitation a bien été envoyé."},
        undefined,
        true
      );
      await dispatch(
        registrationsActions.reloadElementsFromList(state.currentProject.project._id, [
          newInvitedRegistration._id,
        ])
      );
    } catch {
      return Promise.reject();
    }
  },
};

export const registrationsSelectors = {
  selectEditing: (state) => state.registrations.editing,
  selectCurrent: (state) => state.registrations.current,
  selectList: (state) => state.registrations.list,
  selectListWithMetadata: (state) =>
    state.registrations.list?.map((r) => {
      if (r.availabilitySlots.length > 1)
        console.log(
          r.user.email,
          r.availabilitySlots,
          [...(r.availabilitySlots || [])].sort((a, b) => moment(a.start) - moment(b.start))
        );
      return {
        ...r,
        ...getRegistrationMetadata(r, projectsSelectors.selectEditing(state)),
        teamsSubscriptionsNames: r.teamsSubscriptions?.map((t) => t.name).join(", "),
        nextOrCurrentSession: getNextOrCurrentSession(
          r,
          sessionsSelectors.selectSlotsList(state),
          teamsSelectors.selectList(state)
        ),
        arrivalDateTime: [...(r.availabilitySlots || [])].sort(
          (a, b) => moment(a.start) - moment(b.start)
        )?.[0]?.start,
        departureDateTime: [...(r.availabilitySlots || [])].sort(
          (a, b) => moment(b.end) - moment(a.end)
        )?.[0]?.end,
      };
    }),
};
export const registrationsReducer = registrationsSlice.reducer;

export const registrationsActions = {
  ...registrationsSlice.actions,
  ...asyncActions,
};
