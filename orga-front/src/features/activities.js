import {createSlice} from "@reduxjs/toolkit";
import {
  loadEntityFromBackend,
  loadListFromBackend,
  persistEntityInBackend,
  removeEntityInBackend,
} from "../helpers/reduxUtilities";
import {sessionsActions} from "./sessions";
import {OFFLINE_MODE} from "../helpers/offlineModeUtilities";

export const activitiesSlice = createSlice({
  name: "activities",
  initialState: {
    list: [],
    init: false,
    editing: {},
  },
  reducers: {
    addToList: (state, action) => {
      state.list = [action.payload, ...state.list];
    },
    updateInList: (state, action) => {
      state.list = [action.payload, ...state.list.filter((i) => i._id !== action.payload._id)];
    },
    initContext: (state, action) => {
      state.init = action.payload;
    },
    initList: (state, action) => {
      // console.log('initList',action.payload);
      state.init = action.payload.project;
      state.list = action.payload.list;
    },
    changeEditing: (state, action) => {
      state.editing = {
        ...state.editing,
        ...action.payload,
      };
    },
    setEditing: (state, action) => {
      state.editing = action.payload;
    },
    changeSlotsToEditing: (state, action) => {
      state.editing = {
        ...state.editing,
        slots: action.payload,
      };
    },
    addStewardsToEditing: (state, action) => {
      state.editing.stewards.push(...action.payload);
    },
    removeStewardToEditing: (state, action) => {
      state.editing.stewards = state.editing.stewards.filter((t) => t._id !== action.payload._id);
    },
    addPlacesToEditing: (state, action) => {
      state.editing.places.push(...action.payload);
    },
    removePlaceToEditing: (state, action) => {
      state.editing.places = state.editing.places.filter((t) => t._id !== action.payload._id);
    },
  },
});

const asyncActions = {
  loadList: () => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await loadListFromBackend(
      "activities",
      projectId,
      state.activities.init,
      () => dispatch(activitiesActions.initContext(projectId)),
      (data) => dispatch(activitiesActions.initList({list: data, project: projectId}))
    );
  },
  loadEditing: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    return loadEntityFromBackend(
      "activities",
      entityId,
      projectId,
      state.activities.editing,
      () =>
        dispatch(
          activitiesActions.setEditing({
            _id: "new",
            stewards: [],
            places: [],
          })
        ),
      (data) => dispatch(activitiesActions.setEditing(data)),
      {
        navigateBackIfFail: !OFFLINE_MODE,
        notFoundAction: () =>
          OFFLINE_MODE &&
          dispatch(
            activitiesActions.setEditing(state.activities.list.find((s) => s._id === entityId))
          ),
      }
    );
  },
  persist: (fieldsToUpdate?: any) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id || fieldsToUpdate.project; // If no project id, fll back on the fields given

    // If some fields are given as argument, directly take this to update the registration
    const payload = fieldsToUpdate || state.activities.editing;

    return persistEntityInBackend(
      "activities",
      {...payload, project: projectId},
      projectId,
      (data) => dispatch(activitiesActions.addToList(data)),
      (data) => {
        dispatch(activitiesActions.updateInList(data));
        // Update all dependencies next time
        dispatch(sessionsActions.initContext(false));
      }
    );
  },
  remove: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await removeEntityInBackend(
      "activities",
      entityId,
      projectId,
      state.activities.list,
      (newActivitiesList) =>
        dispatch(
          activitiesActions.initList({
            list: newActivitiesList,
            project: state.activities.init,
          })
        )
    );
  },
  loadEditingHistory: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    !OFFLINE_MODE &&
      (await loadEntityFromBackend(
        "activities",
        `${entityId}/history`,
        projectId,
        state.activities.editing,
        null,
        (data) => dispatch(activitiesActions.changeEditing({history: data}))
      ));
  },
};

export const activitiesSelectors = {
  selectEditing: (state) => state.activities.editing,
  selectList: (state) => state.activities.list,
};

export const activitiesReducer = activitiesSlice.reducer;

export const activitiesActions = {
  ...activitiesSlice.actions,
  ...asyncActions,
};
