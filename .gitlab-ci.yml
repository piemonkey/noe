# More info about SSH HELPER : "https://gitlab.com/gitlab-cd/ssh-template/raw/master/ssh.yml"
.ssh_helper: &ssh_helper |
  function ssh_init() {
      SSH_PRIVATE_KEY="$1"
      SSH_KNOWN_HOSTS="$2"
      test -n "$SSH_PRIVATE_KEY" || ( echo "missing variable SSH_PRIVATE_KEY" && exit 1)
      test -n "$SSH_KNOWN_HOSTS" || ( echo "missing variable SSH_KNOWN_HOSTS" && exit 1)
      which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
      eval $(ssh-agent -s)
      echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
      mkdir -p ~/.ssh
      chmod 700 ~/.ssh
      ssh-keyscan -H "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
      chmod 644 ~/.ssh/known_hosts
  }

  function ssh_run() {
      USER=$1
      HOST=$2
      PKEY=$3
      COMMAND=$4
      ssh_init "$PKEY" $HOST
      ssh $USER@$HOST $COMMAND
  }

.deploy_helper: &deploy_helper |
  function deploy() {
    SERVER_IP=$1
    ENV_FILE_FOLDER_NAME=$2
    BRANCH_NAME=$3
    DELAY_AFTER_INSCRIPTION_FRONT=$4
    DELAY_AFTER_API=$5
    DELAY_AFTER_ORGA_FRONT=$6

    SERVICES="api inscription-front orga-front ia-back"

    FULL_COMMAND="
      cd noe ;


      echo ■■■■■■■■■■ PREPARATION... ■■■■■■■■■■ ;

      echo ■■■■■ Fetching last changes for the branch "$BRANCH_NAME"... ;
      git fetch origin $BRANCH_NAME ;

      echo ■■■■■ Checking if there are any changes to every service in the project... ;
      CHANGED_FILES_GLOBAL=\$(git diff-tree -r --name-only --no-commit-id origin/$BRANCH_NAME $BRANCH_NAME) ;
      echo \$CHANGED_FILES_GLOBAL ;

      echo ■■■■■ Checking if there are incoming changes to any package.json file in the project... ;
      CHANGED_PACKAGE_JSON_FILES=\$(git diff-tree -r --name-only --no-commit-id origin/$BRANCH_NAME $BRANCH_NAME | grep package.json) ;
      echo \$CHANGED_PACKAGE_JSON_FILES ;

      echo ■■■■■ Resetting all files on the remote origin of branch "$BRANCH_NAME". Checking out branch... ;
      git reset --hard origin/$BRANCH_NAME ;
      git checkout $BRANCH_NAME ;

      echo ■■■■■ Copying the .env.prod file from folder ../$ENV_FILE_FOLDER_NAME... ;
      cp ../$ENV_FILE_FOLDER_NAME/.env.prod .env.prod ;
    
      for service in $SERVICES ; 
      do 
        if [ \$(echo \$CHANGED_PACKAGE_JSON_FILES | grep \$service | wc -c) -gt 1 ] ; 
        then 
          echo ■■■■■ package.json has changed in container \$service. Reinstalling node_modules. ;
          make install-docker-deps-prod-\$service ; 
        fi ;
      done ; 


      echo ■■■■■■■■■■ READY FOR LAUNCH ! ■■■■■■■■■■ ;

      if [ \$(echo \$CHANGED_FILES_GLOBAL | grep inscription-front | wc -c) -gt 1 ] ;
      then 
        echo ■■■■■ Source files in inscription-front have changed. Restarting container and waiting $DELAY_AFTER_INSCRIPTION_FRONT... ;
        make force-start-prod-inscription-front && sleep $DELAY_AFTER_INSCRIPTION_FRONT ;
      fi ;
    
      if [ \$(echo \$CHANGED_FILES_GLOBAL | grep api | wc -c) -gt 1 ] ;
      then 
        echo ■■■■■ Source files in api have changed. Restarting container and waiting $DELAY_AFTER_API... ;
        make force-start-prod-api && sleep $DELAY_AFTER_API ;
      fi ;

      if [ \$(echo \$CHANGED_FILES_GLOBAL | grep orga-front | wc -c) -gt 1 ] ;
      then 
        echo ■■■■■ Source files in orga-front have changed. Restarting orga-front container and waiting $DELAY_AFTER_ORGA_FRONT... ;
        make force-start-prod-orga-front && sleep $DELAY_AFTER_ORGA_FRONT ;
      fi ;


      echo ■■■■■ Restart complete.
    "

    echo "Command:" $FULL_COMMAND

    ssh_run "debian" "$SERVER_IP" "$SSH_PRIVATE_KEY" "$FULL_COMMAND"
  }

stages:
  - Code style
  - Test
  - Deploy [Heroku]
  - Deploy

#### CODE STYLE ####

.code_style:
  stage: Code style
  image: node:latest
  before_script:
    - npm install -g prettier@2.3.2

Prettier:
  extends: .code_style
  script: make prettier-check

## Eslint stage doesn't work yet
# Eslint:
#   extends: .code_style
#   before_script:
#     - >
#       npm install -g eslint-config-prettier
#       eslint-config-react-app
#       eslint-plugin-import
#       eslint-plugin-jest
#       eslint-plugin-jsx-a11y
#       eslint-plugin-react
#       eslint-plugin-react-hooks
#       eslint-plugin-testing-library
#       eslint-webpack-plugin
#       eslint-plugin-flowtype
#     - npm install @typescript-eslint/eslint-plugin --prefix api
#     - npm install @typescript-eslint/eslint-plugin --prefix ia-back
#   script: make lint

.deploy:
  stage: Deploy
  before_script:
    - *ssh_helper
    - *deploy_helper

Deploy Demo:
  extends: .deploy
  environment:
    name: deploy_demo
    url: demo.noe-app.io
  script:
    - SERVER_IP=51.75.14.196
    - ENV_FILE_FOLDER_NAME=demo-noe
    - BRANCH_NAME=deploy_demo
    - DELAY_AFTER_INSCRIPTION_FRONT=4m
    - DELAY_AFTER_API=45s
    - DELAY_AFTER_ORGA_FRONT=5m
    - deploy $SERVER_IP $ENV_FILE_FOLDER_NAME $BRANCH_NAME $DELAY_AFTER_INSCRIPTION_FRONT $DELAY_AFTER_API $DELAY_AFTER_ORGA_FRONT
  only:
    - deploy_demo

Deploy Prod:
  extends: .deploy
  environment:
    name: deploy_prod
    url: noe-app.io
  script:
    - SERVER_IP=141.94.214.170
    - ENV_FILE_FOLDER_NAME=prod-noe
    - BRANCH_NAME=deploy_prod
    - DELAY_AFTER_INSCRIPTION_FRONT=160s
    - DELAY_AFTER_API=45s
    - DELAY_AFTER_ORGA_FRONT=250s
    - deploy $SERVER_IP $ENV_FILE_FOLDER_NAME $BRANCH_NAME $DELAY_AFTER_INSCRIPTION_FRONT $DELAY_AFTER_API $DELAY_AFTER_ORGA_FRONT
  only:
    - deploy_prod
##### TESTS ####
#
#.test:
#  stage: Test
#  image: docker/compose:latest
#  services:
#    - docker:dind
#  before_script:
#    - docker version
#    - docker-compose version
#
#api test:
#  extends: .test
#  script: docker-compose -f docker-compose-test-api.yml up --exit-code-from=api
#
#ia-back test:
#  extends: .test
#  script: docker-compose -f docker-compose-test-computing.yml up --exit-code-from=ia-back
#  artifacts:
#    reports:
#      cobertura: ia-back/coverage/cobertura-coverage.xml

##### HEROKU DEPLOYMENT ####
#
#.deploy_to_heroku:
#  image: ruby:latest
#  type: deploy
#  before_script:
#    - apt-get update -qy
#    - apt-get install -y ruby-dev
#    - gem install dpl
#
## STAGING HEROKU #
#
#.staging_heroku:
#  extends: .deploy_to_heroku
#  stage: Deploy [Heroku]
#  only:
#    - master
#
#orga-front staging:
#  extends: .staging_heroku
#  script:
#    - cd orga-front
#    - dpl --provider=heroku --app=noe-frontend --api-key=$HEROKU_API_KEY
#
#inscription-front staging:
#  extends: .staging_heroku
#  script:
#    - cd inscription-front
#    - dpl --provider=heroku --app=noe-end-user-frontend --api-key=$HEROKU_API_KEY
#
#api staging:
#  extends: .staging_heroku
#  script:
#    - cd api
#    - dpl --provider=heroku --app=noe-backend --api-key=$HEROKU_API_KEY
#
#ia-back staging:
#  extends: .staging_heroku
#  script:
#    - cd ia-back
#    - dpl --provider=heroku --app=noe-computing-backend --api-key=$HEROKU_API_KEY
#
