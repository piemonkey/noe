import {Button, Modal, notification} from "antd";
import {WifiOutlined} from "@ant-design/icons";
import React from "react";
import {personName} from "./utilities";
import {t} from "i18next";
import {Trans} from "react-i18next";

let serverNotAvailableModalVisible = false;

export const displaySessionsInconsistenciesNotifications = (inconsistenciesList) => {
  if (inconsistenciesList.length > 0) {
    const listOfMessages = [];
    for (let inconsistency of inconsistenciesList) {
      for (let inconsistencyDetail of inconsistency.inconsistencies) {
        let message;
        if (inconsistencyDetail.type === "availabilitiesOverlap") {
          switch (inconsistencyDetail.entity) {
            case "project":
              message = t("sessions:inconsistenciesNotification.isOutOfProjectSlots");
              break;
            case "place":
              message = t("sessions:inconsistenciesNotification.isOutOfPlaceSlots", {
                entitiesInvolved: inconsistencyDetail.entitiesInvolved
                  .map((e) => e.name)
                  .join(", "),
              });
              break;
            case "steward":
              message = t("sessions:inconsistenciesNotification.isOutOfStewardSlots", {
                entitiesInvolved: inconsistencyDetail.entitiesInvolved.map(personName).join(", "),
              });
              break;
            default:
          }
        } else if (inconsistencyDetail.type === "alreadyUsedEntity") {
          switch (inconsistencyDetail.entity) {
            case "place":
              message = t("sessions:inconsistenciesNotification.placesConflictAtTheSameTime", {
                entitiesInvolved: inconsistencyDetail.entitiesInvolved
                  .map((e) => e.name)
                  .join(", "),
              });
              break;
            case "steward":
              message = t("sessions:inconsistenciesNotification.stewardConflictAtTheSameTime", {
                entitiesInvolved: inconsistencyDetail.entitiesInvolved.map(personName).join(", "),
              });
              break;
            default:
          }
        }
        listOfMessages.push(`La plage n°${inconsistency.slot} ${message}.`);
      }
    }
    console.log(inconsistenciesList[0]);
    displayNotification("open", `inconsistency-${inconsistenciesList[0].session}`, {
      message: t("sessions:inconsistenciesNotification.title"),
      description: (
        <ul>
          {listOfMessages.map((message) => (
            <li>{message}</li>
          ))}
        </ul>
      ),
      duration: 5 + inconsistenciesList.length * 2,
    });
  }
};

export const displayNotification = (type, key, {buttonText, onClickButton, ...props}) =>
  notification[type]({
    key: key,

    btn: buttonText && onClickButton && (
      <Button
        type="primary"
        onClick={() => {
          onClickButton();
          notification.close(key);
        }}>
        {buttonText}
      </Button>
    ),
    duration: 0, // by default, notif will stay forever
    placement: "bottomRight",
    ...props, // Override with props if given
  });

export const displayServerNotAvailableErrorModal = () => {
  if (!serverNotAvailableModalVisible) {
    serverNotAvailableModalVisible = true;
    Modal.error({
      icon: <WifiOutlined />,
      title: t("common:serverUnavailableModal.title"),
      content: <Trans ns="common" i18nKey="serverUnavailableModal.content" />,
      okText: t("common:serverUnavailableModal.tryAgain"),
      onOk: () => (serverNotAvailableModalVisible = false),
      onCancel: () => (serverNotAvailableModalVisible = false),
      closable: true,
      maskClosable: true,
    });
  }
};
