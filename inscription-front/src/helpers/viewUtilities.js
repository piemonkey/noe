import React, {useContext, useEffect, useMemo, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {defaultPagination, viewActions, viewSelectors} from "../features/view";
import {CardElement} from "../components/common/LayoutElement";
import {Transfer} from "antd";
import {addKeyToItemsOfList} from "./tableUtilities";
import {useTranslation} from "react-i18next";

export function debounce(func, wait = 166) {
  let timeout;
  function debounced(...args) {
    // eslint-disable-next-line consistent-this
    const that = this;
    const later = () => {
      func.apply(that, args);
    };
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  }

  debounced.clear = () => {
    clearTimeout(timeout);
  };

  return debounced;
}

// Returns the navbar height, or zero if the navbar isn't present
export const navbarHeight = () =>
  document.querySelector(".mobile-navbar-container")?.offsetHeight || 0;

// Tells if a component is still visible on the screen or not
export const isNotVisibleAnymore = (elem) => {
  return elem
    ? document.documentElement.scrollTop > elem.offsetTop + elem.offsetHeight - navbarHeight()
    : true;
};

// Allows to put a shadow on a sticky object when it begins to stick.
export const useStickyShadow = (querySelector = ".sticky-navbar", placement = "top") => {
  useViewEventListener(
    "scroll",
    debounce(() => {
      const elem = document.querySelector(querySelector);
      // Once the element becomes sticky, add shadow
      if (elem) {
        if (
          (placement === "top" && window.scrollY === elem.offsetTop) ||
          (placement === "bottom" &&
            window.scrollY + window.innerHeight === elem.offsetTop + elem.offsetHeight)
        ) {
          elem.classList.add(`sticking-shadow-${placement}`);
        } else {
          elem.classList.remove(`sticking-shadow-${placement}`);
        }
      }
    }, 17)
  );
};

// Helper to quickly implement javascript tweaks based on a window event
export const useViewEventListener = (eventType, listener, elementToWatch = window) => {
  useEffect(() => {
    elementToWatch.addEventListener(eventType, listener);
    return () => elementToWatch.removeEventListener(eventType, listener);
  });
};

export const getFullHeightWithMargins = (element) =>
  element
    ? element.offsetHeight + // Height
      parseInt(window.getComputedStyle(element).getPropertyValue("margin-top")) +
      parseInt(window.getComputedStyle(element).getPropertyValue("margin-bottom"))
    : 0;

/**
 * Window dimensions provider logic
 */
const getWindowDimensions = () => {
  const {innerWidth: windowWidth, innerHeight: windowHeight} = window;
  return {isMobileView: windowWidth < 768, windowWidth, windowHeight};
};
const WindowDimensionsContext = React.createContext(getWindowDimensions());
export const WindowDimensionsProvider: React.FC = ({children}) => {
  const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

  const debouncedSetDimensions = debounce(
    (windowDimensions) => setWindowDimensions(windowDimensions),
    100
  );

  useEffect(() => {
    function handleResize() {
      debouncedSetDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return (
    <WindowDimensionsContext.Provider value={windowDimensions}>
      {children}
    </WindowDimensionsContext.Provider>
  );
};

export const useWindowDimensions = () => {
  const store = useContext(WindowDimensionsContext);
  return store;
};

/**
 * Browser tab title changer
 * @param baseName root name that appears all the time
 * @param currentPageRoot the variable containing the current page uri
 * @param titlesForPages key/value pairs of page titles
 * @param suffix end name that appears all the time
 */
export const useBrowserTabTitle = (baseName, currentPageRoot, titlesForPages, suffix) => {
  useEffect(() => {
    if (baseName && currentPageRoot) {
      const title = `${baseName ? baseName : ""}${
        titlesForPages[currentPageRoot] ? " - " + titlesForPages[currentPageRoot] : ""
      }${suffix ? " - " + suffix : ""}`;
      document.title = title;
    }
  }, [baseName, currentPageRoot]);
};

// use the useMemo hooks so the debounce function is defined once. Otherwise, it is defined at each rerender and each event
// fires a new debounce function and nothing works
export const useDebounce = (action, wait = 500) => useMemo(() => debounce(action, wait), []);

export const useSavedPagination = () => {
  const dispatch = useDispatch();
  const pagination = useSelector(viewSelectors.selectPagination) || defaultPagination;
  const setPagination = (current, pageSize) =>
    dispatch(viewActions.changePagination({current, pageSize}));
  return [pagination, setPagination];
};

export const useSavedWindowScroll = () => {
  const dispatch = useDispatch();
  const reduxScroll = useSelector(viewSelectors.selectScroll) || 0;

  useViewEventListener(
    "scroll",
    debounce(() => {
      dispatch(viewActions.changeScroll(window.scrollY));
    }, 50)
  );

  useEffect(() => {
    window.scrollTo(0, reduxScroll);
  }, [window.location.pathname]);
};

/**
 * Table columns view selectors
 */
export const useColumnsBlacklistingSelector = (endpoint) => {
  const columnsBlacklist = useSelector(
    endpoint
      ? viewSelectors.selectCustomColumnsBlacklist(endpoint)
      : viewSelectors.selectColumnsBlacklist
  );

  const filterBlacklistedColumns = (columns) =>
    columnsBlacklist ? columns.filter((c) => !columnsBlacklist.includes(c.dataIndex)) : columns;

  const ColumnsBlacklistingSelector = ({columns}) => {
    const {t} = useTranslation();
    const dispatch = useDispatch();
    return (
      <CardElement
        title={t("common:listPage.settings.columnsToDisplay.title")}
        bodyStyle={{overflow: "auto"}}>
        <p>{t("common:listPage.settings.columnsToDisplay.savedOnYourDeviceOnly")}</p>
        <Transfer
          titles={[
            t("common:listPage.settings.columnsToDisplay.displayed"),
            t("common:listPage.settings.columnsToDisplay.hidden"),
          ]}
          dataSource={addKeyToItemsOfList(
            columns.filter((c) => c.title),
            "dataIndex"
          )}
          targetKeys={columnsBlacklist}
          onChange={(value) => dispatch(viewActions.changeColumnsBlacklist(value))}
          listStyle={{width: "100%", height: 300}}
          render={(item) => item.title}
        />
      </CardElement>
    );
  };

  return [filterBlacklistedColumns, ColumnsBlacklistingSelector];
};
