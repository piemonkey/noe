import {useEffect} from "react";
import {useDispatch} from "react-redux";

export const useLoadEditing = (
  elementsActions,
  idInUrl,
  additionalActions,
  clonedElement,
  dontClean
) => {
  const dispatch = useDispatch();

  useEffect(() => {
    additionalActions && additionalActions();
    if (idInUrl) {
      if (idInUrl === "clone") dispatch(elementsActions.changeEditing(clonedElement));
      else if (idInUrl === "groupedit") dispatch(elementsActions.loadEditing("new"));
      else dispatch(elementsActions.loadEditing(idInUrl));
    }

    if (!dontClean) return () => dispatch(elementsActions.setEditing({}));
  }, [idInUrl]);
};
