import React, {useRef, useState} from "react";
import {Button, Input, message, Tag} from "antd";
import {SearchOutlined} from "@ant-design/icons";
import {listSorter, normalize} from "./listUtilities";
import {WaitingInvitationTag} from "./registrationsUtilities";
import {t} from "i18next";
import {URLS} from "../app/configuration";

const {Search} = Input;

export const addKeyToItemsOfList = (data, keyName = undefined) => {
  return data ? data?.map((t, i) => ({...t, key: keyName ? t[keyName] : i})) : [];
};

export const fieldToData = (fields, acc = {}) => {
  return fields.reduce((accumulator, currentValue) => {
    accumulator[currentValue.name] = currentValue.value;
    return accumulator;
  }, acc);
};

export const dataToFields = (data, formatFieldsFn = undefined) => {
  let clone = {...data};

  formatFieldsFn && formatFieldsFn(clone);

  const fields = Object.keys(clone).reduce((accumulator, currentValue) => {
    accumulator.push({name: currentValue, value: clone[currentValue]});
    return accumulator;
  }, []);
  return fields;
};

export const useCopyColumns = (dataSource, columns, handleDisplayConfigChangeFn?) => {
  const [currentDataSource, setCurrentDataSource] = useState();

  return {
    columns: columns.map((column) => ({
      ...column,
      onHeaderCell: (column) => ({
        ...column.onHeaderCell,
        onContextMenu: (e) => {
          e.preventDefault();
          const searchTextFn = column.searchText || ((record) => record[column.dataIndex]);
          const copyableText = (currentDataSource || dataSource)?.map(searchTextFn).join(", ");
          navigator.clipboard
            .writeText(copyableText)
            .then(() => message.success(t("common:messages.columnCopiedToClipboard")));
        },
      }),
    })),
    handleDisplayConfigChange: (pagination, filters, sorter, extra) => {
      setCurrentDataSource(extra.currentDataSource);
      handleDisplayConfigChangeFn &&
        handleDisplayConfigChangeFn(pagination, filters, sorter, extra);
    },
    currentDataSource,
  };
};

export const useSearchInColumns = (columns) => {
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  let searchInput = useRef(null);

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters, confirm) => {
    clearFilters();
    setSearchText("");
    confirm();
  };

  const enableColumnSearchProps = (dataIndex, columnTitle, searchText) => ({
    filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
      <div className="containerH" style={{padding: 8}}>
        <Search
          autoComplete="new-password" // Prevent any autocomplete service to complete this, cause autoComplete="off" doesn't seem to work
          enterButton
          style={{maxWidth: 250}}
          value={selectedKeys[0]}
          // On press search button
          onSearch={() => handleSearch(selectedKeys, confirm, dataIndex)}
          // On keyboard input
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          // on press enter
          onPressEnter={(e) => {
            e.stopPropagation();
            handleSearch(selectedKeys, confirm, dataIndex);
          }}
          placeholder={t("common:filterIn", {name: columnTitle})}
          ref={searchInput}
        />
        <Button type="link" onClick={() => clearFilters && handleReset(clearFilters, confirm)}>
          {t("common:erase")}
        </Button>
      </div>
    ),
    filterIcon: (filtered) => <SearchOutlined style={{color: filtered ? "#1890ff" : "#999999"}} />,
    onFilter: (text, record) => {
      if (searchText) return normalize(searchText(record)).includes(normalize(text));
      else if (record[dataIndex]) {
        return normalize(record[dataIndex]).includes(normalize(text));
      } else {
        return "";
      }
    },
    onFilterDropdownVisibleChange: (visible) =>
      visible && setTimeout(() => searchInput.current?.select(), 100),
  });

  return columns.map((column) => ({
    // The column data
    ...column,
    // Enable search options if needed
    ...(column.searchable
      ? enableColumnSearchProps(column.dataIndex, column.title, column.searchText)
      : undefined),
  }));
};

// Same function as in api/src/controllers/pdf.ts
export const cleanAnswer = (answer, separator = ",\n") => {
  // If the answer is a checkbox answer, it is like an object. So we keep only the true values and display them
  if (typeof answer === "object") {
    if (answer.maskName && answer.value) {
      // If the answer looks like a formatted phone number...
      return answer.value;
    } else {
      // Else if it looks like a multiple choice answser...
      return Object.entries(answer)
        .filter((entry) => entry[1])
        .map((entry) => entry[0])
        .join(separator);
    }
  } else {
    return answer;
  }
};

export const ROLES_MAPPING = [
  {label: t("registrations:schema.role.options.guest"), value: "guest", color: "#6cdac5"},
  {label: t("registrations:schema.role.options.contrib"), value: "contrib", color: "#5f98ed"},
  {label: t("registrations:schema.role.options.admin"), value: "admin", color: "#1330f5"},
];

export const roleTag = (text, small = false) => {
  const role = ROLES_MAPPING.find((r) => r.value === text);
  return <Tag color={role?.color}>{small ? role?.label[0] : role?.label}</Tag>;
};

// Generate participants columns
export const generateRegistrationsColumns = (project, {start, middle, end} = {}) =>
  [
    ...(start || []),
    {
      title: t("users:schema.firstName.label"),
      dataIndex: "firstName",
      render: (text, record) => (
        <>
          {record.role && roleTag(record.role, true)}
          {record.user?.firstName}
        </>
      ),
      sorter: (a, b) => listSorter.text(a.user?.firstName, b.user?.firstName),
      searchable: true,
      searchText: (record) => record.user?.firstName,
    },
    {
      title: t("users:schema.lastName.label"),
      dataIndex: "lastName",
      render: (text, record) => record.user?.lastName,
      sorter: (a, b) => listSorter.text(a.user?.lastName, b.user?.lastName),
      defaultSortOrder: "ascend",
      searchable: true,
      searchText: (record) => record.user?.lastName,
    },
    {
      title: t("users:schema.email.label"),
      dataIndex: "email",
      render: (text, record) => (
        <>
          {record.invitationToken && <WaitingInvitationTag />}
          {record.user.email}
        </>
      ),
      sorter: (a, b) => listSorter.text(a.user.email, b.user.email),
      searchable: true,
      searchText: (record) => record.user.email,
    },
    ...(middle || []),
    ...(project.formMapping?.map(({formField, columnName}) => ({
      title: columnName,
      dataIndex: columnName,
      render: (text, record) => cleanAnswer(record.specific?.[formField]),
      sorter: (a, b) =>
        listSorter.text(cleanAnswer(a.specific?.[formField]), cleanAnswer(b.specific?.[formField])),
      searchable: true,
      searchText: (record) => cleanAnswer(record.specific?.[formField]),
    })) || []),
    {
      title: t("common:schema.tags.label"),
      dataIndex: "tags",
      sorter: (a, b) => listSorter.text(a.tags?.join(" "), b.tags?.join(" ")),
      render: (text, record) => record.tags?.map((tagName) => <Tag>{tagName}</Tag>),
      searchable: true,
      searchText: (record) => record.tags?.join(" "),
    },
    project.useTeams && {
      title: t("teams:label"),
      dataIndex: "team",
      render: (text, record) =>
        record.teamsSubscriptions.map((teamSubscription) => teamSubscription.team.name).join(", "),
      sorter: (a, b) => listSorter.text(a.teamsSubscriptionsNames, b.teamsSubscriptionsNames),
      searchable: true,
      searchText: (record) => record.teamsSubscriptionsNames,
    },
    {
      title: t("registrations:numberOfDaysOfPresence.label"),
      dataIndex: "numberOfDaysOfPresence",
      sorter: (a, b) => listSorter.number(a.numberOfDaysOfPresence, b.numberOfDaysOfPresence),
      searchable: true,
      searchText: (record) => record.numberOfDaysOfPresence,
      width: 95,
    },
    ...(end || []),
  ].filter((el) => !!el);

export const generateSubscriptionInfo = (registration, sessionSubscription, projectId) => {
  const sessionSubscriptionTeam = registration.teamsSubscriptions
    .map((ts) => ts.team)
    ?.find((t) => t._id === sessionSubscription.team);
  if (sessionSubscriptionTeam) {
    return (
      <>
        {t("sessions:schema.subscriptionInfo.viaTeam")}{" "}
        <a href={`${URLS.ORGA_FRONT}/${projectId}/teams/${sessionSubscriptionTeam._id}`}>
          {sessionSubscriptionTeam.name}
        </a>
      </>
    );
  }
};
