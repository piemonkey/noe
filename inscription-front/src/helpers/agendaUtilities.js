// noinspection AllyJsxUnregisteredKeyInspection

import {Button, Drawer, Dropdown, Menu, message} from "antd";
import moment from "moment";
import React, {useEffect, useState} from "react";
import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  DownOutlined,
  PictureOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import {DayView} from "@devexpress/dx-react-scheduler-material-ui";
import {FormElement} from "../components/common/FormElement";
import {InputElement} from "../components/common/InputElement";
import {currentProjectSelectors} from "../features/currentProject";
import {useSelector} from "react-redux";
import {t} from "i18next";
import {useTranslation} from "react-i18next";

//*******************************//
//******* SLOT UTILITIES ********//
//*******************************//

// DISPLAY THE SLOT NUMBER
export const slotNumberString = (slot) => {
  const numberOfSlotsInSession = slot.session.slots.length;
  return numberOfSlotsInSession > 1
    ? "(" +
        (slot.session.slots.findIndex((s) => s._id === slot._id) + 1) +
        "/" +
        numberOfSlotsInSession +
        ") "
    : "";
};

// DISPLAY THE SLOT NAME
export const getSessionName = (session, teams?) => {
  const sessionName = session.name?.length > 0 ? session.name + " - " : "";
  const teamName =
    teams?.find((t) => t._id === (session.team?._id || session.team))?.name || session.team?.name;
  const sessionNameSuffix = teamName ? ` (${teamName})` : "";
  return sessionName + session.activity?.name + sessionNameSuffix;
};

export const getElementsFromListInSlot = (elementName, slot, getKey: (s: any) => string) =>
  slot[elementName]?.length > 0 && slot[`${elementName}SessionSynchro`] === false
    ? slot[elementName].map(getKey)
    : slot.session[elementName].map(getKey);

//*******************************//
//****** DISPLAY UTILITIES ******//
//*******************************//

// Handles dates when end date is at midnight o'clock. Without it, any session ending at midnight will
// overflow on the next day, which is really annoying. This manager aims to remove this behavior.
export const midnightDateManager = {
  dataToDisplay: (slot) => {
    // If end date is midnight, remove one second
    const endDate = moment(slot.endDate);
    if (endDate.hour() === 0 && endDate.minutes() === 0) endDate.subtract(1, "minute");
    return {...slot, endDate};
  },
  displayToData: (existingEndDate, newEndDate) => {
    // If end date is midnight, remove one minute
    const endDate = moment(existingEndDate);
    newEndDate = moment(newEndDate);
    if (endDate.hour() === 0 && endDate.minutes() === 0) newEndDate.add(1, "minute");
    return newEndDate;
  },
};

// LOADING MESSAGES
const LOADING_MESSAGE_KEY = "loading-message";
export const displayLoadingMessage = () =>
  message.open({
    key: LOADING_MESSAGE_KEY,
    type: "loading",
    content: t("common:loading"),
  });

// Destroy the loading message when done loading
export const useKillLoadingMessage = () => useEffect(() => message.destroy(LOADING_MESSAGE_KEY));

// USEEFFECT HELPERS
export const trySeveralTimes = (
  actionToTry,
  whatToDoAfter,
  ...{limitOfTrials = 5, frequency = 100, errorHandling}
) => {
  let trials = 0;
  const tryInterval = setInterval(() => {
    try {
      if (actionToTry()) {
        clearInterval(tryInterval);
        whatToDoAfter && whatToDoAfter();
      } else {
        trials += 1;
        if (trials >= limitOfTrials) {
          clearInterval(tryInterval);
        }
      }
    } catch (e) {
      errorHandling && errorHandling(e);
    }
  }, frequency);
};

export const useInitialScrollToProjectAvailabilities = (agendaDisplayParams, cellDisplayHeight) => {
  // Scroll to the optimal place should be done once. If done, will be set to true in useEffect
  const [initialScrollIsDone, setInitialScrollIsDone] = useState(false);

  // Scroll on page load to reach the best optimal place to be on the view
  useEffect(() => {
    // The main table element has this class (it's not the only one, but it will always come first in the querySelector)
    if (!initialScrollIsDone && agendaDisplayParams?.shouldScroll && cellDisplayHeight) {
      trySeveralTimes(
        () => {
          // TODO difference between frontends is normal
          const mainTimeTableElement =
            document.querySelector(".MuiTable-root")?.offsetParent?.offsetParent;
          // Scroll to reach the diff between the agenda display and the project availabilities
          mainTimeTableElement.scrollTop =
            agendaDisplayParams.hoursDiff * cellDisplayHeight * (60 / CELL_DURATION_MINUTES);
          return true;
        },
        () => setInitialScrollIsDone(true),
        {frequency: 30}
      );
    }
  }, [agendaDisplayParams, cellDisplayHeight]);
};
// Synchronize the height of the little ticks on the side of the calendar

export const synchronizeTicksOnSideOfAgenda = (className, cellDisplayHeight) => {
  // The trick is to spot the timescale labels but there is no eaasy hook (class names change all the time)
  // We created a custom class on which we can get a hook, named "timescale-cell".
  trySeveralTimes(() => {
    const elements = document.querySelector(`.${className}`)?.parentNode?.parentNode?.parentNode
      ?.parentNode?.parentNode?.lastChild?.firstChild.childNodes; // Go to the TicksLayout-table-XXXX and iterate
    if (elements?.length > 0) {
      elements.forEach((el) => (el.firstChild.style.height = `${cellDisplayHeight}px`));
      return true;
    }
  });
};

//*****************************//
//******* AGENDA PARAMS *******//
//*****************************//

// Cell sizes and duration
export const CELL_DURATION_MINUTES = 30; // The smallest unit of time for drag and drop
export const CELLS_DISPLAY_HEIGHTS = [25, 35, 45];

// DEFAULT AGENDA PARAMS
export const DEFAULT_AGENDA_PARAMS = {
  slotsOnEachOther: false,
  cellDisplayHeight: CELLS_DISPLAY_HEIGHTS[1],
};

// Number of days to display by default
const MAX_DAYS_TO_DISPLAY_BY_DEFAULT = 4;
export const getNumberOfDaysToDisplay = (windowWidth, project) =>
  Math.min(
    Math.max(1, Math.ceil((windowWidth - 280) / 450)), // Screen adaption
    moment(project.end).diff(project.start, "day") + 1, // Not more than the project opening dates
    MAX_DAYS_TO_DISPLAY_BY_DEFAULT // Default limit
  );

// Returns the hour of the day based on hours and minutes
const hoursOfDay = (momentDate) => momentDate.minutes() / 60.0 + momentDate.hours();

// Tells if a slot is located on two different days
const slotEndsAfterMidnight = (slot) => {
  const start = moment(slot.start);
  const end = moment(slot.end);
  const length = end.diff(start, "minutes", true);
  return start.hour() * 60 + start.minute() + length > 1439;
};

// Get the min and max hour of all slots
const getSlotsMinMaxHours = (slots) => {
  // If no slots, return
  if (!slots || !(slots?.length > 0)) return;

  let minStart, maxEnd;
  for (const slot of slots) {
    if (slotEndsAfterMidnight(slot)) {
      // If any of the slots ends after midnight, display all the hours
      return {start: 0, end: 24};
    } else {
      // Else, compute dates depending on the
      const slotStart = hoursOfDay(moment(slot.start));
      const slotEnd = hoursOfDay(moment(slot.end));
      minStart = minStart && minStart < slotStart ? minStart : slotStart;
      maxEnd = maxEnd && maxEnd > slotEnd ? maxEnd : slotEnd;
    }
  }

  return {start: minStart, end: maxEnd};
};

export const getAgendaDisplayStartDate = (project) => {
  const now = moment();
  const start = moment(project.start);
  const end = moment(project.end);

  // If we are during the event, start at the current date. Else, start at project start date
  return (now.isBefore(start) || now.isAfter(end) ? start : now).unix() * 1000;
};

// Get the min and max hour of the project availability slots
const getProjectMinMaxHours = (project) => {
  // If there is no availability slots, don't return anything, we can't calculate.
  if (!(project?.availabilitySlots?.length > 0)) return;

  const projectSlotThatEndsAfterMidnight = project.availabilitySlots.find((slot) =>
    slotEndsAfterMidnight(slot)
  );
  // If some slots pass through midnight, we have to display everything.
  if (projectSlotThatEndsAfterMidnight) return;

  // If not, then just take the event's slots as reference
  return {
    start: hoursOfDay(moment(project.start)),
    end: hoursOfDay(moment(project.end)),
  };
};

// Get the display parameters to give to the Agenda view
export const getAgendaDisplayParams = (slots, project) => {
  let params;
  const projectMinMaxHours = getProjectMinMaxHours(project);
  const slotsMinMaxHours = getSlotsMinMaxHours(slots);

  if (projectMinMaxHours && slotsMinMaxHours) {
    // If there are both slots and project hours, calculate based on both

    const slotsExistBeforeProjectHours = slotsMinMaxHours.start < projectMinMaxHours.start;
    const slotsExistAfterProjectHours = slotsMinMaxHours.end > projectMinMaxHours.end;

    params = {
      start: Math.floor(
        slotsExistBeforeProjectHours ? slotsMinMaxHours.start : projectMinMaxHours.start
      ),
      end: Math.ceil(slotsExistAfterProjectHours ? slotsMinMaxHours.end : projectMinMaxHours.end),
      shouldScroll: true,
    };

    // Set up the scroll diff
    params.hoursDiff =
      slotsMinMaxHours && slotsExistBeforeProjectHours
        ? Math.floor(projectMinMaxHours.start) - params.start
        : 0;
  } else if (slotsMinMaxHours || projectMinMaxHours) {
    // If there are only one of them, just use the ones we have
    params = {
      start: Math.floor(
        slotsMinMaxHours?.start !== undefined ? slotsMinMaxHours?.start : projectMinMaxHours?.start
      ),
      end: Math.ceil(
        slotsMinMaxHours?.end !== undefined ? slotsMinMaxHours?.end : projectMinMaxHours?.end
      ),
    };
  } else {
    // if there are none, fail safe
    params = {start: 0, end: 24};
  }

  params.start = Math.max(params.start - 1, 0); // Display one hour before display start, but do not go below 0 hours
  params.end = Math.min(params.end + 1, 24); // Display one hour after display end, but do not go after 24 hours
  return params;
};

//************************************//
//****** AGENDA BASE COMPONENTS ******//
//************************************//

// Left and right arrows to navigate in the agenda days
export const NavButton = ({type, onClick}) => (
  <Button
    type="link"
    size="large"
    icon={type === "forward" ? <ArrowRightOutlined /> : <ArrowLeftOutlined />}
    onClick={onClick}
  />
);

// Renders the date labels on the side of the agenda
export const getTimeScaleLabelComponent =
  (cellDisplayHeight) =>
  ({time, ...otherProps}) => {
    // If there is no 'time', it means it's the very first, which has no time in it.
    const style = time
      ? {height: cellDisplayHeight, lineHeight: `${cellDisplayHeight}px`}
      : {height: cellDisplayHeight / 2}; // It should be twice as small.

    // Only display time when it's o'clock
    const isOClock = time?.getMinutes() === 0;

    return (
      <DayView.TimeScaleLabel
        className="timescale-cell" // Needed to get a hook
        time={isOClock ? time : undefined}
        style={style}
        {...otherProps}
      />
    );
  };

//*********************************//
//****** AGENDA CONTROLS BAR ******//
//*********************************//

export const NumberOfDaysSelector = ({
  numberOfDaysDisplayed,
  setNumberOfDaysDisplayed,
  isMobileView = false,
}) => {
  const {t} = useTranslation();
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  return (
    <Dropdown
      trigger={["click"]}
      overlay={
        <Menu
          onClick={(item) => displayLoadingMessage().then(() => setNumberOfDaysDisplayed(item.key))}
          items={[
            {
              label: t("sessions:agenda.numberOfDaysSelector.allTheEvent"),
              key:
                moment(currentProject.end)
                  .startOf("day")
                  .diff(moment(currentProject.start).startOf("day"), "days") + 1,
            },
            {type: "divider"},
            {label: t("sessions:agenda.numberOfDaysSelector.days", {count: 1}), key: 1},
            {label: t("sessions:agenda.numberOfDaysSelector.days", {count: 3}), key: 3},
            {label: t("sessions:agenda.numberOfDaysSelector.days", {count: 5}), key: 5},
            {type: "divider"},
            {
              label: "Autre",
              children: [...Array(14).keys()].map((i) => ({
                label: t("sessions:agenda.numberOfDaysSelector.days", {count: i + 2}),
                key: i + 2,
              })),
            },
          ]}
        />
      }>
      <Button>
        {isMobileView
          ? t("sessions:agenda.numberOfDaysSelector.daysMobile", {
              count: parseInt(numberOfDaysDisplayed),
            })
          : t("sessions:agenda.numberOfDaysSelector.days", {
              count: parseInt(numberOfDaysDisplayed),
            })}
        {!isMobileView && <DownOutlined style={{marginLeft: 6}} />}
      </Button>
    </Dropdown>
  );
};

export const AgendaSettingsDrawer = ({
  children,
  cellDisplayHeight,
  slotsOnEachOther,
  setAgendaParams,
  buttonStyle,
}) => {
  const [drawerVisible, setDrawerVisible] = useState(false);

  const printAgendaView = () => {
    const toHide = [
      document.getElementsByTagName("aside")[0],
      document.getElementsByTagName("header")[0],
      document.getElementsByClassName("navbar-menu-button")[0],
      document.getElementsByClassName("MuiToolbar-root")[0],
      document.getElementsByClassName("session-filter-toolbar")[0],
      document.getElementsByClassName("ant-segmented")[0],
      ...document.getElementsByClassName("ant-drawer"),
    ].filter((el) => !!el);
    const toHideWithOldValues = toHide.map((el) => ({el, value: el.style.display}));

    // Get the before-last-one, then last child
    const agendaSpace = Array.from(
      document.getElementsByClassName("scheduler-wrapper")[0].childNodes
    ).slice(-2, -1)[0].lastChild;

    toHide.forEach((el) => (el.style.display = "none"));
    agendaSpace.style.overflowY = "visible";

    window.print();

    toHideWithOldValues.forEach(({el, value}) => (el.style.display = value));
    agendaSpace.style.overflowY = "auto";
  };

  return (
    <>
      <Button
        style={buttonStyle}
        icon={<SettingOutlined />}
        type="link"
        shape="circle"
        onClick={() => setDrawerVisible(true)}
      />
      <Drawer placement="right" onClose={() => setDrawerVisible(false)} visible={drawerVisible}>
        <h3>{t("sessions:agenda.settingsDrawer.title")}</h3>
        <FormElement style={{marginTop: 26}} className="container-grid">
          {children}

          {/*Select if we want to see events one on the other or one next to each other. */}
          <InputElement.Select
            label={t("sessions:agenda.settingsDrawer.slotsOnEachOther.label")}
            onChange={(value) =>
              displayLoadingMessage().then(() => setAgendaParams({slotsOnEachOther: value}))
            }
            defaultValue={slotsOnEachOther}
            options={[
              {
                value: false,
                label: t("sessions:agenda.settingsDrawer.slotsOnEachOther.options.false"),
              },
              {
                value: true,
                label: t("sessions:agenda.settingsDrawer.slotsOnEachOther.options.true"),
              },
            ]}
          />

          {/*Increase or decrease the cells heights*/}
          <InputElement.Select
            label={t("sessions:agenda.settingsDrawer.cellDisplayHeight.label")}
            onChange={(value) =>
              displayLoadingMessage().then(() => setAgendaParams({cellDisplayHeight: value}))
            }
            defaultValue={cellDisplayHeight}
            options={[
              {
                value: CELLS_DISPLAY_HEIGHTS[0],
                label: t("sessions:agenda.settingsDrawer.cellDisplayHeight.options.compact"),
              },
              {
                value: CELLS_DISPLAY_HEIGHTS[1],
                label: t("sessions:agenda.settingsDrawer.cellDisplayHeight.options.comfort"),
              },
              {
                value: CELLS_DISPLAY_HEIGHTS[2],
                label: t("sessions:agenda.settingsDrawer.cellDisplayHeight.options.large"),
              },
            ]}
          />

          <Button icon={<PictureOutlined />} onClick={printAgendaView}>
            {t("sessions:agenda.settingsDrawer.exportAgendaViewButton")}
          </Button>
        </FormElement>
      </Drawer>
    </>
  );
};
