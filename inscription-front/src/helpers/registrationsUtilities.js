import {slotEndIsBeforeBeginning} from "../components/utils/Availability";
import {MailOutlined} from "@ant-design/icons";
import {Tag} from "antd";
import {useTranslation} from "react-i18next";
import {t} from "i18next";

export const flatFormInputs = (components) => {
  if (!components) return [];

  const tree = components.map((comp) => {
    if (comp.input) {
      // If the component is a simple one, return it
      return comp;
    } else {
      let branchs = [];
      // Put together all child components present in the "components" and "columns" arguments, and then apply recursively the flatFormInputs function on it
      if (comp.components) branchs = branchs.concat(comp.components);
      if (comp.columns) branchs = branchs.concat(comp.columns);
      return flatFormInputs(branchs);
    }
  });
  // Flatten the return value
  return tree.flat();
};

const truncate = (str, n) => str.replace(new RegExp("(.{" + n + "})..+"), "$1...");

export const getRegistrationMetadata = (registration, project) => {
  const data = {};
  if (registration) {
    // Availabilities are clean
    const firstSlot = registration.availabilitySlots?.[0];
    const firstSlotIsDefined = !!(firstSlot?.start && firstSlot?.end);
    data.firstSlotEndIsBeforeBeginning = firstSlotIsDefined && slotEndIsBeforeBeginning(firstSlot);
    data.firstSlotIsOk = firstSlotIsDefined && !data.firstSlotEndIsBeforeBeginning;

    data.datesAlert =
      registration.booked && !firstSlotIsDefined
        ? t("registrations:registrationValidation.selectDatesToRegister")
        : data.firstSlotEndIsBeforeBeginning
        ? t("registrations:registrationValidation.endCannotBeBeforeStart")
        : undefined;

    // All mandatory form fields are filled
    const unfulfilledMandatoryFormFields = [];
    for (let formComponent of flatFormInputs(project.formComponents)) {
      if (formComponent.validate.required) {
        if (registration.specific) {
          const value = registration.specific[formComponent.key];
          if (value === undefined || value === "" || value === [] || value === false) {
            unfulfilledMandatoryFormFields.push(formComponent.label);
          }
        } else {
          // If the form response object doesn't exist, it means the user has not touched the form yet.
          // So only give an alert if the user is already booked (ie. he has at least clicked on "Save")
          unfulfilledMandatoryFormFields.push(formComponent.label);
        }
      }
    }
    data.formIsOk = unfulfilledMandatoryFormFields.length === 0;
    data.formAlert =
      registration.booked && !data.formIsOk
        ? t("registrations:registrationValidation.someFormFieldsAreMandatory", {
            list: unfulfilledMandatoryFormFields.map((field) => truncate(field, 30)).join('", "'),
          })
        : undefined;

    // Mandatory Ticketing
    data.ticketingIsOk =
      !project.ticketingMode || registration?.[`${project.ticketingMode}Tickets`]?.length > 0;
    data.ticketingAlert =
      registration.booked && !data.ticketingIsOk
        ? t("registrations:registrationValidation.ticketingIsMandatory")
        : undefined;

    data.everythingIsOk = data.firstSlotIsOk && data.formIsOk && data.ticketingIsOk;

    return data;
  }
};

export const getInitializedRegistration = (registration, project, initializeDatabaseMetadata) => {
  const validationFields = getRegistrationMetadata(registration, project);
  const newRegistration = {
    ...registration,
    ...validationFields, // instant validation fields, always up to date
    touched: !initializeDatabaseMetadata, // To tell if the registration has been touched and is not in sync anymore with database
  };
  if (initializeDatabaseMetadata) {
    newRegistration.inDatabase = validationFields; // To keep a copy of the saved state
  }
  return newRegistration;
};

export const WaitingInvitationTag = () => {
  const {t} = useTranslation();
  return (
    <Tag icon={<MailOutlined />} color="processing">
      {t("users:waitingInvitation")}
    </Tag>
  );
};
