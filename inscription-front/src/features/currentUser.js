import {createSlice} from "@reduxjs/toolkit";
import {navigate} from "@reach/router";
import {projectsActions} from "./projects.js";
import {currentProjectActions} from "./currentProject.js";
import {usersActions} from "./users.js";
import {fetchWithMessages} from "../helpers/reduxUtilities";
import {sessionsActions} from "./sessions";
import {viewActions} from "./view";
import {registrationsActions} from "./registrations";
import i18n, {t} from "i18next";
import {URLS} from "../app/configuration";

export const currentUserSlice = createSlice({
  name: "currentUser",
  initialState: {
    user: {},
    authenticatedUser: {},
    connected: undefined,
    connectionError: undefined,
    connectionNotice: undefined,
    dirty: false,
  },
  reducers: {
    changeLogin: (state, action) => {
      state.user.email = action.payload;
    },
    changeLastName: (state, action) => {
      state.user.lastName = action.payload;
    },
    changeFirstName: (state, action) => {
      state.user.firstName = action.payload;
    },
    changeUser: (state, action) => {
      state.user = action.payload;
    },
    changeAuthenticatedUser: (state, action) => {
      state.authenticatedUser = action.payload;
    },
    changePassword: (state, action) => {
      state.user.password = action.payload;
    },
    changeConnected: (state, action) => {
      state.connected = action.payload;
    },
    changeConnectionError: (state, action) => {
      state.connectionError = action.payload;
    },
    changeConnectionNotice: (state, action) => {
      state.connectionNotice = action.payload;
    },
    reset: (state, action) => {
      state.user = {};
      state.authenticatedUser = {};
      state.connected = false;
    },
    setDirty: (state, action) => {
      state.dirty = action.payload;
    },
  },
});

const asyncActions = {
  signUp: () => async (dispatch, getState) => {
    const state = getState();

    fetchWithMessages(
      "users",
      {
        noAuthNeeded: true,
        method: "POST",
        body: state.currentUser.user,
      },
      {},
      false
    )
      .then(() => {
        navigate("./login");
        dispatch(
          currentUserActions.changeConnectionNotice(t("users:alerts.accountSuccessfullyCreated"))
        );
      })
      .catch(() =>
        dispatch(currentUserActions.changeConnectionError(t("users:alerts.emailAlreadyInUse")))
      );
  },
  logIn: () => async (dispatch, getState) => {
    const state = getState();
    try {
      const data = await fetchWithMessages(
        "auth/authenticate",
        {
          noAuthNeeded: true,
          method: "POST",
          body: state.currentUser.user,
        },
        {200: t("users:messages.logInSuccessful")},
        false
      );

      localStorage.setItem("token", data.jwt_token);
      dispatch(projectsActions.clean());
      dispatch(currentProjectActions.cleanProject());
      dispatch(currentUserActions.changeConnected(undefined));
    } catch (e) {
      dispatch(
        currentUserActions.changeConnectionError(t("users:alerts.emailAndPasswordDontMatch"))
      );
    }
  },
  logOut: () => async (dispatch, getState) => {
    dispatch(currentProjectActions.cleanProject());
    dispatch(viewActions.setSearchParams({}));
    dispatch(projectsActions.cleanProjectList());
    localStorage.removeItem("token");
    localStorage.removeItem("x-connected-as-user");
    dispatch(currentUserActions.reset());
    dispatch(registrationsActions.setCurrent(undefined));
    dispatch(registrationsActions.setAuthenticated(undefined));
  },
  refreshAuthTokens: () => async (dispatch, getState) => {
    const state = getState();

    // Clean the connected-as-user token if there is no impersonated user
    // for the moment (ie. both current and authenticated users are equal)
    if (
      state.currentUser.user._id === state.currentUser.authenticatedUser._id &&
      localStorage.getItem("x-connected-as-user")?.length > 0
    ) {
      localStorage.removeItem("x-connected-as-user");
    }

    // If no login token, then it means the user should disconnect, that's all
    if (!localStorage.getItem("token")) {
      dispatch(currentUserActions.changeConnected(false));
      return;
    }

    try {
      // If there is a login token, check it
      const data = await fetchWithMessages(
        "auth/refreshAuthTokens",
        {method: "GET"},
        {
          401: {
            type: "info",
            message: t("users:messages.youHaveBeenDisconnectedNotice"),
          },
        }
      );

      // Change connection status and store the connection token
      localStorage.setItem("token", data.jwt_token);
      dispatch(currentUserActions.changeConnected(true));

      // Change the connected currentUser and real authenticatedUser
      dispatch(currentUserActions.changeUser(data.user));
      dispatch(usersActions.changeEditing(data.authenticatedUser));
      dispatch(currentUserActions.changeAuthenticatedUser(data.authenticatedUser));
    } catch (e) {
      dispatch(currentUserActions.changeConnected(false));
    }
  },
  changeConnectedAsUser: (newConnectedAsUser) => async (dispatch, getState) => {
    const state = getState();
    const currentUser = state.currentUser.user;
    const projectId = state.currentProject.project._id;

    // If newConnectedAsUser is unset, reset to the original authenticated user
    if (!newConnectedAsUser) newConnectedAsUser = state.currentUser.authenticatedUser;

    // If the new user id is different from the connectedAsUser, then make the changes
    if (newConnectedAsUser._id !== currentUser._id) {
      await dispatch(currentUserActions.setDirty(true));

      await dispatch(registrationsActions.setCurrent(undefined));
      await dispatch(currentUserActions.changeUser(newConnectedAsUser));
      localStorage.setItem("x-connected-as-user", newConnectedAsUser._id);

      await dispatch(currentUserActions.refreshAuthTokens());
      await dispatch(sessionsActions.changeListFiltered([]));

      dispatch(currentUserActions.setDirty(false));

      // Reinitialize the sessions list with data relative to the impersonated user
      dispatch(sessionsActions.initList({list: [], project: undefined}));
      dispatch(sessionsActions.loadList(projectId));
    }
  },
  forgotPassword: () => async (dispatch, getState) => {
    const state = getState();

    fetchWithMessages("auth/password/sendResetEmail", {
      noAuthNeeded: true,
      noResponseData: true,
      method: "POST",
      // TODO test
      queryParams: {lang: i18n.language},
      body: {
        email: state.currentUser.user.email,
        url: URLS.CURRENT,
      },
    }).then(() => {
      dispatch(projectsActions.clean());
      dispatch(currentProjectActions.cleanProject());
      dispatch(
        currentUserActions.changeConnectionNotice(t("users:alerts.passwordResetEmailSentNotice"))
      );
    });
  },
  checkPasswordResetToken: (token) => async (dispatch, getState) => {
    fetchWithMessages(`auth/password/checkResetToken/${token}`, {
      noAuthNeeded: true,
      noResponseData: true,
      method: "GET",
    })
      .then(() => {
        dispatch(projectsActions.clean());
        dispatch(currentProjectActions.cleanProject());
      })
      .catch(() =>
        dispatch(
          currentUserActions.changeConnectionError(t("users:alerts.paswordResetTokenInvalid"))
        )
      );
  },
  resetPassword: (token) => async (dispatch, getState) => {
    const state = getState();

    fetchWithMessages("auth/password/reset", {
      noAuthNeeded: true,
      noResponseData: true,
      method: "POST",
      body: {
        token: token,
        password: state.currentUser.user.password,
      },
    })
      .then(() => {
        dispatch(projectsActions.clean());
        dispatch(currentProjectActions.cleanProject());
        dispatch(
          currentUserActions.changeConnectionNotice(t("users:alerts.passwordHasBeenChanged"))
        );
        navigate("./login");
      })
      .catch(() =>
        dispatch(
          currentUserActions.changeConnectionError(t("users:alerts.errorWhenChangingPassword"))
        )
      );
  },
  deleteAccount: () => async (dispatch, getState) => {
    const state = getState();
    await fetchWithMessages(
      `users/${state.currentUser.authenticatedUser._id}`,
      {method: "DELETE", noResponseData: true},
      {200: t("users:messages.accountSuccessfullyDeleted")}
    );
    dispatch(currentUserActions.logOut());
  },
};

export const currentUserSelectors = {
  selectUser: (state) => state.currentUser.user,
  selectAuthenticatedUser: (state) => state.currentUser.authenticatedUser,
  selectConnected: (state) => state.currentUser.connected,
  selectConnectionError: (state) => state.currentUser.connectionError,
  selectConnectionNotice: (state) => state.currentUser.connectionNotice,
  selectDirty: (state) => state.currentUser.dirty,
};

export const currentUserReducer = currentUserSlice.reducer;

export const currentUserActions = {
  ...currentUserSlice.actions,
  ...asyncActions,
};
