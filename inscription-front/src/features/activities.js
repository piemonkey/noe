import {createSlice} from "@reduxjs/toolkit";
import {loadListFromBackend} from "../helpers/reduxUtilities";

export const activitiesSlice = createSlice({
  name: "activities",
  initialState: {
    list: [],
    init: false,
    editing: {},
  },
  reducers: {
    initContext: (state, action) => {
      state.init = action.payload;
    },
    initList: (state, action) => {
      // console.log('initList',action.payload);
      state.init = action.payload.project;
      state.list = action.payload.list;
    },
  },
});

const asyncActions = {
  loadList: () => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await loadListFromBackend(
      "activities",
      projectId,
      state.activities.init,
      () => dispatch(activitiesActions.initContext(projectId)),
      (data) => dispatch(activitiesActions.initList({list: data, project: projectId}))
    );
  },
};

export const activitiesSelectors = {
  selectList: (state) => state.activities.list,
};

export const activitiesReducer = activitiesSlice.reducer;

export const activitiesActions = {
  ...activitiesSlice.actions,
  ...asyncActions,
};
