import {createSlice} from "@reduxjs/toolkit";
import moment from "moment";
import {
  fetchWithMessages,
  loadListFromBackend,
  loadEntityFromBackend,
  displayLoadingMessage,
  killLoadingMessage,
} from "../helpers/reduxUtilities";
import {getSessionMetadata, getVolunteeringCoefficient} from "../helpers/sessionsUtilities";
import {searchInObjectsList} from "../helpers/listUtilities";
import {registrationsActions} from "./registrations";
import {ACTIVATING_OFFLINE_MODE, OFFLINE_MODE} from "../helpers/offlineModeUtilities";
import {t} from "i18next";

export const specialCategoriesFilterOptions = ["volunteering", "allTheRest"];

const computeSessionEnd = (session) => {
  if (session.slots) {
    return moment.max(...session.slots.map((s) => moment(s.end))).format();
  } else {
    return undefined;
  }
};

const computeSessionStart = (session) => {
  if (session.slots) {
    return moment.min(...session.slots.map((s) => moment(s.start))).format();
  } else {
    return undefined;
  }
};
const computeSessionStartAndEnd = (session) => {
  session.start =
    session.slots && session.slots.length > 0 ? computeSessionStart(session) : session.start;
  session.end =
    session.slots && session.slots.length > 0 ? computeSessionEnd(session) : session.end;
};

const compareDate = (d1, d2) => {
  return (
    d1.getFullYear() === d2.getFullYear() &&
    d1.getMonth() === d2.getMonth() &&
    d1.getDate() === d2.getDate()
  );
};

const computeSameTimeImpactSubscription = (sessionList, updatedSession, registration) => {
  const updatedImpacts = [];
  for (const impactedSession of updatedSession.sameTimeSessions) {
    let impactedSessionInList = sessionList.find(
      (s) => s._id === impactedSession.sameTimeSession._id
    );
    if (impactedSessionInList) {
      impactedSessionInList = {...impactedSessionInList};
      impactedSessionInList.sameTimeSessions = [...impactedSessionInList.sameTimeSessions];
      const impactedSameTimeSessionIndex = impactedSessionInList.sameTimeSessions.findIndex(
        (sts) => sts.sameTimeSession._id === updatedSession._id
      );
      let impactedSameTimeSession =
        impactedSessionInList.sameTimeSessions[impactedSameTimeSessionIndex];
      impactedSameTimeSession = {...impactedSameTimeSession};
      impactedSameTimeSession.sameTimeSessionRegistered = registration;
      impactedSessionInList.sameTimeSessions[impactedSameTimeSessionIndex] =
        impactedSameTimeSession;

      updatedImpacts.push(impactedSessionInList);
    }
  }
  return updatedImpacts;
};

export const sessionsSlice = createSlice({
  name: "sessions",
  initialState: {
    init: undefined,
    list: [],
    listFiltered: [],
    editing: {},
    filter: {
      searchBar: "",
      registeredFilter: undefined,
      availableFilter: false,
      showPastSessionsFilter: false,
      stewardFilter: false,
      dateFilter: undefined,
      categoriesFilter: [],
    },
    slotList: [],
    slotListFiltered: [],
    initCategories: false,
    categories: [],
  },
  reducers: {
    updateInList: (state, action) => {
      state.list[state.list.findIndex((session) => session._id === action.payload._id)] =
        action.payload;
    },
    initList: (state, action) => {
      state.init = action.payload.project;
      state.list = action.payload.list;
      state.slotList = state.list
        .map((session) => session.slots.map((slot) => ({...slot, session})))
        .flat();
    },
    setEditing: (state, action) => {
      state.editing = action.payload;
    },
    selectEditingByIdFromList: (state, action) => {
      state.editing = state.list[state.list.map((session) => session._id).indexOf(action.payload)];
    },
    changeEditing: (state, action) => {
      let newSession = action.payload;
      let oldSession = {...state.editing};

      if (newSession.activity) {
        if (
          (oldSession.activity && newSession.activity._id !== oldSession.activity._id) ||
          (!oldSession.activity && newSession._id === "new")
        ) {
          newSession.slots = newSession.activity.slots.map((s) => ({
            duration: s.duration,
            stewardsSessionSynchro: true,
            placesSessionSynchro: true,
          }));

          // Create the slots based on the session start
          let timeTrace = moment(newSession.start);
          for (const slot of newSession.slots) {
            slot.start = timeTrace.format();
            timeTrace.add(slot.duration, "minutes");
            slot.end = timeTrace.format();
          }
        }
      }

      state.editing = {
        ...newSession,
        stewards: newSession.stewards || state.editing.stewards,
        places: newSession.places || state.editing.places,
        slots: newSession.slots || state.editing.slots,
      };

      computeSessionStartAndEnd(state.editing);
    },
    changeFilter: (state, action) => {
      state.filter = {
        ...state.filter,
        ...action.payload,
      };
    },
    changeListFiltered: (state, action) => {
      state.listFiltered = action.payload;
      state.slotListFiltered = state.listFiltered
        .map((session) => session.slots.map((slot) => ({...slot, session: session})))
        .flat();
    },
    resetState: (state) => {
      state.init = false;
      state.initCategories = false;
      state.list = [];
      state.listFiltered = [];
      state.slotList = [];
      state.slotListFiltered = [];
      state.categories = [];
    },

    // Categories stuff
    initCategoriesContext: (state, action) => {
      state.initCategories = action.payload;
    },
    initCategoriesList: (state, action) => {
      state.categories = action.payload;
    },
  },
});

const multiStateActions = {
  updateFilteredList: (payload) => (dispatch, getState) => {
    const state = getState();

    // update the filter
    const filter = {...state.sessions.filter, ...payload};
    dispatch(sessionsActions.changeFilter(payload));

    // Apply filter & update the data
    const registeredFilter = filter.registeredFilter;
    const availableFilter = filter.availableFilter;
    const showPastSessionsFilter = filter.showPastSessionsFilter;
    const stewardFilter = filter.stewardFilter;
    const dateFilter = new Date(filter.dateFilter);

    // Filter with searchbar
    let filteredSessions = searchInObjectsList(filter.searchBar, state.sessions.list, (session) => [
      session.name,
      session.activity.name,
      session.activity.category.name,
      ...(session.activity.secondaryCategories || []),
    ]);

    // Filter with all the rest
    filteredSessions = filteredSessions
      .filter((session) => {
        // This code is shared across frontends, cf. orga-front/src/helpers/agendaUtilities.js filterAppointments()
        let categories;
        if (filter.categoriesFilter?.length > 0) {
          // Filter by "special categories"
          let specialCategoriesFilter;
          const filterVolunteering = filter.categoriesFilter.includes("volunteering");
          const filterAllTheRest = filter.categoriesFilter.includes("allTheRest");
          if (filterAllTheRest && !filterVolunteering) {
            specialCategoriesFilter = getVolunteeringCoefficient(session) === 0;
          } else if (filterVolunteering && !filterAllTheRest) {
            specialCategoriesFilter = getVolunteeringCoefficient(session) > 0;
          } else if (filterVolunteering && filterAllTheRest) {
            specialCategoriesFilter = true; // if we have both volunteering and allTheRest, it's like we don't filter at all
          } else {
            specialCategoriesFilter = false; // if we have none, then we don't wanna filter with it at all
          }

          // Then filter only by category so we need to remove the special categories entries from the rest
          const pureCategoriesFilter = filter.categoriesFilter.filter(
            (category) => !specialCategoriesFilterOptions.includes(category)
          );

          categories =
            specialCategoriesFilter ||
            (pureCategoriesFilter.length > 0
              ? pureCategoriesFilter.includes(session.activity.category._id)
              : false); // Don't filter if there are no pure categories selected
        } else {
          categories = true; // Don't filter if deactivated
        }

        const subscribed =
          registeredFilter === undefined || registeredFilter === null // Not set, or set to "All"
            ? true // Don't filter if deactivated
            : session.subscribed === registeredFilter || session.isSteward; // Either it is, and we display the ones for the selection + sessions where we are steward

        const steward = stewardFilter ? session.isSteward === stewardFilter : true; // Don't filter if deactivated

        const date = filter.dateFilter ? compareDate(dateFilter, new Date(session.start)) : true; // Don't filter if deactivated

        const showPastSessions = showPastSessionsFilter
          ? true
          : moment(session.end).isAfter(moment()); // Filter sessions that are only after now

        let available;
        if (availableFilter) {
          const currentProject = state.currentProject.project;
          const currentRegistration = state.registrations.current;

          const {
            isSteward,
            sessionIsFull,
            participantIsNotAvailable,
            alreadyStewardOnOtherSession,
            alreadySubscribedToOtherSession,
            registrationIncomplete,
          } = getSessionMetadata(session, currentRegistration);

          const subscriptionIsDisabled =
            sessionIsFull ||
            registrationIncomplete ||
            ((alreadySubscribedToOtherSession || alreadyStewardOnOtherSession) &&
              currentProject.notAllowOverlap) ||
            participantIsNotAvailable ||
            isSteward;

          available = !subscriptionIsDisabled;
        } else {
          available = true;
        }

        return categories && subscribed && showPastSessions && steward && date && available;
      })
      .sort((a, b) => (moment(a.start).isBefore(b.start) ? -1 : 1));

    dispatch(sessionsActions.changeListFiltered(filteredSessions));
  },
};

type LoadType = "subscribed";

const asyncActions = {
  loadList:
    (options: {
      type: LoadType, // No type means we get everything
      fromDate: Date | Number,
      silent: boolean,
    }) =>
    async (dispatch, getState) => {
      const state = getState();
      const projectId = state.currentProject.project._id;

      if (state.sessions.init !== projectId) {
        dispatch(sessionsActions.initList({list: [], project: projectId}));

        !options?.silent && displayLoadingMessage();
        const data = await fetchWithMessages(`projects/${projectId}/sessions/selectiveLoad`, {
          method: "POST",
          queryParams: {
            loadParticipantsData: OFFLINE_MODE,
            ...(ACTIVATING_OFFLINE_MODE ? {} : options), // If activation of offline mode is on, just get everything directly
          },
          body: {
            alreadyLoaded: state.sessions.list.map((s) => s._id),
          },
        });

        !options?.silent && killLoadingMessage();
        if (!data) return; // If fetch fails, don't go further

        const newSessionsList = [...data.list, ...state.sessions.list];
        dispatch(sessionsActions.initList({list: newSessionsList, project: projectId}));
        dispatch(multiStateActions.updateFilteredList());

        // If not all sessions are loaded, then load the rest automatically
        if (!data.allLoaded) {
          const dataRest = await fetchWithMessages(`projects/${projectId}/sessions/selectiveLoad`, {
            method: "POST",
            queryParams: {
              loadParticipantsData: OFFLINE_MODE,
            },
            body: {
              alreadyLoaded: newSessionsList.map((s) => s._id),
            },
          });

          if (dataRest?.list.length > 0) {
            const allSessions = [...dataRest.list, ...newSessionsList];
            dispatch(sessionsActions.initList({list: allSessions, project: projectId}));
            dispatch(multiStateActions.updateFilteredList());
          }
        }
      }
    },
  loadCategoriesList: () => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await loadListFromBackend(
      "categories",
      projectId,
      state.sessions.initCategories,
      () => dispatch(sessionsActions.initCategoriesContext(projectId)),
      (data) => dispatch(sessionsActions.initCategoriesList(data)),
      false
    );
  },
  loadEditing: (entityId) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    return loadEntityFromBackend(
      "sessions",
      entityId,
      projectId,
      state.sessions.editing,
      null, // We will never create new sessions from the end user front
      (data) => dispatch(sessionsActions.setEditing(data)),
      {
        navigateBackIfFail: !OFFLINE_MODE,
        notFoundAction: () =>
          OFFLINE_MODE &&
          dispatch(sessionsActions.setEditing(state.sessions.list.find((s) => s._id === entityId))),
      }
    );
  },
  subscribe: (payload) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;
    const sessionId = payload === undefined ? state.sessions.editing._id : payload;
    const sessionEndpoint = `projects/${projectId}/sessions/${sessionId}`;

    try {
      // Apply subscribing
      const {registration, updatedSession} = await fetchWithMessages(
        `${sessionEndpoint}/subscribe`,
        {
          method: "GET",
        },
        {
          200: t("sessions:messages.subscriptionSuccessful"),
          405: {
            type: "error",
            message: t("sessions:messages.noMoreSpaceAvailable"),
          },
        },
        undefined,
        true
      );

      // Always update the session even with an error, so we have the last version of it if it failed
      dispatch(sessionsActions.updateInList(updatedSession));
      const updatedImpacts = computeSameTimeImpactSubscription(
        state.sessions.list,
        updatedSession,
        true
      );
      for (let updatedImpact of updatedImpacts) {
        dispatch(sessionsActions.updateInList(updatedImpact));
      }

      // Make all the final update at the very end
      dispatch(multiStateActions.updateFilteredList());
      if (registration) dispatch(registrationsActions.setCurrentWithMetadata(registration));

      // Only if no payload was given, change the editing state
      if (!payload) dispatch(sessionsActions.setEditing(updatedSession));
    } catch (e) {
      // Do nothing
    }
  },
  unsubscribe: (payload) => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;
    const sessionId = payload === undefined ? state.sessions.editing._id : payload;
    const sessionEndpoint = `projects/${projectId}/sessions/${sessionId}`;

    // Apply subscribing
    const {registration, updatedSession} = await fetchWithMessages(
      `${sessionEndpoint}/unsubscribe`,
      {method: "GET"},
      {
        200: t("sessions:messages.unsubscribeSuccessful"),
        401: t("sessions:messages.cantUnsubscribe"),
      }
    );

    dispatch(sessionsActions.updateInList(updatedSession));
    const updatedImpacts = computeSameTimeImpactSubscription(
      state.sessions.list,
      updatedSession,
      false
    );
    for (let updatedImpact of updatedImpacts) {
      dispatch(sessionsActions.updateInList(updatedImpact));
    }

    dispatch(multiStateActions.updateFilteredList());
    dispatch(registrationsActions.setCurrentWithMetadata(registration));

    // Only if no payload was given, change the editing state
    if (!payload) dispatch(sessionsActions.setEditing(updatedSession));
  },
};

export const sessionsSelectors = {
  selectList: (state) => state.sessions.list,
  selectListFiltered: (state) => state.sessions.listFiltered,
  selectEditing: (state) => state.sessions.editing,
  selectListFilter: (state) => state.sessions.filter,

  selectSlotsList: (state) => state.sessions.slotList,
  selectSlotsListFiltered: (state) => state.sessions.slotListFiltered,

  selectCategoriesList: (state) => state.sessions.categories,
};

export const sessionsReducer = sessionsSlice.reducer;

export const sessionsActions = {
  ...sessionsSlice.actions,
  ...multiStateActions,
  ...asyncActions,
};
