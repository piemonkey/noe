import {createSlice} from "@reduxjs/toolkit";
import {fetchWithMessages} from "../helpers/reduxUtilities";
import {sessionsActions} from "./sessions";
import {placesActions} from "./places";
import {activitiesActions} from "./activities";
import {stewardsActions} from "./stewards";
import {registrationsActions} from "./registrations";

export const currentProjectSlice = createSlice({
  name: "currentProject",
  initialState: {
    project: {},
  },
  reducers: {
    clean: (state, action) => {
      state.project = {};
    },
    changeProject: (state, action) => {
      state.project = action.payload;
    },
  },
});

const asyncActions = {
  load: (projectId) => async (dispatch, getState) => {
    const state = getState();

    const currentUser = state.currentUser.user;
    const userIsConnected = !!currentUser?._id;

    // If the user is connected, load the full project, else, just get public info about the project
    const project = await fetchWithMessages(
      userIsConnected ? `projects/${projectId}` : `projects/${projectId}/public`,
      {method: "GET"}
    );

    if (!project) return; // If fetch fails, don't go further

    project.public = !userIsConnected;
    dispatch(currentProjectActions.changeProject(project));
  },
  cleanProject: () => async (dispatch, getState) => {
    await Promise.all([
      dispatch(currentProjectActions.clean()),
      dispatch(currentProjectActions.cleanProjectEntities()),
      // also clean registrations so it doesn't interfere
      dispatch(registrationsActions.setCurrent(undefined)),
      dispatch(registrationsActions.setAuthenticated(undefined)),
    ]);
  },
  cleanProjectEntities: () => async (dispatch, getState) => {
    const payload = {project: false, list: []};
    await Promise.all([
      dispatch(placesActions.initList(payload)),
      dispatch(activitiesActions.initList(payload)),
      dispatch(stewardsActions.initList(payload)),
      dispatch(sessionsActions.resetState()),
    ]);
  },
};

export const currentProjectSelectors = {
  selectProject: (state) => state.currentProject.project,
};

export const currentProjectReducer = currentProjectSlice.reducer;

export const currentProjectActions = {
  ...currentProjectSlice.actions,
  ...asyncActions,
};
