import {createSlice} from "@reduxjs/toolkit";
import {
  fetchWithMessages,
  loadEntityFromBackend,
  loadListFromBackend,
} from "../helpers/reduxUtilities";
import {
  getInitializedRegistration,
  getRegistrationMetadata,
} from "../helpers/registrationsUtilities";
import {sessionsActions} from "./sessions";
import {message, Modal, Result} from "antd";
import React from "react";
import {currentProjectSelectors} from "./currentProject";
import moment from "moment";
import {t} from "i18next";

export const registrationsSlice = createSlice({
  name: "registrations",
  initialState: {
    list: [],
    init: false,
    current: undefined,
    authenticated: undefined,
  },
  reducers: {
    addToList: (state, action) => {
      state.list = [action.payload, ...state.list];
    },
    updateInList: (state, action) => {
      state.list = [action.payload, ...state.list.filter((i) => i._id !== action.payload._id)];
    },
    initContext: (state, action) => {
      state.init = action.payload;
    },
    initList: (state, action) => {
      state.init = action.payload.project;
      state.list = action.payload.list;
    },
    setCurrent: (state, action) => {
      state.current = action.payload;
    },
    changeCurrent: (state, action) => {
      state.current = {
        ...state.current,
        ...action.payload,
      };
    },
    setAuthenticated: (state, action) => {
      state.authenticated = action.payload;
    },
    changeAuthenticated: (state, action) => {
      state.authenticated = {
        ...state.authenticated,
        ...action.payload,
      };
    },
  },
});

const asyncActions = {
  loadList: () => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;

    await loadListFromBackend(
      "registrations",
      projectId,
      state.registrations.init,
      () => dispatch(registrationsActions.initContext(projectId)),
      (data) => dispatch(registrationsActions.initList({list: data, project: projectId}))
    );
  },
  loadCurrent: () => async (dispatch, getState) => {
    const state = getState();
    const projectId = state.currentProject.project._id;
    const currentUserId = state.currentUser.user._id;
    const authenticatedUserId = state.currentUser.authenticatedUser._id;

    return loadEntityFromBackend(
      "registrations",
      "current",
      projectId,
      state.registrations.current,
      null,
      (loadedRegistration) => {
        // If there is a r, but booked is not defined yet, initialize it to false.
        // We also then consider that it's the first visit of the person on the app
        if (!loadedRegistration.booked) {
          loadedRegistration.booked = false;
          loadedRegistration.firstVisit = true;
        }

        // Update and initialize the current registration
        dispatch(registrationsActions.setCurrentWithMetadata(loadedRegistration, true, true));

        // Also change the authenticated
        if (loadedRegistration.user._id === authenticatedUserId) {
          dispatch(registrationsActions.changeAuthenticated(loadedRegistration));
        }
      },
      {
        navigateBackIfFail: false,
        notFoundAction: () => {
          // If the current registration is not yet initialized, initialize it
          if (!state.registrations.current?._id) {
            const newRegistration = {
              _id: "new",
              availabilitySlots: [],
              specific: {},
              booked: false,
              firstVisit: true,
              user: currentUserId,
              project: projectId,
            };
            dispatch(registrationsActions.setCurrentWithMetadata(newRegistration, true));
            dispatch(registrationsActions.changeAuthenticated(newRegistration));
          }
        },
        silentFailIfNotFound: true,
      }
    );
  },
  register: (silentProgressMessage) => async (dispatch, getState) => {
    const state = getState();

    const currentProject = state.currentProject.project;
    const currentRegistration = state.registrations.current;

    const registrationIsNew = currentRegistration._id === "new";

    try {
      const updatedCurrentRegistration = await fetchWithMessages(
        `projects/${currentProject._id}/registrations/` +
          (registrationIsNew ? "" : currentRegistration._id),
        {
          method: registrationIsNew ? "POST" : "PATCH", // Update, or create if there is no registration id
          body: {
            ...currentRegistration,
            booked: true,
          },
        },
        {
          200: currentRegistration.everythingIsOk
            ? currentRegistration.inDatabase.everythingIsOk && t("common:modificationsSaved")
            : silentProgressMessage
            ? null
            : {
                type: "info",
                duration: 5,
                message: t("registrations:messages.savedProgress"),
              },
        },
        t("registrations:messages.defaultError")
      );

      if (currentRegistration.everythingIsOk && !currentRegistration.inDatabase.everythingIsOk) {
        Modal.success({
          icon: false,
          content: (
            <Result
              status="success"
              title={t("registrations:messages.bravoYouAreRegistered")}
              // TODO see how to make a registration complete email notification
              // subTitle={
              //   <>
              //     <br />
              //     Vous allez recevoir un email de confirmation à votre adresse{" "}
              //     <strong>{currentRegistration.user.email}</strong>.
              //   </>
              // }
            />
          ),
          closable: true,
          maskClosable: true,
        });
      }

      // Update and initialize the current registration
      dispatch(registrationsActions.setCurrentWithMetadata(updatedCurrentRegistration, true));
      // Update in list
      dispatch(registrationsActions.updateInList(updatedCurrentRegistration));
    } catch (e) {
      return Promise.reject();
    }
  },
  unregister: () => async (dispatch, getState) => {
    const state = getState();

    const currentProject = state.currentProject.project;
    const currentRegistration = state.registrations.current;

    try {
      const updatedCurrentRegistration = await fetchWithMessages(
        `projects/${currentProject._id}/registrations/${currentRegistration._id}`,
        {
          method: "PATCH",
          body: {
            booked: false,
            availabilitySlots: [],
            specific: {},
            sessionsSubscriptions: [],
            teamsSubscriptions: [],
            steward: null,
            helloAssoTickets: [],
            customTicketingTickets: [],
          },
        },
        {200: t("registrations:messages.unregistrationSuccessful")},
        t("registrations:messages.defaultError")
      );

      // Update and initialize the current registration
      dispatch(registrationsActions.setCurrentWithMetadata(updatedCurrentRegistration, true));
      // Update in list
      dispatch(registrationsActions.updateInList(updatedCurrentRegistration));
      // Reinitialize the sessions list to remove
      dispatch(sessionsActions.updateFilteredList());
    } catch (e) {
      return Promise.reject();
    }
  },
  setCurrentWithMetadata:
    (payload, initializeDatabaseMetadata = false, resetData = false) =>
    async (dispatch, getState) => {
      const state = getState();
      const registration = resetData ? payload : {...state.registrations.current, ...payload};
      dispatch(
        registrationsActions.setCurrent({
          ...getInitializedRegistration(
            registration,
            state.currentProject.project,
            initializeDatabaseMetadata
          ),
        })
      );
    },
  getPdfPlanning: () => async (dispatch, getState) => {
    const state = getState();

    try {
      return await fetchWithMessages(
        `projects/${state.currentProject.project._id}/pdf/registrationPlanning/${state.registrations.current._id}`,
        {isBlob: true, method: "GET"},
        {},
        false,
        true
      );
    } catch {
      return Promise.reject();
    }
  },
  removeTicketFromRegistration: (ticketId) => async (dispatch, getState) => {
    const state = getState();
    const currentProject = state.currentProject.project;
    const currentRegistration = state.registrations.current;
    const ticketsField = `${currentProject.ticketingMode}Tickets`;
    const existingTickets = currentRegistration[ticketsField] || [];
    dispatch(
      registrationsActions.setCurrentWithMetadata({
        [ticketsField]: existingTickets.filter((ticket) => ticket.id !== ticketId),
      })
    );
  },
  addTicketToRegistration: (ticketId) => async (dispatch, getState) => {
    const state = getState();
    ticketId = parseInt(ticketId);
    const currentProject = state.currentProject.project;
    if (!currentProject.ticketingMode) return;
    const currentRegistration = state.registrations.current;
    const ticketsField = `${currentProject.ticketingMode}Tickets`;
    const existingTickets = currentRegistration[ticketsField] || [];

    // Light check to prevent the user giving two times the same ticket before saving changes for real.
    // Does not replace a proper project-wide check that is done in the backend later
    const existingTicketFound = existingTickets.find(
      (existingTicket) => existingTicket.id === ticketId
    );

    if (existingTicketFound) {
      message.warn(t("registrations:ticketing.messages.alreadyUsedTicket"));
    } else {
      const {firstSlotIsOk, formIsOk} = currentRegistration;
      const allTheRestOfTheRegistrationIsOk = firstSlotIsOk && formIsOk;
      fetchWithMessages(
        `projects/${currentProject._id}/ticketing/check/${ticketId}`,
        {method: "POST"},
        {
          // Don't display success message if we know the registration will be complete and saved juste after (and the user will be notified)
          200: !allTheRestOfTheRegistrationIsOk && {
            type: "success",
            duration: 4,
            message: t("registrations:ticketing.messages.ticketAdded"),
          },
          405: {type: "error", message: t("registrations:ticketing.messages.alreadyUsedTicket")},
          404: {type: "error", message: t("registrations:ticketing.messages.ticketNotFound")},
        },
        undefined,
        true
      ).then((checkedTicket) => {
        dispatch(
          registrationsActions.setCurrentWithMetadata({
            [ticketsField]: [...existingTickets, checkedTicket],
          })
        );
        // only register if the availabilities are correct, else we can't
        firstSlotIsOk && dispatch(registrationsActions.register(true));
      });
    }
  },
};

export const registrationsSelectors = {
  selectCurrent: (state) => state.registrations.current,
  selectAuthenticated: (state) => state.registrations.authenticated,
  selectList: (state) => state.registrations.list,
  selectListWithMetadata: (state) =>
    state.registrations.list?.map((r) => ({
      ...r,
      ...getRegistrationMetadata(r, currentProjectSelectors.selectProject(state)),
      teamsSubscriptionsNames: r.teamsSubscriptions?.map((t) => t.name).join(", "),
      arrivalDateTime: [...(r.availabilitySlots || [])].sort(
        (a, b) => moment(a.start) - moment(b.start)
      )?.[0]?.start,
      departureDateTime: [...(r.availabilitySlots || [])].sort(
        (a, b) => moment(b.end) - moment(a.end)
      )?.[0]?.end,
    })),
};

export const registrationsReducer = registrationsSlice.reducer;

export const registrationsActions = {
  ...registrationsSlice.actions,
  ...asyncActions,
};
