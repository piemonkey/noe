import {createSlice} from "@reduxjs/toolkit";
import {getLocalStorage, setLocalStorage} from "../helpers/localStorageUtilities";
import {DEFAULT_AGENDA_PARAMS} from "../helpers/agendaUtilities";

export const paginationPageSizes = [10, 20, 40, 60, 100];
export const defaultPagination = {pageSize: paginationPageSizes[2], current: 1};

export const getUrlSearchParams = () => {
  const urlSearchString = window.location.search;
  const searchParamsInfo = {};
  if (urlSearchString?.length > 0) {
    const urlSearchParams = new URLSearchParams(urlSearchString);
    searchParamsInfo.firstName = urlSearchParams.get("firstName");
    searchParamsInfo.lastName = urlSearchParams.get("lastName");
    searchParamsInfo.email = urlSearchParams.get("email");
    searchParamsInfo.ticketId = urlSearchParams.get("ticketId");
    searchParamsInfo.invitationToken = urlSearchParams.get("invitationToken");
  }
  return searchParamsInfo;
};

export const viewSlice = createSlice({
  name: "view",
  initialState: {
    searchParams: getUrlSearchParams(),
    scroll: {},
    sorting: {},
    pagination: {},
    columnsBlacklist: getLocalStorage("columnsBlacklist", {}),
    agendaParams: getLocalStorage("agendaParams", DEFAULT_AGENDA_PARAMS),
    sessionsViewMode: getLocalStorage("sessionsViewMode", "list"),
  },
  reducers: {
    setSearchParams: (state, action) => {
      state.searchParams = action.payload;
    },
    changeScroll: (state, action) => {
      state.scroll = {
        ...state.scroll,
        [window.location.pathname]: action.payload,
      };
    },
    changeSorting: (state, action) => {
      state.sorting = {
        ...state.sorting,
        [window.location.pathname]: action.payload,
      };
    },
    changePagination: (state, action) => {
      state.pagination = {
        ...state.pagination,
        [window.location.pathname]: action.payload,
      };
    },
    changeColumnsBlacklist: (state, action) => {
      state.columnsBlacklist = {
        ...state.columnsBlacklist,
        [window.location.pathname]: action.payload,
      };
      setLocalStorage("columnsBlacklist", state.columnsBlacklist);
    },
    changeAgendaParams: (state, action) => {
      const newParams = {
        ...state.agendaParams,
        [action.payload.projectId]: {
          ...state.agendaParams[action.payload.projectId],
          ...action.payload.params,
        },
      };
      state.agendaParams = newParams;
      setLocalStorage("agendaParams", newParams);
    },
    changeSessionsViewMode: (state, action) => {
      state.sessionsViewMode = action.payload;
      setLocalStorage("sessionsViewMode", action.payload);
    },
  },
});

const asyncActions = {};

export const viewSelectors = {
  selectScroll: (state) => state.view.scroll[window.location.pathname],
  selectSorting: (state) => state.view.sorting[window.location.pathname],
  selectPagination: (state) => state.view.pagination[window.location.pathname],
  selectColumnsBlacklist: (state) => state.view.columnsBlacklist[window.location.pathname],
  selectCustomColumnsBlacklist: (path) => (state) =>
    state.view.columnsBlacklist[window.location.pathname.split("/", 3).concat(path).join("/")],
  selectSearchParams: (state) => state.view.searchParams,
  selectAgendaParams: (state) => state.view.agendaParams[state.currentProject.project._id],
  selectSessionsViewMode: (state) => state.view.sessionsViewMode,
};

export const viewReducer = viewSlice.reducer;

export const viewActions = {
  ...viewSlice.actions,
  ...asyncActions,
};
