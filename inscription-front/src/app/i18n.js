import i18n from "i18next";
import {initReactI18next} from "react-i18next";
import BrowserLanguageDetector from "i18next-browser-languagedetector";
import resources from "../../locales";

export const initI18n = () =>
  i18n
    .use(BrowserLanguageDetector)
    .use(initReactI18next)
    .init({
      resources,
      debug: process.env !== "production",
      interpolation: {escapeValue: false},
      fallbackLng: "fr",
    });
