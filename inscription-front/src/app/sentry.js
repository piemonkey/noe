import {init, createReduxEnhancer} from "@sentry/react";
import {Integrations} from "@sentry/tracing";

export const initSentry = () =>
  process.env.NODE_ENV === "production" &&
  init({
    dsn: process.env.REACT_APP_SENTRY_DSN,
    integrations: [new Integrations.BrowserTracing()],
    environment: process.env.REACT_APP_MODE,
    tracesSampleRate: 0.2,
    normalizeDepth: 10,
  });

export const getSentryReduxEnhancer = () =>
  createReduxEnhancer({
    stateTransformer: (state) => {
      try {
        // Transform the state to remove sensitive information
        let transformedState = state;

        if (transformedState.currentUser?.user)
          transformedState = Object.assign({}, transformedState, {
            currentUser: {
              user: {xsrfToken: null},
            },
          });

        if (transformedState.currentProject?.project) {
          if (transformedState.currentProject.project.helloAsso)
            transformedState = Object.assign({}, transformedState, {
              currentProject: {
                project: {
                  helloAsso: {clientId: null, clientSecret: null},
                },
              },
            });

          if (transformedState.currentProject.project.tiBillet)
            transformedState = Object.assign({}, transformedState, {
              currentProject: {
                project: {
                  tiBillet: {apiKey: null},
                },
              },
            });
        }
        return transformedState;
      } catch (e) {
        console.error(
          "Error when transforming Redux state for Sentry. Sending without currentUser and currentProjet.",
          e
        );
        return {...state, currentUser: null, currentProject: null};
      }
    },
  });
