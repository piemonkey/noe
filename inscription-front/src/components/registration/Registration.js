import React, {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Alert, Button, Divider, message, Modal, Popconfirm, Result, Tooltip} from "antd";
import {currentProjectSelectors} from "../../features/currentProject.js";
import moment from "moment";
import {LayoutElement} from "../common/LayoutElement";
import {currentUserSelectors} from "../../features/currentUser";
import {QuestionCircleOutlined} from "@ant-design/icons";
import {RegistrationForm} from "../common/RegistrationForm";
import {RegistrationTicketing} from "./RegistrationTicketing";

import {
  isNotVisibleAnymore,
  useStickyShadow,
  useViewEventListener,
  debounce,
} from "../../helpers/viewUtilities";
import {RegistrationAvailabilities} from "./RegistrationAvailabilities";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {Link} from "@reach/router";
import {trySeveralTimes} from "../../helpers/agendaUtilities";
import {ConnectedAsAlert} from "../participants/ParticipantList";
import {textLogo, URLS} from "../../app/configuration";
import {Trans} from "react-i18next";
import {t} from "i18next";

const noeOpenSourceParagraph = (
  <div style={{marginTop: 12, opacity: 0.8}}>
    <Trans
      ns="common"
      i18nKey="noeOpenSourceParagraph"
      components={{
        noeTextLogo: (
          <img src={textLogo} height="18" style={{display: "inline-block", marginBottom: 4}} />
        ),
        mailToLink: (
          <a
            href={`mailto:${process.env.REACT_APP_CONTACT_US_EMAIL}?subject=J'aimerais%20en%20savoir%20plus%20sur%20NOÉ`}
            rel="noreferrer"
          />
        ),
      }}
    />
  </div>
);

const InfoMessage = ({message}) => (
  <Alert style={{border: "none", marginTop: 20, marginBottom: 10}} message={message} type="info" />
);

const helpCarouselContent = [
  {
    title: t("registrations:main.helpCarousel.dates.title"),
    description: t("registrations:main.helpCarousel.dates.description"),
    step: "dates",
    src: "1.gif",
  },
  {
    title: t("registrations:main.helpCarousel.form.title"),
    description: t("registrations:main.helpCarousel.form.description"),
    step: "form",
    src: "2.gif",
  },
  {
    title: t("registrations:main.helpCarousel.ticketing.title"),
    description: t("registrations:main.helpCarousel.ticketing.description"),
    step: "ticketing",
    src: "5.gif",
  },
];

// DATES
const humanizeDate = (date, customFormat) =>
  moment(date)
    .locale("fr")
    .format(customFormat || "D MMMM YYYY");

export function Registration({giveAccessToSessionsPage}) {
  const dispatch = useDispatch();

  // *********************** //
  // ******** DATA ********* //
  // *********************** //

  // SELECTORS
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

  // DATA DEFINITION
  const {booked, formIsOk, ticketingIsOk, firstSlotIsOk, datesAlert, formAlert, ticketingAlert} =
    currentRegistration;
  const stewardLinkedToRegistration = currentRegistration.steward; // can be undefined
  const currentAvailabilities = currentRegistration.availabilitySlots;
  const projectIsNotOpened = currentProject.openingState === "notOpened";
  const ticketingMandatory = !!currentProject.ticketingMode;

  // STATES
  const formIoData = useRef();
  const [showHelpModal, setShowHelpModal] = useState(false);

  // ************************ //
  // ******* HELPERS ******** //
  // ************************ //

  // Make the bottom save button sticky
  useStickyShadow(".sticky-bottom-buttons", "bottom");

  const checkForm = () =>
    formIoData.current?.isValid !== undefined ? formIoData.current.isValid : formIsOk;
  const everythingIsOkInDatabase = currentRegistration?.inDatabase.everythingIsOk;
  const shouldDisplaySaveProgressButton = firstSlotIsOk && !everythingIsOkInDatabase;

  // ************************ //
  // **** INFO MESSAGES ***** //
  // ************************ //

  const infoMessages = {
    dates: (
      <Trans
        i18nKey="main.infoMessages.dates"
        ns="registrations"
        values={{
          start: humanizeDate(currentProject.start),
          end: humanizeDate(currentProject.end),
        }}
      />
    ),
    ticketing: (
      <>
        {firstSlotIsOk &&
          `${t("registrations:main.infoMessages.datesRecap", {
            availabilities: currentAvailabilities.map((slot) =>
              t("registrations:main.infoMessages.fromDateToDate", {
                start: humanizeDate(slot.start, "D MMMM"),
                end: humanizeDate(slot.end, "D MMMM"),
              })
            ),
          })} `}
        {t("registrations:main.infoMessages.ticketing")}
      </>
    ),
  };

  // ************************* //
  // ********* REDUX ********* //
  // ************************* //

  // Save form data to backend whenever we quit or refresh the page
  useEffect(() => {
    return () => {
      dispatch(
        registrationsActions.setCurrentWithMetadata({
          specific: formIoData.current?.data,
        })
      );
    };
  }, [JSON.stringify(formIoData?.current?.data || {})]);

  // REGISTER ACTION
  const registerToProject = async (
    allowIncompleteRegistration,
    autoSaveSilentlyIfIncomplete = false
  ) => {
    // Save the form data in Redux
    dispatch(
      registrationsActions.setCurrentWithMetadata({
        specific: formIoData.current?.data,
      })
    );

    // Check if everything is ok in Redux...
    const everythingOKInRedux = firstSlotIsOk && checkForm() && ticketingIsOk;
    if (!everythingOKInRedux) {
      // If we don't allow incomplete registrations and autoSaveSilently is off, warn the user about it and stop saving
      if (!allowIncompleteRegistration && !autoSaveSilentlyIfIncomplete) {
        message.error(
          everythingIsOkInDatabase
            ? // If the registration is already OK in DB but the user wants to save with missing info, it is refused.
              t("registrations:messages.registrationIncompleteCantSaveModifications")
            : // If the registration is not yet complete, but the user only wants to register (and not just save data)
            firstSlotIsOk
            ? t("registrations:messages.registrationIncompleteCanAlreadySaveProgress")
            : t("registrations:messages.registrationIncompleteShouldSetDatesOfPresence")
        );
        return;
      }
    }

    // ... Then, persist to backend only if the first slot is not valid
    if (firstSlotIsOk) {
      // If the user is already registered, we don't want to save the data automatically of an already registered user silently, only during the first-time registration process.
      if (everythingIsOkInDatabase && autoSaveSilentlyIfIncomplete) return;

      dispatch(registrationsActions.register(autoSaveSilentlyIfIncomplete));
    }
  };

  // UNREGISTER ACTION
  const unregisterFromProject = () => {
    formIoData.current = undefined;
    dispatch(registrationsActions.unregister());
  };

  // ************************* //
  // ****** COMPONENTS ******* //
  // ************************* //

  const RegistrationButton = (props) => (
    <Button
      type="primary"
      className="success-button"
      onClick={() => registerToProject(false)}
      {...props}>
      {everythingIsOkInDatabase
        ? t("registrations:main.saveModifications")
        : t("registrations:main.registerToTheEvent")}
    </Button>
  );

  const SaveProgressButton = (props) => (
    <Tooltip title={t("registrations:main.saveProgressButton.tooltip")}>
      <Button type="primary" onClick={() => registerToProject(true, false)} {...props}>
        {t("registrations:main.saveProgressButton.title")}
      </Button>
    </Tooltip>
  );

  const UnregistrationButton = () =>
    (!stewardLinkedToRegistration || (stewardLinkedToRegistration && everythingIsOkInDatabase)) && (
      <Popconfirm
        title={
          everythingIsOkInDatabase
            ? t("registrations:main.unregisterButton.youWillBeUnregisteredFromActivities")
            : t("registrations:main.unregisterButton.youWillLooseYouProgress")
        }
        okText={
          everythingIsOkInDatabase
            ? t("registrations:main.unregisterButton.yesUnregister")
            : t("registrations:main.unregisterButton.yesRestartFromBeginning")
        }
        okButtonProps={{danger: true}}
        cancelText={t("common:no")}
        disabled={stewardLinkedToRegistration}
        icon={<QuestionCircleOutlined style={{color: "red"}} />}
        onConfirm={unregisterFromProject}>
        <Tooltip
          title={
            stewardLinkedToRegistration
              ? t("registrations:main.unregisterButton.linkedToStewardCantUnregister")
              : undefined
          }>
          <Button danger disabled={stewardLinkedToRegistration}>
            {t("registrations:main.unregisterButton.title")}
          </Button>
        </Tooltip>
      </Popconfirm>
    );

  const WelcomeAlert = ({message, description, type, className}) => (
    <Alert
      className={`my-3 ${className}`}
      message={message}
      description={
        <>
          {description}
          {noeOpenSourceParagraph}
        </>
      }
      type={type}
    />
  );

  // ************************* //
  // ******* ERGONOMY ******** //
  // ************************* //

  useViewEventListener(
    "scroll",
    debounce(() => {
      const mainRegistrationButtonNotVisible = isNotVisibleAnymore(
        document.getElementById("main-registration-button")
      );

      trySeveralTimes(() => {
        // Display a second registration button if the first one is hidden
        const secondaryRegistrationButtons = document.getElementById(
          "secondary-registration-buttons"
        );
        secondaryRegistrationButtons.style.transform = mainRegistrationButtonNotVisible
          ? "translateY(0)"
          : "translateY(90px)";
        return true;
      });
    }, 50)
  );

  return (
    <div className="page-container">
      {(currentProject.full || projectIsNotOpened) && !booked ? (
        <Result
          icon={<div></div>} // No icon
          title={
            projectIsNotOpened
              ? t("registrations:main.eventNotOpen.notYet")
              : t("registrations:main.eventNotOpen.full")
          }
          subTitle={t("registrations:main.eventNotOpen.comeBackLater")}
        />
      ) : (
        <>
          <LayoutElement.PageHeading
            className="tabs-page-header"
            title={
              everythingIsOkInDatabase
                ? t("registrations:labelMyRegistration")
                : t("registrations:labelRegister")
            }
            customButtons={
              <>
                {booked && <UnregistrationButton />}
                {shouldDisplaySaveProgressButton && (
                  <SaveProgressButton id="main-registration-button" />
                )}
                <RegistrationButton id="main-registration-button" />
              </>
            }
          />

          <ConnectedAsAlert />

          {/* WELCOME MESSAGE AND ALERTS */}
          {everythingIsOkInDatabase === false ? (
            !booked ? (
              // The user is really new and has no saved registration data
              <WelcomeAlert
                message={t("registrations:main.welcomeMessages.newUser.message", {
                  userName: currentUser.firstName,
                })}
                description={t("registrations:main.welcomeMessages.newUser.description", {
                  projectName: currentProject.name,
                })}
                type="info"
              />
            ) : (
              // Data is saved, but registration is incomplete
              <WelcomeAlert
                message={t("registrations:main.welcomeMessages.registrationSaved.message")}
                description={t("registrations:main.welcomeMessages.registrationSaved.description")}
                type="info"
              />
            )
          ) : (
            // The user is successfully registered
            <WelcomeAlert
              className="bounce-in"
              message={t("registrations:main.welcomeMessages.registrationComplete.message")}
              description={
                <Trans
                  ns="registrations"
                  i18nKey="main.welcomeMessages.registrationComplete.description"
                  components={{linkToPlanning: <Link to="./sessions/subscribed" />}}
                />
              }
              type="success"
            />
          )}

          {(datesAlert || formAlert || ticketingAlert) && (
            <Alert
              className="my-4"
              message={t("registrations:messages.registrationIncomplete")}
              type="error"
              description={
                <ul style={{margin: 0}}>
                  {[datesAlert, formAlert, ticketingAlert]
                    .filter((el) => el)
                    .map((alert) => (
                      <li>{alert}</li>
                    ))}
                </ul>
              }
            />
          )}

          {/* REGISTRATION PAGE */}
          <div style={{marginTop: 30, marginBottom: 30}} className="fade-in">
            {/* PART 1 - Availabilities */}
            <RegistrationAvailabilities
              infoMessage={<InfoMessage message={infoMessages.dates} />}
              currentProject={currentProject}
              formIoData={formIoData}
              currentRegistration={currentRegistration}
              currentAvailabilities={currentAvailabilities}
              stewardLinkedToRegistration={stewardLinkedToRegistration}
            />

            {/* PART 2 - Form */}
            <Divider style={{marginBottom: 35}} />
            <h3 className="list-element-header">{t("registrations:form.title")}</h3>
            <p>
              <Trans
                i18nKey="form.mandatoryFieldsNotice"
                ns="registrations"
                components={{
                  redColor: <span style={{color: "red"}} />,
                }}
              />
            </p>
            <RegistrationForm
              currentProject={currentProject}
              userFormResponses={currentRegistration.specific}
              dataRef={formIoData}
            />

            {/* PART 3 - Ticketing */}
            {ticketingMandatory && (
              <>
                <Divider style={{marginBottom: 35}} />
                <RegistrationTicketing
                  currentProject={currentProject}
                  infoMessage={<InfoMessage message={infoMessages.ticketing} />}
                  saveDataFn={() => registerToProject(true, true)}
                />
              </>
            )}
          </div>

          {/* REGISTRATION BUTTONS BOTTOM BAR */}
          <div
            id="secondary-registration-buttons"
            className="sticky-bottom-buttons full-width-content"
            style={{
              transform: "translateY(90px)",
              position: "sticky",
              transition: "all 0.3s ease-in-out",
              bottom: 0,
              zIndex: 50,
              background: "white",
            }}>
            <div className="page-content">
              <div className="containerH" style={{justifyContent: "center", padding: "5px 0"}}>
                {shouldDisplaySaveProgressButton ? (
                  <SaveProgressButton size="large" />
                ) : (
                  <RegistrationButton size="large" />
                )}
              </div>
            </div>
          </div>

          {/*  Just to let some padding at the end for the secondary buttons to make a great translation and not move the whole page*/}
          <div style={{paddingBottom: 90}}></div>
        </>
      )}

      {/* HELP CAROUSEL MODAL */}
      <LayoutElement.HelpToast
        onClick={() => setShowHelpModal(true)}
        tooltipMessage={t("registrations:main.helpToast")}
      />
      <Modal
        width={"max(90vw, 400pt)"}
        visible={showHelpModal}
        footer={false}
        onCancel={() => setShowHelpModal(false)}>
        <LayoutElement.HelpCarousel
          content={
            currentProject.ticketingMode
              ? helpCarouselContent
              : helpCarouselContent.filter((content) => content.step !== "ticketing")
          }
          baseUrl={`${URLS.API}/assets/registrationHelp`}
        />
      </Modal>
    </div>
  );
}
