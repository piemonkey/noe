import {Alert, Button, Card, Form} from "antd";
import {Availability} from "../utils/Availability";
import moment from "moment";
import React, {useState} from "react";
import {FormElement} from "../common/FormElement";
import {InputElement} from "../common/InputElement";
import {fieldToData} from "../../helpers/tableUtilities";
import {useDispatch} from "react-redux";
import {registrationsActions} from "../../features/registrations";
import {personName} from "../../helpers/utilities";
import {textLogo} from "../../app/configuration";
import {Trans, useTranslation} from "react-i18next";
import {CardElement} from "../common/LayoutElement";

export function RegistrationAvailabilities({
  currentProject,
  currentRegistration,
  stewardLinkedToRegistration,
  currentAvailabilities,
  formIoData,
  infoMessage,
}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const [availabilitySlotsInterface, setAvailabilitySlotsInterface] = useState(
    currentAvailabilities.length <= 1 ? "simple" : "advanced"
  );
  const stewardLinkedHasDefinedAvailabilities =
    stewardLinkedToRegistration?.availabilitySlots?.length > 0;

  // ************************* //
  // ***** AVAILABILITIES **** //
  // ************************* //

  const simpleAvailabilityFields = [
    {
      name: "start",
      value:
        currentAvailabilities && currentAvailabilities[0]?.start
          ? moment(currentAvailabilities[0]?.start)
          : undefined,
    },
    {
      name: "end",
      value:
        currentAvailabilities && currentAvailabilities[0]?.end
          ? moment(currentAvailabilities[0]?.end)
          : undefined,
    },
  ];

  const changeAvailabilitySlots = (slots) =>
    dispatch(
      registrationsActions.setCurrentWithMetadata({
        availabilitySlots: slots,
        specific: formIoData.current?.data,
      })
    );

  // FORMS FIELDS MANAGEMENT
  const registrationFieldsToData = (fields) => {
    const data = fieldToData(fields);
    data.start = data.start?.format();
    data.end = data.end?.format();
    return data;
  };

  const SimpleAvailability = () => (
    <FormElement
      fields={simpleAvailabilityFields}
      onFieldsChange={(newFields, allFields) => {
        const fieldsData = registrationFieldsToData(allFields);
        changeAvailabilitySlots([{start: fieldsData.start, end: fieldsData.end}]);
      }}>
      <div className="list-element-header">
        <h3>{t("registrations:availabilities.title")}</h3>
        {infoMessage}
      </div>
      <div className="containerH" style={{float: "left", flexWrap: "wrap", whiteSpace: "nowrap"}}>
        <div className="containerH" style={{alignItems: "baseline"}}>
          {t("registrations:availabilities.iArriveOn")}
          <InputElement.DateTime
            id="start"
            name="start"
            defaultPickerValue={moment(currentProject.start)}
            disableDatesIfOutOfProject
            disableDatesBeforeNow={false}
            style={{marginLeft: 4, marginRight: 4}}
            onOk={() =>
              !currentAvailabilities[0]?.end &&
              setTimeout(() => document.getElementById("end")?.focus(), 500)
            }
          />
        </div>
        <div className="containerH" style={{alignItems: "baseline"}}>
          {t("registrations:availabilities.andILeaveOn")}
          <InputElement.DateTime
            id="end"
            name="end"
            defaultPickerValue={moment(currentAvailabilities[0]?.start || currentProject.start)}
            disableDatesIfOutOfProject
            disableDatesBeforeNow={false}
            style={{marginLeft: 4, marginRight: 4}}
            onOk={() =>
              !currentAvailabilities[0]?.start &&
              setTimeout(() => document.getElementById("start")?.focus(), 500)
            }
          />
          .
        </div>
      </div>
    </FormElement>
  );

  return (
    <>
      {availabilitySlotsInterface === "simple" && ( // zero availabilitySlots given: simple interface
        <div className="containerV">
          <SimpleAvailability />

          <div className="containerH" style={{alignItems: "baseline", color: "grey"}}>
            <p>
              Mes dates de présence sont un peu compliquées :
              <Button
                type="link"
                style={{paddingLeft: 8}}
                onClick={() => setAvailabilitySlotsInterface("advanced")}>
                accéder à l'éditeur avancé
              </Button>
            </p>
          </div>
        </div>
      )}

      {availabilitySlotsInterface === "advanced" && ( // one or more availability: choice between simple and advanced
        <>
          <Availability
            title={t("registrations:availabilities.title")}
            disableDatesIfOutOfProject
            defaultPickerValue={[moment(currentProject.start), moment(currentProject.start)]}
            data={currentAvailabilities}
            onChange={changeAvailabilitySlots}
          />

          {currentAvailabilities &&
            currentAvailabilities?.length <= 1 && ( // Display only if there's zero or one slot
              <div
                className="containerH"
                style={{marginTop: 25, alignItems: "baseline", color: "grey"}}>
                <p>
                  Mes dates de présence sont simples :
                  <Button
                    type="link"
                    style={{paddingLeft: 8}}
                    onClick={() => setAvailabilitySlotsInterface("simple")}>
                    retour à l'éditeur simplifié
                  </Button>
                </p>
              </div>
            )}
        </>
      )}

      {stewardLinkedToRegistration && ( // Alert when the user is linked to a steward
        <Alert
          style={{marginBottom: 20}}
          message={t("registrations:availabilities.linkedToStewardAlert.message", {
            name: personName(currentRegistration.steward),
          })}
          description={
            <>
              {t("registrations:availabilities.linkedToStewardAlert.description")}
              {stewardLinkedHasDefinedAvailabilities && (
                <CardElement
                  style={{marginTop: 20, marginBottom: 0, opacity: 0.9}}
                  size="small"
                  title={t("registrations:availabilities.linkedToStewardAlert.dates.title")}>
                  <Trans
                    ns="registrations"
                    i18nKey="availabilities.linkedToStewardAlert.dates.description"
                  />
                  <Availability disabled data={stewardLinkedToRegistration.availabilitySlots} />
                </CardElement>
              )}
            </>
          }
          type="info"
        />
      )}
    </>
  );
}
