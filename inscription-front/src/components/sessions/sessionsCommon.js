import {useDispatch, useSelector} from "react-redux";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {currentProjectSelectors} from "../../features/currentProject";
import React, {useEffect, useState} from "react";
import {DatePicker, Input, Segmented, Select, Tag, Tooltip} from "antd";
import {BarsOutlined, CalendarOutlined} from "@ant-design/icons";
import {useDebounce} from "../../helpers/viewUtilities";
import moment from "moment";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {viewActions, viewSelectors} from "../../features/view";
import {GetPdfPlanningButton} from "../common/EditPage";
import {teamsActions} from "../../features/teams";
import {useTranslation} from "react-i18next";
const {Search} = Input;
const {CheckableTag} = Tag;

export const useLoadSessionsView = (viewUrl, viewMode, dispatch) => {
  const sessionFilter = useSelector(sessionsSelectors.selectListFilter);
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  useEffect(() => {
    const isAgendaView = viewMode === "agenda";
    const eventIsFinished = moment().isAfter(moment(currentProject.end));

    // If we are on the agenda view, or if the past sessions filter is activated, or if the event is already finished,
    // Then load all sessions (= no dateToLoadFrom). Else, only load from now date.
    const dateToLoadFrom =
      isAgendaView || sessionFilter.showPastSessionsFilter || eventIsFinished
        ? undefined
        : Date.now();

    dispatch(sessionsActions.loadList({type: viewUrl, fromDate: dateToLoadFrom}));

    // In the agenda view, always activate past sessions.
    // If we are after the end of the event, also systematically load past sessions.
    // TODO optimize this, as updateFilteredList is also called in the loadList function
    if (isAgendaView || eventIsFinished)
      dispatch(sessionsActions.updateFilteredList({showPastSessionsFilter: true}));

    dispatch(sessionsActions.loadCategoriesList());
    dispatch(teamsActions.loadList());
  }, [viewUrl, sessionFilter.showPastSessionsFilter]);
};

export const sessionShouldBeShown = (viewUrl, session) => {
  const realMaxNumberOfParticipants =
    session.computedMaxNumberOfParticipants === null
      ? Infinity // cf. getMaxParticipantsBasedOnPlaces() function in backend
      : session.computedMaxNumberOfParticipants;
  return viewUrl === "subscribed"
    ? session.isSteward || (session.subscribed && realMaxNumberOfParticipants > 0)
    : session.isSteward || realMaxNumberOfParticipants > 0;
};

export const ChangeSessionViewButtons = ({viewMode, navigate, isMobileView}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const sessionsViewMode = useSelector(viewSelectors.selectSessionsViewMode);
  const isListView = sessionsViewMode === "list";

  // Resynchronize viewMode if needed
  useEffect(() => {
    if (viewMode !== sessionsViewMode) {
      dispatch(viewActions.changeSessionsViewMode(viewMode));
    }
  }, [viewMode, sessionsViewMode]);

  const toggleView = () => navigate(isListView ? "agenda" : ".");

  return (
    <div
      className={
        (viewMode === "agenda" && !isMobileView ? "with-margins" : "") +
        (viewMode === "list" ? " bounce-in-strong" : "")
      }
      style={{
        position: "fixed",
        bottom: isMobileView ? 10 : 20,
        right: isMobileView ? 10 : 26,
        zIndex: 1,
      }}>
      <div
        id="view-mode-changer-select"
        className="bg-noe-gradient shadow"
        style={{
          padding: 5,
          borderRadius: 10,
        }}>
        <div style={{background: "white", borderRadius: 6, overflow: "hidden"}}>
          <Segmented
            onChange={toggleView}
            options={[
              {
                label: isMobileView
                  ? t("sessions:changeViewButton.list")
                  : t("sessions:changeViewButton.listView"),
                value: "list",
                icon: <BarsOutlined />,
              },
              {
                label: isMobileView
                  ? t("sessions:changeViewButton.agenda")
                  : t("sessions:changeViewButton.agendaView"),
                value: "agenda",
                icon: <CalendarOutlined />,
              },
            ]}
            value={sessionsViewMode}
            style={{height: 40}}
            size="large"
          />
        </div>
      </div>
    </div>
  );
};

export const SessionFilterControls = React.memo(
  ({agendaView = false, isMobileView, viewUrl, className, style, customButtons}) => {
    const {t} = useTranslation();
    const dispatch = useDispatch();

    const viewOnlySubscribed = viewUrl === "subscribed";

    const categories = useSelector(sessionsSelectors.selectCategoriesList);
    const sessionsFilter = useSelector(sessionsSelectors.selectListFilter);
    const currentProject = useSelector(currentProjectSelectors.selectProject);
    const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

    // Cause with debounce, we can't directly synchronize the search bar with the redux state, we need a parallel state like this
    const [searchBarText, setSearchBarText] = useState(sessionsFilter.searchBar);

    const participantIsLinkedWithASteward = currentRegistration.steward;
    const everythingIsOkInDatabase = currentRegistration.inDatabase.everythingIsOk;

    useEffect(() => {
      // Deactivate date filtering for agenda view
      if (agendaView && sessionsFilter.dateFilter) onChangeDate(undefined);

      if (viewOnlySubscribed) {
        // Deactivate the subscribed not subscribed select on subscribed viewUrl
        if (sessionsFilter.registeredFilter !== undefined) onChangeRegistered(undefined);

        // Deactivate the available or not filter
        if (sessionsFilter.availableFilter) onChangeAvailable(false);
      }
    }, [agendaView, viewOnlySubscribed]);

    ///////////////////////////////////////////
    // Filter functions
    ///////////////////////////////////////////

    const debouncedSearch = useDebounce(
      (searchText) => dispatch(sessionsActions.updateFilteredList({searchBar: searchText})),
      500
    );

    const onChangeSearch = (e) => {
      setSearchBarText(e.target.value); // Set the instant searchtext state
      debouncedSearch(e.target.value); // Then fire the debounced function to update Redux
    };

    const onChangeRegistered = (value) =>
      dispatch(sessionsActions.updateFilteredList({registeredFilter: value}));

    const onChangeAvailable = (value) =>
      dispatch(sessionsActions.updateFilteredList({availableFilter: value}));

    const onChangeShowPastSessions = (value) => {
      dispatch(sessionsActions.updateFilteredList({showPastSessionsFilter: value}));
    };

    const onChangeStewardFilter = (value) =>
      dispatch(sessionsActions.updateFilteredList({stewardFilter: value}));

    const onChangeDate = (date) => dispatch(sessionsActions.updateFilteredList({dateFilter: date}));

    const onChangeCategories = (filter) =>
      dispatch(sessionsActions.updateFilteredList({categoriesFilter: filter}));

    return (
      <div
        className={`containerH buttons-container ${className}`}
        style={{alignItems: "center", marginBottom: 0, marginTop: 0, ...style}}>
        {currentRegistration.inDatabase.everythingIsOk && !agendaView && (
          <GetPdfPlanningButton
            elementsActions={registrationsActions}
            id={currentRegistration._id}
            tooltip={t("sessions:searchControls.pdfPlanningButton.tooltip")}
            type="primary"
            noText={isMobileView}
          />
        )}

        <Search
          placeholder={t("sessions:searchControls.searchBar.placeholder")}
          value={searchBarText}
          style={{minWidth: 200, flexGrow: 4, flexBasis: 3}}
          onChange={onChangeSearch}
          enterButton
        />

        <Select
          style={{minWidth: 200, flexGrow: 4, flexBasis: 5}}
          allowClear
          value={sessionsFilter.categoriesFilter}
          mode="multiple"
          placeholder={t("sessions:searchControls.categoriesFilter.placeholder")}
          onChange={onChangeCategories}
          options={[
            {
              key: "volunteering",
              value: "volunteering",
              label: t("sessions:searchControls.categoriesFilter.options.volunteering"),
            },
            {
              key: "allTheRest",
              value: "allTheRest",
              label: t("sessions:searchControls.categoriesFilter.options.allTheRest"),
            },
            ...categories.map((category) => ({
              key: category._id,
              value: category._id,
              label: category.name,
            })),
          ]}
        />

        {customButtons}

        {!agendaView && (
          <DatePicker
            style={{flexBasis: 0, minWidth: 130}}
            value={sessionsFilter.dateFilter}
            format={"ddd D MMM"}
            showTime={false}
            placeholder={t("sessions:searchControls.dateFilter.placeholder")}
            onChange={onChangeDate}
          />
        )}

        {everythingIsOkInDatabase && !viewOnlySubscribed && (
          <Select
            style={{flexBasis: 0, minWidth: 120}}
            options={[
              {label: t("sessions:searchControls.registeredFilter.options.null"), value: null},
              {label: t("sessions:searchControls.registeredFilter.options.true"), value: true},
              {label: t("sessions:searchControls.registeredFilter.options.false"), value: false},
            ]}
            allowClear
            placeholder={t("sessions:searchControls.registeredFilter.placeholder")}
            onChange={onChangeRegistered}
            value={sessionsFilter.registeredFilter}
          />
        )}
        <div style={{flexGrow: 0}}>
          {!agendaView && !moment().isAfter(moment(currentProject.end)) && (
            <Tooltip title={t("sessions:searchControls.showPastSessionsFilter.tooltip")}>
              <CheckableTag
                checked={sessionsFilter.showPastSessionsFilter}
                onChange={onChangeShowPastSessions}>
                Passées
              </CheckableTag>
            </Tooltip>
          )}

          {!viewOnlySubscribed && (
            <Tooltip title={t("sessions:searchControls.availableFilter.tooltip")}>
              <CheckableTag checked={sessionsFilter.availableFilter} onChange={onChangeAvailable}>
                Disponibles
              </CheckableTag>
            </Tooltip>
          )}

          {participantIsLinkedWithASteward && (
            <Tooltip title={t("sessions:searchControls.stewardFilter.tooltip")}>
              <CheckableTag checked={sessionsFilter.stewardFilter} onChange={onChangeStewardFilter}>
                Encadré.es
              </CheckableTag>
            </Tooltip>
          )}
        </div>
      </div>
    );
  },
  (prevProps, nextProps) => prevProps.viewUrl === nextProps.viewUrl
);
