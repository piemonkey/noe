import React, {useCallback} from "react";
import {useSelector, useDispatch} from "react-redux";
import {sessionsSelectors, sessionsActions} from "../../features/sessions.js";
import {Button, Card, List, Result} from "antd";
import {SessionShowSmall} from "./SessionShow";
import {
  SessionFilterControls,
  ChangeSessionViewButtons,
  sessionShouldBeShown,
  useLoadSessionsView,
} from "./sessionsCommon";
import {LayoutElement} from "../common/LayoutElement";
import {
  useStickyShadow,
  useSavedWindowScroll,
  useSavedPagination,
  useWindowDimensions,
} from "../../helpers/viewUtilities";
import moment from "moment";
import {currentProjectSelectors} from "../../features/currentProject";
import {CarryOutOutlined} from "@ant-design/icons";
import {paginationPageSizes} from "../../features/view";
import {registrationsSelectors} from "../../features/registrations";
import {ConnectedAsAlert} from "../participants/ParticipantList";
import {Trans, useTranslation} from "react-i18next";

const LoadPastSessionsFakeCard = () => {
  const dispatch = useDispatch();
  return (
    <div
      className="containerV"
      style={{height: "100%", minHeight: 250, padding: "15px 15px 50px 15px"}}>
      <Card
        className="containerV"
        style={{
          background: "rgba(0, 0, 0, 0.03)",
          height: "100%",
          color: "gray",
          fontWeight: "bold",
          textAlign: "center",
          justifyContent: "center",
        }}>
        <Trans
          i18nKey="list.toSeePastSessionsClickOnButton"
          ns="sessions"
          components={{
            button: (
              <Button
                style={{marginTop: 10}}
                onClick={() => {
                  dispatch(sessionsActions.updateFilteredList({showPastSessionsFilter: true}));
                }}
              />
            ),
          }}
        />
      </Card>
    </div>
  );
};

export const SessionList = ({navigate, viewUrl}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const {isMobileView} = useWindowDimensions();

  useStickyShadow();

  const sessions = useSelector(sessionsSelectors.selectListFiltered);
  const sessionsShown = sessions.filter((s) => sessionShouldBeShown(viewUrl, s));
  const sessionFilter = useSelector(sessionsSelectors.selectListFilter);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const displayWelcomeMessage =
    sessionsShown?.length === 0 &&
    (!currentRegistration.sessionsSubscriptions ||
      currentRegistration.sessionsSubscriptions.length === 0);

  const subtitle =
    viewUrl === "subscribed" ? (
      <Trans i18nKey="list.subtitleSubscribed" ns="sessions" />
    ) : (
      <Trans i18nKey="list.subtitleAll" ns="sessions" />
    );

  useSavedWindowScroll();
  const [pagination, setPagination] = useSavedPagination();

  useLoadSessionsView(viewUrl, "list", dispatch);

  if (!sessionFilter.showPastSessionsFilter && moment(currentProject.start).isBefore(moment())) {
    sessionsShown.splice(0, 0, {isLoadPastSessionsFakeCard: true});
  }

  const onSubscribe = (id) => dispatch(sessionsActions.subscribe(id));
  const onUnsubscribe = (id) => dispatch(sessionsActions.unsubscribe(id));

  // Memoizing this component avoids triple rendering of the whole page when subscribing/unsubscribing to a session
  const MemoizedSessionShowSmall = React.memo(({session}) =>
    session.isLoadPastSessionsFakeCard ? (
      <LoadPastSessionsFakeCard />
    ) : (
      <SessionShowSmall
        hoverable
        session={session}
        onClick={() => navigate(`../${session._id}`)}
        onUnsubscribe={() => onUnsubscribe(session._id)}
        onSubscribe={() => onSubscribe(session._id)}
        withMargins
      />
    )
  );

  const getMemoizedSessionSmall = useCallback(
    (session) => <MemoizedSessionShowSmall session={session} />,
    []
  );

  return (
    <div className="page-container">
      <div style={{paddingBottom: 10, background: "white"}}>
        <LayoutElement.PageHeading
          title={viewUrl === "subscribed" ? t("sessions:labelSubscribed") : t("sessions:labelAll")}
        />
      </div>
      <div style={{paddingBottom: 16, background: "white"}}>{subtitle}</div>
      <ConnectedAsAlert />

      <div className={`full-width-content with-margins ${!isMobileView ? "sticky-navbar" : ""}`}>
        <SessionFilterControls isMobileView={isMobileView} viewUrl={viewUrl} />
      </div>
      {viewUrl === "subscribed" && displayWelcomeMessage ? (
        <Result
          icon={<CarryOutOutlined />} // No icon
          title={t("sessions:list.subscribedSplashScreen.title")}
          subTitle={<Trans ns="sessions" i18nKey="list.subscribedSplashScreen.subtitle" />}
          extra={
            <Button type="primary" onClick={() => navigate("../all")}>
              {t("sessions:list.subscribedSplashScreen.subscribeToSessionsButton")}
            </Button>
          }
        />
      ) : (
        <List
          style={{marginTop: "10px", marginBottom: "40px"}}
          grid={{gutter: 16, xs: 1, sm: 1, md: 2, lg: 2, xl: 3, xxl: 4}}
          dataSource={sessionsShown}
          renderItem={getMemoizedSessionSmall}
          pagination={{
            position: "both",
            style: {textAlign: "center"},
            size: isMobileView && "small",
            showSizeChanger: true,
            onChange: (current, pageSize) => {
              setPagination(current, pageSize);
              window.scrollTo({top: 0, behavior: "smooth"});
            },
            current: pagination.current,
            pageSize: pagination.pageSize,
            pageSizeOptions: paginationPageSizes,
            total: sessionsShown.length,
          }}
        />
      )}
      <ChangeSessionViewButtons navigate={navigate} isMobileView={isMobileView} viewMode="list" />
    </div>
  );
};
