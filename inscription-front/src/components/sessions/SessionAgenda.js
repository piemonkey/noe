import React, {useCallback, useEffect, useMemo, useState} from "react";
import {navigate} from "@reach/router";
import {useDispatch, useSelector} from "react-redux";
import {Button, Drawer, InputNumber, Modal, Popover, Tooltip} from "antd";
import {
  CalendarOutlined,
  DownOutlined,
  EnvironmentOutlined,
  ReloadOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import {EditingState, IntegratedEditing, ViewState} from "@devexpress/dx-react-scheduler";
import {
  AppointmentForm,
  Appointments,
  CurrentTimeIndicator,
  DateNavigator,
  DayView,
  Scheduler,
  Toolbar,
} from "@devexpress/dx-react-scheduler-material-ui";
import {SessionShow, SessionShowSmall} from "./SessionShow";
import {LayoutElement} from "../common/LayoutElement";
import {sessionsActions, sessionsSelectors} from "../../features/sessions.js";
import {currentProjectSelectors} from "../../features/currentProject.js";
import {slotOverlapsAnOtherOne} from "../utils/Availability";
import {navbarHeight, useWindowDimensions} from "../../helpers/viewUtilities";
import {getSessionMetadata} from "../../helpers/sessionsUtilities";
import {
  AgendaSettingsDrawer,
  CELL_DURATION_MINUTES,
  DEFAULT_AGENDA_PARAMS,
  getAgendaDisplayParams,
  getAgendaDisplayStartDate,
  getElementsFromListInSlot,
  getNumberOfDaysToDisplay,
  getSessionName,
  getTimeScaleLabelComponent,
  midnightDateManager,
  NavButton,
  NumberOfDaysSelector,
  slotNumberString,
  synchronizeTicksOnSideOfAgenda,
  useInitialScrollToProjectAvailabilities,
  useKillLoadingMessage,
} from "../../helpers/agendaUtilities";
import {
  ChangeSessionViewButtons,
  SessionFilterControls,
  sessionShouldBeShown,
  useLoadSessionsView,
} from "./sessionsCommon";
import {registrationsSelectors} from "../../features/registrations";
import {viewActions, viewSelectors} from "../../features/view";
import {listRenderer} from "../../helpers/listUtilities";
import {CustomErrorBoundary} from "../common/LayoutStructure";
import moment from "moment";
import {useTranslation} from "react-i18next";

const onEditingAppointmentChange = (changedData) => {
  if (changedData && changedData.type !== "vertical") {
    navigate(changedData.session._id);
  }
};

export function SessionAgenda({viewUrl}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // This component is responsive with three layouts:
  // - large screens: all the filter controls are in the agenda toolbar
  // - medium screens: the controls are above the agenda toolbar
  // - mobile screens: the filter controls are located in a drawer that can be opened with a button in the agenda toolbar
  const {isMobileView, windowWidth, windowHeight} = useWindowDimensions();
  const isLargeScreen = windowWidth > 1350;

  // Substract the height from the navbar and the session filter toolbar if it is displayed (for medium screens)
  const schedulerHeight =
    windowHeight -
    navbarHeight() -
    (document.querySelector(".session-filter-toolbar")?.offsetHeight || 0);

  // *********************** //
  // **** DATA FETCHING **** //
  // *********************** //

  const allSlots = useSelector(sessionsSelectors.selectSlotsList);
  const slotsFiltered = useSelector(sessionsSelectors.selectSlotsListFiltered);

  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const agendaParams = useSelector(viewSelectors.selectAgendaParams);

  useLoadSessionsView(viewUrl, "agenda", dispatch);

  // ************************ //
  // ******** STATES ******** //
  // ************************ //

  const {
    // Set the cell display height layout
    cellDisplayHeight,
    // Number of days to display. Will be set asynchronously in useEffect depending on the number of places and other params
    numberOfDaysDisplayed,
    // Start date for the agenda display
    currentAgendaDate,
    // Place Events on top of each other or next to each other
    slotsOnEachOther,
  } = {
    ...DEFAULT_AGENDA_PARAMS,
    ...agendaParams,
    numberOfDaysDisplayed: agendaParams?.numberOfDaysDisplayed
      ? agendaParams?.numberOfDaysDisplayed
      : getNumberOfDaysToDisplay(windowWidth, currentProject),
    currentAgendaDate: agendaParams?.currentAgendaDate
      ? agendaParams?.currentAgendaDate
      : getAgendaDisplayStartDate(currentProject),
  };

  const [sessionShowModalVisible, setSessionShowModalVisible] = useState(false);
  const [sessionFilterDrawerVisible, setSessionFilterDrawerVisible] = useState(false);

  const setAgendaParams = (params) =>
    dispatch(viewActions.changeAgendaParams({projectId: currentProject._id, params}));

  // ************************ //
  // **** MEMOIZED DATA **** //
  // ************************ //
  // everything that can easily be computed only once //

  const agendaDisplayParams = useMemo(
    () => getAgendaDisplayParams(allSlots, currentProject),
    [allSlots, currentProject]
  );

  // Only show the slots where there the number of participants is above zero, or where we are a steward
  const slotsShown = slotsFiltered?.filter((s) => sessionShouldBeShown(viewUrl, s.session));

  // The appointments (ie. the slots from all the sessions, formatted in the way that the Agenda accepts it
  const appointments = useMemo(
    () =>
      slotsShown?.map((s) =>
        midnightDateManager.dataToDisplay({
          ...s,
          id: s._id,
          title: slotNumberString(s) + getSessionName(s.session),
          startDate: s.start,
          endDate: s.end,
          location: getElementsFromListInSlot("places", s, (el) => el.name).join(", "),
          placeId: getElementsFromListInSlot("places", s, (el) => el._id),
          color: s.session.activity?.category?.color,
        })
      ),
    [slotsFiltered, viewUrl]
  );

  // ************************* //
  // ***** ASYNC EFFECTS ***** //
  // ************************* //

  // APPEARANCE TWEAKS

  useInitialScrollToProjectAvailabilities(agendaDisplayParams, cellDisplayHeight);
  useEffect(
    () => synchronizeTicksOnSideOfAgenda("timescale-cell", cellDisplayHeight),
    [cellDisplayHeight, agendaDisplayParams, currentAgendaDate]
  );
  useKillLoadingMessage();

  // ************************* //
  // ** APPOINTMENTS BLOCKS ** //
  // ************************* //

  // Appointment popover when we hover the block
  const SessionPreview = ({session, onClick}) => (
    <SessionShowSmall
      onClick={onClick}
      style={{width: 350}}
      session={session}
      onUnsubscribe={() => dispatch(sessionsActions.unsubscribe(session._id))}
      onSubscribe={() => dispatch(sessionsActions.subscribe(session._id))}
    />
  );

  // Generic appointment component
  const CustomAppointment = ({
    style,
    children,
    component: Component,
    opacity,
    data,
    ...restProps
  }) => (
    // Component variable must have an uppercase letter to be considered as a component by React
    <Component
      {...restProps}
      data={data}
      className={`max-opacity-on-hover ${restProps.className}`}
      style={{
        ...style,
        color: "white",
        backgroundColor: data.color,
        transition: "0.4s ease-in",
        transitionDelay: "0.4s",
        opacity,
      }}>
      <div style={{marginLeft: 8, marginTop: 5}}>
        <div
          style={{
            lineHeight: 1.5,
            whiteSpace: "normal",
            overflow: "hidden",
            display: "-webkit-box",
            WebkitLineClamp: "3",
            WebkitBoxOrient: "vertical",
            fontWeight: "bold",
          }}>
          {data.title}
        </div>
        <div style={{overflow: "hidden", whiteSpace: "nowrap", paddingTop: 1}}>
          {listRenderer.timeRangeFormat(data.start, data.end)}
        </div>

        {currentProject.usePlaces && data?.location.length > 0 && (
          <div style={{marginTop: 4, lineHeight: 1.2, opacity: 0.8}}>
            <EnvironmentOutlined /> {data?.location}
          </div>
        )}
      </div>
    </Component>
  );

  // Renders the appointment block in the agenda view.
  // useCallback(React.memo()) helps avoiding approx 300% of re-rendering on start and 30% after
  const Appointment = useCallback(
    React.memo(
      ({style, ...props}) => {
        const session = props.data.session;

        const {shouldBeDimmed, inConflictWithRegistrationDates} = getSessionMetadata(
          session,
          currentRegistration
        );

        if (inConflictWithRegistrationDates) props.data.title = "⚠️ " + props.data.title;

        const openSessionShowModal = () => {
          // Load editing state before displaying SessionShow component
          dispatch(sessionsActions.loadEditing(session._id)).then(() =>
            setSessionShowModalVisible(true)
          );
        };

        // Blue outline for sessions where we are steward
        if (session.isSteward || session.subscribed) {
          // Blue for session as a steward, Green for sessions subscribed
          const borderColor = session.isSteward ? "rgb(75, 104, 252)" : "rgb(82, 196, 26)";
          style = {
            ...style,
            border: `5px solid ${borderColor}`,
            outline: "1.6px solid white",
            outlineOffset: -6,
          };
        }

        const appointmentComp = (
          <CustomAppointment
            component={Appointments.Appointment}
            style={style}
            opacity={shouldBeDimmed ? 0.3 : 1}
            onClick={openSessionShowModal}
            {...props}
          />
        );

        return isMobileView ? (
          appointmentComp
        ) : (
          // zIndex=100 to put the popover above the Sidebar, but below the SessionShow modal
          <Popover
            zIndex={100}
            content={<SessionPreview session={session} />}
            mouseEnterDelay={0.6}>
            {appointmentComp}
          </Popover>
        );
      },
      (prevProps, nextProps) =>
        JSON.stringify(prevProps.data.session) === JSON.stringify(nextProps.data.session)
    ),
    // Don't put the currentProject as a dependency cause it's not necessary and causes a whole re-render for
    // nothing on each session subscribe/unsubscribe
    []
  );

  // ************************** //
  // ***** SUB-COMPONENTS ***** //
  // ************************** //

  // Renders a cell of the Agenda day view
  // useCallback(React.memo()) helps avoiding approx 300% of re-rendering on start and cuts all the rendering after that
  const TimeTableCell = useCallback(
    React.memo((props) => {
      // Get the cell start and end hour
      const cellSlot = {start: props.startDate, end: props.endDate};
      const overlapsWithElements = [];

      // Check if the cell is out of project availabilities
      if (!slotOverlapsAnOtherOne(cellSlot, currentProject.availabilitySlots)) {
        overlapsWithElements.push("project");
      }

      // Add the appropriate classes to the cell, so it can be painted if there are some overlaps
      const className =
        overlapsWithElements.length > 0
          ? `${overlapsWithElements.join("-")}-disabled-date`
          : undefined;
      return (
        <DayView.TimeTableCell
          {...props}
          style={{height: cellDisplayHeight}}
          className={className}
        />
      );
    }),
    [currentProject.availabilitySlots, cellDisplayHeight]
  );

  const DayScaleCell = (props) => (
    <DayView.DayScaleCell
      style={{cursor: "pointer"}}
      onDoubleClick={() =>
        setAgendaParams({
          currentAgendaDate: props.startDate.getTime(),
          numberOfDaysDisplayed: 1,
        })
      }
      {...props}
    />
  );

  // Agenda date navigation arrows and selector
  const DateNavigatorRoot = useCallback(
    React.memo((props) => {
      const goToProjectStartDate = moment(currentProject.start).isAfter(moment());

      // Extract month, capitalize first letter, and trim if on mobile device
      let dateText = props.navigatorText.replace(/\d+(-\d+)? ?/g, "");
      dateText = dateText.charAt(0).toUpperCase() + dateText.slice(1); // capitalize day
      if (isMobileView && dateText.length > 3)
        dateText = dateText.length !== 4 ? dateText.slice(0, 3) + "." : dateText;

      return (
        <div
          ref={props.rootRef}
          className="containerH buttons-container"
          style={{flexShrink: 0, alignItems: "center"}}>
          <div>
            <NavButton type="back" onClick={() => props.onNavigate("back")} />
            <NavButton type="forward" onClick={() => props.onNavigate("forward")} />
          </div>
          <Tooltip title={t("sessions:agenda.dateNavigator.dateSelectorTooltip")}>
            <Button onClick={props.onVisibilityToggle}>{dateText}</Button>
          </Tooltip>
          <Tooltip
            title={
              goToProjectStartDate
                ? t("sessions:agenda.dateNavigator.goToBeginningOfEvent")
                : t("sessions:agenda.dateNavigator.goToToday")
            }>
            <Button
              type="link"
              icon={<CalendarOutlined />}
              onClick={() =>
                setAgendaParams({
                  currentAgendaDate: goToProjectStartDate
                    ? moment(currentProject.start).unix() * 1000
                    : moment().unix() * 1000,
                })
              }>
              {!isMobileView &&
                (goToProjectStartDate
                  ? t("sessions:agenda.dateNavigator.beginning")
                  : t("sessions:agenda.dateNavigator.today"))}
            </Button>
          </Tooltip>
        </div>
      );
    }),
    [isMobileView]
  );

  // Where all the agenda controls are
  const AgendaControlsBar = useCallback(
    React.memo(() => {
      return isMobileView ? (
        <>
          {/*On mobile, display a button to open the drawer to filter the sessions*/}
          <Button
            type="primary"
            onClick={() => setSessionFilterDrawerVisible(true)}
            style={{marginRight: 10, marginLeft: 10, flexGrow: 1}}
            icon={<SearchOutlined />}>
            {t("sessions:agenda.filter")}
          </Button>
          <NumberOfDaysSelector
            isMobileView={true}
            numberOfDaysDisplayed={numberOfDaysDisplayed}
            setNumberOfDaysDisplayed={(value) => setAgendaParams({numberOfDaysDisplayed: value})}
          />
        </>
      ) : (
        <div
          style={{width: "100%", alignItems: "center", flexWrap: "nowrap", overflowX: "auto"}}
          className="containerH buttons-container">
          {/*On large screens, display the sessions filter controls directly in the agenda controls bar*/}
          {/*Separator*/} <div style={{width: 20}} />
          {isLargeScreen ? (
            <SessionFilterControls
              agendaView
              viewUrl={viewUrl}
              style={{flexGrow: 1, width: "100%"}}
            />
          ) : (
            <div />
          )}
          <div
            className="containerH buttons-container"
            style={{alignItems: "center", flexGrow: 0, flexShrink: 0}}>
            {/*Number of days to display*/}
            {/*Separator*/} <div style={{width: 12}} />
            <NumberOfDaysSelector
              isMobileView={isMobileView}
              numberOfDaysDisplayed={numberOfDaysDisplayed}
              setNumberOfDaysDisplayed={(value) => setAgendaParams({numberOfDaysDisplayed: value})}
            />
            {/*Separator*/} <div style={{width: 10}} />
            <AgendaSettingsDrawer
              cellDisplayHeight={cellDisplayHeight}
              slotsOnEachOther={slotsOnEachOther}
              setAgendaParams={setAgendaParams}
            />
          </div>
        </div>
      );
    }),
    [isMobileView, isLargeScreen, numberOfDaysDisplayed, cellDisplayHeight, viewUrl]
  );

  // The modal is always visible but hiddedn with a {sessionShowModal && ...} condition in the render.
  // it improves by 50% the amount of re-renderings.
  const SessionShowModal = useCallback(
    React.memo(() => (
      <Modal
        visible
        onCancel={() => {
          dispatch(sessionsActions.setEditing({}));
          setSessionShowModalVisible(false);
        }}
        width={"min(90vw, 1200px)"}
        footer={false}
        centered>
        <div className="modal-page-content">
          <SessionShow asModal navigatePathRoot={"../"} />
        </div>
      </Modal>
    )),
    []
  );

  // ************************** //
  // **** RENDER FUNCTION ***** //
  // ************************** //

  // If everything isn't loaded yet, stop here, no need to go further. Display a pending spinner.
  return currentProject._id !== undefined && slotsFiltered !== undefined ? (
    <div className="full-width-content scheduler-wrapper">
      {/*Drawer we can open on mobile view to get the controls*/}
      {isMobileView && (
        <Drawer
          placement="top"
          height="auto" // adjust to content
          closable={false}
          onClose={() => setSessionFilterDrawerVisible(false)}
          visible={sessionFilterDrawerVisible}>
          <SessionFilterControls
            agendaView
            viewUrl={viewUrl}
            customButtons={
              <AgendaSettingsDrawer
                buttonStyle={{flexGrow: 0}}
                cellDisplayHeight={cellDisplayHeight}
                slotsOnEachOther={slotsOnEachOther}
                setAgendaParams={setAgendaParams}
              />
            }
          />
        </Drawer>
      )}
      {/*Session filter above the agenda toolbar for medium screens*/}
      {!isLargeScreen && !isMobileView && (
        <div className="session-filter-toolbar" style={{marginLeft: 20, marginRight: 20}}>
          <SessionFilterControls agendaView style={{margin: 0, padding: 0}} viewUrl={viewUrl} />
        </div>
      )}
      {/*Error boundary to return back to normal state if it is buggy with slotsOnEachOther*/}
      <CustomErrorBoundary
        onError={() => setAgendaParams(DEFAULT_AGENDA_PARAMS)}
        extra={
          <Button
            icon={<ReloadOutlined />}
            onClick={() => {
              setAgendaParams(DEFAULT_AGENDA_PARAMS);
              window.location.reload();
            }}>
            {t("common:errorBoundary.reloadPage")}
          </Button>
        }>
        {/*The big agenda component*/}

        <Scheduler data={appointments} locale="fr-FR" height={schedulerHeight}>
          <ViewState
            defaultCurrentDate={currentAgendaDate}
            currentDate={currentAgendaDate}
            onCurrentDateChange={(date) => setAgendaParams({currentAgendaDate: date.getTime()})}
            defaultCurrentViewName="Day"
          />
          <EditingState onEditingAppointmentChange={onEditingAppointmentChange} />
          <DayView
            timeTableCellComponent={TimeTableCell}
            timeScaleLabelComponent={getTimeScaleLabelComponent(cellDisplayHeight)}
            startDayHour={agendaDisplayParams.start}
            endDayHour={agendaDisplayParams.end}
            cellDuration={CELL_DURATION_MINUTES}
            intervalCount={numberOfDaysDisplayed}
            dayScaleCellComponent={DayScaleCell}
          />
          <Toolbar flexibleSpaceComponent={AgendaControlsBar} />
          <DateNavigator rootComponent={DateNavigatorRoot} />
          <Appointments
            appointmentComponent={Appointment}
            placeAppointmentsNextToEachOther={!slotsOnEachOther}
          />
          <IntegratedEditing />
          <AppointmentForm visible={false} />
          <CurrentTimeIndicator />
        </Scheduler>
      </CustomErrorBoundary>
      {/*The SessionShow modal*/}
      {sessionShowModalVisible && <SessionShowModal />}
      <ChangeSessionViewButtons navigate={navigate} isMobileView={isMobileView} viewMode="agenda" />
    </div>
  ) : (
    <LayoutElement.Pending />
  );
}
