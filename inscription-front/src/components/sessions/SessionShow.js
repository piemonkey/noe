import {Alert, Button, Card, List, Popconfirm, Tag, Tooltip} from "antd";
import React from "react";
import Paragraph from "antd/es/typography/Paragraph";
import {InputElement} from "../common/InputElement";
import {CardElement, LayoutElement} from "../common/LayoutElement";
import {FormElement} from "../common/FormElement";
import {TableElement} from "../common/TableElement";
import {useDispatch, useSelector} from "react-redux";
import {currentProjectSelectors} from "../../features/currentProject";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {stewardsActions} from "../../features/stewards";
import {activitiesActions} from "../../features/activities";
import {placesActions} from "../../features/places";
import {listRenderer} from "../../helpers/listUtilities";
import {FullscreenOutlined, QuestionCircleOutlined, WarningOutlined} from "@ant-design/icons";
import {Link, navigate} from "@reach/router";
import {useLoadEditing} from "../../helpers/editingUtilities";
import {getSessionMetadata, getVolunteeringCoefficient} from "../../helpers/sessionsUtilities";
import {registrationsSelectors} from "../../features/registrations";
import {getSessionName} from "../../helpers/agendaUtilities";
import {teamsSelectors} from "../../features/teams";
import {generateRegistrationsColumns, generateSubscriptionInfo} from "../../helpers/tableUtilities";
import {personName} from "../../helpers/utilities";
import {TextDisplayer} from "../common/TextEditor";
import {Trans, useTranslation} from "react-i18next";
import {t} from "i18next";
import {URLS} from "../../app/configuration";

const SessionFilling = ({
  computedMaxNumberOfParticipants,
  numberOfParticipants,
  showLabel = true,
  ...otherProps
}) => {
  const {t} = useTranslation();
  // cf. getMaxParticipantsBasedOnPlaces() function in backend
  if (computedMaxNumberOfParticipants === null) return t("sessions:filling.freeSubscription");

  const fillingText =
    numberOfParticipants !== undefined && computedMaxNumberOfParticipants
      ? numberOfParticipants + "/" + computedMaxNumberOfParticipants
      : 0;

  return (
    <InputElement.Filling
      formItemProps={{style: {maxWidth: 250, marginBottom: showLabel ? undefined : 0}}}
      label={showLabel ? t("sessions:filling.label") : undefined}
      percent={(numberOfParticipants / computedMaxNumberOfParticipants) * 100}
      status={numberOfParticipants >= computedMaxNumberOfParticipants ? "exception" : ""}
      text={fillingText}
      {...otherProps}
    />
  );
};

const getSessionInfoAlert = (
  sessionSubscription,
  isSteward,
  blockSubscriptions,
  registrationIncomplete,
  participantIsNotAvailable,
  sessionIsFull,
  alreadyStewardOnOtherSession,
  alreadySubscribedToOtherSession,
  inConflictWithRegistrationDates
) => {
  let message;
  if (isSteward) {
    message = {
      type: "info",
      message: t("sessions:alerts.isSteward"),
    };
  } else if (sessionSubscription?.team) {
    message = {
      type: "info",
      message: t("sessions:alerts.belongsToTeam"),
    };
  } else if (blockSubscriptions) {
    return; // Don't show anything if subscriptions are blocked for project
  } else if (registrationIncomplete) {
    message = {
      type: "info",
      message: t("sessions:alerts.mustCompleteRegistration"),
    };
  } else if (participantIsNotAvailable) {
    message = {
      type: "warning",
      message: t("sessions:alerts.participantIsNotAvailable"),
    };
  } else if (sessionIsFull) {
    message = {type: "info", message: t("sessions:alerts.sessionIsFull")};
  } else if (alreadyStewardOnOtherSession) {
    message = {
      type: "warning",
      message: t("sessions:alerts.alreadyStewardOnOtherSession"),
    };
  } else if (alreadySubscribedToOtherSession) {
    message = {
      type: "warning",
      message: t("sessions:alerts.alreadySubscribedToOtherSession"),
    };
  }

  // TODO this doesn't work anymore, looks like it is never shown
  if (inConflictWithRegistrationDates) {
    message = {
      message: (
        <>
          <p>{t("sessions:alerts.inConflictWithRegistrationDates")}</p>
          {message.message}
        </>
      ),
      ...(message || []),
    };
  }

  return message;
};

const SubscribeUnsubscribeButton = ({
  sessionSubscription,
  onSubscribe,
  onUnsubscribe,
  sessionIsFull,
  registrationIncomplete,
  participantIsNotAvailable,
  isSteward,
  alreadySubscribedToOtherSession,
  alreadyStewardOnOtherSession,
  currentProject,
  tooltipMessage,
  volunteeringBeginsSoon,
  block = false,
}) => {
  const authenticatedUserRegistration = useSelector(registrationsSelectors.selectAuthenticated);
  const teams = useSelector(teamsSelectors.selectList);

  const blockSubscriptionsOnProject =
    currentProject.blockSubscriptions &&
    authenticatedUserRegistration &&
    !authenticatedUserRegistration.role;

  const blockVolunteeringUnsubscribeIfBeginsSoon =
    currentProject.blockVolunteeringUnsubscribeIfBeginsSoon;

  const registrationButtonClassName =
    alreadySubscribedToOtherSession || alreadyStewardOnOtherSession
      ? "warning-button"
      : "success-button";

  const subscriptionIsDisabled =
    sessionIsFull ||
    registrationIncomplete ||
    participantIsNotAvailable ||
    isSteward ||
    ((alreadySubscribedToOtherSession || alreadyStewardOnOtherSession) &&
      currentProject.notAllowOverlap);

  const disabledStyle = {borderColor: "rgb(100,100,100)", color: "#555"};

  if (isSteward) {
    return (
      <Tooltip title={tooltipMessage}>
        <Button type="dashed" disabled style={disabledStyle} block={block}>
          {t("stewards:label")}
        </Button>
      </Tooltip>
    );
  } else if (sessionSubscription) {
    // -------- USER IS SUBSCRIBED : show UNSUBSCRIBE button ------------

    // Case for team members
    const teamLinkedToRegistration = teams.find((t) => t._id === sessionSubscription.team);
    if (teamLinkedToRegistration) {
      return (
        <Tooltip title={tooltipMessage}>
          <Button type="dashed" disabled style={disabledStyle} block={block}>
            {t("teams:teamMember")}
          </Button>
        </Tooltip>
      );
    }

    if (blockSubscriptionsOnProject) {
      return (
        <Button disabled block={block}>
          Inscrit⋅e
        </Button>
      );
    }

    // Case for volunteering that begins soon but not allowed
    if (
      volunteeringBeginsSoon &&
      blockVolunteeringUnsubscribeIfBeginsSoon &&
      !authenticatedUserRegistration.role
    )
      return (
        <Tooltip title={t("sessions:subscribeUnsubscribeButton.volunteeringSoonWarning.tooltip")}>
          <Button disabled block={block}>
            {t("sessions:unsubscribe")}
          </Button>
        </Tooltip>
      );

    const confirmUnsubscribeMessage = volunteeringBeginsSoon ? (
      <div style={{maxWidth: 450}}>
        <Trans
          ns="sessions"
          i18nKey="subscribeUnsubscribeButton.volunteeringSoonWarning.confirmText"
        />
      </div>
    ) : (
      t("sessions:subscribeUnsubscribeButton.confirmUnsubscribe")
    );

    return (
      <Popconfirm
        title={confirmUnsubscribeMessage}
        onConfirm={onUnsubscribe}
        okText={t("common:yes")}
        okButtonProps={{danger: true}}
        icon={<QuestionCircleOutlined style={{color: "red"}} />}
        cancelText={t("common:no")}>
        <Button type={teamLinkedToRegistration && "dashed"} danger block={block}>
          {t("sessions:unsubscribe")}
        </Button>
      </Popconfirm>
    );
  } else {
    // -------- USER IS UNSUBSCRIBED : show SUBSCRIBE button ------------

    const subscribeButton = (
      <Button
        type="primary"
        onClick={!tooltipMessage && onSubscribe}
        className={subscriptionIsDisabled ? undefined : registrationButtonClassName}
        disabled={subscriptionIsDisabled}
        block={block}>
        S'inscrire
      </Button>
    );

    return tooltipMessage ? (
      <Popconfirm
        title={
          <>
            <p>{tooltipMessage}</p>
            {t("sessions:stillWantToRegisterConfirm")}
          </>
        }
        onConfirm={onSubscribe}
        okText={t("common:yes")}
        okButtonProps={{className: registrationButtonClassName}}
        cancelText={t("common:no")}
        disabled={subscriptionIsDisabled}>
        <Tooltip title={tooltipMessage}>{subscribeButton}</Tooltip>
      </Popconfirm>
    ) : (
      subscribeButton
    );
  }
};

const CategoryTagWithVolunteeringMajoration = ({
  volunteeringCoefficient,
  category,
  children,
  ...props
}) => {
  let volunteeringMajoration;
  if (volunteeringCoefficient > 0 && volunteeringCoefficient !== 1) {
    const percent = Math.round((volunteeringCoefficient - 1) * 100);
    volunteeringMajoration = `${Math.sign(percent) === 1 ? "+" : "-"}${Math.abs(percent)}%`;
  }

  return children || volunteeringMajoration ? (
    <Tag
      title={
        volunteeringMajoration
          ? t("sessions:volunteeringMajorationTag", {volunteeringMajoration})
          : undefined
      }
      color={category.color}
      {...props}>
      {children}
      {volunteeringMajoration && <strong style={{fontSize: 15}}> {volunteeringMajoration}</strong>}
    </Tag>
  ) : null;
};

export function SessionShow({id, navigatePathRoot, asModal = false}) {
  const dispatch = useDispatch();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const session = useSelector(sessionsSelectors.selectEditing);
  const sessions = useSelector(sessionsSelectors.selectList);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

  useLoadEditing(
    sessionsActions,
    id,
    () => {
      dispatch(stewardsActions.loadList());
      dispatch(activitiesActions.loadList());
      currentProject.usePlaces && dispatch(placesActions.loadList());
      dispatch(sessionsActions.loadList());
    },
    undefined,
    asModal // Don't clean if displayed as a modal so there is no glitch when making the page full screen
  );

  // Prevents rendering as long as we don't have crucial info to display
  if (!session._id) return null;

  const sessionSubscription = currentRegistration.sessionsSubscriptions?.find(
    (ss) => ss.session === session._id
  );

  const sameTimeSessionsRegistered = session.sameTimeSessions?.filter(
    (sts) => sts.sameTimeSessionRegistered
  );

  const otherSessionsOfSameActivityRegisterable = sessions.filter((otherSession) => {
    const notCurrentSession = session._id !== otherSession._id;
    const hasSameActivity = session.activity?._id === otherSession.activity._id;
    return notCurrentSession && hasSameActivity;
  });

  const subscriptionInfoColumn = {
    title: t("sessions:schema.subscriptionInfo.label"),
    render: (text, record) => {
      const sessionSubscription = record.sessionsSubscriptions.find(
        (ss) => ss.session === session._id
      );
      return generateSubscriptionInfo(record, sessionSubscription, currentProject._id);
    },
  };

  const {
    isSteward,
    sessionIsFull,
    participantIsNotAvailable,
    alreadyStewardOnOtherSession,
    alreadySubscribedToOtherSession,
    registrationIncomplete,
    volunteeringBeginsSoon,
    inConflictWithRegistrationDates,
  } = getSessionMetadata(session, currentRegistration);

  const infoAlert = getSessionInfoAlert(
    sessionSubscription,
    isSteward,
    currentProject.blockSubscriptions,
    registrationIncomplete,
    participantIsNotAvailable,
    sessionIsFull,
    alreadyStewardOnOtherSession,
    alreadySubscribedToOtherSession,
    inConflictWithRegistrationDates
  );

  const onSubscribe = () => dispatch(sessionsActions.subscribe());
  const onUnsubscribe = () => dispatch(sessionsActions.unsubscribe());

  console.log(session);

  return (
    <div className="page-container containerV fade-in">
      <LayoutElement.PageHeading
        backButton={!asModal}
        className="page-header"
        title={session.activity?.name}
        customButtons={
          <>
            {asModal && (
              <Tooltip title={t("sessions:show.showFullPage")}>
                <Button
                  type="link"
                  style={{flexGrow: 0}}
                  onClick={() => navigate(`${navigatePathRoot}${session._id}`)}
                  icon={<FullscreenOutlined />}
                />
              </Tooltip>
            )}
            <SubscribeUnsubscribeButton
              onUnsubscribe={onUnsubscribe}
              onSubscribe={onSubscribe}
              sessionIsFull={sessionIsFull}
              isSteward={isSteward}
              sessionSubscription={sessionSubscription}
              registrationIncomplete={registrationIncomplete}
              tooltipMessage={infoAlert?.message}
              alreadySubscribedToOtherSession={alreadySubscribedToOtherSession}
              alreadyStewardOnOtherSession={alreadyStewardOnOtherSession}
              participantIsNotAvailable={participantIsNotAvailable}
              currentProject={currentProject}
              volunteeringBeginsSoon={volunteeringBeginsSoon}
            />
          </>
        }
      />

      <FormElement>
        <div className="summary">
          <div
            className="containerH"
            style={{
              alignItems: "baseline",
              flexWrap: "wrap",
              marginTop: 15,
              rowGap: 8,
              marginBottom: 26,
            }}>
            <CategoryTagWithVolunteeringMajoration
              volunteeringCoefficient={getVolunteeringCoefficient(session)}
              category={session.activity?.category}>
              {session.activity?.category.name}
            </CategoryTagWithVolunteeringMajoration>
            {session.activity?.secondaryCategories?.map((tagName, index) => (
              <Tag key={index}>{tagName}</Tag>
            ))}
          </div>

          {!infoAlert && sessionSubscription && (
            <Alert
              type="success"
              showIcon
              style={{marginBottom: 26}}
              message={t("sessions:show.youAreSubscribed")}
            />
          )}
          {infoAlert && <Alert style={{marginBottom: 26}} {...infoAlert} />}

          {session.activity?.summary?.length > 0 && (
            <CardElement>{session.activity?.summary}</CardElement>
          )}
        </div>
        <div className="container-grid two-thirds-one-third">
          <div className="scroll-container">
            {session.activity?.description?.length > 0 && (
              <CardElement title={t("sessions:schema.description.label")}>
                <TextDisplayer value={session.activity?.description} />
              </CardElement>
            )}
            {session.participants && (
              <TableElement.WithTitle
                showHeader
                title={t("registrations:labelPlural")}
                subtitle={t("sessions:show.youSeeThisInfoBecauseYouAre", {
                  role: currentRegistration.role
                    ? t("registrations:roles.orgaOfTheEvent")
                    : t("registrations:roles.stewardOnThisSession"),
                })}
                columns={[...generateRegistrationsColumns(currentProject), subscriptionInfoColumn]}
                onRow={(record) => ({
                  onDoubleClick:
                    currentRegistration.role &&
                    (() =>
                      navigate(
                        `${URLS.ORGA_FRONT}/${currentProject._id}/participants/${record._id}`
                      )),
                })}
                dataSource={session.participants}
              />
            )}
          </div>
          <div className="info container-grid">
            <CardElement title={t("sessions:show.info")}>
              <div className="containerV container-grid">
                <InputElement.Custom label={t("sessions:show.timeSlot")}>
                  {session.slots.length > 1 && (
                    <Alert
                      message={t("sessions:show.thisSessionHasMultipleSlots", {
                        count: session.slots.length,
                      })}
                      style={{marginBottom: 12}}
                    />
                  )}
                  <List
                    rowKey="_id"
                    dataSource={session.slots}
                    renderItem={(slot) => (
                      <div>{listRenderer.longDateTimeRangeFormat(slot.start, slot.end)}</div>
                    )}
                  />
                </InputElement.Custom>

                {session.stewards.length > 0 && (
                  <InputElement.Custom label={t("stewards:labelPlural")}>
                    <List
                      dataSource={session.stewards}
                      renderItem={(item) => (
                        <div>
                          <Tooltip title={item.summary}>{personName(item)}</Tooltip>
                        </div>
                      )}
                    />
                  </InputElement.Custom>
                )}

                <SessionFilling
                  computedMaxNumberOfParticipants={session.computedMaxNumberOfParticipants}
                  numberOfParticipants={session.numberParticipants}
                />

                {currentProject.usePlaces && session.places.length > 0 && (
                  <InputElement.Custom
                    label={t("sessions:show.place")}
                    formItemProps={{style: {marginBottom: 0}}}>
                    <List
                      dataSource={session.places}
                      renderItem={(item) => (
                        <div>
                          <Tooltip title={item.summary}>{item.name}</Tooltip>
                        </div>
                      )}
                    />
                  </InputElement.Custom>
                )}
              </div>
            </CardElement>

            {sameTimeSessionsRegistered?.length > 0 && (
              <CardElement title={t("sessions:show.conflicts")} style={{borderColor: "orange"}}>
                <p>
                  Ce sont les sessions auxquelles vous êtes déjà inscrit⋅e, et qui entrent en
                  conflit car elles se déroulent sur les mêmes heures.
                </p>
                <List
                  dataSource={sameTimeSessionsRegistered}
                  renderItem={(stSession) => (
                    <Link
                      to={
                        asModal
                          ? `../../${stSession.sameTimeSession._id}`
                          : `../${stSession.sameTimeSession._id}`
                      }>
                      <Card style={{marginTop: 10, borderColor: "orange"}} hoverable>
                        <strong>{stSession.sameTimeSession.activity.name}</strong>
                        <List
                          rowKey="_id"
                          dataSource={stSession.sameTimeSlots}
                          renderItem={(slot) => (
                            <div>
                              {listRenderer.longDateTimeRangeFormat(
                                slot.sameTimeSlot.start,
                                slot.sameTimeSlot.end
                              )}
                            </div>
                          )}
                        />
                      </Card>
                    </Link>
                  )}
                />
              </CardElement>
            )}
            {otherSessionsOfSameActivityRegisterable?.length > 0 && (
              <CardElement title={t("sessions:show.otherTimeSlots")}>
                <List
                  grid
                  style={{margin: -4}}
                  dataSource={otherSessionsOfSameActivityRegisterable}
                  renderItem={(session) => (
                    <Link to={asModal ? `../../${session._id}` : `../${session._id}`}>
                      <Card style={{margin: 4}} hoverable>
                        {listRenderer.longDateFormat(session.start)}
                        <br />
                        {listRenderer.timeRangeFormat(session.start, session.end)}
                      </Card>
                    </Link>
                  )}
                />
              </CardElement>
            )}
          </div>
        </div>
      </FormElement>
    </div>
  );
}

export function SessionShowSmall({
  session,
  onSubscribe,
  onUnsubscribe,
  hoverable = false,
  style,
  onClick,
  withMargins,
}) {
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const volunteeringCoefficient = getVolunteeringCoefficient(session);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const sessionSubscription = currentRegistration.sessionsSubscriptions?.find(
    (ss) => ss.session === session._id
  );

  const {
    isSteward,
    sessionIsFull,
    participantIsNotAvailable,
    alreadyStewardOnOtherSession,
    alreadySubscribedToOtherSession,
    registrationIncomplete,
    volunteeringBeginsSoon,
    shouldBeDimmed,
    inConflictWithRegistrationDates,
  } = getSessionMetadata(session, currentRegistration);

  const infoAlert = getSessionInfoAlert(
    sessionSubscription,
    isSteward,
    currentProject.blockSubscriptions,
    registrationIncomplete,
    participantIsNotAvailable,
    sessionIsFull,
    alreadyStewardOnOtherSession,
    alreadySubscribedToOtherSession,
    inConflictWithRegistrationDates
  );

  const cardOpacity = shouldBeDimmed ? 0.5 : 1;

  return (
    <div className="containerV" style={{height: "100%", padding: withMargins && "15px 15px"}}>
      <div style={{position: "relative", zIndex: 1}}>
        <CategoryTagWithVolunteeringMajoration
          volunteeringCoefficient={volunteeringCoefficient}
          category={session.activity?.category}
          style={{
            opacity: cardOpacity,
            color: session.activity?.category.color,
            backgroundColor: "white",
            position: "absolute",
            top: 4,
            right: -4,
          }}
        />
        {inConflictWithRegistrationDates && (
          <div
            style={{
              position: "absolute",
              background: "white",
              borderRadius: 50,
              height: 26,
              width: 26,
              paddingLeft: 4,
              paddingTop: 1.5,
              top: 4,
              left: 4,
            }}>
            <WarningOutlined style={{fontSize: 18, color: "red"}} />
          </div>
        )}
      </div>
      <Tooltip title={infoAlert?.message} mouseEnterDelay={0.6}>
        <Card
          hoverable={hoverable}
          title={
            <div style={{textAlign: "center"}}>
              <div
                style={{
                  // Allow activity name to go on 3 lines not more
                  whiteSpace: "normal",
                  fontWeight: "bold",
                  overflow: "hidden",
                  display: "-webkit-box",
                  WebkitLineClamp: "3",
                  WebkitBoxOrient: "vertical",
                }}>
                {getSessionName(session)}
              </div>
              <CategoryTagWithVolunteeringMajoration
                category={session.activity?.category}
                style={{
                  textOverflow: "ellipsis",
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                  maxWidth: 300,
                  margin: "-5px 0 -10px 0",
                  opacity: 0.85,
                }}>
                {session.activity?.category.name}
              </CategoryTagWithVolunteeringMajoration>
            </div>
          }
          style={{
            opacity: cardOpacity,
            transition: "opacity 0.3s ease-in-out, box-shadow .3s, border-color .3s", // Also get the transition for bow-shadow cause otherwie it will be overriden
            border:
              volunteeringCoefficient > 0
                ? `2px solid ${session.activity.category.color}`
                : isSteward && "1px solid rgba(0, 0, 255, 0.18)",
            outline: inConflictWithRegistrationDates ? "3px solid #FF9999" : undefined,
            outlineOffset: inConflictWithRegistrationDates ? 4 : undefined,
            background: isSteward && "rgba(0, 0, 255, 0.06)",
            height: "100%",
            overflow: "hidden",
            ...style,
          }}
          headStyle={{
            background: session.activity?.category.color,
            color: "white",
            borderRadius: 0,
          }}
          onClick={onClick}>
          <div style={{textAlign: "center", marginBottom: 15}}>
            {listRenderer.longDateTimeRangeFormat(session.start, session.end)}
          </div>
          {session.activity?.summary && (
            <Paragraph ellipsis={{rows: 2}} style={{color: "grey"}}>
              {session.activity.summary}
            </Paragraph>
          )}
          <div className="containerH buttons-container">
            <strong style={{flexGrow: 0, marginBottom: 15}}>{t("sessions:filling.label")}:</strong>
            <SessionFilling
              showLabel={false}
              computedMaxNumberOfParticipants={session.computedMaxNumberOfParticipants}
              numberOfParticipants={session.numberParticipants}
            />
          </div>
          <div
            className="containerH"
            style={{
              alignItems: "baseline",
              flexWrap: "wrap",
              margin: 8,
              rowGap: 8,
              justifyContent: "center",
            }}>
            {session.activity?.secondaryCategories?.map((tagName, index) => (
              <Tag key={index}>{tagName}</Tag>
            ))}
          </div>
        </Card>
      </Tooltip>
      <div
        style={{
          position: "relative",
          bottom: 16,
          minHeight: 32,
          marginLeft: "25%",
          marginRight: "25%",
        }}>
        <SubscribeUnsubscribeButton
          onUnsubscribe={onUnsubscribe}
          onSubscribe={onSubscribe}
          sessionIsFull={sessionIsFull}
          isSteward={isSteward}
          sessionSubscription={sessionSubscription}
          tooltipMessage={infoAlert?.message}
          registrationIncomplete={registrationIncomplete}
          alreadySubscribedToOtherSession={alreadySubscribedToOtherSession}
          alreadyStewardOnOtherSession={alreadyStewardOnOtherSession}
          participantIsNotAvailable={participantIsNotAvailable}
          currentProject={currentProject}
          volunteeringBeginsSoon={volunteeringBeginsSoon}
          block
        />
      </div>
    </div>
  );
}
