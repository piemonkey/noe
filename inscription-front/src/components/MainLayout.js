import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {CalendarFilled, CalendarOutlined, PlaySquareOutlined} from "@ant-design/icons";
import {currentUserActions, currentUserSelectors} from "../features/currentUser.js";
import {LayoutElement} from "./common/LayoutElement";
import {LayoutStructure, setAppTheme} from "./common/LayoutStructure";
import {useBrowserTabTitle} from "../helpers/viewUtilities";
import {useRedirectToProjectIfOffline} from "../helpers/offlineModeUtilities";
import {instanceName, URLS} from "../app/configuration";
import {useTranslation} from "react-i18next";

export const MainLayout = ({page, children}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const authenticatedUser = useSelector(currentUserSelectors.selectAuthenticatedUser);

  useRedirectToProjectIfOffline();

  // ****** TAB TITLE ******

  useBrowserTabTitle(instanceName, page, {
    projects: t("projects:labelMyProjects"),
    "public-projects": t("projects:labelPublicProjects"),
  });

  // ***** RESET STUFF ******

  // On Main layout, reset app theme
  setAppTheme(undefined);

  // If we are on the main page, reset the connection as another user automatically
  useEffect(() => {
    if (authenticatedUser._id) dispatch(currentUserActions.changeConnectedAsUser(undefined));
  }, [authenticatedUser]);

  // ****** SIDE MENU ******

  const menu = {
    top: (
      <LayoutElement.Menu
        selectedItem={page}
        items={[
          // User events
          authenticatedUser._id && {
            label: t("projects:labelMyProjects"),
            key: "projects",
            icon: <CalendarOutlined />,
          },

          // Public events
          {
            label: t("projects:labelPublicProjects"),
            key: "public-projects",
            icon: <CalendarFilled />,
          },
        ]}
      />
    ),
    footer: (
      <LayoutElement.Menu
        selectedItem={page}
        items={[
          // Orga front
          authenticatedUser.superAdmin && {
            label: t("common:pagesNavigation.orgaFront"),
            url: `${URLS.ORGA_FRONT}/projects`,
            icon: <PlaySquareOutlined />,
          },
        ]}
      />
    ),
  };

  return (
    <LayoutStructure title={instanceName} menu={menu} profileUser={authenticatedUser}>
      {children}
    </LayoutStructure>
  );
};
