import React, {useEffect, useState} from "react";
import {Alert, Button, Checkbox, Tooltip} from "antd";
import {LogoutOutlined, UserSwitchOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import {currentProjectSelectors} from "../../features/currentProject.js";
import {currentUserActions, currentUserSelectors} from "../../features/currentUser";
import {listRenderer, listSorter} from "../../helpers/listUtilities";
import {ListPage} from "../common/ListPage";
import {generateRegistrationsColumns} from "../../helpers/tableUtilities";
import {useColumnsBlacklistingSelector} from "../../helpers/viewUtilities";
import {CardElement} from "../common/LayoutElement";
import {VolunteeringGauge} from "../common/VolunteeringGauge";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {InputElement} from "../common/InputElement";
import {FormElement} from "../common/FormElement";
import {navigate} from "@reach/router";
import {personName} from "../../helpers/utilities";
import moment from "moment";
import {Trans, useTranslation} from "react-i18next";

export const ConnectedAsAlert = ({fallback = null}) => {
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const authenticatedUser = useSelector(currentUserSelectors.selectAuthenticatedUser);
  return currentUser._id !== authenticatedUser._id ? (
    <Alert
      type="info"
      showIcon
      style={{marginBottom: 26}}
      message={
        <Trans
          i18nKey="alerts.currentlyConnectedAs"
          ns="users"
          values={{personName: personName(currentUser), email: currentUser?.email}}
        />
      }
    />
  ) : (
    fallback
  );
};

export function ParticipantList() {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const project = useSelector(currentProjectSelectors.selectProject);
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const authenticatedUser = useSelector(currentUserSelectors.selectAuthenticatedUser);
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelector();
  const registrations = useSelector(registrationsSelectors.selectListWithMetadata);
  const [displayUnbookedUsers, setDisplayUnbookedUsers] = useState(false);
  const [advancedMode, setAdvancedMode] = useState(false);

  useEffect(() => {
    dispatch(registrationsActions.loadList());
  }, []);

  useEffect(() => {
    const connectedAsParticipantId = new URLSearchParams(window.location.search).get("connectedAs");
    const requestedConnectedAsRegistration = registrations.find(
      (registration) => registration._id === connectedAsParticipantId
    );
    if (requestedConnectedAsRegistration) {
      const url = new URL(window.location);
      url.searchParams.delete("connectedAs");
      window.history.replaceState(null, null, url.toString());
      activateConnectionAsUserForRegistration(requestedConnectedAsRegistration._id);
    }
  }, [registrations]);

  const registrationsExceptAuthenticatedUser = registrations.filter(
    (r) =>
      // Don't include yourself
      r.user._id !== authenticatedUser._id &&
      // Display unbooked users or not
      (displayUnbookedUsers || r.availabilitySlots?.length > 0)
  );

  const activateConnectionAsUserForRegistration = (registrationId) => {
    const connectedAsUser = registrationsExceptAuthenticatedUser.find(
      (r) => r._id === registrationId
    ).user;
    dispatch(currentUserActions.changeConnectedAsUser(connectedAsUser));
    navigate("./registration");
  };

  const returnToNormal = () => dispatch(currentUserActions.changeConnectedAsUser(undefined));

  const columns = generateRegistrationsColumns(project, {
    middle: [
      {
        title: t("stewards:label"),
        dataIndex: "steward",
        render: (text, record) => personName(record.steward),
        sorter: (a, b) => listSorter.text(personName(a.steward), personName(b.steward)),
        searchText: (record) => personName(record.steward),
        searchable: true,
      },
      project.useTeams && {
        title: t("registrations:schema.team.label"),
        dataIndex: "team",
        render: (text, record) => record.teamsSubscriptionsNames,
        sorter: (a, b) => listSorter.text(a.teamsSubscriptionsNames, b.teamsSubscriptionsNames),
        searchable: true,
        searchText: (record) => record.teamsSubscriptionsNames,
      },
    ],
    end: [
      {
        title: t("registrations:schema.hasCheckedIn.label"),
        dataIndex: "hasCheckedIn",
        render: (text, record) => <Checkbox checked={record.hasCheckedIn} disabled />,
        sorter: (a, b) => listSorter.number(a.hasCheckedIn, b.hasCheckedIn),
        width: 95,
      },
      {
        title: t("registrations:schema.arrivalDateTime.label"),
        dataIndex: "arrivalDateTime",
        render: (text, record) => {
          // If arrived, green. If not arrived and date is past, red. Else, no color
          const color = record.hasCheckedIn
            ? "green"
            : moment().isAfter(record.arrivalDateTime) && "red";
          return (
            <span style={{color}}>
              {listRenderer.longDateTimeFormat(record.arrivalDateTime, true)}
            </span>
          );
        },
        sorter: (a, b) => listSorter.date(a.arrivalDateTime, b.arrivalDateTime),
        searchable: true,
        searchText: (record) => listRenderer.longDateTimeFormat(record.arrivalDateTime, true),
        width: 155,
      },
      {
        title: t("registrations:schema.departureDateTime.label"),
        dataIndex: "departureDateTime",
        render: (text, record) => {
          // If arrived, green. If not arrived and date is past, red. Else, no color
          const color =
            record.hasCheckedIn && moment().isAfter(record.departureDateTime) && "green";
          return (
            <span style={{color}}>
              {listRenderer.longDateTimeFormat(record.departureDateTime, true)}
            </span>
          );
        },
        sorter: (a, b) => listSorter.date(a.departureDateTime, b.departureDateTime),
        searchable: true,
        searchText: (record) => listRenderer.longDateTimeFormat(record.departureDateTime, true),
        width: 155,
      },
      advancedMode && {
        title: t("registrations:schema.hidden.label"),
        dataIndex: "hidden",
        render: (text, record) => <Checkbox checked={record.hidden} disabled />,
        sorter: (a, b) => listSorter.number(a.hidden, b.hidden),
        width: 100,
      },
      {
        title: t("registrations:schema.everythingIsOk.label"),
        dataIndex: "everythingIsOk",
        render: (text, record) => (record.everythingIsOk ? "✔️" : "❌"),
        sorter: (a, b) => listSorter.number(a.everythingIsOk, b.everythingIsOk),
        width: 115,
      },
      {
        title: t("registrations:schema.voluntaryCounter.label"),
        dataIndex: "voluntaryCounter",
        render: (text, record) => <VolunteeringGauge registration={record} />,
        sorter: (a, b) => listSorter.number(a.voluntaryCounter, b.voluntaryCounter),
      },
      {
        key: "action",
        fixed: "right",
        render: (text, record) => (
          <div style={{margin: 4, textAlign: "right"}}>
            {record.user._id === currentUser?._id ? (
              <Tooltip title={t("registrations:actions.stopChangeIdentity")} placement="left">
                <Button
                  style={{marginRight: 8}}
                  danger
                  type="primary"
                  onClick={() => returnToNormal(dispatch)}
                  icon={<LogoutOutlined />}
                />
              </Tooltip>
            ) : (
              <Tooltip title={t("registrations:actions.changeIdentity")} placement="left">
                <Button
                  type="link"
                  icon={<UserSwitchOutlined />}
                  onClick={() => activateConnectionAsUserForRegistration(record._id)}
                />
              </Tooltip>
            )}
          </div>
        ),
        width: 56,
      },
    ],
  });

  return (
    <ListPage
      title={t("registrations:actions.changeIdentity")}
      subtitle={
        <ConnectedAsAlert
          fallback={<p>{t("registrations:actions.changeIdentityDescription")}</p>}
        />
      }
      entitiesName={t("stewards:labelPlural")}
      rowClassName={(record) =>
        record.user._id === currentUser?._id
          ? "ant-table-row-selected"
          : record.everythingIsOk
          ? ""
          : "ant-table-row-danger"
      }
      editable={false}
      deletable={false}
      navigateFn={activateConnectionAsUserForRegistration}
      columns={filterBlacklistedColumns(columns)}
      dataSource={registrationsExceptAuthenticatedUser}
      settingsDrawerContent={
        <FormElement>
          <ColumnsBlacklistingSelector columns={columns} />
          <CardElement title={t("registrations:list.dataDisplayOptionsTitle")}>
            <div className="container-grid">
              <InputElement.Switch
                label={t("registrations:list.displayUnbookedUsers")}
                checked={displayUnbookedUsers}
                onChange={setDisplayUnbookedUsers}
              />
              <InputElement.Switch
                label={t("registrations:list.advancedMode")}
                checked={advancedMode}
                onChange={setAdvancedMode}
              />
            </div>
          </CardElement>
        </FormElement>
      }
    />
  );
}
