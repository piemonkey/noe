import React, {useEffect, useState} from "react";
import {Button, Form, Modal, Input, message, ConfigProvider} from "antd";
import moment from "moment";
import {TableElement} from "../common/TableElement";
import {listRenderer, listSorter} from "../../helpers/listUtilities";
import {FormElement} from "../common/FormElement";
import {InputElement} from "../common/InputElement";
import {addKeyToItemsOfList} from "../../helpers/tableUtilities";
import {useTranslation} from "react-i18next";

export const slotEndIsBeforeBeginning = (slot) =>
  moment(slot.start).toDate() > moment(slot.end).toDate();

export const slotOverlapsAnOtherOne = (slot, availabilitySlots, ignoreThisSlot = undefined) => {
  if (!availabilitySlots) return false;

  for (let existingSlot of availabilitySlots) {
    if (
      ignoreThisSlot &&
      existingSlot.start === ignoreThisSlot.start &&
      existingSlot.end === ignoreThisSlot.end
    ) {
      continue;
    }

    // Check if the slot overlaps an other slot
    if (
      moment(slot.start).toDate() < moment(existingSlot.end).toDate() &&
      moment(slot.end).toDate() > moment(existingSlot.start).toDate()
    ) {
      return true;
    }
  }
  return false;
};

// TODO this component should REALLY be refactored
export function Availability({
  onChange,
  customButtons,
  defaultPickerValue, // Doesn't work with rangePicker, but hopefully a new version of AntDesign will solve the issue
  disableDatesIfOutOfProject, // True: We grey out all the dates out of the project scope / False: only dates in the past
  blockDisabledDates, // We prevent from choosing grayed out dates
  disabled,
  title,
  data,
}) {
  const {t} = useTranslation();
  const [availabilitySlots, setAvailabilitySlots] = useState();

  const [generalAddSlotButton, setGeneralAddSlotButton] = useState(false);
  const [disableAddSlotButton, setDisableAddSlotButton] = useState(true);
  const [disableUpdateSlotButton, setDisableUpdateSlotButton] = useState(false);
  const [showModalUpdateSlot, setShowModalUpdateSlot] = useState(false);
  const [currentSlot, setCurrentSlot] = useState();
  const [removedSlot, setRemovedSlot] = useState();
  const [updateSlot, setUpdateSlot] = useState();
  const [form] = Form.useForm();
  const [modalForm] = Form.useForm();

  const validateSlot =
    (setDisableButton, ignoreThisSlot = undefined) =>
    (_, value) => {
      // If null, OK
      if (value == null) {
        setDisableButton(true);
        return Promise.resolve();
      }

      const slot = formatSlotFromField(value);

      // Make various checks
      if (slotOverlapsAnOtherOne(slot, availabilitySlots, ignoreThisSlot)) {
        setDisableButton(true);
        return Promise.reject(t("common:availability.errors.slotOverlapsAnotherOne"));
      }
      if (slotEndIsBeforeBeginning(slot)) {
        setDisableButton(true);
        return Promise.reject(t("common:availability.errors.slotEndIsBeforeBeginning"));
      }

      // If it passes, it's ok
      setDisableButton(false);
      return Promise.resolve();
    };

  const formatSlotFromField = (fields) => {
    return {
      start: moment(fields[0].toDate()).seconds(0).format(),
      end: moment(fields[1].toDate()).seconds(0).format(),
    };
  };

  useEffect(() => {
    setAvailabilitySlots(data ? data : []);
  }, [data]);

  useEffect(() => {
    if (currentSlot) {
      let newData = [...availabilitySlots, currentSlot];
      setAvailabilitySlots(newData);
      form.resetFields();
      if (onChange) {
        onChange(newData);
      }
    }
  }, [currentSlot]);

  useEffect(() => {
    if (removedSlot) {
      let newData = [...availabilitySlots];
      newData.splice(removedSlot.key, 1);
      setAvailabilitySlots(newData);
      if (onChange) {
        onChange(newData);
      }
    }
  }, [removedSlot]);

  useEffect(() => {
    if (updateSlot) {
      let newData = [...availabilitySlots];
      newData[updateSlot.oldSlot.key] = updateSlot.newSlot;
      setAvailabilitySlots(newData);
      modalForm.resetFields();
      if (onChange) {
        onChange(newData);
      }
    }
  }, [updateSlot]);

  const updateTimeSlot = () => {
    if (disableUpdateSlotButton) {
      message.error(t("common:availability.errors.oopsDateIncorrect"));
      return;
    }
    let values = modalForm.getFieldsValue().slot;

    if (values !== undefined) {
      let oldSlot = modalForm.getFieldsValue().oldSlot;
      let slot = formatSlotFromField(values);
      setUpdateSlot({newSlot: slot, oldSlot: oldSlot});
    }

    setShowModalUpdateSlot(false);
  };

  const addTimeSlot = () => {
    if (disableAddSlotButton) {
      message.error(t("common:availability.errors.oopsDateIncorrect"));
      return;
    }
    let values = form.getFieldsValue().slot;

    if (values !== undefined) {
      let slot = formatSlotFromField(values);
      setCurrentSlot(slot);
      setDisableAddSlotButton(true);
    }
  };

  const editSlot = (record) => {
    modalForm.setFieldsValue({
      slot: [moment(record.start), moment(record.end)],
      oldSlot: record,
    });
    setShowModalUpdateSlot(true);
  };

  const sessionSlotsTable = addKeyToItemsOfList(availabilitySlots);

  const columns = [
    {
      title: t("common:availability.startEnd"),
      key: "dateRange",
      sorter: (a, b) => listSorter.date(a.start, b.start),
      defaultSortOrder: "ascend",
      render: (text, record, index) =>
        listRenderer.longDateTimeRangeFormat(record.start, record.end),
    },
  ];

  return (
    <>
      <div>
        {(title || !disabled) && (
          <div className="header-space-between list-element-header">
            <h3>{title}</h3>
            <div className="containerH buttons-container">
              {customButtons}
              {!disabled && (
                <FormElement form={form} style={{flexGrow: 1}}>
                  <div className="containerH buttons-container">
                    {generalAddSlotButton && (
                      <>
                        <InputElement.DateTimeRange
                          autoFocus
                          formItemProps={{style: {margin: 0}}}
                          name="slot"
                          defaultPickerValue={defaultPickerValue}
                          disableDatesIfOutOfProject={disableDatesIfOutOfProject}
                          blockDisabledDates={blockDisabledDates}
                          rules={[{validator: validateSlot(setDisableAddSlotButton)}]}
                        />

                        <Button onClick={addTimeSlot}>{t("common:add")}</Button>
                      </>
                    )}
                    {availabilitySlots?.length > 0 && !generalAddSlotButton && (
                      <Button onClick={() => setGeneralAddSlotButton(true)}></Button>
                    )}
                  </div>
                </FormElement>
              )}
            </div>
          </div>
        )}

        <ConfigProvider
          renderEmpty={
            !generalAddSlotButton &&
            !disabled &&
            (() => (
              <Button onClick={() => setGeneralAddSlotButton(true)}>
                {t("common:availability.addNewSlots")}
              </Button>
            ))
          }>
          <TableElement.Simple
            columns={columns}
            rowKey="start"
            scroll={{y: "50vh", x: 400}}
            dataSource={sessionSlotsTable}
            onEdit={!disabled && editSlot}
            onDelete={!disabled && setRemovedSlot}
          />
        </ConfigProvider>
      </div>

      <Modal
        title={t("common:availability.modifyASlot")}
        visible={showModalUpdateSlot}
        onOk={updateTimeSlot}
        onCancel={() => setShowModalUpdateSlot(false)}>
        <FormElement form={modalForm}>
          <div
            className="containerH"
            style={{
              flexWrap: "wrap",
            }}>
            <InputElement.DateTimeRange
              label={t("common:availability.slot")}
              name="slot"
              defaultPickerValue={defaultPickerValue}
              disableDatesIfOutOfProject={disableDatesIfOutOfProject}
              blockDisabledDates={blockDisabledDates}
              rules={[
                {
                  validator: validateSlot(
                    setDisableUpdateSlotButton,
                    modalForm.getFieldValue("oldSlot")
                  ),
                },
              ]}
            />
            <Form.Item hidden name="oldSlot">
              <Input />
            </Form.Item>
          </div>
        </FormElement>
      </Modal>
    </>
  );
}
