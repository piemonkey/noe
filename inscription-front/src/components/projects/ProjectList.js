import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {projectsActions, projectsSelectors} from "../../features/projects.js";
import {listRenderer, normalize} from "../../helpers/listUtilities";
import {currentProjectActions, currentProjectSelectors} from "../../features/currentProject";

import {currentUserSelectors} from "../../features/currentUser";
import {useNavigate} from "@reach/router";
import {GridPage} from "../common/GridPage";
import {CardElement} from "../common/LayoutElement";
import {WelcomePageEditor} from "../common/welcomePageEditor/WelcomePageEditor";
import {SaveOutlined} from "@ant-design/icons";
import {Tag, Tooltip} from "antd";
import {useTranslation} from "react-i18next";

export function ProjectList({displayAllPublicProjects = false}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const projects = useSelector(projectsSelectors.selectList);
  const publicProjects = useSelector(projectsSelectors.selectPublicList);

  const alreadyLoadedProject = useSelector(currentProjectSelectors.selectProject);
  const authenticatedUser = useSelector(currentUserSelectors.selectAuthenticatedUser);

  useEffect(() => {
    dispatch(projectsActions.loadList(displayAllPublicProjects));
  });

  const cleanStateAndNavigate = async (projectId) => {
    const shouldReload =
      alreadyLoadedProject._id &&
      alreadyLoadedProject._id !== projectId &&
      alreadyLoadedProject.slug !== projectId;
    // If there is already a loaded project, and that it's not the same as the requested project, clean everything. Otherwise, keep the data
    if (shouldReload) await dispatch(currentProjectActions.cleanProject());

    const projectToNavigateTo = (displayAllPublicProjects ? publicProjects : projects).find(
      (project) => project.slug === projectId || project._id === projectId
    );

    navigate(
      // Then navigate only after cleaning (to welcome page if no registration, or if there is a registration, let the ProjectLayout decide)
      authenticatedUser.registrations?.find(
        (registration) => registration.project === projectToNavigateTo._id
      )
        ? `/${projectToNavigateTo.slug || projectToNavigateTo._id}`
        : `/${projectToNavigateTo.slug || projectToNavigateTo._id}/welcome`
    );
  };

  return (
    <GridPage
      title={
        displayAllPublicProjects ? t("projects:labelPublicProjects") : t("projects:labelMyProjects")
      }
      subtitle={
        <div style={{marginBottom: 15}}>
          {displayAllPublicProjects
            ? t("projects:subtitlePublicProjects")
            : t("projects:subtitleMyProjects")}
        </div>
      }
      searchItems={(item, searchText) => normalize(item.name).includes(normalize(searchText))}
      renderItem={(item) => (
        <CardElement
          title={
            <div style={{textAlign: "center"}}>
              <div
                style={{
                  // Allow activity name to go on 3 lines not more
                  whiteSpace: "normal",
                  fontWeight: "bold",
                  overflow: "hidden",
                  display: "-webkit-box",
                  WebkitLineClamp: "3",
                  WebkitBoxOrient: "vertical",
                }}>
                <strong>
                  {item.name}
                  {alreadyLoadedProject._id === item._id && authenticatedUser.superAdmin && (
                    <Tooltip title={t("projects:list.alreadySavedContentIconTooltip")}>
                      <SaveOutlined style={{marginLeft: 8, opacity: 0.6}} />
                    </Tooltip>
                  )}
                </strong>
              </div>
              {item.start && item.end && (
                <Tag style={{width: "auto"}}>
                  {listRenderer.longDateRangeFormat(item.start, item.end, true)}
                </Tag>
              )}
            </div>
          }
          borderless
          hoverable
          onClick={() => cleanStateAndNavigate(item.slug || item._id)}
          style={{margin: 15, overflow: "hidden", height: 300, borderColor: "#c9c9c9"}}
          headStyle={{background: "#fafafa"}}>
          <div style={{zoom: 0.3, overflow: "hidden"}}>
            <WelcomePageEditor readOnly value={item.content} />
          </div>
        </CardElement>
      )}
      dataSource={displayAllPublicProjects ? publicProjects : projects}
    />
  );
}
