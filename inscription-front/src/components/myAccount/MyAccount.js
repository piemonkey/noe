import React, {useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import {currentUserActions} from "../../features/currentUser.js";
import {usersSelectors, usersActions} from "../../features/users.js";
import {Button, Modal, Form, Collapse, Alert, Popconfirm, message} from "antd";
import {InputElement} from "../common/InputElement";
import {EditPage} from "../common/EditPage";
import {checkPasswordsAreSame, validatePassword} from "../common/ConnectionPage";
import {CardElement} from "../common/LayoutElement";
import {DisplayOfflineModeFeatureSwitch} from "../../helpers/offlineModeUtilities";
import {useTranslation} from "react-i18next";

const {Panel} = Collapse;

const checkOldPasswordIsNotTheSameValidator = ({getFieldValue}) => ({
  validator(_, value) {
    const newPasswordGiven = getFieldValue("oldPassword");
    const oldPasswordGiven = value;

    return newPasswordGiven.length && !oldPasswordGiven.length
      ? Promise.reject(new Error("Vous devez saisir votre ancien mot de passe pour le changer."))
      : newPasswordGiven === oldPasswordGiven
      ? Promise.reject(new Error("Le nouveau mot de passe doit être différent du précédent."))
      : Promise.resolve();
  },
});

export function MyAccount({visibleState}) {
  const {i18n, t} = useTranslation();
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const user = useSelector(usersSelectors.selectEditing);
  const [showModal, setShowModal] = visibleState;

  const onValidation = (formData) => {
    if (formData.locale !== user.locale) {
      i18n.changeLanguage(formData.locale);
    }
    dispatch(usersActions.persist({...user, ...formData}));
    form.resetFields(["oldPassword", "password", "confirmPassword"]);
    return Promise.resolve();
  };

  const DeleteAccountZone = () => {
    const [canDeleteAccount, setCanDeleteAccount] = useState();
    return (
      <CardElement
        style={{borderColor: "red", marginBottom: 0}}
        headStyle={{borderColor: "red"}}
        title={<strong style={{color: "red"}}>{t("users:edit.dangerZone.title")}</strong>}>
        <Alert
          style={{marginBottom: 26}}
          type="error"
          message={t("users:edit.dangerZone.accountDeletionAlert.message")}
          description={t("users:edit.dangerZone.accountDeletionAlert.description")}
        />

        <InputElement.Text
          style={{marginBottom: 20}}
          label={t("users:edit.dangerZone.writeIWantToDeleteMyAccountToClick")}
          onChange={(event) =>
            setCanDeleteAccount(
              event.target.value === t("users:edit.dangerZone.iWantToDeleteMyAccount")
            )
          }
          placeholder={t("users:edit.dangerZone.iWantToDeleteMyAccount")}
        />
        <Popconfirm
          visible={canDeleteAccount ? undefined : false}
          title={t("common:areYouSure")}
          onConfirm={() => dispatch(currentUserActions.deleteAccount())}
          okText={t("users:edit.dangerZone.yesDeleteMyAccount")}
          okButtonProps={{danger: true}}
          cancelText={t("common:cancel")}>
          <Button danger disabled={!canDeleteAccount} type="primary">
            {t("users:edit.dangerZone.deleteAccountButton")}
          </Button>
        </Popconfirm>
      </CardElement>
    );
  };

  return (
    <Modal
      visible={showModal}
      width={"min(90vw, 1000pt)"}
      footer={false}
      onCancel={() => setShowModal(false)}>
      <div className="modal-page-content">
        <EditPage
          formOverride={form}
          editTitle={t("users:labelMyAccount")}
          navigateAfterValidation={false}
          backButton={false}
          customButtons={
            <Button
              type="primary"
              danger
              onClick={() => {
                dispatch(currentUserActions.logOut());
                setShowModal(false);
                message.success(t("users:messages.logOutSuccessful"));
              }}>
              {t("users:edit.logOutButton")}
            </Button>
          }
          onValidation={onValidation}
          record={user}
          initialValues={user}>
          <CardElement>
            <div className="container-grid two-per-row">
              <InputElement.Text i18nNs="users" name="firstName" />
              <InputElement.Text i18nNs="users" name="lastName" />
              <InputElement.Text i18nNs="users" name="email" disabled />
              <InputElement.Select
                i18nNs="users"
                name="locale"
                options={[
                  {value: "fr", label: "🇫🇷 " + t("users:schema.locale.options.french")},
                  {value: "en", label: "🇬🇧 " + t("users:schema.locale.options.english")},
                ]}
              />
            </div>
          </CardElement>

          <Collapse>
            <Panel header={t("users:edit.changePassword.title")}>
              <div className="container-grid">
                <InputElement.Password
                  label={t("users:schema.password.labelCurrent")}
                  name="oldPassword"
                  placeholder={t("users:schema.password.placeholder")}
                  autoComplete="current-password"
                  rules={[{required: true}]}
                />

                <div className="container-grid two-per-row">
                  <InputElement.Password
                    label={t("users:schema.password.labelNew")}
                    name="password"
                    dependencies={["oldPassword"]}
                    placeholder={t("users:schema.password.placeholder")}
                    autoComplete="new-password"
                    rules={[
                      checkOldPasswordIsNotTheSameValidator,
                      {validator: validatePassword(form)},
                      {min: 8},
                    ]}
                  />

                  <InputElement.Password
                    label={t("users:schema.password.labelConfirmNew")}
                    name="confirmPassword"
                    placeholder={t("users:schema.password.placeholder")}
                    autoComplete="new-password"
                    dependencies={["password"]}
                    rules={[{validator: checkPasswordsAreSame(form)}]}
                  />
                </div>
              </div>
            </Panel>
          </Collapse>

          <Collapse style={{marginTop: 26}}>
            <Panel header={t("common:offlineMode.title")}>
              <DisplayOfflineModeFeatureSwitch />
            </Panel>
          </Collapse>

          <Collapse style={{marginTop: 26}}>
            <Panel header={t("users:edit.accountManagement")}>
              <DeleteAccountZone />
            </Panel>
          </Collapse>
        </EditPage>
      </div>
    </Modal>
  );
}
