import React, {useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import {currentProjectSelectors, currentProjectActions} from "../../features/currentProject.js";
import {RegisterToProjectPopup, WelcomeMessagePopup} from "./welcomeMessagePopup";
import {currentUserSelectors} from "../../features/currentUser";
import {WelcomePageEditor} from "../common/welcomePageEditor/WelcomePageEditor";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {useBrowserTabTitle} from "../../helpers/viewUtilities";
import {instanceName} from "../../app/configuration";
import {useTranslation} from "react-i18next";

export function Welcome({envId, navigate}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  const connected = useSelector(currentUserSelectors.selectConnected);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

  // Necessary when the user is not connected at all
  useEffect(() => {
    if (
      (currentProject._id !== envId && currentProject.slug !== envId) || // if the project to load is not the same as the current project, it has to be loaded
      (currentProject.public && connected) || //Or... if it's loaded in public mode whereas the user is now connected, we have to reload it again also
      (!currentProject.public && !connected)
    ) {
      dispatch(currentProjectActions.load(envId)).catch(() => navigate("/projects"));
    }
  }, [envId, connected]);

  useBrowserTabTitle(currentProject.name, "welcome", {welcome: t("welcome:label")}, instanceName);

  // Load project registration
  useEffect(() => {
    // Only load the registration if we are connected and if it is not loaded yet
    connected &&
      currentProject._id &&
      !currentRegistration._id &&
      dispatch(registrationsActions.loadCurrent());
  }, [connected, currentProject._id]);

  return (
    <div
      className={`containerV bg-white ${connected ? "full-width-content" : 0}`}
      style={{paddingBottom: connected ? 0 : 90}}>
      {/* Welcome message when not connected at all */}
      {!connected && currentProject._id && (
        <WelcomeMessagePopup project={currentProject} navigate={navigate} />
      )}
      {/* Reminder message to tell people to register to the event */}
      {connected && currentProject._id && (
        <RegisterToProjectPopup currentRegistration={currentRegistration} navigate={navigate} />
      )}
      {currentProject.content && <WelcomePageEditor value={currentProject.content} readOnly />}
    </div>
  );
}
