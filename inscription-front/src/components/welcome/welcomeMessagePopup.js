import {Button, message} from "antd";
import React from "react";
import {useSelector} from "react-redux";
import {viewSelectors} from "../../features/view";
import {useTranslation} from "react-i18next";

const welcomeMessagePopupKey = "welcomeMessagePopup";

const BottomMessage = ({message, children}) => (
  <div className="ant-message-bottom ant-message bounce-in">
    <div className="ant-message-notice">
      <div className="ant-message-notice-content">
        <div className="containerH buttons-container">
          <p style={{margin: "8px 15px"}}>{message}</p>
          <div style={{margin: "8px 15px"}}>{children}</div>
        </div>
      </div>
    </div>
  </div>
);

export const WelcomeMessagePopup = ({project, navigate}) => {
  const {t} = useTranslation();
  const searchParamsInfo = useSelector(viewSelectors.selectSearchParams);
  const projectIsNotOpened = project.openingState === "notOpened";
  const welcomeMessage = projectIsNotOpened
    ? t("welcome:eventIsNotOpen")
    : project.full
    ? t("welcome:eventIsFull")
    : t("welcome:eventIsOpen");

  return (
    <BottomMessage
      message={
        <>
          {!project.full &&
            searchParamsInfo.firstName &&
            t("welcome:hiFirstName", {
              firstName: searchParamsInfo.firstName,
            })}
          {welcomeMessage}
        </>
      }>
      {project.full || projectIsNotOpened ? (
        <Button
          type="primary"
          onClick={() => {
            message.destroy(welcomeMessagePopupKey);
            navigate("../../../signup");
          }}>
          {t("welcome:registerToOtherEvents")}
        </Button>
      ) : (
        <Button
          type="primary"
          onClick={() => {
            message.destroy(welcomeMessagePopupKey);
            navigate("../signup");
          }}>
          {t("welcome:register")}
        </Button>
      )}
      <Button
        style={{marginLeft: 8}}
        onClick={() => {
          message.destroy(welcomeMessagePopupKey);
          navigate("../login");
        }}>
        {t("common:iAlreadyHaveAnAccount")}
      </Button>
    </BottomMessage>
  );
};

export const RegisterToProjectPopup = ({navigate, currentRegistration}) => {
  const {t} = useTranslation();
  if (currentRegistration._id !== "new" && currentRegistration.inDatabase.everythingIsOk)
    return null;

  return (
    <BottomMessage
      message={
        currentRegistration._id === "new"
          ? t("welcome:pleaseRegisterToTheEvent")
          : t("welcome:pleaseCompleteYourRegistration")
      }>
      <Button
        type="primary"
        onClick={() => {
          message.destroy(welcomeMessagePopupKey);
          navigate("../registration");
        }}>
        {currentRegistration._id === "new"
          ? t("welcome:register")
          : t("welcome:completeMyRegistration")}
      </Button>
    </BottomMessage>
  );
};
