import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Table, Tooltip} from "antd";
import {
  DoubleRightOutlined,
  EditOutlined,
  SelectOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import {CardElement, LayoutElement} from "./LayoutElement";
import {InputElement} from "./InputElement";

import {
  getFullHeightWithMargins,
  navbarHeight,
  useSavedPagination,
  useWindowDimensions,
} from "../../helpers/viewUtilities";
import {paginationPageSizes, viewActions, viewSelectors} from "../../features/view";
import {DeleteButton} from "./EditPage";
import {useCopyColumns, useSearchInColumns} from "../../helpers/tableUtilities";
import {listSorter, useDrawer} from "../../helpers/listUtilities";
import {navigate, useMatch} from "@reach/router";
import {fetchWithMessages} from "../../helpers/reduxUtilities";
import {currentProjectSelectors} from "../../features/currentProject";
import {useLocalStorageState} from "../../helpers/localStorageUtilities";
import {FormElement} from "./FormElement";
import {useTranslation} from "react-i18next";

const useEntitySchema = (filterSchema) => {
  const [entitySchema, setEntitySchema] = useState([]);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const {endpoint} = useMatch("/:envId/:endpoint");

  useEffect(() => {
    fetchWithMessages(`projects/${currentProject._id}/${endpoint}/schema`, {
      method: "GET",
    }).then((schema) => setEntitySchema(schema.filter(filterSchema || (() => true))));
  }, []);

  return [entitySchema, endpoint];
};

const GroupEditionButton = ({selectedRowKeys}) => {
  const {t} = useTranslation();
  const [visible, setVisible] = useState(false);
  const {useAI, usePlaces, useTeams} = useSelector(currentProjectSelectors.selectProject);
  const [selectedFields, setSelectedFields] = useState([]);
  const [entitySchema, endpoint] = useEntitySchema((field) => {
    if (
      (!useAI && field.useAI) ||
      (!usePlaces && field.usePlaces) ||
      (!useTeams && field.useTeams) ||
      field.noGroupEditing
    ) {
      return false;
    } else {
      return true;
    }
  });

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setSelectedFields(selectedRowObject);
    },
  };

  return (
    <>
      <Tooltip title={t("common:listPage.groupedEdition.tooltip")}>
        <Button
          icon={<EditOutlined />}
          type="link"
          disabled={selectedRowKeys.length === 0}
          onClick={() => setVisible(true)}
        />
      </Tooltip>

      <LayoutElement.ElementSelectionModal
        title={t("common:listPage.groupedEdition.modalTitle")}
        subtitle={t("common:listPage.groupedEdition.modalSubtitle")}
        visible={visible}
        onOk={() => {
          navigate(`${endpoint}/groupedit`, {
            state: {
              groupEditing: {
                fieldsToUpdate: selectedFields,
                elements: selectedRowKeys,
              },
            },
          });
        }}
        onCancel={() => setVisible(false)}
        rowSelection={rowSelection}
        selectedRowKeys={selectedFields}
        rowKey="key"
        size="small"
        showHeader={false}
        setSelectedRowKeys={setSelectedFields}
        columns={[
          {
            dataIndex: "label",
            defaultSortOrder: "ascend",
            sorter: (a, b) => listSorter.text(a.label, b.label),
          },
        ]}
        dataSource={entitySchema}
      />
    </>
  );
};

export function ListPage({
  title,
  entitiesName,
  subtitle,
  customButtons,
  multipleActionsButtons: MultipleActionsButtons,
  buttonTitle,
  settingsDrawerContent,

  header = true,
  fullPage = true,

  elementsActions,
  columns: rawColumns,
  dataSource,

  editable = true,
  deletable = true,
  clickable,
  navigateFn,
  navigable = false,
  groupEditable = false,

  rowClassName,
  expandable,
}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const [pagination, setPagination] = useSavedPagination();
  const {isMobileView} = useWindowDimensions();
  const sorting = useSelector(viewSelectors.selectSorting);
  const [displaySize, setDisplaySize] = useLocalStorageState(
    "listDisplaySize",
    isMobileView ? "small" : "middle"
  );
  const [setSettingsDrawerVisible, SettingsDrawer] = useDrawer({
    width: isMobileView ? undefined : "700px",
  });

  /*********** Multiple selection **************************/

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [enableMultipleSelection, setEnableMultipleSelection] = useState(false);

  const rowSelection = enableMultipleSelection && {
    selectedRowKeys,
    onChange: (newSelectedRowKeys) => setSelectedRowKeys(newSelectedRowKeys),
  };

  const onBatchDelete = async () => {
    const notDeleted = [];
    for (const id of selectedRowKeys) {
      try {
        await dispatch(elementsActions.remove(id));
      } catch {
        notDeleted.push(id);
      }
    }
    setSelectedRowKeys(notDeleted);
  };

  /*********** Sorting / Filtering memorization ************/

  let {columns, handleDisplayConfigChange, currentDataSource} = useCopyColumns(
    dataSource,
    rawColumns,
    (pagination, filters, sorter, extra) => {
      dispatch(viewActions.changeSorting({filteredInfo: filters, sortedInfo: sorter}));
    }
  );

  const clearDisplayConfig = () => {
    dispatch(viewActions.changeSorting({}));
  };

  columns = columns.map((column) => {
    const savedSortingOrder =
      sorting?.sortedInfo?.column?.dataIndex === column.dataIndex && sorting?.sortedInfo?.order;
    return {
      // The column data
      ...column,
      // Add the saved sorting order if found
      sortOrder: savedSortingOrder || column.sortOrder,
      filteredValue: sorting?.filteredInfo?.[column.dataIndex],
    };
  });

  columns = useSearchInColumns(columns);

  if (editable || deletable || navigable) {
    columns.push({
      key: "action",
      width: 9 + (editable ? 32 + 9 : 0) + (deletable ? 32 + 9 : 0) + (navigable ? 32 + 9 : 0),
      render: (text, record) => (
        <div
          className="containerH buttons-container"
          style={{justifyContent: "flex-end", flexWrap: "nowrap"}}>
          {navigable ? (
            <Button
              style={{flexGrow: 0}}
              type="link"
              icon={<DoubleRightOutlined />}
              onClick={() => navigateFn(record._id)}
            />
          ) : (
            <>
              {editable && (
                <Button
                  style={{flexGrow: 0}}
                  type="link"
                  icon={<EditOutlined />}
                  onClick={() => navigateFn(record.slug || record._id)}
                />
              )}
              {deletable && (
                <DeleteButton onConfirm={() => dispatch(elementsActions.remove(record._id))} />
              )}
            </>
          )}
        </div>
      ),
    });
  }

  const resize = () => {
    if (header) {
      try {
        const heightOffset =
          getFullHeightWithMargins(document.querySelector(".ant-table-header")) + // table header row
          getFullHeightWithMargins(document.querySelector(".subtitle")) + // subtitle
          getFullHeightWithMargins(document.querySelector(".ant-table-pagination")) + // table footer row
          getFullHeightWithMargins(document.querySelector(".list-page-header")) + // page title header
          navbarHeight() + // The height of the navbar if we're in mobile mode
          3; // Security margin
        const tableBody = document.querySelector(".ant-table-body");
        tableBody.style.maxHeight = `calc(100vh - ${heightOffset}px)`;
        tableBody.style.minHeight = `calc(100vh - ${heightOffset}px)`;
      } catch {
        //Do nothing
      }
    }
  };
  useEffect(resize);
  useEffect(() => setTimeout(resize, 3));

  return (
    <div
      className={`containerV ${fullPage ? "full-width-content" : ""}`}
      style={{background: "#fafafa"}}>
      {header && (
        <LayoutElement.PageHeading
          className="list-page-header"
          title={title}
          customButtons={
            <>
              {!enableMultipleSelection && customButtons}

              {sorting?.sortedInfo?.column && ( // There is a sorter activated
                <Button type="link" onClick={clearDisplayConfig}>
                  {t("common:listPage.resetFilters")}
                </Button>
              )}

              {(deletable || MultipleActionsButtons || groupEditable) &&
                (enableMultipleSelection ? (
                  <>
                    <span style={{marginRight: 8}}>{selectedRowKeys.length} sélectionnés.</span>

                    {MultipleActionsButtons && (
                      <MultipleActionsButtons
                        selectedRowKeys={selectedRowKeys}
                        setSelectedRowKeys={setSelectedRowKeys}
                      />
                    )}

                    {groupEditable && <GroupEditionButton selectedRowKeys={selectedRowKeys} />}

                    {deletable && (
                      <DeleteButton
                        onConfirm={onBatchDelete}
                        disabled={selectedRowKeys.length === 0}
                      />
                    )}

                    <Button
                      type="link"
                      danger
                      onClick={() => {
                        setEnableMultipleSelection(false);
                        setSelectedRowKeys([]);
                      }}>
                      Annuler
                    </Button>
                  </>
                ) : (
                  <Tooltip title={t("common:listPage.groupedSelection.tooltip")}>
                    <Button
                      type="link"
                      style={{marginRight: 12}}
                      icon={<SelectOutlined />}
                      onClick={() => setEnableMultipleSelection(true)}
                    />
                  </Tooltip>
                ))}
            </>
          }
          buttonTitle={buttonTitle}
          onButtonClick={() => navigateFn("new")}
        />
      )}
      {subtitle && <div className="subtitle with-margins">{subtitle}</div>}
      <div>
        <div style={{position: "fixed", right: 10, zIndex: 3}}>
          <Tooltip title={t("common:listPage.displaySettings.tooltip")} placement="topRight">
            <Button
              icon={<SettingOutlined />}
              style={{transform: "translateY(8px)"}}
              className="shadow"
              onClick={() => setSettingsDrawerVisible(true)}
            />
          </Tooltip>
        </div>

        <Table
          scroll={{
            y: "100vh",
            x: (columns.length - 1) * 160 + 70,
          }}
          columns={columns}
          rowClassName={rowClassName || ((record) => record.updatedAt && "ant-table-row-updated")}
          pagination={{
            position: ["bottomCenter"],
            current: pagination.current,
            pageSize: pagination.pageSize,
            pageSizeOptions: paginationPageSizes,
            total: dataSource.length,
            showSizeChanger: true,
            showTotal: (total, range) =>
              `${range[0]}-${range[1]} sur ${currentDataSource?.length || total} ${(!isMobileView
                ? entitiesName || title
                : ""
              ).toLowerCase()} `,
            onChange: setPagination,
          }}
          onRow={
            clickable !== false &&
            navigateFn &&
            ((record) => ({onDoubleClick: () => navigateFn(record.slug || record._id)}))
          }
          expandable={expandable}
          size={displaySize}
          rowKey="_id"
          dataSource={dataSource}
          rowSelection={rowSelection}
          onChange={handleDisplayConfigChange}
        />
      </div>

      <SettingsDrawer>
        <h3 style={{marginBottom: 20}}>{t("common:settings")}</h3>
        {settingsDrawerContent}
        <CardElement title={t("common:listPage.settings.displayDensity.cardTitle")}>
          <FormElement>
            <InputElement.Select
              label={t("common:listPage.settings.displayDensity.label")}
              onChange={setDisplaySize}
              defaultValue={displaySize}
              options={[
                {
                  value: "small",
                  label: t("common:listPage.settings.displayDensity.options.small"),
                },
                {
                  value: "middle",
                  label: t("common:listPage.settings.displayDensity.options.comfort"),
                },
              ]}
            />
          </FormElement>
        </CardElement>
      </SettingsDrawer>
    </div>
  );
}
