import {Alert, Card} from "antd";
import React, {useEffect} from "react";
import {currentUserSelectors} from "../../features/currentUser";
import {useDispatch, useSelector} from "react-redux";
import {currentProjectActions, currentProjectSelectors} from "../../features/currentProject";
import {FormElement} from "./FormElement";
import {Link, navigate} from "@reach/router";
import {connectionPageLogo} from "../../app/configuration";
import {setAppTheme} from "./LayoutStructure";
import {useTranslation} from "react-i18next";
import {t} from "i18next";

export const checkPasswordsAreSame = (form) => (_, value) => {
  if (!value || form.getFieldValue("password") === value) {
    return Promise.resolve();
  }
  return Promise.reject(new Error(t("common:formValidation.password.dontMatch")));
};

export const validatePassword = (form) => (_, value) => {
  if (!value || value.length === 0) return Promise.resolve();

  const containsDigit = /\d/.test(value);
  const containsLowerCaseLetter = /[a-z]/.test(value);
  const containsUpperCaseLetter = /[A-Z]/.test(value);

  const missing = [];
  containsDigit || missing.push(t("common:formValidation.password.aNumber"));
  containsLowerCaseLetter || missing.push(t("common:formValidation.password.aLowerCaseLetter"));
  containsUpperCaseLetter || missing.push(t("common:formValidation.password.anUpperCaseLetter"));

  if (missing.length === 0) return Promise.resolve();

  return Promise.reject(
    t("common:formValidation.password.mustContainList", {
      field: form.getFieldInstance("password").input.labels[0].innerText,
      missing,
    })
  );
};

export const ConnectionPage = ({envId, subtitle, children, buttons, className, ...otherProps}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const connectionError = useSelector(currentUserSelectors.selectConnectionError);
  const connectionNotice = useSelector(currentUserSelectors.selectConnectionNotice);

  const currentProject = useSelector(currentProjectSelectors.selectProject);

  if (currentProject.theme) setAppTheme(currentProject.theme);

  useEffect(() => {
    envId
      ? dispatch(currentProjectActions.load(envId)).catch(() => navigate("/login"))
      : dispatch(currentProjectActions.cleanProject());
  }, []);

  // On unmount (ie. connection), fill the body element with the default background color, for a smooth transition, then remove the background color
  useEffect(() => () => {
    document.body.style.setProperty(
      "background",
      "linear-gradient(135deg, var(--noe-accent-1-90), 40%, var(--noe-accent-2))"
    );
    setTimeout(() => document.body.style.removeProperty("background"), 700);
  });

  // Load the page if there is no envId, or if the project is actually loaded
  return !envId || currentProject._id ? (
    <div
      style={{
        minHeight: "100vh",
        justifyContent: "center",
        background: "linear-gradient(135deg, var(--noe-accent-1-90), 40%, var(--noe-accent-2))",
        transition: "1s background",
      }}
      className={`containerH ${className} fade-in`}>
      <div
        style={{
          width: "min(95vw, 500px)",
          flexGrow: 0,
          padding: "40px 0",
          justifyContent: "center",
          alignItems: "stretch",
        }}
        className="containerV">
        <img
          src={connectionPageLogo}
          height={45}
          style={{marginBottom: 40, marginRight: "auto", marginLeft: "auto"}}
        />
        <Card style={{padding: 5}}>
          <div style={{textAlign: "center", paddingBottom: "15pt"}}>
            {currentProject?.name && (
              <h1 className="fade-in">{currentProject.name.toUpperCase()}</h1>
            )}
            {subtitle}
          </div>
          {connectionError && (
            <Alert style={{marginBottom: "10pt"}} type="error" message={connectionError} />
          )}
          {connectionNotice && (
            <Alert style={{marginBottom: "10pt"}} type="success" message={connectionNotice} />
          )}
          <FormElement
            className="containerV buttons-container"
            style={{paddingTop: "10pt", paddingBottom: "10pt", alignItems: "stretch"}}
            {...otherProps}>
            {children}
          </FormElement>
          <div
            className="containerV buttons-container"
            style={{paddingTop: "5pt", alignItems: "stretch"}}>
            {buttons}
          </div>
        </Card>

        <Link
          to="/public-projects"
          style={{
            position: "relative",
            color: "white",
            alignSelf: "center",
            marginTop: 26,
            paddingBottom: -32,
            opacity: 0.9,
          }}>
          {t("common:connectionPage.seeOtherEventsAvailable")}
        </Link>
      </div>
    </div>
  ) : null;
};
