import {Form} from "antd";
import React from "react";
import isHotkey from "is-hotkey";
import {useTranslation} from "react-i18next";
const isCtrlSPressed = isHotkey("mod+S");
const isCtrlEnterPressed = isHotkey("mod+Enter");

export const FormElement = ({validateAction, children, ...otherProps}) => {
  const {t} = useTranslation();
  return (
    <Form
      layout={"vertical"}
      onKeyDown={
        (e) =>
          validateAction && // There is a validation action
          (isCtrlSPressed(e) || // Event was a press on Ctrl + S
            isCtrlEnterPressed(e)) && // Event was a press on Ctrl + Enter
          validateAction(isCtrlSPressed(e)) // ... Then, validate and give the info and if Ctrl+S, then pass a true value
      }
      {...otherProps}
      validateMessages={{
        required: t("common:formValidation.required"),
        types: {
          email: t("common:formValidation.types.email"),
          url: t("common:formValidation.types.url"),
        },
        string: {
          min: t("common:formValidation.string.min"),
        },
      }}
      requiredMark={false}>
      {children}
    </Form>
  );
};
