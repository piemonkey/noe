import React from "react";
import {Form} from "@formio/react";
import i18n from "i18next";

export const RegistrationForm = ({currentProject, userFormResponses, dataRef, formOptions}) => (
  <Form
    form={{components: currentProject.formComponents}}
    onChange={(submission) => {
      if (submission.changed && dataRef) {
        dataRef.current = submission;
      }
    }}
    submission={{data: userFormResponses}}
    options={{...formOptions, i18n}}
  />
);
