import React from "react";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "../../features/currentProject";
import {Progress, Tooltip} from "antd";
import {listRenderer} from "../../helpers/listUtilities";
import {useTranslation} from "react-i18next";

export function VolunteeringGauge({registration, style}) {
  const {t} = useTranslation();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  if (currentProject && registration) {
    const {minMaxVolunteering} = currentProject;
    const {voluntaryCounter, numberOfDaysOfPresence} = registration;
    if (minMaxVolunteering && (voluntaryCounter || voluntaryCounter === 0)) {
      const [minVolunteering, maxVolunteering] = minMaxVolunteering;
      const max = minVolunteering + maxVolunteering;

      let color, contextualMessage;
      if (voluntaryCounter < minVolunteering) {
        color = "red";
        contextualMessage = t("registrations:volunteeringGauge.hint.moreNeeded");
      } else {
        if (voluntaryCounter > maxVolunteering) {
          color = "dodgerblue";
          contextualMessage = t("registrations:volunteeringGauge.hint.lessNeeded");
        } else {
          color = "#52c41a"; // Same color as success buttons
          contextualMessage = t("registrations:volunteeringGauge.hint.itsPerfect");
        }
      }

      const tooltipMessage = (
        <>
          <p>
            <strong>{contextualMessage}</strong>
          </p>
          <p>
            {t("registrations:volunteeringGauge.fullRegistrationRecap", {
              numberOfDaysOfPresence: numberOfDaysOfPresence,
              total: listRenderer.durationFormat(voluntaryCounter * numberOfDaysOfPresence),
              totalPerDay: listRenderer.durationFormat(voluntaryCounter),
            })}
          </p>
          {t("registrations:volunteeringGauge.projectRecap", {
            min: listRenderer.durationFormat(minVolunteering),
            max: listRenderer.durationFormat(maxVolunteering),
          })}
        </>
      );

      let gaugePercentage = (100 * voluntaryCounter) / max;
      if (gaugePercentage < 1) gaugePercentage = 3; // Always see a little bit of red, even if the jauge is empty

      return (
        <Tooltip title={tooltipMessage} placement="right">
          <Progress
            style={style}
            percent={gaugePercentage}
            strokeWidth={15}
            trailColor="#d9d9d9ee"
            showInfo={false}
            strokeColor={color}
          />
        </Tooltip>
      );
    }
    // Fallback: don't display anything if there is no loaded project
    return null;
  }
}
