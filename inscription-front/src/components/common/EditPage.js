import React, {useEffect, useRef, useState} from "react";
import {message, Form, Button, Tooltip, Popconfirm, Collapse, Timeline, Alert, Modal} from "antd";
import {Link, useNavigate} from "@reach/router";
import {FormElement} from "./FormElement";
import {CardElement, LayoutElement} from "./LayoutElement";
import {fieldToData as fieldToDataBase} from "../../helpers/tableUtilities";
import {useDispatch, useSelector} from "react-redux";
import {
  CopyOutlined,
  DeleteOutlined,
  FilePdfOutlined,
  QuestionCircleOutlined,
} from "@ant-design/icons";
import {listRenderer} from "../../helpers/listUtilities";
import {OFFLINE_MODE} from "../../helpers/offlineModeUtilities";
import {personName, pick} from "../../helpers/utilities";
import {Trans, useTranslation} from "react-i18next";
import {currentProjectSelectors} from "../../features/currentProject";

export const useNewElementModal = (ElementEdit) => {
  const [showNewEntityModal, setShowNewEntityModal] = useState(false);

  // Return the CreateButton, and the Modal Component
  return [
    setShowNewEntityModal,
    () =>
      showNewEntityModal ? (
        <ElementEdit asModal modalVisible={true} setModalVisible={setShowNewEntityModal} id="new" />
      ) : null,
  ];
};

export const DeleteButton = ({onConfirm, disabled}) => {
  const {t} = useTranslation();
  return disabled ? (
    <Button style={{flexGrow: 0}} danger disabled type="link" icon={<DeleteOutlined />} />
  ) : (
    <Popconfirm
      title={t("common:areYouSure")}
      placement="topRight"
      onConfirm={onConfirm}
      okText={t("common:yesDelete")}
      okButtonProps={{danger: true}}
      icon={<QuestionCircleOutlined style={{color: "red"}} />}
      cancelText={t("common:no")}>
      <Button style={{flexGrow: 0}} danger type="link" icon={<DeleteOutlined />} />
    </Popconfirm>
  );
};

export const ModificationsHistoryTimeline = ({record, elementsActions, root = "../.."}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const {history, ...recordWithoutHistory} = record || {};

  useEffect(() => {
    dispatch(elementsActions.loadEditingHistory(record?._id));
  }, [JSON.stringify(recordWithoutHistory)]);

  if (!history) return null;
  if (!(history.length > 0)) return t("common:editPage.noHistory");

  return (
    <>
      <Timeline style={{marginTop: 15}}>
        {history.map(({date, registration, diff}, index) => (
          <Timeline.Item key={index}>
            <strong>{listRenderer.longDateTimeFormat(date)}</strong>
            {registration && (
              <span>
                {" – "}
                <Link to={`${root}/participants/${registration._id}`}>
                  {personName(registration.user)}
                </Link>
              </span>
            )}
            <CardElement
              size="small"
              borderless
              bodyStyle={{paddingTop: 5, marginBottom: -20}}
              style={{overflow: "hidden", marginTop: 10}}>
              {Object.entries(diff).map(([key, values], index) => (
                <div key={index}>
                  <span style={{marginLeft: 12}}>{key}</span>
                  {Array.isArray(values) ? (
                    <div
                      className="container-grid two-per-row"
                      style={{gap: 0, maxHeight: 300, overflow: "auto"}}>
                      {values[1] !== undefined ? (
                        // Modification
                        <>
                          <pre style={{background: "#fdf5f5", padding: "5px 12px"}}>
                            {JSON.stringify(values[0], null, 2)}
                          </pre>
                          <pre style={{background: "#f3fcf3", padding: "5px 12px"}}>
                            {JSON.stringify(values[1], null, 2)}
                          </pre>
                        </>
                      ) : (
                        // New stuff
                        <pre style={{background: "#f3fcf3", padding: "5px 12px"}}>
                          {JSON.stringify(values[0], null, 2)}
                        </pre>
                      )}
                    </div>
                  ) : (
                    // Deep object
                    <pre style={{background: "#f7f7f7", padding: "5px 12px", maxHeight: 300}}>
                      {JSON.stringify(values, null, 2)}
                    </pre>
                  )}
                </div>
              ))}
            </CardElement>
          </Timeline.Item>
        ))}
      </Timeline>
      <Collapse style={{marginBottom: 26}}>
        <Collapse.Panel header={t("common:rawData")} key={1}>
          <pre>{JSON.stringify(history, null, 2)}</pre>
        </Collapse.Panel>
      </Collapse>
    </>
  );
};

export const CloneButton = ({onClick}) => {
  const {t} = useTranslation();
  return (
    <Tooltip title={t("common:editPage.cloneFromCurrentInfo")}>
      <Button type="link" style={{flexGrow: 0}} icon={<CopyOutlined />} onClick={onClick} />
    </Tooltip>
  );
};

export const GetPdfPlanningButton = ({id, elementsActions, tooltip, noText, type}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const downloadButton = useRef();
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  const getPdfPlanning = () => {
    dispatch(elementsActions.getPdfPlanning(id))
      .then((pdfFile) => {
        let pdfPlanningUrl = URL.createObjectURL(pdfFile);
        const fileName = `${t("sessions:labelSubscribed")} - ${currentProject.name}.pdf`;

        downloadButton.current.setAttribute("href", pdfPlanningUrl);
        downloadButton.current.setAttribute("download", fileName);
        downloadButton.current.click();
      })
      .catch(() => message.error(t("common:pdfButton.pdfCouldNotBeGenerated")));
  };

  return (
    <>
      <a ref={downloadButton} style={{display: "none"}} />
      <Tooltip placement="bottomLeft" title={tooltip || t("common:pdfButton.tooltip")}>
        <Button
          style={{flexGrow: 0}}
          type={type || "link"}
          onClick={getPdfPlanning}
          icon={<FilePdfOutlined />}>
          {!noText && t("common:pdfButton.label")}
        </Button>
      </Tooltip>
    </>
  );
};

export function EditPage({
  // The titles to display depending on the context (creation / edition / cloning / group editing)
  createTitle,
  editTitle,
  cloningTitle,
  groupEditingTitle,

  record, // The object want to modify
  elementsActions, // The redux actions for this element
  onValidation, // function activated when the user validates the form  onFieldsChange, // what to do when the form has been touched
  fieldToData = fieldToDataBase, // custom fieldToData function. Defaults to the original fieldToData()
  fieldsThatNeedReduxUpdate, // The fields that need Redux to be updated when they are changed (so that they trigger a page refresh)

  deletable = false, // Display the delete button or not
  backButton = true, // [boolean] display a back button, or not | default: true
  customButtons, // [HTML elements] custom buttons if you need to display some
  createAndStayButton = true, // Display the "Créer et rester" button

  clonable, // Enables the cloning feature (and displays the cloning button)
  clonedElement, // Tells if we are currently cloning the element

  formOverride,
  forceModifButtonActivation = false, // [boolean] force activation of the modif button | default: false
  outerChildren, // children that must be outside the form element
  navigateAfterValidation = true, // [false, or any string] where to go after the validation. false means no navigate. | default: -1 (means go back)
  groupEditing, // should be in the form {entities: "The entities IDs to modify", schema: "the schema definition of the object", fieldsToUpdate: "the fields keys we want to update"}

  noSaveButton = false,

  asModal, // Tell if the EditPage is displayed in a modal or not
  modalVisible, // If the EditPage modal is visible or not
  setModalVisible, // The function to set if the modal is visible or not

  children,

  ...formElementProps // props for the Form Element if needed
}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  let [form] = Form.useForm();
  if (formOverride) form = formOverride;
  const navigate = useNavigate();
  const [modifButtonActivation, setModifButtonActivation] = useState(!!record.dirty);
  const [formData, setFormData] = useState();

  // Create page is either a creation page or a cloning page
  const isCreatePage = !groupEditing && record._id === "new";

  const title = isCreatePage
    ? clonedElement
      ? cloningTitle
      : createTitle
    : groupEditing
    ? groupEditingTitle
    : editTitle;
  const buttonTitle = isCreatePage
    ? clonedElement
      ? t("common:editPage.title.clone")
      : t("common:editPage.title.create")
    : groupEditing
    ? t("common:editPage.title.saveXElements", {count: groupEditing.elements.length})
    : t("common:editPage.title.save");

  const shouldDisplayHistory = !isCreatePage && elementsActions?.loadEditingHistory;

  const validateAndNavigate = (navigateToElementAfterValidation = false) => {
    const fieldsKeysToUpdate = groupEditing
      ? groupEditing.fieldsToUpdate.map((field) => field.key)
      : undefined;
    form
      .validateFields(fieldsKeysToUpdate)
      .then(async () => {
        let elementId;
        if (onValidation) {
          elementId = await onValidation(formData, fieldsKeysToUpdate);
        } else {
          const payload = {...record, ...formData};
          if (groupEditing) {
            const onlyFieldsToUpdate = pick(payload, fieldsKeysToUpdate);
            for (const entityId of groupEditing.elements) {
              await dispatch(elementsActions.persist({...onlyFieldsToUpdate, _id: entityId}));
            }
          } else {
            elementId = await dispatch(elementsActions.persist(payload));
          }
        }

        if (noSaveButton) return;

        if (asModal) setModalVisible(false);
        // If in a modal, just close the modal
        else if (navigateToElementAfterValidation) navigate(`./${elementId}`, {replace: true});
        else if (navigateAfterValidation && !clonedElement)
          navigate(typeof navigateAfterValidation === "string" ? navigateAfterValidation : -1);
        else setModifButtonActivation(false);
      })
      .catch(() => message.error(t("common:editPage.formInvalid")));
  };

  const onChange = (changedFields, allFields) => {
    if (changedFields.length > 0) {
      setModifButtonActivation(true);

      // If there is a custom onFieldsChange function, use it
      const formData = fieldToData(allFields);
      setFormData(formData);

      // If some necessary fields are updated, then also update Redux state
      if (
        fieldsThatNeedReduxUpdate?.length > 0 &&
        changedFields.find((field) => fieldsThatNeedReduxUpdate.includes(field.name[0]))
      ) {
        dispatch(elementsActions.changeEditing({...record, ...formData, dirty: true}));
      }
    }
  };

  const onDelete = () => {
    dispatch(elementsActions.remove(record._id));
    navigateAfterValidation && navigate(-1);
  };

  const editPageContent = (
    <div className="page-container">
      <LayoutElement.PageHeading
        backButton={backButton && !asModal}
        className="edit-page-header"
        title={title}
        buttonTitle={!noSaveButton && buttonTitle}
        onButtonClick={!noSaveButton && (() => validateAndNavigate())}
        forceModifButtonActivation={
          modifButtonActivation || forceModifButtonActivation || !!clonedElement
        }
        customButtons={
          !isCreatePage ? (
            <>
              {customButtons}
              {clonable && (
                <CloneButton
                  onClick={() =>
                    navigate("clone", {
                      replace: true,
                      state: {clonedElement: {...record, ...formData, _id: "new"}},
                    })
                  }
                />
              )}
              {deletable && <DeleteButton onConfirm={onDelete} />}
            </>
          ) : (
            createAndStayButton &&
            !clonedElement &&
            !asModal && (
              <Tooltip title={t("common:editPage.createAndStayButton.tooltip")}>
                <Button
                  onClick={() => {
                    validateAndNavigate(true);
                  }}
                  disabled={!(modifButtonActivation || forceModifButtonActivation)}>
                  {t("common:editPage.createAndStayButton.label")}
                </Button>
              </Tooltip>
            )
          )
        }
      />

      {noSaveButton && (
        <div
          style={{
            maxHeight: record.dirty ? 300 : 0,
            position: "sticky",
            top: 20,
            zIndex: 2,
            overflow: "hidden",
            transition: "all 1s",
            transitionDelay: "0.4s",
          }}
          className="fade-in">
          <Alert
            size="small"
            type={record.dirty ? "info" : "success"}
            action={
              <Button size="small" type="primary" onClick={() => validateAndNavigate(true)}>
                Enregistrer
              </Button>
            }
            style={{marginBottom: 26}}
            message={
              record.dirty ? (
                <Trans ns="common" i18nKey="editPage.keyboardShortcutsHint" />
              ) : (
                t("common:allGood")
              )
            }
          />
        </div>
      )}

      {groupEditing && (
        <Alert
          style={{marginBottom: 26}}
          message={t("common:editPage.youAreEditingDocumentsInTheSameTime.message", {
            count: groupEditing.elements.length,
          })}
          description={
            <Trans
              ns="common"
              i18nKey="editPage.youAreEditingDocumentsInTheSameTime.description"
              components={{
                listOfFields: listRenderer.listOfClickableElements(
                  groupEditing.fieldsToUpdate.map((field) => field.label),
                  (el) => <strong style={{color: "darkred"}}>{el}</strong>
                ),
              }}
            />
          }
        />
      )}

      {clonedElement && (
        <Alert
          style={{marginBottom: 26}}
          message={t("common:editPage.cloningInfoAlert.message")}
          description={t("common:editPage.cloningInfoAlert.description")}
        />
      )}

      {record._id && (
        <div className="fade-in">
          <FormElement
            form={form}
            onFieldsChange={onChange}
            validateAction={validateAndNavigate}
            {...formElementProps}>
            {children}
          </FormElement>
          {outerChildren}
          {shouldDisplayHistory && !OFFLINE_MODE && (
            <Collapse className="fade-in">
              <Collapse.Panel header={t("common:editPage.modificationsHistory")}>
                <ModificationsHistoryTimeline record={record} elementsActions={elementsActions} />
              </Collapse.Panel>
            </Collapse>
          )}
        </div>
      )}
    </div>
  );

  return asModal ? (
    <Modal
      width={"min(90vw, 1000pt)"}
      visible={modalVisible}
      onCancel={() => setModalVisible(false)}
      footer={null}>
      <div className="modal-page-content">{editPageContent}</div>
    </Modal>
  ) : (
    editPageContent
  );
}
