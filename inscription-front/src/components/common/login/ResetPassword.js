import React, {useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import {currentUserSelectors, currentUserActions} from "../../../features/currentUser.js";
import {Button, Form} from "antd";
import {InputElement} from "../InputElement";
import {navigate, useLocation} from "@reach/router";
import {ConnectionPage, validatePassword, checkPasswordsAreSame} from "../ConnectionPage";
import {useTranslation} from "react-i18next";

export function ResetPassword() {
  const {t} = useTranslation();
  const [form] = Form.useForm();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const connectionError = useSelector(currentUserSelectors.selectConnectionError);
  const dispatch = useDispatch();
  let query = new URLSearchParams(useLocation().search);
  const token = query.get("token");

  useEffect(() => {
    dispatch(currentUserActions.checkPasswordResetToken(token));
  }, [token]);

  const onChange = (changedFields, allFields) => {
    dispatch(currentUserActions.changePassword(allFields[0].value));
  };

  const resetPassword = () => {
    form.validateFields().then(() => {
      dispatch(currentUserActions.changeConnectionError(undefined));
      dispatch(currentUserActions.changeConnectionNotice(undefined));
      dispatch(currentUserActions.resetPassword(token));
    });
  };

  const goToForgotPasswordPage = () => {
    // Flush alerts
    dispatch(currentUserActions.changeConnectionError(undefined));
    dispatch(currentUserActions.changeConnectionNotice(undefined));
    navigate("../forgotpassword");
  };

  return (
    <ConnectionPage
      form={form}
      subtitle={t("users:schema.password.labelNew")}
      fields={[{name: "password", value: currentUser.password}]}
      onFieldsChange={onChange}
      validateAction={resetPassword}
      buttons={
        connectionError ? (
          <Button onClick={goToForgotPasswordPage}>
            {t("common:connectionPage.sendPasswordRecoveryEmailAgain")}
          </Button>
        ) : (
          <Button type="primary" onClick={resetPassword}>
            {t("common:connectionPage.validatePasswordChange")}
          </Button>
        )
      }>
      <InputElement.Password
        label={t("users:schema.password.labelNew")}
        name="password"
        size="large"
        placeholder={t("users:schema.password.placeholder")}
        autoComplete="new-password"
        rules={[{required: true}, {validator: validatePassword(form)}, {min: 8}]}
      />

      <InputElement.Password
        label={t("users:schema.password.labelConfirmNew")}
        name="confirmPassword"
        size="large"
        placeholder={t("users:schema.password.placeholder")}
        autoComplete="new-password"
        dependencies={["password"]}
        rules={[{required: true}, {validator: checkPasswordsAreSame(form)}]}
      />
    </ConnectionPage>
  );
}
