import React, {useEffect, useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import {currentUserSelectors, currentUserActions} from "../../../features/currentUser.js";
import {Button, Form} from "antd";
import {InputElement} from "../InputElement";
import {navigate} from "@reach/router";
import {ConnectionPage, validatePassword} from "../ConnectionPage";
import {fieldToData, ROLES_MAPPING} from "../../../helpers/tableUtilities";
import {viewSelectors} from "../../../features/view";
import {fetchWithMessages} from "../../../helpers/reduxUtilities";
import {currentProjectSelectors} from "../../../features/currentProject";
import {Trans, useTranslation} from "react-i18next";

export function SignUp({envId}) {
  const {t} = useTranslation();
  const [form] = Form.useForm();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const searchParamsInfo = useSelector(viewSelectors.selectSearchParams);
  const [invitedRegistration, setInvitedRegistration] = useState();
  const dispatch = useDispatch();

  useEffect(() => {
    if (searchParamsInfo.invitationToken && currentProject._id) {
      fetchWithMessages(
        `projects/${currentProject._id}/registrations/byInvitationToken/${searchParamsInfo.invitationToken}`,
        {noAuthNeeded: true, method: "GET"},
        {},
        false
      )
        .then((invitedRegistration) => {
          setInvitedRegistration(invitedRegistration);
          form.setFieldsValue(invitedRegistration.user);
        })
        .catch(() =>
          dispatch(
            currentUserActions.changeConnectionError(
              t("common:connectionPage.invitationTokenInvalid")
            )
          )
        );
    }
  }, [currentProject._id]);

  // Don't render if there is an invitation token and we haven't fetched the invited registration yet
  if (searchParamsInfo.invitationToken && !invitedRegistration) return null;

  const onChange = (changedFields, allFields) =>
    dispatch(currentUserActions.changeUser(fieldToData(allFields)));

  const signUp = () =>
    form
      .validateFields()
      .then(() => {
        dispatch(currentUserActions.changeConnectionError(undefined));
        dispatch(currentUserActions.signUp());
      })
      .catch(() => {
        /*do nothing*/
      });

  const goToLogInPage = () => {
    // Flush alerts
    dispatch(currentUserActions.changeConnectionError(undefined));
    navigate("./login");
  };

  const roleLabel =
    invitedRegistration?.role &&
    ROLES_MAPPING.find((r) => r.value === invitedRegistration.role).label.toLowerCase();
  const greetingName =
    invitedRegistration?.user.firstName || invitedRegistration?.steward?.firstName;

  return (
    <ConnectionPage
      form={form}
      envId={envId}
      subtitle={
        invitedRegistration ? (
          <>
            {greetingName && <p>{t("welcome:hiFirstName", {firstName: greetingName})}</p>}
            <p>
              {roleLabel ? (
                invitedRegistration?.steward ? (
                  <Trans
                    ns="common"
                    i18nKey="connectionPage.invitations.asStewardAndRole"
                    values={{role: roleLabel}}
                  />
                ) : (
                  <Trans
                    ns="common"
                    i18nKey="connectionPage.invitations.asRole"
                    values={{role: roleLabel}}
                  />
                )
              ) : (
                <Trans ns="common" i18nKey="connectionPage.invitations.asSteward" />
              )}
              <Trans
                ns="common"
                i18nKey="connectionPage.invitations.onTheEvent"
                values={{projectName: currentProject.name}}
              />
              .
            </p>
            {t("common:connectionPage.invitations.fillYourInformationToCreateAccount")}
          </>
        ) : (
          t("common:connectionPage.newAccount")
        )
      }
      initialValues={{
        firstName: currentUser.firstName || searchParamsInfo.firstName,
        lastName: currentUser.lastName || searchParamsInfo.lastName,
        email: currentUser.email || searchParamsInfo.email,
        password: currentUser.password,
      }}
      onFieldsChange={onChange}
      validateAction={signUp}
      buttons={
        <>
          <Button type="primary" onClick={signUp}>
            {t("common:createAccount")}
          </Button>
          <Button onClick={goToLogInPage}>{t("common:iAlreadyHaveAnAccount")}</Button>
        </>
      }>
      <InputElement.Text
        name="firstName"
        i18nNs="users"
        size="large"
        autoComplete="given-name"
        rules={[{required: true}]}
      />
      <InputElement.Text
        name="lastName"
        i18nNs="users"
        size="large"
        autoComplete="family-name"
        rules={[{required: true}]}
      />
      <InputElement.Text
        name="email"
        i18nNs="users"
        size="large"
        autoComplete="email"
        disabled={invitedRegistration}
        rules={[{required: true}, {type: "email"}]}
      />
      <InputElement.Password
        name="password"
        i18nNs="users"
        size="large"
        autoComplete="new-password"
        rules={[{required: true}, {validator: validatePassword(form)}, {min: 8}]}
      />
    </ConnectionPage>
  );
}
