import Editor from "@react-page/editor";
import "@react-page/editor/lib/index.css";
import editorPlugins from "./plugins";

export const WelcomePageEditor = ({
  value,
  style = {height: "100%"},
  readOnly = false,
  onChange,
}) => (
  <Editor
    lang="fr"
    onChange={onChange}
    cellPlugins={editorPlugins}
    value={value}
    style={style}
    readOnly={readOnly}
  />
);
