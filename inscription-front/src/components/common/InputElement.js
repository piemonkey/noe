import {
  DatePicker,
  TimePicker,
  Progress,
  Form,
  Input,
  InputNumber,
  Select,
  Switch,
  Slider,
  Button,
  Tooltip,
} from "antd";
import React from "react";
import moment from "moment";
import {currentProjectSelectors} from "../../features/currentProject";
import {useSelector} from "react-redux";
import {TextEditor} from "./TextEditor";
import {navigate} from "@reach/router";
import {EyeInvisibleOutlined, EyeOutlined} from "@ant-design/icons";
import {listSorter, normalize, removeDuplicates} from "../../helpers/listUtilities";
import {t} from "i18next";
import {FormItemProps} from "antd";
import {useTranslation} from "react-i18next";

const formTag = ({i18nNs, name, label, rules, tooltip, formItemProps, ...otherProps}, inputTag) => {
  if (i18nNs) {
    const keyRoot = `${i18nNs}:schema.${name}`;
    label = t(`${keyRoot}.label`);
    otherProps.placeholder = t(`${keyRoot}.placeholder`);
  }
  return (
    <Form.Item label={label} name={name} tooltip={tooltip} rules={rules} {...formItemProps}>
      {inputTag(otherProps)}
    </Form.Item>
  );
};

const disabledDateBeforeNow = (current) => {
  const now = moment();
  return current && current.isBefore(now, "day");
};

const disabledDateOutOfProject = (current, project) => {
  const start = moment(project.start),
    end = moment(project.end);
  return current && (current.isBefore(start, "day") || current.isAfter(end, "day"));
};

const isADisabledDate = (project, disableDatesIfOutOfProject, disableDatesBeforeNow) => {
  return (current) => {
    return (
      (disableDatesIfOutOfProject ? disabledDateOutOfProject(current, project) : false) ||
      (disableDatesBeforeNow !== false ? disabledDateBeforeNow(current) : false)
    );
  };
};

const setGreyedOutDates =
  (project, disableDatesIfOutOfProject, disableDatesBeforeNow) => (current) => {
    const disabled = isADisabledDate(
      project,
      disableDatesIfOutOfProject,
      disableDatesBeforeNow
    )(current);
    return (
      <div className={`ant-picker-cell-inner ${disabled ? "disabled-date" : undefined}`}>
        {current.date()}
      </div>
    );
  };

function DateTag({
  isRange, //Display range picker or date picker
  blockDisabledDates, // Block dates out of the project
  disableDatesIfOutOfProject, // grey out either only days before today, or also the dates out of the project
  disableDatesBeforeNow,
  onChange,
  ...otherProps
}) {
  const project = useSelector(currentProjectSelectors.selectProject);

  const newProps = {
    onChange: (value) => {
      // Reset seconds and milliseconds to zero
      if (isRange) {
        value.forEach((date) => date.set({second: 0, millisecond: 0}));
      } else {
        value.set({second: 0, millisecond: 0});
      }
      onChange(value);
    },
    bordered: false,
    format: moment.localeData().longDateFormat("LLL"),
    showTime: {minuteStep: 5},
    dropdownClassName: "date-picker-panel",
  };

  if (blockDisabledDates) {
    newProps.disabledDate = isADisabledDate(
      project,
      disableDatesIfOutOfProject,
      disableDatesBeforeNow
    );
  } else {
    newProps.dateRender = setGreyedOutDates(
      project,
      disableDatesIfOutOfProject,
      disableDatesBeforeNow
    );
  }

  return isRange ? (
    <DatePicker.RangePicker {...newProps} {...otherProps} />
  ) : (
    <DatePicker {...newProps} {...otherProps} />
  );
}

export const InputElement = () => null;

type InputElementProps = {
  i18nNs: string,
  name: string,
  label: string,
  rules: string,
  tooltip: string,
  formItemProps: FormItemProps,
  ...otherProps,
};

/**
 * TEXT COMPONENTS
 */
InputElement.Text = (props: InputElementProps) =>
  formTag(props, (otherProps) => <Input bordered={false} {...otherProps} />);

InputElement.TextArea = (props) =>
  formTag(props, (otherProps) => (
    <Input.TextArea
      autoSize={{minRows: 2}}
      // allow to return to the lines by overriding the validation of the web page with "enter"
      onKeyDown={(event) => event.key === "Enter" && event.stopPropagation()}
      bordered={false}
      {...otherProps}
    />
  ));

InputElement.Editor = (props: InputElementProps) =>
  formTag(props, (otherProps) => <TextEditor {...otherProps} />);

InputElement.Password = (props: InputElementProps) =>
  formTag(props, (otherProps) => (
    <Input.Password
      bordered={false}
      iconRender={(visible) => (visible ? <EyeOutlined /> : <EyeInvisibleOutlined />)}
      {...otherProps}
    />
  ));

InputElement.Number = (props: InputElementProps) =>
  formTag(props, (otherProps) => <InputNumber bordered={false} keyboard {...otherProps} />);

const TagsSelect = ({elementsSelectors, ...props}: InputElementProps) => {
  const {t} = useTranslation();
  const elements = useSelector(elementsSelectors.selectList);
  const tagsOptions = removeDuplicates(
    elements?.reduce((acc, a) => (a[props.name] ? [...a[props.name], ...acc] : acc), [])
  )
    .sort(listSorter.text)
    .map((string) => ({
      key: string,
      value: string,
      label: string,
    }));

  return (
    <InputElement.Select
      placeholder={t("common:inputElement.addTagsPlaceholder")}
      mode="tags"
      options={tagsOptions}
      {...props}
    />
  );
};
InputElement.TagsSelect = TagsSelect;

/**
 * CHOICE COMPONENTS
 */
InputElement.Switch = (props: InputElementProps) =>
  formTag(
    {formItemProps: {valuePropName: "checked", ...props.formItemProps}, ...props},
    (otherProps) => <Switch {...otherProps} />
  );

InputElement.Select = (props: InputElementProps) =>
  formTag(props, (otherProps) => (
    <Select
      bordered={false}
      filterOption={(inputValue, option) =>
        normalize(option?.label).indexOf(normalize(inputValue)) !== -1
      }
      // prevent selection with Enter to trigger full page validation
      onKeyDown={(event) => event.key === "Enter" && event.stopPropagation()}
      {...otherProps}>
      {props.children}
    </Select>
  ));

/**
 * DATE COMPONENTS
 */

InputElement.Time = (props: InputElementProps) =>
  formTag(props, (otherProps) => (
    <TimePicker minuteStep={5} bordered={false} format="HH:mm" {...otherProps} />
  ));

InputElement.DateTime = (props: InputElementProps) =>
  formTag(props, (otherProps) => <DateTag {...otherProps} />);

InputElement.DateTimeRange = (props: InputElementProps) =>
  formTag(props, (otherProps) => <DateTag isRange {...otherProps} />);

/**
 * OTHER COMPONENTS
 */

InputElement.Slider = (props: InputElementProps) =>
  formTag(props, (otherProps) => <Slider {...otherProps} />);

InputElement.Filling = (props: InputElementProps) =>
  formTag(props, (otherProps) => (
    <div className="containerH buttons-container">
      <div style={{flexGrow: 100}}>
        <Progress
          percent={otherProps.percent}
          size="small"
          showInfo={false}
          status={otherProps.status}
          {...otherProps}
        />
      </div>
      <div>{otherProps.text}</div>
    </div>
  ));

/**
 * WRAPPER COMPONENTS
 */

InputElement.Custom = (props: InputElementProps) => formTag(props, (otherProps) => props.children);

const WithEntityLinks = ({endpoint, entity, children, createButtonText, setShowNewEntityModal}) => {
  const {t} = useTranslation();
  return (
    <div>
      <div
        className="containerH buttons-container"
        style={{alignItems: "start", flexWrap: "nowrap", marginBottom: 24}}>
        {children}
        {entity && (
          <Tooltip title={t("common:inputElement.accessToSelectedElement")} placement="topRight">
            <Button
              style={{marginTop: 35, flexGrow: 0, paddingRight: 0, marginRight: 0}}
              type="link"
              onClick={() => navigate(`../${endpoint}/${entity?._id || entity}`)}
              icon={<EyeOutlined />}
            />
          </Tooltip>
        )}
        {createButtonText && setShowNewEntityModal && (
          <Tooltip title={createButtonText} placement="topRight">
            <Button
              type="link"
              onClick={() => setShowNewEntityModal(true)}
              style={{
                marginTop: 35,
                flexGrow: 0,
                paddingLeft: 5,
                paddingRight: 0,
                marginRight: 0,
              }}>
              {createButtonText.split(" ")[0]}
            </Button>
          </Tooltip>
        )}
      </div>
    </div>
  );
};
InputElement.WithEntityLinks = WithEntityLinks;
