const CracoLessPlugin = require("craco-less");

module.exports = {
  webpack: {
    configure: (webpackConfig) => {
      // Remove the plugin that restricts scope to the /src folder, so we can access locales from the root.
      const scopePluginIndex = webpackConfig.resolve.plugins.findIndex(
        ({constructor}) => constructor && constructor.name === "ModuleScopePlugin"
      );
      webpackConfig.resolve.plugins.splice(scopePluginIndex, 1);

      // Locales loader
      webpackConfig.module.rules.push({
        test: /locales/,
        loader: "@turtle328/i18next-loader",
        query: {basenameAsNamespace: true},
      });
      return webpackConfig;
    },
  },
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              "@primary-color": "#3775e1",
              "@heading-color": "rgb(0, 10, 46)", // heading text color
              "@text-color": "rgba(46, 48, 56, 0.9)", // major text color
              "@text-color-secondary": "rgba(46, 48, 56, 0.6)", // secondary text color
              "@disabled-color": "rgba(46, 48, 56, 0.35)", // disable state color
              "@layout-header-background": "@heading-color", // sidebar background
              "@border-radius-base": "5px", // major border radius
              "@collapse-panel-border-radius": "10px", // other border radius
              "@card-radius": "10px", // other border radius
              "@btn-border-radius-base": "50px",
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
