import {Request, Response, NextFunction, Router} from "express";
const fetch = require("node-fetch");
import {Launcher} from "../computing/launcher";
import * as moment from "moment";

export class ComputingController {
  router: Router;

  constructor() {
    this.router = Router();
    this.setupRoutes();
  }

  configFactory = (dataNOE: any) => {
    return {
      // On each node, when createing all the possibilities, only keep the X ones with the highest heuristic
      branchCreation: 5,
      // Nb of nodes for which we don't find any child possible candidate.
      // Helps define if we are stuck with no solution at all or not.
      skippedLimit: 30,
      // Nb of times that we will create/navigate through nodes.
      // General limit of the algo.
      maxIteration: 10000,
      // When creating the agenda of possibilities, what is the smallest time period
      lengthMinuteStep: 30,
      type: "config", // TODO: maybe not used anymore
      // Ponderations of each heuristic in the scoring of the possibilities
      heuristic: {
        progress: 24,
        cleanPlacement: 6,
        totalLengthRatio: 0, // TODO: will be used someday
        moyStar: 3,
        moyFreeTime: 0, // TODO: maybe rename to moyTrainerFreeTime
        sameCategory: 0, // TODO: maybe rename to notSameCategory (depending on the inversion or not)
        scattering: 3,
      },
      project: dataNOE.project,
    };
  };

  genRoomSlots = (dataNOE: any) => {
    console.log("genRoom:", dataNOE);
    const roomAvailability: any[] = [];

    for (const room of dataNOE.places) {
      for (const timeSlot of room.availabilitySlots) {
        roomAvailability.push({
          startAvailability: moment.parseZone(timeSlot.start),
          endAvailability: moment.parseZone(timeSlot.end),
          keyRoom: room._id,
          volume: 1000,
          type: "roomAvailability",
          equipment: [],
        });
      }
    }
    return roomAvailability;
  };

  genTrainerSlots = (dataNOE: any) => {
    const trainerAvailability: any[] = [];
    for (const trainer of dataNOE.stewards) {
      for (const availabilitySlot of trainer.availabilitySlots) {
        trainerAvailability.push({
          startAvailability: availabilitySlot.start,
          endAvailability: availabilitySlot.end,
          affectation: dataNOE.activities
            .filter((t: any) => t.stewards.includes(trainer._id))
            .map((t: any) => t._id),
          keyTrainer: trainer._id,
          type: "trainerAvailability",
        });
      }
    }
    return trainerAvailability;
  };

  /*
    Prepare the data for the AI and handle the launcher
   */
  genSessions = async (dataNOE: any) => {
    const launcher = new Launcher();

    const roomAvailability = this.genRoomSlots(dataNOE);

    const trainerAvailability = this.genTrainerSlots(dataNOE);

    const activities: any[] = dataNOE.activities.map(
      (
        t: any
      ): {
        key: any;
        description: any;
        title: any;
        dependecy: any;
        keyCategory: any;
        roomAffectation: any;
        nonBlockingActivity: boolean;
        curiculums: any;
        equipment: any;
        slots: any;
        length: any;
        occurrence: any;
        existingSessions: any;
        minNumberOfStewards: any;
      } => ({
        key: t._id,
        description: t.name,
        nonBlockingActivity: t.nonBlockingActivity,
        title: t.name,
        dependecy: [],
        keyCategory: t.category,
        roomAffectation: t.places,
        minNumberOfStewards: t.minNumberOfStewards,
        curiculums: [],
        equipment: [],
        slots: [],
        length: t.slots.reduce(
          (accumulator: any, currentValue: any) => accumulator + currentValue.duration,
          0
        ), // length of all the timeSlots of an activity
        occurrence: t.numberOfSessions,
        existingSessions: dataNOE.sessions.filter((session: any) => session.activity === t._id),
      })
    );

    // The final object that we will send to the IA
    const data: {
      training: any;
      config: any;
      roomAvailability: any; // TODO maybe migrate in room
      trainerAvailability: any; // TODO maybe migrate in trainer
      trainer: any;
      room: any;
    } = {
      training: activities,
      config: this.configFactory(dataNOE),
      roomAvailability: roomAvailability,
      trainerAvailability: trainerAvailability,
      trainer: dataNOE.stewards.map((t: any) => ({
        key: t._id,
        type: "trainer",
        name: `${t.firstName} ${t.lastName}`,
      })),
      room: dataNOE.places.map((p: any) => ({
        key: p._id,
        name: p.name,
        volume: 1000,
        type: "room",
      })),
    };

    // TODO: we could just refactor more cleanly all the stuff above
    //  and the following line would be the only thing we need to call the IA
    // YAYYAYYAYAYYYYYYY Launch the IAAAAA
    return launcher.launchWithData(data, false);
  };

  /**
   *   IA Entry point. Express calls it to start the computing.
   */
  testCall = async (req: Request, res: Response) => {
    const headers = {authorization: req.headers.authorization};
    headers["Content-Type"] = "application/json";
    try {
      const projectId: string = req.params.idProject;

      // Get all the data from the project
      const response = await fetch(
        `${process.env.REACT_APP_API_INTERNAL_URL}/v1.0/projects/${projectId}/allData`,
        {
          headers: headers,
        }
      );
      const dataNOE = await response.json();

      // the launcher will be able to launch the IA
      this.genSessions(dataNOE)
        .then(async (result: any) => {
          // return the IA response through Express
          const sessions: any[] = [];
          for (const part of result.solution.filter((s: any) => s.isScheduleFrozen === false)) {
            sessions.push({
              stewards: part.keyTrainer,
              activity: part.key,
              places: part.keyRoom,
              start: part.start,
              end: part.end,
            });
          }

          const responseInject = await fetch(
            `${process.env.REACT_APP_API_INTERNAL_URL}/v1.0/projects/${projectId}/replaceSessions`,
            {
              method: "POST",
              body: JSON.stringify(sessions),
              headers: headers,
            }
          );
          fetch(`${process.env.REACT_APP_API_INTERNAL_URL}/v1.0/computing/next`, {
            method: "POST",
            body: JSON.stringify({
              state: "SUCCESS",
              result: result,
            }),
            headers: headers,
          });
        })
        .catch((err) => {
          throw err;
        });
      res.status(200).json({launched: "ok"});
    } catch (e) {
      console.log("ERROR", e);
      await fetch(`${process.env.REACT_APP_API_INTERNAL_URL}/v1.0/computing/next`, {
        method: "POST",
        body: JSON.stringify({
          state: "FAILED",
        }),
        headers: headers,
      });
      res.status(500).end();
    }
  };

  setupRoutes() {
    this.router.get("/testCall/:idProject", this.testCall);
  }
}

const computingController = new ComputingController();
const ComputingRouter = computingController.router;

export {ComputingRouter};
