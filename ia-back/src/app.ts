import * as express from "express";
import {Request, Response, NextFunction} from "express";
import * as cors from "cors";
import helmet = require("helmet");
import expressValidator = require("express-validator");
import bodyParser = require("body-parser");
import cookieParser = require("cookie-parser");
import {logger, loggerMiddleware} from "./services/logger";
import {ComputingRouter} from "./controllers/computing";
import {globalErrorHandler, initSentry} from "./helpers/errorsUtilities";

declare global {
  namespace Express {
    export interface Request {
      apiMajorVersion: number;
      apiMinorVersion: number;
    }
  }
}

// Creates and configures an ExpressJS web server.
class App {
  public express: any;

  //Run configuration methods on the Express instance.
  constructor() {
    this.express = express();
    this.express.disable("x-powered-by");
    this.middleware();
    this.routes();
  }

  // SETUP EXPRESS MIDDLEWARE
  private middleware(): void {
    // Initialize Sentry
    if (process.env.NODE_ENV === "production") {
      initSentry(this.express);
    }

    // Helmet: protection against well known vulnerabilities
    this.express.use(helmet());

    // Request logger
    this.express.use(loggerMiddleware);

    // Cross origin ressources sharing
    this.express.use(cors({credentials: true, origin: true}));

    // Parsing options
    this.express.use(bodyParser.json({limit: "15mb"}));
    this.express.use(bodyParser.urlencoded({extended: false}));
    this.express.use(cookieParser());

    // Request validation
    this.express.use(expressValidator());
    // this.express.use(passport.initialize());
  }

  // Check the API version and redirect to the appropriate routing
  private versionMiddleware(req: Request, res: Response, next: NextFunction) {
    const allowedVersions = ["v1.0"];

    if (req.params.version) {
      if (allowedVersions.indexOf(req.params.version) == -1) {
        return res.status(404).end();
      }
      const result = req.params.version.match(/v(\d)\.(\d+)/);
      if (result && result.length >= 2) {
        req.apiMajorVersion = parseInt(result[0]);
        req.apiMinorVersion = parseInt(result[1]);
        return next();
      } else {
        return res.status(404).send();
      }
    } else {
      req.apiMajorVersion = 1;
      req.apiMinorVersion = 0;
      return next();
    }
  }

  // Configure API endpoints.
  private routes(): void {
    const router = express.Router();

    // Manage version first
    this.express.use("/:version(v\\d.\\d+)", this.versionMiddleware, router);

    // Route handler
    router.use("/computing/", ComputingRouter);

    // Routes for documentation
    this.express.use("/", this.versionMiddleware, router);
    // For sentry configuration and troubleshooting
    // this.express.get("/debug-sentry-mKGEKXQyfGC6D5", (req: any, res: any) => {
    //   throw new Error("My first Sentry error!");
    // });
    this.express.get("*", (req: any, res: any) => {
      res.status(404).send();
    });

    // Global error Handler
    this.express.use(globalErrorHandler);
  }
}

export const app = new App().express;
