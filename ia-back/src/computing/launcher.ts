const Solver = require("./solver");
const request = require("request");

export class Launcher {
  constructor() {}

  // TODO: maybe not used anymore, now we use #launchWithData
  launch(url: any, mute: any) {
    return new Promise((resolve, reject) => {
      console.log(url);
      request(
        url,
        {
          json: true,
        },
        (err: any, result: any, body: any) => {
          const rawData: any = result.body;

          const data = {
            training: rawData.training,
            trainer: rawData.trainer,
            roomAvailability: rawData.roomAvailability,
            trainerAvailability: rawData.trainerAvailability,
            frezzedSlot: rawData.frezzedSlot,
          };

          const config = rawData.config;
          const solver = new Solver(mute);
          const out = solver.execute(data, config);
          resolve(out);
        }
      );
    });
  }

  /**
   * Launch the IA. It calls the solver with the appropriate data and returns the data to Express.
   * It's basically an intermediary
   * @param rawData
   */
  launchWithData(rawData: any, mute: any) {
    return new Promise((resolve, reject) => {
      const data = {
        training: rawData.training,
        trainer: rawData.trainer,
        room: rawData.room,
        roomAvailability: rawData.roomAvailability,
        trainerAvailability: rawData.trainerAvailability,
        frezzedSlot: rawData.frezzedSlot,
      };

      const config = rawData.config;
      const solver = new Solver(mute);
      const out = solver.execute(data, config);
      resolve(out);
    });
  }
}
