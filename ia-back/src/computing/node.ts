module.exports = class Node {
  config: any;
  stack: any;
  resources: any;
  solutions: any;
  depth: any;
  status: any;
  roomAvailabilityStart: any;
  roomAvailabilityEnd: any;
  candidate: any;
  roomAvailabilityLength: any;
  children: any;
  slotDisable: any;
  heuristicDetail: any;
  heuristic: any;

  /**
   * Constructor. Builds the node and computes its heuristics.
   * @param config The IA engine configuration
   * @param stack The stack of sessions TODO: need to know if the deleted sessions are also inside
   * @param resources The big "data" object given to the Solver#execute function
   * @param solutions All the sessions that have already been placed. For the initial node, this is the array of all
   * the frozen sessions that we have to deal with
   * @param depth The depth of the A* tree in which the Node is (equivalent to the number of sessions already
   * placed, as one depth equals to adding one more session to the solution)
   */
  constructor(config: any, stack: any, resources: any, solutions: any, depth: any) {
    this.config = config;
    this.stack = stack;
    this.resources = resources;
    this.solutions = solutions;
    this.depth = depth;
    this.status = "pending"; // pending | failed | success
    let roomAvailabilityStart: any;
    let roomAvailabilityEnd: any;

    // If there is any room availability declared, then define the very beginning and the very end
    // of the placement possibilities for the sessions, otherwise rely on the project availability Slots
    if (this.resources.roomAvailability.length > 0) {
      this.resources.roomAvailability.forEach((ra: any) => {
        if (
          roomAvailabilityStart == undefined ||
          ra.startAvailability.getTime() < roomAvailabilityStart.getTime()
        ) {
          roomAvailabilityStart = ra.startAvailability;
        }
        if (
          roomAvailabilityEnd == undefined ||
          ra.endAvailability.getTime() > roomAvailabilityEnd.getTime()
        ) {
          roomAvailabilityEnd = ra.endAvailability;
        }
      });
    } else if (config.project.start !== undefined && config.project.end !== undefined) {
      roomAvailabilityStart = config.project.start;
      roomAvailabilityEnd = config.project.end;
    } else {
      throw new Error(
        "Cannot start the algo if no project availability and no room availability at all"
      );
    }
    this.roomAvailabilityStart = roomAvailabilityStart;
    this.roomAvailabilityEnd = roomAvailabilityEnd;
    this.roomAvailabilityLength = roomAvailabilityEnd.getTime() - roomAvailabilityStart.getTime();

    // The candidate is the additional session that we have placed for this node
    this.candidate = this.stack[this.depth];

    // Calculate the node heuristic directly when it is created
    this.computeHeurisic();
  }

  /**
   *   Create the X valid nodes with the highest heuristics (X based on the engine config)
   *   @returns An array of Nodes that have the highest heuristics
   */
  takeStep() {
    if (this.depth < this.stack.length) {
      const slotAvailability: any[] = [];
      const slotDisable: any[] = [];

      this.candidate.slotsAvaible.forEach((sa: any) => {
        let conflict = undefined;
        this.solutions.forEach((solution: any) => {
          const startSolutionTime = solution.start;
          const endSolutionTime = solution.end;

          if (this.candidate.dependencies != undefined && this.candidate.dependencies.length > 0) {
            if (
              this.candidate.dependencies.map((d: any) => d.dependency).includes(solution.id) &&
              endSolutionTime.getTime() >= sa.startStepTime.getTime()
            ) {
              conflict = "dependencyBefore";
              slotDisable.push({
                id: this.candidate.id,
                key: this.candidate.key,
                startStepTime: sa.startStepTime,
                endStepTime: sa.endStepTime,
                dependencies: JSON.stringify(
                  this.candidate.dependencies.map((d: any) => d.dependency)
                ),
                conflict: "dependencyBefore",
              });
            }
          }

          if (
            this.candidate.reverseDependencies != undefined &&
            this.candidate.reverseDependencies.length > 0
          ) {
            if (
              this.candidate.reverseDependencies
                .map((d: any) => d.dependency)
                .includes(solution.id) &&
              startSolutionTime.getTime() <= sa.endStepTime.getTime()
            ) {
              conflict = "dependencyAfter";
              slotDisable.push({
                id: this.candidate.id,
                key: this.candidate.key,
                startStepTime: sa.startStepTime,
                endStepTime: sa.endStepTime,
                reverseDependencies: JSON.stringify(
                  this.candidate.reverseDependencies.map((d: any) => d.dependency)
                ),
                conflict: "dependencyAfter",
              });
            }
          }

          if (
            !(
              sa.startStepTime.getTime() >= endSolutionTime.getTime() ||
              sa.endStepTime.getTime() <= startSolutionTime.getTime()
            )
          ) {
            if (solution.keyRoom == sa.keyRoom && !this.candidate.nonBlockingActivity) {
              conflict = "room";
              slotDisable.push({
                id: this.candidate.id,
                key: this.candidate.key,
                startStepTime: sa.startStepTime,
                endStepTime: sa.endStepTime,
                keyRoom: sa.keyRoom,
                keyTrainer: JSON.stringify(sa.keyTrainer),
                conflict: "room",
                slotConflict: solution.id,
              });
            }
            let conflictTrainer = false;
            const conflictTrainerDetail = [];

            if (solution.inscriptionFree == undefined && sa.inscriptionFree == undefined) {
              for (const solutionTrainer of solution.keyTrainer) {
                for (const saTrainer of sa.keyTrainer) {
                  if (saTrainer == solutionTrainer) {
                    conflictTrainer = true;
                    conflictTrainerDetail.push(solution);
                  }
                }
              }
            }
            if (conflictTrainer == true) {
              conflict = "trainer";
              conflictTrainerDetail.forEach((ctd) => {
                slotDisable.push({
                  id: this.candidate.id,
                  key: this.candidate.key,
                  startStepTime: sa.startStepTime,
                  endStepTime: sa.endStepTime,
                  keyRoom: sa.keyRoom,
                  keyTrainer: JSON.stringify(sa.keyTrainer),
                  conflict: "trainer",
                  slotConflict: ctd.id,
                });
              });
            }
          }
        });

        if (conflict == undefined) {
          slotAvailability.push({
            startStepTime: sa.startStepTime,
            endStepTime: sa.endStepTime,
            keyRoom: sa.keyRoom,
            keyTrainer: sa.keyTrainer,
            roomStartAvailability: sa.roomStartAvailability,
          });
        }
      });

      this.children = [];
      const childCandidates: any[] = [];

      // if there exists any solution without conflict...
      if (slotAvailability.length > 0) {
        slotAvailability.forEach((slotAvailabilityChosen) => {
          const solution = {
            id: this.candidate.id,
            key: this.candidate.key,
            keyRoom: slotAvailabilityChosen.keyRoom,
            keyTrainer: slotAvailabilityChosen.keyTrainer,
            start: slotAvailabilityChosen.startStepTime,
            end: slotAvailabilityChosen.endStepTime,
            roomStartAvailability: slotAvailabilityChosen.roomStartAvailability,
            keyCategory: this.candidate.keyCategory,
            colorCategory: this.candidate.colorCategory,
            length: this.candidate.length,
            freezed: this.candidate.freezed,
            secret: this.candidate.secret,
            inscriptionFree: this.candidate.inscriptionFree,
            volunteerFactor: this.candidate.volunteerFactor,
            dependencies: this.candidate.dependencies,
            reverseDependencies: this.candidate.reverseDependencies,
            dependenciesDepth: this.candidate.dependenciesDepth,
            isScheduleFrozen: this.candidate.isScheduleFrozen,
          };

          // TODO maybe not used, last comment was: "to delete no usage"
          const solutions: any[] = [];
          this.solutions.forEach((s: any) => {
            solutions.push(s);
          });
          solutions.push(solution);

          // ... create the Nodes that we are sure that their configuration has no conflict (we don't wanna create dead Nodes)
          const child = new Node(
            this.config,
            this.stack,
            this.resources,
            solutions,
            this.depth + 1
          );

          childCandidates.push(child);
        });
      } else {
        // ... else, just set it as disabled
        this.slotDisable = slotDisable;
        this.status = "failed";
      }

      const output = [];

      // Only output the number of children wanted in the engine config "branchCreation".
      // If some nodes have the same exact heuristic, we prefer to keep more solutions rather than
      // strictly cutting at the exact number "branchCreation".
      childCandidates.sort((a, b) => b.heuristic - a.heuristic);
      let i = 0;
      let nbCandidatesSelected = 0;
      while (nbCandidatesSelected < this.config.branchCreation && i < childCandidates.length) {
        const closeCandidates = output.filter((o) => o.heuristic == childCandidates[i].heuristic);
        if (closeCandidates.length == 0) {
          output.push(childCandidates[i]);
          nbCandidatesSelected++;
        }
        i++;
      }

      return output;
    } else {
      // If at the end the stack, we're good and we found a complete solution!
      this.status = "success";
      return [];
    }
  }

  /**
   * Computes the heuristic. Each heuristic has a range between 0 and 1.
   * It returns nothing, it just sets the heuristic of the node
   */
  computeHeurisic() {
    const solutionsNotFrozen = this.solutions.filter((s: any) => s.isScheduleFrozen != true);
    this.heuristicDetail = {};

    // if the depth reaches the number of all the sessiosn we have to place, then it's better: progress = 1.
    this.heuristicDetail.progress = this.depth / this.stack.length;

    // TODO freezed should not be used anymore, see why it's used there
    // Clean Placement  = 1 if placed at the beginning of the room availability.
    // The heuristic of all th clean placements of all sessions.
    this.heuristicDetail.cleanPlacement = solutionsNotFrozen
      .filter((s: any) => s.freezed == undefined)
      .reduce((accumulator: any, currentValue: any) => {
        const clean = currentValue.start.getTime() == currentValue.roomStartAvailability.getTime();
        return accumulator + (clean == true ? 1 : 0);
      }, 0);
    this.heuristicDetail.cleanPlacement =
      this.heuristicDetail.cleanPlacement / solutionsNotFrozen.length;

    const solutionsWithReverseDependencies = solutionsNotFrozen.filter((s: any) => {
      return s.reverseDependencies != undefined && s.reverseDependencies.length > 0;
    });

    let moyStar;
    if (solutionsWithReverseDependencies.length > 0) {
      moyStar =
        solutionsWithReverseDependencies
          .map((s: any) => s.start.getTime() - this.roomAvailabilityStart)
          .reduce((accumulator: any, currentValue: any) => accumulator + currentValue, 0) /
        solutionsWithReverseDependencies.length /
        this.roomAvailabilityLength;
    } else {
      moyStar = 0;
    }
    moyStar = 1 - moyStar;
    this.heuristicDetail.moyStar = moyStar;

    const truncatedRoomAvailabilityStart = new Date(
      this.roomAvailabilityStart.getFullYear(),
      this.roomAvailabilityStart.getMonth() + 1,
      this.roomAvailabilityStart.getDate()
    );
    const truncatedRoomAvailabilityEnd = new Date(
      this.roomAvailabilityEnd.getFullYear(),
      this.roomAvailabilityEnd.getMonth() + 1,
      this.roomAvailabilityEnd.getDate()
    );
    const currentDate = truncatedRoomAvailabilityStart;
    // console.log('truncatedRoomAvailability', truncatedRoomAvailabilityStart, truncatedRoomAvailabilityEnd);

    let scattering;

    const solutionsWithoutReverseDependencies = this.solutions.filter((s: any) => {
      return (
        s.keyCategory != "BENEVOL" &&
        (s.reverseDependencies == undefined || s.reverseDependencies.length == 0)
      );
    });

    const trainingCounts: any[] = [];
    solutionsWithoutReverseDependencies.forEach((s: any) => {
      const truncateDate = new Date(
        s.start.getFullYear(),
        s.start.getMonth() + 1,
        s.start.getDate()
      );
      const trainingCountsExisting = trainingCounts.filter(
        (tce: any) => tce.day.getTime() - truncateDate.getTime() == 0
      );
      if (trainingCountsExisting.length > 0) {
        trainingCountsExisting[0].counter = trainingCountsExisting[0].counter + 1;
      } else {
        trainingCounts.push({
          day: truncateDate,
          counter: 1,
        });
      }
    });
    let minTraining = Infinity;
    let maxTraining = 0;
    trainingCounts.forEach((tc) => {
      minTraining = Math.min(tc.counter, minTraining);
      maxTraining = Math.max(tc.counter, maxTraining);
    });
    scattering =
      this.solutions.length == 0
        ? 1
        : 1 - (maxTraining - minTraining) / solutionsWithoutReverseDependencies.length;

    this.heuristicDetail.scattering = scattering;

    // Final step: compute a ponderation of each heuristic
    this.heuristic =
      this.heuristicDetail.progress * this.config.heuristic.progress +
      this.heuristicDetail.cleanPlacement * this.config.heuristic.cleanPlacement +
      this.heuristicDetail.moyStar * this.config.heuristic.moyStar +
      this.heuristicDetail.scattering * this.config.heuristic.scattering;

    // TODO to keep (legacy code that can be useful)
    // let minStart;
    // let maxEnd;
    // this.solutions.forEach(s => {
    //   if ((minStart == undefined) || (s.start.getTime() < minStart.getTime())) {
    //     minStart = s.start;
    //   }
    //   if ((maxEnd == undefined) || (s.end.getTime() > maxEnd.getTime())) {
    //     maxEnd = s.end;
    //   }
    // });
    // this.heuristicDetail.totalLengthRatio = 1 - ((maxEnd - minStart) / this.roomAvailabilityLength);

    // let freeTime = this.resources.trainer.map(trainer => {
    //   let totalTrainerTime = this.solutions.filter(s => s.keyTrainer == trainer.key).reduce((accumulator, currentValue) => accumulator + (currentValue.end.getTime() - currentValue.start.getTime()), 0)
    //   let attendance = this.resources.trainerAvailability.filter(t => t.keyTrainer == trainer.key).reduce((accumulator, currentValue) => accumulator + (currentValue.endAvailability.getTime() - currentValue.startAvailability.getTime()), 0)
    //   // console.log(trainer.key,(attendance- totalTrainerTime)/attendance);
    //   return attendance == 0 ? 1 : (attendance - totalTrainerTime) / attendance;
    //
    // })
    // freeTime = freeTime.filter(ft => ft != 1 && ft != NaN);
    // // console.log('--------');
    // // console.log("freeTime",freeTime);
    // let totalFreeTime = freeTime.reduce((accumulator, currentValue) => currentValue + accumulator, 0);
    // // console.log('freetime',freeTime,totalFreeTime);
    // this.heuristicDetail.moyFreeTime = totalFreeTime / freeTime.length;

    // console.log('Solutions length',this.solutions.length);
    // let sameCategory =this.solutions.map(s=>{
    //   let sameTimeSolutions = this.solutions.filter(other=>{
    //       return (!(s.start.getTime() > other.end.getTime() || s.end.getTime() < other.start.getTime())&&s.key!=other.key);
    //   })
    //   // console.log(' sameTimeSolutions',sameTimeSolutions.length);
    //   if(sameTimeSolutions.length>0){
    //     let sameCategoryNB=sameTimeSolutions.filter(sts=>(sts.keyCategory==s.keyCategory));
    //     // console.log('  sameTimeCategory',sameCategoryNB.length);
    //     return sameCategoryNB.length/sameTimeSolutions.length;
    //   }else{
    //     return 0;
    //   }
    // })
    // // console.log(sameCategory);
    // sameCategory=1-(sameCategory.reduce((accumulator, currentValue)=>{return accumulator + currentValue},0)/sameCategory.length);
    // this.heuristicDetail.sameCategory=sameCategory;

    // console.log('heuristicDetail',this.heuristicDetail);
    // this.heuristic = this.heuristicDetail.progress * this.config.heuristic.progress  + this.heuristicDetail.cleanPlacement * this.config.heuristic.cleanPlacement + this.heuristicDetail.totalLengthRatio * this.config.heuristic.totalLengthRatio  + this.heuristicDetail.moyStar * this.config.heuristic.moyStar + this.heuristicDetail.moyFreeTime * this.config.heuristic.moyFreeTime + this.heuristicDetail.sameCategory  * this.config.heuristic.sameCategory;
  }
};
