const fs = require("fs");

const dateTimeReviver = (key: string, value: any) => {
  let a;
  if (typeof value === "string") {
    a = /^\d{4}-\d{2}-\d{2}.+/.exec(value);
    if (a) {
      // @ts-ignore
      return new Date(a);
    }
  }
  return value;
};

const parseData = (file: string) => {
  const rawdata = fs.readFileSync(file);
  const dataNOE = JSON.parse(rawdata, dateTimeReviver);
  return dataNOE;
};

export {parseData};
