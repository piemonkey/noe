/* arr[]  ---> Input Array
data[] ---> Temporary array to store current combination
start & end ---> Starting and Ending indexes in arr[]
index  ---> Current index in data[]
solutionsSize ---> Size of a combination to be printed
resultArray[] ---> Final array where the data is returned */
const combinationUtil = (
  arr: Array<any>,
  tmpData: Array<any>,
  start: number,
  end: number,
  index: number,
  solutionsSize: number,
  resultArray: Array<any>
) => {
  if (index == solutionsSize) {
    const solution = [];
    for (let j = 0; j < solutionsSize; j++) {
      solution.push(tmpData[j]);
    }
    resultArray.push(solution);
  }

  // replace index with all possible elements. The condition
  // "end-i+1 >= r-index" makes sure that including one element
  // at index will make a combination with remaining elements
  // at remaining positions
  for (let i = start; i <= end && end - i + 1 >= solutionsSize - index; i++) {
    tmpData[index] = arr[i];
    combinationUtil(arr, tmpData, i + 1, end, index + 1, solutionsSize, resultArray);
  }
};

// The main function that prints all combinations of size solutionSize
// in sourceArray[]. This function mainly uses combinationUtil()
export const combineSolutions = (sourceArray: Array<any>, solutionsSize: number) => {
  // A temporary array to store all combination one by one
  const tmpData = new Array(solutionsSize);
  // The result
  const resultArray: any[] = [];

  // Print all combination using temporary array 'data[]'
  combinationUtil(sourceArray, tmpData, 0, sourceArray.length - 1, 0, solutionsSize, resultArray);
  return resultArray;
};
