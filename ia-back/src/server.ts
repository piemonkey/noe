import * as dotenv from "dotenv";
import fetch from "node-fetch";

dotenv.config();

import * as http from "http";
import {logger} from "./services/logger";
import {app} from "./app";

logger.verbose("backend:server");

const port = normalizePort(process.env.PORT || 4010);
app.set("port", port);

const resetComputations = async () => {
  // Reset computations because if one is stille running when starting up, it means the IA was killed
  fetch(`${process.env.REACT_APP_API_INTERNAL_URL}/v1.0/computing/reset/`, {
    method: "POST",
  });
};

resetComputations();

const server = http.createServer(app);
server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

function normalizePort(val: number | string): number | string | boolean {
  const port: number = typeof val === "string" ? parseInt(val, 10) : val;
  if (isNaN(port)) return val;
  else if (port >= 0) return port;
  else return false;
}

function onError(error: NodeJS.ErrnoException): void {
  if (error.syscall !== "listen") throw error;
  const bind = typeof port === "string" ? "Pipe " + port : "Port " + port;
  switch (error.code) {
    case "EACCES":
      logger.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case "EADDRINUSE":
      logger.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening(): void {
  const addr = server.address();
  const bind = typeof addr === "string" ? `pipe ${addr}` : `port ${addr.port}`;
  logger.verbose(`listening on ${bind}`);
}
